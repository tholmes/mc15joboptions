# Filename: lhe.py
# Description:
# Author: Fabian Wilk
# Created: Tue Jun 14 10:23:25 2016
#
# (c) by Fabian Wilk
#
# This file is licensed under a Creative Commons Attribution-ShareAlike 4.0
# International License.
#
# You should have received a copy of the license along with this work.
# If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
# For use with Powheg+Pythia8

from __future__ import print_function
from __future__ import absolute_import

import re


class Record(object):
    def __init__(self, **kwargs):
        if not set(kwargs.keys()) == set(self.FIELD_NAMES):
            raise RuntimeError("invalid field names")

        self.__dict__.update({field: self.FIELD_TYPES[field](value)
                              for field, value in kwargs.items()})

    @classmethod
    def from_string(cls, string):
        return cls(**dict(zip(cls.FIELD_NAMES, string.split())))

    def to_string(self):
        return self.FMT.format(**{field: getattr(self, field)
                                  for field in self.FIELD_NAMES})


class EventInfo(Record):
    #: These are the allowed field names for each LHE Event Info Record.
    FIELD_NAMES = ['nparticles', 'id', 'weight', 'scale', 'A_QED', 'A_QCD']

    #: These are the python types of each field.
    FIELD_TYPES = {
        'nparticles': int,
        'id': int,
        'weight': float,
        'scale': float,
        'A_QCD': float,
        'A_QED': float
    }

    #: This is the formatting string.
    FMT = "{nparticles:7d} {id:6d} {weight: .5E} {scale: .5E} {A_QED: .5E} {A_QCD: .5E}"


class Particle(Record):
    #: These are the allowed field names for each LHE Particle Record.
    FIELD_NAMES = [
        'pdg_id', 'status', 'mother0', 'mother1', 'colour0', 'colour1', 'px',
        'py', 'pz', 'e', 'm', 'lifetime', 'spin'
    ]

    #: These are the python types of each field.
    FIELD_TYPES = {
        'pdg_id': int,
        'status': int,
        'mother0': int,
        'mother1': int,
        'colour0': int,
        'colour1': int,
        'px': float,
        'py': float,
        'pz': float,
        'e': float,
        'm': float,
        'lifetime': float,
        'spin': float
    }

    #: This is the formatting string.
    FMT = "{pdg_id:8d} {status:5d} {mother0:5d} {mother1:5d} {colour0:5d} {colour1:5d} {px: .9E} {py: .9E} {pz: .9E} {e: .9E} {m: .9E} {lifetime: .5E} {spin: .3E}"

    def mothers(self):
        return (self.event.particles[mo]
                for mo in xrange(self.mother0 - 1, self.mother1))

    def daughters(self):
        self_id = self.index() + 1
        for p in self.event:
            if p.mother0 <= self_id <= p.mother1:
                yield p

    def index(self):
        return self.event.particles.index(self)


class Event(object):
    def __init__(self, info, particles, extra_lines=None):
        self.info = info
        self.particles = particles
        self.extra_lines = extra_lines or []

        for p in self.particles:
            p.event = self

    @classmethod
    def from_lines(cls, lines):
        info = None
        particles = []
        extra_lines = []

        for i, datum in enumerate(lines):
            # The first line is the info block.
            if not info:
                info = EventInfo.from_string(datum)
                continue

            # The lines [1, N_PARTICLE] define particles.
            if 1 <= i <= info.nparticles:
                particles.append(Particle.from_string(datum))
                continue

            # All subsequent lines are additional configuration which is
            # retrieved but isn't parsed.
            extra_lines.append(datum)

        return cls(info=info,
                   particles=particles,
                   extra_lines=extra_lines)

    def to_string(self):
        lines = [self.info.to_string() + '\n']
        for p in self.particles:
            lines.append(p.to_string() + '\n')
        lines.extend(self.extra_lines)

        return ''.join(lines)

    def __len__(self):
        return len(self.particles)

    def __getitem__(self, index):
        if not isinstance(index, (int, long)):
            raise TypeError("index must be integer")

        return self.particles[index]

    def __delitem__(self, index):
        if not isinstance(index, (int, long)):
            raise TypeError("index must be integer")

        # When removing a particle at a location ``index`` -- which corresponds
        # to LHE id ``i_lhe`` -- we need to correct two things:
        #
        #   1.) Any particle that had ``i_lhe`` as mother, needs to get the
        #       mother of the particle to be removed as mother.
        #
        #          - If both have exactly one mother, perform the replacement
        #            verbatim.
        #          - If the particle that is deleted has two mothers but the
        #            child particle has only one (the particle to be deleted),
        #            set the two mother reference of the child particle to the
        #            ones of the particle to be deleted.
        #          - If the particle that is deleted has one mother but the
        #            child particle has two, abort.
        #
        #   2.) Since we've changed the LHE indexing, any particle that had a
        #       mother reference ``mother > i_lhe`` must get a new mother
        #       reference ``mother - 1``
        p_rm = self[index]
        i_lhe = index + 1

        mo0, mo1 = p_rm.mother0, p_rm.mother1
        for p in self.particles:
            if p is p_rm:
                continue

            if p.mother0 == i_lhe or p.mother1 == i_lhe:
                if p.mother0 != p.mother1:
                    raise RuntimeError(
                        "can not delete particle whose child has also other parents")

                p.mother0 = mo0
                p.mother1 = mo1

        # Remove the particle.
        del self.particles[index]

        for p in self.particles:
            if p.mother0 > i_lhe:
                p.mother0 -= 1
            if p.mother1 > i_lhe:
                p.mother1 -= 1

        # Decrement the particle counter.
        self.info.nparticles -= 1


class File(object):
    def __init__(self, file_name):
        self._file_name = file_name

        self._header = None

    def __enter__(self):
        self._fo = open(self._file_name, 'r')
        self._read_header()

        return self

    def __exit__(self, type, value, traceback):
        self._fo.close()

    def _read_header(self):
        rx_header = re.compile(r'<LesHouchesEvents version="[^"]+">\n')
        rx_first = re.compile(r'<event>\n')

        line = self._fo.readline()
        if not rx_header.match(line):
            raise RuntimeError("missing LHE file header")

        pos = self._fo.tell()
        line = self._fo.readline()
        header_lines = []

        while line:
            if rx_first.match(line):
                self._fo.seek(pos)
                break
            else:
                header_lines.append(line)
                pos = self._fo.tell()
                line = self._fo.readline()

        self._header = header_lines

    def __iter__(self):
        return self

    def next(self):
        rx_start = re.compile('<event>\n')
        rx_end = re.compile('</event>\n')
        rx_final = re.compile('</LesHouchesEvents>\n')

        pos = self._fo.tell()
        line = self._fo.readline()

        if rx_final.match(line):
            raise StopIteration()

        if not rx_start.match(line):
            raise RuntimeError("failure to read LHE file: expected event")

        line = self._fo.readline()
        data = []

        while line:
            if rx_end.match(line):
                break

            else:
                data.append(line)
                pos = self._fo.tell()
                line = self._fo.readline()

        return Event.from_lines(data)
