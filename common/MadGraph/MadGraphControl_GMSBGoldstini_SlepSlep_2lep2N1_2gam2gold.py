from MadGraphControl.MadGraphUtils import *
import re
import math, cmath
import os
mode=0
#This mass is always 0
Mgld=0
#Getting neutralino and pgld mass value from filename
Mneu1 = float(re.findall(r'Mneu\d+',runArgs.jobConfig[0])[0][4:])
Mpgld = float(re.findall(r'Mpgld\d+',runArgs.jobConfig[0])[0][5:])

#Determing electron of muon channel from filename, and assigning masses to selectron or smuon accordingly
genSel = False
genSmu = False
if "Msel" in runArgs.jobConfig[0]:
    genSel = True
    Msl4  = float(re.findall(r'Msel\d+',runArgs.jobConfig[0])[0][4:])
    Mpgld = (Mpgld / 100) * Msl4
    Mneu1 = (Msl4 - Mpgld) * (Mneu1 / 10) + Mpgld
    Msl5 = 0
elif "Msmu" in runArgs.jobConfig[0]:
    genSmu = True
    Msl5 = float(re.findall(r'Msmu\d+',runArgs.jobConfig[0])[0][4:])
    Mpgld = (Mpgld / 100) * Msl5
    Mneu1 = (Msl5 - Mpgld) * (Mneu1 / 10) + Mpgld
    Msl4 = 0
#Parameters used to calculate K-factor for neutralino decays, taken from parameters.py file in mssm_goldsitni model
# Partial widths for n1> a gld and n1> a pgld calculated, then ratio used to calculate Kv for an equal BR
MW = 80.4079107
MZ = 91.1876
M32 = 1.0e-13
MPl = 2.435323203596526e18
cw = MW / MZ
sw = math.sqrt(1-cw**2)
RNN1x1 = 0.990612708
RNN1x2 = -0.0311086091
aAT = cw*RNN1x1 + RNN1x2*sw

#Ratio of widths should be 1. Kv calculated from  BR(n1>a gld) [simplified with Mgld=0] / BR (n1 a pgld) [where Mpgld is massive]
Mgld_width = ((-Mgld**2 + Mneu1**2)*((2*aAT**2*Mgld**4*Mneu1**2)/(3.*M32**2*MPl**2) - (4*aAT**2*Mgld**2*Mneu1**4)/(3.*M32**2*MPl**2) + (2*aAT**2*Mneu1**6)/(3.*M32**2*MPl**2)))/(32.*cmath.pi*abs(Mneu1)**3)
Mpgld_width = ((Mneu1**2 - Mpgld**2)*((2*aAT**2*Mneu1**6)/(3.*M32**2*MPl**2) - (4*aAT**2*Mneu1**4*Mpgld**2)/(3.*M32**2*MPl**2) + (2*aAT**2*Mneu1**2*Mpgld**4)/(3.*M32**2*MPl**2)))/(32.*cmath.pi*abs(Mneu1)**3)

Kv = math.sqrt(Mgld_width/Mpgld_width)
print("Kv is ", Kv)
fcard = open('proc_card_mg5.dat','w')
if genSel == True:
    fcard.write("""
                import model mssm_goldstini/
                define p = g u c d s u~ c~ d~ s~
                define j = g u c d s u~ d~ s~
                define gold = gld pgld
                generate p p > er+ er-, (er+ > e+ n1, n1 > a gold), (er- > e- n1, n1 > a gold) @0
                add process p p > er+ er- j, (er+ > e+ n1, n1 > a gold), (er- > e- n1, n1 > a gold) @1
                add process p p > er+ er- j j , (er+ > e+ n1, n1 > a gold), (er- > e- n1, n1 > a gold) @2
                output -f""")
    fcard.close()
elif genSmu == True:
    fcard.write("""
                import model mssm_goldstini/
                define p = g u c d s u~ c~ d~ s~
                define j = g u c d s u~ d~ s~
                define gold = gld pgld
                generate p p > mur+ mur-, (mur+ > mu+ n1, n1 > a gold), (mur- > mu- n1, n1 > a gold) @0
                add process p p > mur+ mur- j, (mur+ > mu+ n1, n1 > a gold), (mur- > mu- n1, n1 > a gold) @1
                add process p p > mur+ mur- j j, (mur+ > mu+ n1, n1 > a gold), (mur- > mu- n1, n1 > a gold) @2
                output -f""")
    fcard.close()
else:
    raise RuntimeError("Generation process not recognised - not selectron or smuon")

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'use_syst':'F',
           'event_norm': 'sum'}
    
runName='run_01'     
process_dir = new_process()
genEvts = int(runArgs.maxEvents  * 1.6)

run_card_name = 'run_card.SM.Slep_lepN1_gamgld.GMSBGoldstini.dat'
if not os.access(run_card_name,os.R_OK):
    get_run_card = subprocess.Popen(['get_files','-data',run_card_name])
    get_run_card.wait()
    if not os.access(run_card_name,os.R_OK):
        print 'ERROR: Could not get run card'
if os.access(run_card_name,os.R_OK):
    rcard = open('rcard.dat','w')
    with open(run_card_name) as old_run_card:
        for line in old_run_card:
            if 'nevents' in line:
                rcard.write('nevents = %d' %(genEvts))
            else:
                rcard.write(line)

print("number of gen events is " , genEvts)
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=genEvts,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#paramcard.wait()                                                                                                   
param_card_name = 'param_card.SM.Slep_lepN1_gamgld.GMSBGoldstini.dat'
if not os.access(param_card_name,os.R_OK):
    get_param_card = subprocess.Popen(['get_files','-data',param_card_name])
    get_param_card.wait()
    if not os.access(param_card_name,os.R_OK):
        print 'ERROR: Could not get param card'                                                                         
if os.access(param_card_name,os.R_OK):
    new_param_card = open('param_card.dat','w')
    with open(param_card_name) as old_param_card:                         
        for line in old_param_card:                                                                                
            if '# mpgld' in line:                                                                                  
                new_param_card.write('  2000039 %e # mpgld \n'%(Mpgld))                                           
            elif '# mgld' in line:                                                                                 
                new_param_card.write('  1000039 %e # mgld \n'%(Mgld))                                               
            elif '# mneu1' in line:                                                                                
                new_param_card.write('  1000022 %e # mneu1 \n'%(Mneu1))                                             
            elif '# msl4' in line:                                                                                  
                new_param_card.write('  2000011 %e # msl4 \n'%(Msl4))                                             
            elif '# msl5' in line:                                                                                  
                new_param_card.write('  2000013 %e # msl5 \n'%(Msl5))                                             
            elif 'DECAY  1000022' in line:                                                                      
                new_param_card.write('DECAY  1000022   Auto \n')                                            
            elif 'DECAY  2000011' in line:                                                                  
                new_param_card.write('DECAY  2000011   Auto \n')                                                 
            elif 'DECAY  2000013' in line:                          
                new_param_card.write('DECAY  2000013   Auto \n')                                                  
            elif '# kv' in line:                                                                               
                new_param_card.write('6 %e # kv \n'%(Kv))                                                        
            else:                                                                                                
                new_param_card.write(line)                                                                         
    old_param_card.close()                                                                                         
    new_param_card.close()                             
    
print_cards()

generate(run_name=runName,run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3)  


if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts
   

#### Shower 
evgenConfig.description = "GMSB Goldstini through slepslep production, masses Msel %d Msmu %d Mneu1 %d Mpgld %d" %(Msl4,Msl5,Mneu1,Mpgld)
evgenConfig.keywords+=['BSM','MSSM','GMSB','2lepton','2photon']
evgenConfig.generators+=["MadGraph","Pythia8","EvtGen"]
evgenConfig.inputfilecheck = runName
evgenConfig.findJets = True
evgenConfig.saveJets = True
#evgenConfig.auxfiles = ['inclusiveP8DsDPlus.pdt', 'G4particle_whitelist.txt', '2014Inclusive.dec', 'pdt.table', 'Bdecays0.dat', 'PDGTABLE.MeV', 'susyParticlePdgid.txt', 'Bs2Jpsiphi.DEC', 'DECAY.DEC']
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

###Pythia8 commands
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands +=["Init:showAllParticleData = on"]
genSeq.Pythia8.Commands+=["1000039:new = gld gld~ 2 0 0 0 0 0.0 0.0 0.0", "1000039:isVisible = False,1000039:mayDecay=False"]
genSeq.Pythia8.Commands+=["2000039:new = pgld pgld~ 2 0 0 %d  0 0.0 0.0 0.0" %(Mpgld), "2000039:isVisible = False,2000039:mayDecay = False"]
genSeq.Pythia8.Commands += ["Merging:Process = guess"]
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
else:
    genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'


###Filter
include("MC15JobOptions/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 20.*GeV
filtSeq.Expression= "(LeptonFilter)"

bonus_file = open('pdg_extras.dat','w')
bonus_file.write('2000039\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'
