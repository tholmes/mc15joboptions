from MadGraphControl.MadGraphUtils import *
runName='test'
fcard = open('proc_card_mg5.dat','w')
mphi = float(runArgs.jobConfig[0].split('_')[5].replace("p",""))
mchi = float(runArgs.jobConfig[0].split('_')[6].replace("c",""))
evgenLog.info('Processing model with masses: (mphi, mchi) = (%e,%e)' %(mphi, mchi))
filter_string = runArgs.jobConfig[0].split('_')[7].replace(".py","")

# PP I am unsure of this parameter, It would be nice to check it
drjj = 0.4

fcard.write("""
import model DMsimp_s_spin0_5f_ybMSbar --modelname
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
#generate p p > xd xd~ / a z w+ w-
generate p p > j xd xd~ / a z w+ w-
add process p p > j j xd xd~ / a z w+ w-
add process p p > j j j xd xd~ / a z w+ w-
output -f
""")

nJetMax=3
ktdurham=40
if mphi/4 > 40:
    ktdurham = mphi/4
evgenLog.info('ktdurham set to %i' %ktdurham)   
process="pp>{Y0,54}j"
removedecays = "off"
dokt = "T"
lhaid = "315200" # are we fine moving to NNPDF3.1?
maxjetflavor = "5"
evgenConfig.process = "pp>bbxdxd"

fcard.close()

if (filter_string == "MET90andJET80"):
    evgenLog.info('MET 90 and jet 80')
   
    include('MC15JobOptions/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 90000.
    include( 'MC15JobOptions/JetFilterAkt6.py')
    filtSeq.QCDTruthJetFilter.MinPt = 80. * 1000.
    filtSeq.QCDTruthJetFilter.MaxPt = 13000. * 1000.
    filtSeq.QCDTruthJetFilter.MaxEta = 3.2
    filtSeq.QCDTruthJetFilter.MinEta = -3.2
   
    filtSeq.Expression = "MissingEtFilter and QCDTruthJetFilter"
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:
        nevents=5000*evt_multiplier
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")
extras = {'lhe_version':'3.0', 
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : lhaid,
          'ickkw'      : '0',
          'ktdurham'   : ktdurham,  
          'pdgs_for_merging_cut' : '1, 2, 3, 4, 5, 21',       
          'maxjetflavor':maxjetflavor,    
          'use_syst'   : 'False',      
          'drjj':drjj,
          }
if dokt == 'T': 
   extras['dokt'] = dokt
process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat', xqcut=0, 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()
paramcard = subprocess.Popen(['get_files','-data','param_card_5FS.dat'])
paramcard.wait()
if not os.access('param_card_5FS.dat',os.R_OK):
    raise RuntimeError( 'ERROR: Could not get param card' )
elif os.access('param_card.dat',os.R_OK):
    raise RuntimeError( 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('param_card_5FS.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if 'mxd' in line:
            newcard.write('      1000022 %e # mxd \n'%(mchi))
        elif 'my0' in line:
            newcard.write('      54 %e # my0 \n'%(mphi))
        elif 'wy0' in line:
            newcard.write('DECAY  54 Auto # wy0 \n')
        elif 'gsxd' in line:
            if model_string == "bbscalar":
                newcard.write('      3 1.000000e+00 # gsxd \n')
            else:
                newcard.write('      3 0.000000e+00 # gsxd \n')
        elif 'gsd33' in line:
            if model_string == "bbscalar":
                newcard.write('      9 1.000000e+00 # gsd33 \n')
            else:
                newcard.write('      9 0.000000e+00 # gsd33 \n')
        elif 'gsu33' in line:
            if model_string == "bbscalar":
                newcard.write('      10 1.000000e+00 # gsu33 \n')
            else:
                newcard.write('      10 0.000000e+00 # gsu33 \n')
        elif 'gpxd' in line:
            if model_string == "bbpseudo":
                newcard.write('      4 1.000000e+00 # gpxd \n')
            else:
                newcard.write('      4 0.000000e+00 # gpxd \n')
        elif 'gpd33' in line:
            if model_string == "bbpseudo":
                newcard.write('      15 1.000000e+00 # gpd33 \n')
            else:
                newcard.write('      15 0.000000e+00 # gpd33 \n')
        elif 'gpu33' in line:
            if model_string == "bbpseudo":
                newcard.write('      16 1.000000e+00 # gpu33 \n')
            else:
                newcard.write('      16 0.000000e+00 # gpu33 \n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name=runName,proc_dir=process_dir,nevents=nevents,random_seed=runArgs.randomSeed)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)             
outputDS=runName+'._00001.events.tar.gz'
runArgs.inputGeneratorFile = outputDS                                                          
evgenConfig.description = 'DM+%s with matching, m_med = %s GeV, m_chi = %s GeV'%(model_string, mphi, mchi)
evgenConfig.keywords = ["exotic","BSM","WIMP", "SUSY"]
evgenConfig.inputfilecheck = runName                                                                                                                                   
evgenConfig.contact = ["Priscilla Pani <ppani@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
if nJetMax<0:
    evgenLog.fatal('njets is not set')
    raise RuntimeError('njet is not set')
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "1000022:all = Chi Chi~ 1 0 0 %s " %mchi,
                           ]
if nJetMax>0:
    genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = %s" %removedecays,
                                "Merging:doKTMerging = on",
                                "Merging:ktType = 1",
                                "Merging:nJetMax = %i"%nJetMax,
                                "Merging:Process = %s"%process,
                                "Merging:TMS = %f"%ktdurham, 
                                "Merging:nQuarksMerge = %s" %maxjetflavor,
                                "Merging:Dparameter = 0.4",
                                ]
