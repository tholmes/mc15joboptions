#############################################
# MadGraph control script for the creation  #
# of dark pion samples                      #
#                                           #
# Setup based on a script by Bryan Ostdiek  #
# * bostdiek@g.harvard.edu                  #
#                                           #
# Adapted for ATLAS by Jochen Jens Heinrich #
# * jochen.jens.heinrich@cern.ch            #
#############################################

from MadGraphControl.MadGraphUtils import *
import os
import sys
from subprocess import call
from shutil import copy

mode = 0

# Check model is known
if dmmodel not in ['Gaugephobic_SU2L', 'Gaugephilic_SU2L', 'Gaugephobic_SU2R']:
        raise RuntimeError("The specified dark meson model is not defined in the control file.")

# Check beam energy is specified
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# We need to run in two stages.
# During stage 1 the total decay widths are calculated and the parameter cards are built
# In the second stage the decay to dark pions is generated

print "//////////////////////////////"
print "// Start processing stage 1 //"
print "//////////////////////////////"

# Write process card for stage 1

with open("proc_card_mg5_stage1.dat", 'w') as fname:

    fname.write('import model {0} \n'.format(dmmodel))
    fname.write('define dall = u d c s t b u~ d~ c~ s~ t~ b~ e+ e- mu+ mu- ta+ ta- vl vl~ w+ w- z h dp+ dp- dp0\n')
    if dmmodel == 'Gaugephobic_SU2R':
        fname.write('define rho = rho0 \n')
    else:
        fname.write('define rho = rho+ rho- rho0 \n')
    fname.write('generate p p > rho, rho > dall dall QED<=4 NP<=4  \n')
    fname.write('output -f\n')
    fname.write('launch\n')
    fname.write('set WDP0 Auto\n')
    fname.write('set WDP Auto\n')
    fname.write('set Wro Auto\n')
    fname.write('set Wrop Auto\n')
    fname.write('set lhc 13\n')
    fname.write('set pionmass {0} \n'.format(dpionmass))
    fname.write('set fermioneta {0} \n'.format(fermioneta))
    fname.write('set xi {0} \n'.format((125.0*125.0)/(dpionmass*dpionmass)))
    fname.write('update dependent \n')
    fname.write('compute_widths dp+ dp0 --body_decay=3 --min_br=0.001\n')
    if dmmodel == 'Gaugephobic_SU2R':
        fname.write('compute_widths rho0 --body_decay=2 --min_br=0.001\n')
    else:
        fname.write('compute_widths rho0 rho+ --body_decay=2 --min_br=0.001\n')

process_dir = new_process(card_loc='proc_card_mg5_stage1.dat')

# Copy parameter card into main directory
copy(process_dir+'/Cards/param_card.dat', 'param_card.dat')

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0',
           'cut_decays':'F',
           'pdlabel':"'nn23lo1'",
           'use_syst':"False"}

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card_stage1.dat',
               nevts=(runArgs.maxEvents),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()

print "//////////////////////////////"
print "// Start processing stage 2 //"
print "//////////////////////////////"

# Write process card for stage 2

with open("proc_card_mg5_stage2.dat", 'w') as fname2:

    fname2.write('import model {0} \n'.format(dmmodel))
    fname2.write('define pi = dp+ dp- dp0\n')
    fname2.write('generate p p > pi pi NP=99 QED=99\n')
    fname2.write('output -f\n')

process_dir_2 = new_process(card_loc='proc_card_mg5_stage2.dat')

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir_2),run_card_new='run_card_stage2.dat',
               nevts=runArgs.maxEvents*3,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
runName='run_01'

# Generation of events
generate(run_card_loc='run_card_stage2.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir_2,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir_2,outputDS=runName+'._00001.events.tar.gz',saveProcDir=True)

### Shower 
evgenConfig.description = 'MadGraph_DarkMesons'
evgenConfig.keywords = ["exotic", "BSM"]
evgenConfig.inputfilecheck = runName
evgenConfig.contact  = [ 'jochen.jens.heinrich@cern.ch' ]
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
