from MadGraphControl.MadGraphUtils import *
import math

safefactor=2.

# define process and specify W decay modes (**all** decays, **had**ronic decays (no b-quarks), **semilep**tonic decays)
# + number of additional partons jets
# default to "had" if nothing specified
try: assert process in ["all", "had", "semilep"]
except NameError: 
    print("process not set, defaulting to 'all'")
    process = "all"

# default to "0" (meaning 0 + 1 parton in matrix element) if nothing specified
try: assert jets in ["01", "0", "1"]
except NameError:
    print("jets variable not set, defaulting to '0'")
    jets = "0"
# ----------------------------------------------------------------------------
# Full decay of W bosons
# ----------------------------------------------------------------------------
if process == "all":
    process_string = """
# define standard model particles W decay products (e, mu, tau, neutrinos, jets, b-jets)
define wdecay = j b b~ l+ l- ta+ ta- vl vl~
"""
    if jets == "01":
        process_string += """
generate p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w+ > wdecay wdecay, w- > wdecay wdecay) @0
add process p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w+ > wdecay wdecay, w- > wdecay wdecay) @1
"""
    elif jets == "0":
        process_string += """
generate p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w+ > wdecay wdecay, w- > wdecay wdecay) @0
"""
    elif jets == "1":
        process_string += """
generate p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w+ > wdecay wdecay, w- > wdecay wdecay) @1
"""
# ----------------------------------------------------------------------------
# Hadronic decay of W bosons
# ----------------------------------------------------------------------------
elif (process == "had"):
    process_string = ""
    if jets == "01":
        process_string = """
generate p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w+ > j j, w- > j j) @0
add process p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w+ > j j, w- > j j) @1
"""
    elif jets == "0":
        process_string = """
generate p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w+ > j j, w- > j j) @0
"""
    elif jets == "1":
        process_string = """
generate p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w+ > j j, w- > j j) @1
"""
# ----------------------------------------------------------------------------
# Semileptonic decay of W bosons
# ----------------------------------------------------------------------------
elif (process == "semilep"):
    process_string = """
define lepton = l+ l-
define neutrino = vl vl~
"""
    if jets == "01":
        process_string = """
generate p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w+ > j j, w- > lepton neutrino) @0
add process p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w- > j j, w+ > lepton neutrino) @0
add process p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w+ > j j, w- > lepton neutrino) @1
add process p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w- > j j, w+ > lepton neutrino) @1
"""
    elif jets == "0":
        process_string = """
generate p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w+ > j j, w- > lepton neutrino) @0
add process p p > zp > n1 n1 hs QED<=2, (hs > w+ w-, w- > j j, w+ > lepton neutrino) @0
"""
    elif jets == "1":
        process_string = """
generate p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w+ > j j, w- > lepton neutrino) @1
add process p p > zp > n1 n1 hs j QED<=2, (hs > w+ w-, w- > j j, w+ > lepton neutrino) @1
"""

# write process card
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model DarkHiggs2MDM
{process}
output -f
""".format(process=process_string))
fcard.close()

# require beam energy to be set as argument
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()

# determine ktdurham cut from dark Higgs mass
# (ktdurham cut sets scale at which event description is split between parton shower and matrix element) 
try:
    ktdurham = int(mhs / 4)
    assert ktdurham > 40
except AssertionError:
    ktdurham = 40

# fetch default LO run_card.dat and set parameters
extras = {'lhe_version':'3.0',
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : 263000,
          'cut_decays': 'F'
          }

# from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#Problems_with_run_card_dat_in_ne
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut=0.0,
               nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()

# write parameter card
paramcard = subprocess.Popen(['get_files','-data','MadGraph_param_card_DarkHiggs2MDM.dat'])
paramcard.wait()
if not os.access('MadGraph_param_card_DarkHiggs2MDM.dat', os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat', os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_param_card_DarkHiggs2MDM.dat','r')
    newcard = open('param_card.dat','w')

    for line in oldcard:
        if '{__COUPLING_GQ__}' in line:
            newcard.write('   1 %e # gq \n'%(gq))
        elif '{__COUPLING_GX__}' in line:
            newcard.write('   2 %e # gx \n'%(gx))
        elif '{__MIXING_TH__}' in line:
            newcard.write('   3 %e # th \n'%(th))
        elif '{__MASS_MDM__}' in line:
            newcard.write('  1000022 %e # MDM \n'%(mdm))
        elif '{__MASS_MHs__}' in line:
            newcard.write('  54 %e # MHs \n'%(mhs))
        elif '{__MASS_MZp__}' in line:
            newcard.write('  55 %e # MZP \n'%(mzp))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

runName='run_01'

#--------------------------------------------------------------------------------------------------- 
# Using the helper function from MadGraphControl for setting up the param_card # Build a new param_card.dat from an existing one # Used values given in "parameters" for MHH and WHH, if not set there, default values are used # Higgs mass is set to 125 GeV by "higgsMass" 
#--------------------------------------------------------------------------------------------------- 
build_param_card(param_card_old='MadGraph_param_card_DarkHiggs2MDM.dat',param_card_new='param_card_new.dat')

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name=runName,proc_dir=process_dir)
# multi-core capability
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

outputDS=arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

# option: disable TestHepMC
# if hasattr(testSeq, "TestHepMC"):
#     testSeq.remove(TestHepMC())

evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.description = "Dark Higgs (WW) Dark Matter from 2MDM UFO"
evgenConfig.keywords = ["exotic","BSM"]
if (process == "all"):
    evgenConfig.process = "generate p p > zp > n1 n1 hs, (hs > w+ w- > all)"
elif (process == "had"):
    evgenConfig.process = "generate p p > zp > n1 n1 hs, (hs > w+ w- > j j j j)"
elif (process == "semilep"):
    evgenConfig.process = "generate p p > zp > n1 n1 hs, (hs > w+ w- > l vl j j)"
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=outputDS
evgenConfig.contact = ["Philipp Mogg <pmogg@cern.ch>"]

#--------------------------------------------------------------
# Herwig 7 (H7UE) showering setup
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="LO")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

#HW7 settings
Herwig7Config.add_commands ("""
cd /Herwig/Particles
create /ThePEG/ParticleData hs
setup hs 54  hs MHs WIDTH MAX_WIDTH 0 0 0 1 0
create /ThePEG/ParticleData Zp
setup Zp 55  Zp MZP WIDTH MAX_WIDTH 0 0 0 3 0
create /ThePEG/ParticleData DM
setup DM 1000022  DM MDM WIDTH MAX_WIDTH 0 0 0 2 1
set /Herwig/Particles/DM:Stable Stable
""")

# run Herwig7
Herwig7Config.run()

# # MET filter
# include("MC15JobOptions/MissingEtFilter.py")
# filtSeq.MissingEtFilter.METCut = 50*GeV
# filtSeq.Expression = "(MissingEtFilter)"

bonus_file = open('pdg_extras.dat','w')
bonus_file.write('54\n')
bonus_file.write('55\n')
bonus_file.write('1000022\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'
