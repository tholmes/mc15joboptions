# Author: K. Potamianos <karolos.potamianos@cern.ch>

from MadGraphControl.MadGraphUtils import *
import fileinput
import shutil
import subprocess
import os

mode=0
gridpack_dir='madevent/'
gridpack_mode=True
name = runArgs.jobConfig[0]
thisDSID = int(name.split(".")[1])
runName = str(thisDSID)

os.environ['FC'] = "gfortran -std=legacy"

useUserScale = [ ]

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=1.2*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
if thisDSID in [ 363274 ]: nevents=12*runArgs.maxEvents if runArgs.maxEvents>0 else 1200

if thisDSID in [ 363273 ]:
    fcard = open('proc_card_mg5.dat','w')
    fcard.write(""" 
    import model sm-no_b_mass
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    generate g g > e+ ve mu- vm~ [QCD]
    add process g g > e- ve~ mu+ vm [QCD]
    output -f""")
    fcard.close()
elif thisDSID in [ 363274 ]:
    fcard = open('proc_card_mg5.dat','w')
    fcard.write(""" 
    import model sm-no_b_mass
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    generate g g > ta+ vt e- ve~ [QCD]
    add process g g > ta- vt~ e+ ve [QCD]
    add process g g > ta+ vt mu- vm~ [QCD]
    add process g g > ta- vt~ mu+ vm [QCD]
    add process g g > ta+ vt ta- vt~ [QCD]
    add process g g > ta- vt~ ta+ vt [QCD]
    output -f""")
    fcard.close()
else:
    print("ERROR: no process definition for DSID {}".format(thisDSID))
    import sys
    sys.exit(1)

name = "ssWW"
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


maxjetflavor = 5
dynamic=-1

if(thisDSID in useUserScale):
   dynamic = 0

process_dir = new_process(grid_pack="madevent/")
extras = { 'lhe_version'   :'3.0',
           'pdlabel'      : "'lhapdf'",
           'lhaid'         : 260000,
           'maxjetflavor'  : maxjetflavor,
           'auto_ptj_mjj'  : False,
           'etal'   : 5,
           'drll'   : 0,
           'drjl'   : 0,
           'use_syst'      : True,
           'dynamical_scale_choice': dynamic
           }

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001)

outputDS=arrange_output(run_name=runName, proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=3)

runArgs.inputGeneratorFile=outputDS

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

if "StoreLHE" in genSeq.Pythia8.__slots__.keys():
    print "Enablihg ABLING storage of LHE record in HepMC by default."
    genSeq.Pythia8.StoreLHE = False

if thisDSID in [ 363274 ]:
    # Set up multi-tau filters
    if not hasattr(filtSeq, "TauFilter" ):
      from GeneratorFilters.GeneratorFiltersConf import TauFilter
      lep13lep7filter = TauFilter("lep13lep7filter")
      filtSeq += lep13lep7filter

    #filtSeq.lep13lep7filter.UseNewOptions = False
    filtSeq.lep13lep7filter.Ntaus = 2
    filtSeq.lep13lep7filter.Nleptaus = 2
    #filtSeq.lep13lep7filter.Nhadtaus = 0
    filtSeq.lep13lep7filter.Ptcutlep = 20000.0 #MeV
    filtSeq.lep13lep7filter.Ptcutlep_lead = 25000.0 #MeV


evgenConfig.minevents = 5000
if thisDSID in [ 363274 ]: evgenConfig.minevents = 100
