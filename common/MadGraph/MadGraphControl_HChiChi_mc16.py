from MadGraphControl.MadGraphUtils import *

#############################################
### Production of scalar long-lived particles
### though a higgs-like scalar mediator
############################################

# PDF
pdflabel = 'lhapdf'
lhaid = 315000 # NNPDF31_lo_as_0118

process = {
    1:  'generate p p > xchi xchi , xchi > ta- ta+ vl , xchi > ta+ ta- vl~',
    2:  'generate p p > xchi xchi , xchi > c b s      , xchi > c~ b~ s~',
    3:  'generate p p > xchi xchi , xchi > vl b b~    , xchi > vl~ b~ b'
}

# parameter mass and average life-time
dictmChi = { 
    312627 : 10, 312628 : 55, 312629 : 100, 
    312630 : 10, 312631 : 55, 312632 : 100, 
    312633 : 10, 312634 : 55, 312635 : 100 }

dictnProcess = { 
    312627 : 1, 312628 : 1, 312629 : 1, 
    312630 : 2, 312631 : 2, 312632 : 2, 
    312633 : 3, 312634 : 3, 312635 : 3 }

dictAvgtau = { 
    312627 : 920, 312628 : 5550, 312629 : 3500, 
    312630 : 920, 312631 : 5550, 312632 : 3500, 
    312633 : 920, 312634 : 5550, 312635 : 3500 }

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

try :    
    modelcode='WIMP_BG_higgsportal_full_loop'
    mChi = dictmChi[runArgs.runNumber]
    nProcess = dictnProcess[runArgs.runNumber]
    avgtau = dictAvgtau[runArgs.runNumber]
except KeyError:
    raise RuntimeError('Bad runNumber')

# basename for madgraph LHEF file
rname = 'run_'+str(runArgs.runNumber)

# writing proc card for MG
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model """+modelcode+"""
define p = g u c d s u~ c~ d~ s~
 define l- = e- mu- ta-
 define l+ = e+ mu+ ta+
 define vl = ve vm vt
 define vl~ = ve~ vm~ vt~
 %s
 output -f
 """ % (process[nProcess]))
fcard.close()

#---------------------------------------------------------------------------
# Energy
#---------------------------------------------------------------------------
    
beamEnergy = -999.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

run_card_extras = { 
    'lhe_version':'3.0',
    'cut_decays':'F',
    'event_norm':'sum',
    'pdlabel':pdflabel,
    'lhaid':lhaid,
    'ptj':'0',
    'ptb':'0',
    'pta':'0',
    'ptl':'0',
    'etaj':'-1',
    'etab':'-1',
    'etaa':'-1',
    'etal':'-1',
    'drjj':'0',
    'drbb':'0',
    'drll':'0',
    'draa':'0',
    'drbj':'0',
    'draj':'0',
    'drjl':'0',
    'drab':'0',
    'drbl':'0',
    'dral':'0' ,
    'use_syst':'T',
    'sys_scalefact': '1 0.5 2',
    'sys_pdf'      : "NNPDF31_lo_as_0118"
    }


safefactor=1.1 #generate extra 10% events in case any fail showering
if runArgs.maxEvents > 0: 
    nevents = runArgs.maxEvents*safefactor
else: nevents = 5000*safefactor

process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(process_dir),
               run_card_new='run_card.dat',
               nevts=nevents,
               rand_seed=runArgs.randomSeed,
               beamEnergy=beamEnergy,
               extras=run_card_extras)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------

param_card_extras = { "MASS": { 'MxChi': mChi}, # chi mass
}

build_param_card(param_card_old="{}/Cards/param_card.dat".format(process_dir),
                 param_card_new='param_card.dat',
                 params=param_card_extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------

generate(run_card_loc='run_card.dat', 
         param_card_loc='param_card.dat', 
         mode=0, njobs=1, 
         run_name=rname, 
         proc_dir=process_dir)

#---------------------------------------------------------------------------
# Arrange LHE file output
#---------------------------------------------------------------------------

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)
# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)
    

# replacing lifetime of scalar, manually
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
    
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:  
        if 'vent' in line or line.startswith("<"):
            newlhe.write(line)
            continue
        newline = line.rstrip('\n')
        columns = (' '.join(newline.split())).split()
        pdgid = int(columns[0])
        if pdgid == 3000001:
            part1 = line[:-22]
            part2 = "%.11E" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        else:
            newlhe.write(line)

oldlhe.close()
newlhe.close()
    
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',
            process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(run_name=rname, proc_dir=process_dir,
               outputDS=rname+'._00001.events.tar.gz',
               lhe_version=3,
               saveProcDir=True)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------

if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000",
			    "15:offIfAny = 11 12 13 14",
			    "-15:offIfAny = 11 12 13 14"]

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.description = "Displaced hadronic jets process WIMP (Higgs portal) with mChi=%sGeV' % (mChi)"
evgenConfig.keywords = ["exotic", "BSM", "BSMHiggs", "longLived"]
evgenConfig.contact  = ['simon.berlendis@cern.ch', 'hao.zhou@cern.ch', 'akvam@cern.ch',
                        'Cristiano.Alpigiani@cern.ch' ]
evgenConfig.process=" --> LLPs"
evgenConfig.inputfilecheck = rname
runArgs.inputGeneratorFile=rname+'._00001.events.tar.gz'
