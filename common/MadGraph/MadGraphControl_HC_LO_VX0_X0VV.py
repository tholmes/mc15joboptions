from MadGraphControl.MadGraphUtils import *

mode=0

### DSID lists (extensions can include systematics samples)
test_1000_0_300 = [451318]
test_0_5000_300 = [451319]
test_1000_0_600 = [451320]
test_0_5000_600 = [451321]
test_1000_0_900 = [451322]
if runArgs.runNumber in test_1000_0_300:
   Khzz = '-5.2481e-01'
   Khww = '-5.2481e-01'
   Khdz = '-2.62405e-01'
   Khdwr = '-2.62405e-01'
   Mass = 300
elif runArgs.runNumber in test_0_5000_300:
   Khzz = '4.07806'
   Khww = '5.2481'
   Khdz = '0'
   Khdwr = '0'
   Mass = 300
elif runArgs.runNumber in test_1000_0_600:
   Khzz = '-5.2481e-01'
   Khww = '-5.2481e-01'
   Khdz = '-2.62405e-01'
   Khdwr = '-2.62405e-01'
   Mass = 600
elif runArgs.runNumber in test_0_5000_600:
   Khzz = '4.07806'
   Khww = '5.2481'
   Khdz = '0'
   Khdwr = '0'
   Mass = 600
elif runArgs.runNumber in test_1000_0_900:
   Khzz = '-5.2481e-01'
   Khww = '-5.2481e-01'
   Khdz = '-2.62405e-01'
   Khdwr = '-2.62405e-01'
   Mass = 900
else:
   raise RuntimeError("runNumber %i not recongnised in these jobOptions."%runArgs.runNumber)
#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in HC model 1000,0
#---------------------------------------------------------------------------------------------------
parameters={
    'frblock':{ 
        'lambda':  '5.000000e+03',
        'ca':      '1.000000e+00',
        'ksm':     '5.000000e-02',
        'khtt':    '0.000000e+00',
        'katt':    '0.000000e+00',
        'khbb':    '0.000000e+00',
        'kabb':    '0.000000e+00',
        'khll':    '0.000000e+00',
        'kall':    '0.000000e+00',
        'khaa':    '0.000000e+00',
        'kaaa':    '0.000000e+00',
        'khza':    '0.000000e+00',
        'kaza':    '0.000000e+00',
        'khgg':    '0.000000e+00',
        'kagg':    '0.000000e+00',
        'khzz':     Khzz,
        'kazz':    '0.000000e+00',
        'khww':     Khww,
        'kaww':    '0.000000e+00',
        'khda':    '0.000000e+00',
        'khdz':     Khdz,
        'khdwr':    Khdwr,
        'khdwi':   '0.000000e+00',
        'khhgg':   '1.000000e+00',
        'kaagg':   '1.000000e+00',
        'kqa':     '1.000000e+00',
        'kqb':     '1.000000e+00',
        'kla':     '1.000000e+00',
        'klb':     '1.000000e+00',
        'kw1':     '1.000000e+00',
        'kw2':     '1.000000e+00',
        'kw3':     '0.000000e+00',
        'kw4':     '0.000000e+00',
        'kw5':     '0.000000e+00',
        'kz1':     '0.000000e+00',
        'kz3':     '1.000000e+00',
        'kz5':     '0.000000e+00',
        'kq':      '1.000000e+00',
        'kq3':     '1.000000e+00',
        'kl':      '1.000000e+00',
        'kg':      '1.000000e+00',
        'ka':      '1.000000e+00',
        'kz':      '1.000000e+00',
        'kw':      '1.000000e+00',
        'kza':     '0.000000e+00'}
    }



#---------------------------------------------------------------------------------------------------
# Setting X0 mass and width for param_card.dat
#---------------------------------------------------------------------------------------------------
resonanceMass= Mass
X0Mass  = {'5000000': '%e # MX0'%resonanceMass}  #Mass
X0Decay = {'5000000':'DECAY 5000000 Auto # WX0'} #Width


#---------------------------------------------------------------------------------------------------
# Generating pp -> VX0, X0 -> VV in HC LO model
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > z x0, z > j j, x0 > l- l+ j j
add process p p > z x0, z > l- l+, x0 > j j j j
add process p p > w+ x0, w+ > j j, x0 > l- l+ j j
add process p p > w- x0, w- > j j, x0 > l- l+ j j
add process p p > z x0, z > l- l+, x0 > l- vl~ j j
add process p p > z x0, z > l- l+, x0 > l+ vl j j
add process p p > w+ x0, w+ > l+ vl, x0 > l- l+ j j
add process p p > w- x0, w- > l- vl~, x0 > l- l+ j j
add process p p > w+ x0 > w+ w+ w-, w+ > l+ vl, w- > j j
add process p p > w- x0 > w- w- w+, w- > l- vl~, w+ > j j
output -f""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()


#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
#Fetch default NLO run_card.dat and set parameters
extras = {'lhe_version'   : '3.0', 
          'cut_decays'    : 'F',
          'pdlabel'       : "'lhapdf'",
          'lhaid'         : '247000',
          'use_syst'      : 'True',
          'sys_scalefact' : '1.0 0.5 2.0',
          'sys_pdf'       : 'NNPDF23_lo_as_0130_qed',
          'parton_shower' : 'PYTHIA8',
	    'dynamical_scale_choice' : '3'}

#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 2.0
nevents    = 10000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor



#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------

build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat',masses=X0Mass,decays=X0Decay,params=parameters)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------
extras={}
extras["scale"] = resonanceMass
extras["dsqrt_q2fact1"] = resonanceMass
extras["dsqrt_q2fact2"] = resonanceMass
extras["pdlabel"]='lhapdf'
extras["lhaid"]=247000
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat',
               nevts=nevents,
               rand_seed=runArgs.randomSeed,
               beamEnergy=beamEnergy,
               scalefact=1.0,
               alpsfact=1.0,
               extras=extras)

print_cards()


runName='run_01'

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
outputDS=arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3)

#######
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
   print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
   njobs = os.environ.pop('ATHENA_PROC_NUMBER')
   #Try to modify the opts underfoot
   if not hasattr(opts,'nprocs'): print 'Did not see option!'
   else: opts.nprocs = 0
   print opts


#### Shower

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'pp->VX0, X0->VV with LO HC model'
evgenConfig.keywords+=['BSM', "Higgs"]
evgenConfig.contact = ['Xin Chen <xin.chen@cern.ch>']
runArgs.inputGeneratorFile=outputDS

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

### Set lepton filters
if not hasattr(filtSeq, "MultiLeptonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
   lepfilter = MultiLeptonFilter("lepfilter")
   filtSeq += lepfilter

filtSeq.lepfilter.Ptcut = 10000.0 #MeV
filtSeq.lepfilter.Etacut = 2.7
filtSeq.lepfilter.NLeptons = 2 #minimum
