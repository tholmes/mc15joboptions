# Script to perform Diagram Removal 1, i.e. set the amplitude of overlapping diagrams to zero.
# Works for Wt, tWZ and tWH. Removes the overlap (and interference) with tt, ttZ and ttH respectively.
# Does not work for tWgamma and tWll.
# Contact: olga.bylund@cern.ch
# Please let me know if you have any questions or problems.

import os
import glob

import DR_functions

pdir=process_dir+'/SubProcesses/'

P_folders= glob.glob(pdir+"P*_*")
mfiles = DR_functions.find_matrix_files(P_folders)

for mfile in mfiles:
    DR_functions.make_support_file(mfile, "the_process.txt", "Process:")


    with open("the_process.txt","r") as f:
        myline = f.readline()
        m2 = myline.split("Process: ")[1]
        the_process = m2.split(" WEIGHTED")[0]

        bindex, windex = DR_functions.return_out_index(the_process)  

    redefine_twidth=False
    DRmode=1
    DR_functions.find_W_prepare_DRXhack(mfile,bindex,windex,redefine_twidth,DRmode)

    if os.path.getsize("mytmp.txt")>0: #if there are hacks to make for this matrix file
        print "not empty mytmp"
        DR_functions.do_DR1_hacks(mfile,"mytmp.txt")
            

os.remove("the_process.txt")
