from MadGraphControl.MadGraphUtils import *
import os
import fileinput
import re

nevents=10000
evgenConfig.minevents=5000
mode=0
gridpack_dir='madevent/'
qCut=25


#---------------------------------------------------------------------------
# Process type based on runNumber:
#---------------------------------------------------------------------------
if runArgs.runNumber==346485:
    process1='generate p p > h a j j [QCD] @0'
    evgenConfig.keywords = ['Higgs','photon','bottom','VBFHiggs']
    evgenConfig.contact = ['Carolyn Gee <Carolyn.Mei.Gee@cern.ch>']
    description='MadGraph_aMC@NLO h->jj 0,2jets@NLO'
    ptgmin=19
    name='MG5_aMCatNLO262.346485.VBFhajj_PDF4LHC_13TeV'
    gridpack='MG5_aMCatNLO262.346485.VBFhajj_PDF4LHC_13TeV'

else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

if hasattr(runArgs, 'inputGenConfFile'):
    if not gridpack in runArgs.inputGenConfFile:
        raise RuntimeError("Please use the correct gridpack for this DSID.")

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+process1+"""
output -f
""")
fcard.close()

#----------------------------------------------------------------------------
# Run Number
#----------------------------------------------------------------------------
if not hasattr(runArgs,'runNumber'):
    raise RunTimeError("No run number found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = { 'lhe_version' : '3.0',
           'parton_shower' :'HERWIGPP',
           'pdlabel'       :"'lhapdf'",
           'lhaid'         :"90000",
           'ptj'           :"15.0",
           'etaj'          :"-1.0",
           'ptgmin'        :ptgmin,
           'epsgamma'      :'1.0', #'0.1',
           'R0gamma'       :'0.4',#'0.1',
           'xn'            :'1', #'2',
           'isoEM'         :'True',
           'etagamma'      :'3.0',
           'bwcutoff'      :'15',
           'maxjetflavor'  :'5',
           'reweight_scale':'True',
           'rw_Rscale_down':0.5,  
           'rw_Rscale_up'  :2.0,
           'rw_Fscale_down':0.5,
           'rw_Fscale_up'  :2.0,
           'reweight_PDF'  :'True',
           'PDF_set_min'   :90001,
           'PDF_set_max'   :90100,
           'dynamical_scale_choice' : '-1', #default value
           'store_rwgt_info':'True'
}


                              

process_dir = new_process(grid_pack=gridpack_dir)


if not hasattr(runArgs, 'inputGenConfFile'):
    with open("PROCNLO_loop_sm-no_b_mass_0/Cards/FKS_params.dat","rw") as file:
        for line in file:
            print line

    print "editing FKS file"

    for line in fileinput.input("PROCNLO_loop_sm-no_b_mass_0/Cards/FKS_params.dat", inplace=2):
        line = re.sub('1.0d-5', '-1d0', line.rstrip())
        print(line)

    print line

else:
    print "FKS file already corrected"

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', 
               xqcut=15,
               nevts=nevents,
               rand_seed=randomSeed,
               beamEnergy=beamEnergy,
               extras=extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=name,grid_pack=True,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed)

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts


#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
output_ds=name+'._00001.events.tar.gz'
arrange_output(run_name=name,
               proc_dir=process_dir,
               outputDS=output_ds
               )
 
evgenConfig.generators  += ["aMcAtNlo","Herwig7"]
evgenConfig.description = 'MadGraph_higgs_NLO_Herwigpp'
evgenConfig.keywords+=['jets']
evgenConfig.inputfilecheck = name
runArgs.inputGeneratorFile=output_ds
evgenConfig.tune =  "PDF4LHC15_nlo_mc_pdfas" 

if hasattr(runArgs,'outputTXTFile'):                                                                                                   
    runArgs.inputGeneratorFile=runArgs.outputTXTFile.split('.tar.gz')[0]+'.events'
    evgenConfig.inputfilecheck=runArgs.outputTXTFile.split('.tar.gz')[0]
else:
    runArgs.inputGeneratorFile='tmp_'+runArgs.inputGeneratorFile

print "runArgs.inputGeneratorFile",runArgs.inputGeneratorFile
lhe_filename=runArgs.inputGeneratorFile
print "lhe_file print test",lhe_filename


from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME
from Herwig7_i.Herwig7ConfigLHEF import Hw7ConfigLHEF

genSeq += Herwig7()

Herwig7Config = Hw7ConfigLHEF(genSeq,runArgs)
HERWIGPP_qCut=qCut 
HERWIGPP_nJetMax=1 


# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_mc_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO") 


# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

import os
if "HERWIG7VER" in os.environ:
   version = os.getenv("HERWIG7VER")
   verh7 = version.split(".")[1]
else:
   verh7 = 0

if int(verh7 == 0):
#   Herwig7Config.add_commands("""
# insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
#set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV   
    Herwig7Config.add_commands("""
 do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
 do /Herwig/Particles/h0:PrintDecayModes
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 
 """)

else:
#   Herwig7Config.add_commands("""
   Herwig7Config.add_commands("""
## ------------------
## Hard process setup
## ------------------
 do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
 do /Herwig/Particles/h0:PrintDecayModes
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 """)


# run Herwig7
Herwig7Config.run()


#--------------------------------------------------------------------------------------------------------------------
# Mjj filtering:
#--------------------------------------------------------------------------------------------------------------------




#--------------------------------------------------------------------------------------------------------------------
# Sample information:
#--------------------------------------------------------------------------------------------------------------------

"""evgenConfig.generators = ["aMcAtNlo", "Herwig7"]
evgenConfig.contact = ['Carolyn Gee <Carolyn.Mei.Gee@cern.ch>']
evgenConfig.description = description
runArgs.inputGeneratorFile=name+'._00001.events.tar.gz'
evgenConfig.minevents=minevents"""


