
try:
    PYTHIA8_nJetMax
except RuntimeError:
    raise RuntimeError("Variable \"PYTHIA8_nJetMax\" is not defined, this is needed to configure Pythia8 FxFx matching settings. Please define it in your jobOptions")
else:
    print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax


try:
    PYTHIA8_qCut
except RuntimeError:
    raise RuntimeError("Variable \"PYTHIA8_qCut\" is not defined, this is needed to configure Pythia8 FxFx matching settings. Please define it in your jobOptions")
else:
    print "PYTHIA8_qCut = %i"%PYTHIA8_qCut
    

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = %f"%PYTHIA8_qCut, #this is the actual merging scale
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx        = on",
                            "JetMatching:qCutME        = 10.0", #this must match the ptj cut in the lhe generation step
                            "JetMatching:nJetMax       = %i"%PYTHIA8_nJetMax, #number of partons in born matrix element for highest multiplicity
                            'JetMatching:jetAlgorithm = 2', #explicit setting of kt-merging for FxFx (however also imposed in the Py8-FxFx inteface)
                            'JetMatching:slowJetPower = 1',#explicit setting of kt-merging for FxFx (however also imposed in the Py8-FxFx inteface)
                            'JetMatching:nQmatch = 5', #4 corresponds to 4-flavour scheme (no matching of b-quarks), 5 for 5-flavour scheme
                            "JetMatching:eTjetMin = 30.", #This seems to be 20 in the Pythia default, it should be equal or lower than qCut
]


if hasattr(genSeq.Pythia8, "UserHook"):     
    genSeq.Pythia8.UserHook = 'JetMatchingMadgraph' 
else:     
    genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph'] 

genSeq.Pythia8.FxFxXS = True


