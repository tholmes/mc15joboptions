## Enable CompHep LHEF reading in Pythia8
include("MC15JobOptions/Pythia8_LHEF.py")
evgenConfig.generators = ["CompHep", "Pythia8"]
