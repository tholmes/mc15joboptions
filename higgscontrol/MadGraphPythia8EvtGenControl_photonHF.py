from MadGraphControl.MadGraphUtils import *

safefactor=3
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

mode=0
#Defaults for run_card.dat
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'parton_shower':'PYTHIA8',
           'ptj':'20',
           'ptb':'20',
           'pta':'30',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'3',
           'etab':'3',
           'etaa':'3',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'60',
           'mmaamax':'200',
           'mmbb':'45',
           'mmbbmax':'-1',
           'drjj':'0.4',
           'drbb':'0.4',
           'draa':'0.4',
           'drbj':'0.4',
           'draj':'0.4',
           'drab':'0.4',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1' }


fcard = open('proc_card_mg5.dat','w')
runName='run_01'     
if (runArgs.runNumber == 341061):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define bs = b b~
    generate p p > a a bs bs
    output -f""")
    fcard.close()
    runName='aabb'
elif (runArgs.runNumber == 341062):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define bs = b b~
    generate p p > a bs bs j
    output -f""")
    fcard.close()
    runName='abbj'
elif (runArgs.runNumber == 341063):
    fcard.write("""
    import model sm
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s u~ c~ d~ s~
    define bs = b b~
    generate p p > a a bs j
    output -f""")
    fcard.close()
    runName='aabj'
elif (runArgs.runNumber == 341064):
    fcard.write("""
    import model sm
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s u~ c~ d~ s~
    define bs = b b~
    generate p p > a bs j j
    output -f""")
    fcard.close()
    runName='abjj'
elif (runArgs.runNumber == 341065):
    extras['mmjj']='60'
    extras['mmjjmax']='-1'
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > a a j j
    output -f""")
    fcard.close()
    runName='aajj'
elif (runArgs.runNumber == 341066):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > a j j j
    output -f""")
    fcard.close()
    runName='ajjj'
elif (runArgs.runNumber == 343437):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define bs = b b~
    generate p p > bs bs j j
    output -f""")
    fcard.close()
    runName='bbjj'
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

build_run_card(run_card_old=get_default_runcard(),run_card_new='run_card.dat',
               xqcut=0,nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
    

process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')  

evgenConfig.contact = ['Jahred Adelman']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
