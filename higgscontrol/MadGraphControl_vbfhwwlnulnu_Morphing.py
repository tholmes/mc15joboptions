######################################################################
# parameters for different morphing samples
######################################################################

param_config = {
  345603 : {"Lambda":1000, "cosa":0.707107, "kAww":213.003, "kAzz":213.003, "kHdwI":3.92762, "kHdwR":3.92762, "kHdz":3.92762, "kHww":95.3836, "kHzz":95.3836, "kSM":48.3764},
  345604 : {"Lambda":1000, "cosa":0.707107, "kAww":164.781, "kAzz":164.781, "kHdwI":388.518, "kHdwR":388.518, "kHdz":388.518, "kHww":-493.791, "kHzz":-493.791, "kSM":41.4102},
  345605 : {"Lambda":1000, "cosa":0.707107, "kAww":301.637, "kAzz":301.637, "kHdwI":-12.041, "kHdwR":-12.041, "kHdz":-12.041, "kHww":-99.3686, "kHzz":-99.3686, "kSM":48.2786},
  345606 : {"Lambda":1000, "cosa":0.707107, "kAww":5.19419, "kAzz":5.19419, "kHdwI":18.3858, "kHdwR":18.3858, "kHdz":18.3858, "kHww":-55.7367, "kHzz":-55.7367, "kSM":42.116},
  345607 : {"Lambda":1000, "cosa":0.707107, "kAww":-472.139, "kAzz":-472.139, "kHdwI":-12.542, "kHdwR":-12.542, "kHdz":-12.542, "kHww":255.844, "kHzz":255.844, "kSM":34.2899},
  345608 : {"Lambda":1000, "cosa":0.707107, "kAww":-185.304, "kAzz":-185.304, "kHdwI":21.9543, "kHdwR":21.9543, "kHdz":21.9543, "kHww":118.85, "kHzz":118.85, "kSM":0.0641366},
  345609 : {"Lambda":1000, "cosa":0.707107, "kAww":53.3354, "kAzz":53.3354, "kHdwI":335.709, "kHdwR":335.709, "kHdz":335.709, "kHww":285.63, "kHzz":285.63, "kSM":41.5},
  345610 : {"Lambda":1000, "cosa":0.707107, "kAww":124.963, "kAzz":124.963, "kHdwI":-57.6339, "kHdwR":-57.6339, "kHdz":-57.6339, "kHww":-27.3816, "kHzz":-27.3816, "kSM":42.7958},
  345611 : {"Lambda":1000, "cosa":0.707107, "kAww":-396.996, "kAzz":-396.996, "kHdwI":3.76993, "kHdwR":3.76993, "kHdz":3.76993, "kHww":496.985, "kHzz":496.985, "kSM":0.000134457},
  345612 : {"Lambda":1000, "cosa":0.707107, "kAww":-58.8848, "kAzz":-58.8848, "kHdwI":70.2453, "kHdwR":70.2453, "kHdz":70.2453, "kHww":0.8478, "kHzz":0.8478, "kSM":50},
  345613 : {"Lambda":1000, "cosa":0.707107, "kAww":409.409, "kAzz":409.409, "kHdwI":40.2696, "kHdwR":40.2696, "kHdz":40.2696, "kHww":164.26, "kHzz":164.26, "kSM":10.7638},
  345614 : {"Lambda":1000, "cosa":0.707107, "kAww":484.346, "kAzz":484.346, "kHdwI":63.4831, "kHdwR":63.4831, "kHdz":63.4831, "kHww":351.521, "kHzz":351.521, "kSM":38.9021},
  345615 : {"Lambda":1000, "cosa":0.707107, "kAww":101.141, "kAzz":101.141, "kHdwI":-57.4363, "kHdwR":-57.4363, "kHdz":-57.4363, "kHww":183.775, "kHzz":183.775, "kSM":40.2977},
  345616 : {"Lambda":1000, "cosa":0.707107, "kAww":37.1997, "kAzz":37.1997, "kHdwI":-9.74372, "kHdwR":-9.74372, "kHdz":-9.74372, "kHww":12.0317, "kHzz":12.0317, "kSM":48.1664},
  345617 : {"Lambda":1000, "cosa":0.707107, "kAww":-53.6814, "kAzz":-53.6814, "kHdwI":-5.32487, "kHdwR":-5.32487, "kHdz":-5.32487, "kHww":132.235, "kHzz":132.235, "kSM":50},
  345618 : {"Lambda":1000, "cosa":0.707107, "kAww":58.3388, "kAzz":58.3388, "kHdwI":9.2922, "kHdwR":9.2922, "kHdz":9.2922, "kHww":55.7372, "kHzz":55.7372, "kSM":50},
  345619 : {"Lambda":1000, "cosa":0.707107, "kAww":25.8794, "kAzz":25.8794, "kHdwI":-24.1788, "kHdwR":-24.1788, "kHdz":-24.1788, "kHww":43.0197, "kHzz":43.0197, "kSM":49.911},
  345620 : {"Lambda":1000, "cosa":0.707107, "kAww":0.964106, "kAzz":0.964106, "kHdwI":23.8109, "kHdwR":23.8109, "kHdz":23.8109, "kHww":-10.3103, "kHzz":-10.3103, "kSM":50},
  345621 : {"Lambda":1000, "cosa":0.707107, "kAww":-1.78431, "kAzz":-1.78431, "kHdwI":23.0304, "kHdwR":23.0304, "kHdz":23.0304, "kHww":166.9, "kHzz":166.9, "kSM":44.6809},
  345622 : {"Lambda":1000, "cosa":0.707107, "kAww":-130.505, "kAzz":-130.505, "kHdwI":-9.74183, "kHdwR":-9.74183, "kHdz":-9.74183, "kHww":-104.475, "kHzz":-104.475, "kSM":50},
  345623 : {"Lambda":1000, "cosa":0.707107, "kAww":400.651, "kAzz":400.651, "kHdwI":-427.524, "kHdwR":-427.524, "kHdz":-427.524, "kHww":-489.462, "kHzz":-489.462, "kSM":45.9839},
  345624 : {"Lambda":1000, "cosa":0.707107, "kAww":-7.33618, "kAzz":-7.33618, "kHdwI":4.62498, "kHdwR":4.62498, "kHdz":4.62498, "kHww":-1.91053, "kHzz":-1.91053, "kSM":0}
}

######################################################################
# fix check_poles issue with helper function
######################################################################

from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

######################################################################

from MadGraphControl.MadGraphUtils import *

#Some setup variables.
mode=0

from os import environ,path
environ['ATHENA_PROC_NUMBER'] = '1'

#Need extra events to avoid Pythia8 problems.
safefactor=50.0
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor


######################################################################
# Configure event generation.
######################################################################

runName='run_01'     

#Set input file for Pythia8.
runArgs.inputGeneratorFile = runName+'._00001.events.tar.gz'

#Make process card.
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-no_b_mass
define p = g d d~ u u~ s s~ c c~ b b~                                   
define j = g d d~ u u~ s s~ c c~ b b~                                 
generate p p > x0 j j $$ w+ w- z / a [QCD]
output ppx0_4l_NLOQCD_FxFx_MadSpin""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Define process directory.
process_dir = new_process()

# fix check_poles issue
replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")

#Fetch default run_card.dat and set parameters
extras = {'lhe_version'   :'3.0',
          'parton_shower' :'PYTHIA8',
          'reweight_scale':'True',
          'maxjetflavor'  :'5',
          #'pdlabel'       :'nn23nlo'
          'pdlabel'       :"'lhapdf'",
          'lhaid'         :260000,
          'pdfwgt'        :'T',
          'use_syst'      :'T',
          'sys_scalefact' :'None',
          'sys_alpsfact'  :'None',
          'sys_matchscale':'None',
          'sys_pdf'       :'NNPDF30_nlo_as_0118'
          }

#Set couplings here.
allparameters={
    'Lambda': 0.0,
    'cosa'  : 0.0,
    'kSM'   : 0.0,
    'kHtt'  : 0.0,
    'kAtt'  : 0.0,
    'kHll'  : 0.0,
    'kAll'  : 0.0,
    'kHaa'  : 0.0,
    'kAaa'  : 0.0,
    'kHza'  : 0.0,
    'kAza'  : 0.0,
    'kHzz'  : 0.0,
    'kAzz'  : 0.0,
    'kHww'  : 0.0,
    'kAww'  : 0.0,
    'kHda'  : 0.0,
    'kHdz'  : 0.0,
    'kHdwR' : 0.0,
    'kHdwI' : 0.0
    }

parameters = {}
if runArgs.runNumber in param_config.keys():
    frblock= {}
    for par,val in allparameters.items():
        if par in param_config[runArgs.runNumber].keys():
            frblock[par] = param_config[runArgs.runNumber][par]
        else:
            frblock[par] = val
    parameters['frblock'] = frblock
else:
    print("unknown DSID: "+str(runArgs.runNumber))
    exit(1)

#Create param card
build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),param_card_new='param_card_new.dat',
                 masses={'25': '1.250000e+02'},params=parameters)

#Create run card.
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#Create MadSpin decay card.
madspin_card_loc='madspin_card.dat'

#NOTE: Can't set random seed in due to crashes when using "set spinmode none".
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
 set spinmode none
#
# specify the decay for the final state particles
define l- = e- mu- ta-
define l+ = e+ mu+ ta+
decay x0 > w+ w- > l+ vl l- vl~
# running the actual code
launch""")
mscard.close()

print_cards()

#Run the event generation!
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runArgs.inputGeneratorFile,lhe_version=3,saveProcDir=True)  

######################################################################
# End of event generation, start configuring parton shower here.
######################################################################

print "Now performing parton showering ..."
   
#### Shower 
environ['ATHENA_PROC_NUMBER'] = '1'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# DF Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter,MultiElectronFilter,MultiMuonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")
filtSeq += MultiElectronFilter("Multi1ElectronFilter")
filtSeq += MultiMuonFilter("Multi1MuonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 18000.
Multi1TLeptonFilter.Etacut = 2.7
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 8000.
Multi2LLeptonFilter.Etacut = 2.7
Multi2LLeptonFilter.NLeptons = 2

Multi1ElectronFilter = filtSeq.Multi1ElectronFilter
Multi1ElectronFilter.Ptcut  = 8000.
Multi1ElectronFilter.Etacut = 2.7
Multi1ElectronFilter.NElectrons = 1

Multi1MuonFilter = filtSeq.Multi1MuonFilter
Multi1MuonFilter.Ptcut  = 8000.
Multi1MuonFilter.Etacut = 2.7
Multi1MuonFilter.NMuons = 1

filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter) and (Multi1ElectronFilter) and (Multi1MuonFilter)"

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "VBF 125 GeV Higgs production in the Higgs Characterization model decaying to wwlnulnu."
evgenConfig.keywords = ['BSM','Higgs','mH125','BSMHiggs','2lepton','dijet','resonance','WW']

evgenConfig.contact = ['Adam Kaluza <Adam.Kaluza@cern.ch>']
evgenConfig.inputfilecheck = runName
