#--------------------------------------------------------------
# on-the-fly generation of H+ MG5 events, montoya@cern.ch
#--------------------------------------------------------------
def build_madfks_inc(madfks_card_old=None,madfks_card_new='madfks_mcatnlo.inc.new'):
    """Build a new madfks_card.dat from an existing one.
    Params should be a dictionary of dictionaries. The first key is the block name, and the second in the param name.
    Eventually madfkss will replace the other arguments, but we need to keep them for backward compatibility for now."""
    # Grab the old madfks card and move it into place
    madfkscard = subprocess.Popen(['get_files','-data',madfks_card_old])
    madfkscard.wait()
    if not os.access(madfks_card_old,os.R_OK):
        mglog.info('Could not get madfks card '+madfks_card_old)
    if os.access(madfks_card_new,os.R_OK):
        mglog.error('Old madfks card at'+str(madfks_card_new)+' in the current directory. Dont want to clobber it. Please move it first.')
        return -1

    oldcard = open(madfks_card_old,'r')
    newcard = open(madfks_card_new,'w')
    lowfrac=0.025
    highfrac=0.25
    for line in oldcard:
        print "line ",line
        print "T/F ",line.find("frac_low=")
        if line.find("frac_low=")!= -1 :
            newcard.write('      parameter (frac_low=%1.3fd0) \n' % (lowfrac))
        elif line.find("frac_upp=")!= -1 :
            newcard.write('      parameter (frac_upp=%1.2fd0) \n' % (highfrac))
        else:
            newcard.write(line)
	

    oldcard.close()
    newcard.close()
    return newcard		

def fix_dynamic_scale(run_card_old=None,run_card_new='run_card.dat'):
    oldcard = open(run_card_old,'r')
    newcard = open(run_card_new,'w')
    for line in oldcard:
        print "line", line
        if line.find("dynamical_scale_choice") != -1 : 
            newcard.write(' 0 = dynamical_scale_choice ! 0 for fixed\n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    return newcard
	

from MadGraphControl.MadGraphUtils import *
#
nevents=10000
mode=0

do_newtheory=1
### DSID lists (extensions can include systematics samples) -> To be filled
#341524-341540
decay_taunu=[341003]
for x in xrange(341524,341541): decay_taunu.append(x)

#341541-341558
decay_wz   =[999999]

#341541-341558 >= 1 lepton filtered tb
#344836-344853 >= 2 lepton filtered tb
decay_tb   =[999999]
for x in xrange(341541,341559): decay_tb.append(x)
for x in xrange(344099,344103): decay_tb.append(x)
for x in xrange(344836,344854): decay_tb.append(x)

# adjust nevents due to filters
if runArgs.runNumber in decay_tb:
    nevents=30000

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in decay_taunu or runArgs.runNumber in decay_tb or runArgs.runNumber in decay_wz:
    fcard.write("""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme False
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    import model 2HDMtypeII                     
    generate p p > t~ h+ b [QCD]               
    output -f""")
    fcard.close()
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#--------------------------------------------------------------
# Charge Higgs, and all other masses in GeV
#--------------------------------------------------------------
###
mhc_str = str(runArgs.jobConfig[0])  # JOB OPTION NAME MUST CONTAIN THE MASS WE WANT TO SIMULATE IN FORMAT LIKE: *_H400_*
mhc=0
int(s) 
for s in mhc_str.split("_"):
    ss=s.replace("H","")  
    if ss.isdigit():
        mhc = int(ss)        
if mhc==0:
   raise RuntimeError("Charged Higgs mass not set, mhc=0, check joOption name %s"%mhc_str)
###
import math
mh1=1.250e+02                 
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2)) 
mh3=mh2

#--------------------------------------------------------------
# Scale -> fixed at (mt+mhc)/3
#--------------------------------------------------------------
if do_newtheory:
    scale=(mhc/3)
else:
    scale=(mhc+1.725e+02)/3

#Fetch default NLO run_card.dat and set parameters. using nn23nlo instead of cteq6l1

extras = { 'lhe_version':'3.0',                        # These two are not present in NLO cards, 
           'cut_decays':'F',                           # lhe version fixed by arrange_output function   
           #'pdlabel':"'lhapdf'",
           #'lhaid':' 244800', 		                          # lhe version fixed by arrange_output function   
           'pdlabel':"'nn23nlo'",
           'parton_shower':'PYTHIA8',
           'fixed_ren_scale':'T',
           'fixed_fac_scale':'T',
          # 'scale':str(scale), 
          # 'dsqrt_q2fact1':str(scale), 
          # 'dsqrt_q2fact2':str(scale), 
           'muR_ref_fixed':str(scale), 
           'muF1_ref_fixed':str(scale), 
           'muF2_ref_fixed':str(scale)
           #'PDF_set_min':'244801',
           #'PDF_set_max':'244900'
           }
process_dir = new_process()

build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
#build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.tmp.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
#print "Haleh \n"
#fix_dynamic_scale(run_card_old='run_card.tmp.dat',run_card_new='run_card.dat')

#Build param_card.dat from UFO, and set Higgs masses
#write_param_card is part of the 2HDMTypeII model
import os
import sys
sys.path.append( os.environ['MADPATH']+'/models/2HDMtypeII/' )
from write_param_card import ParamCardWriter
ParamCardWriter('param_card.TMP.dat')
masses = {'25':str(mh1)+'  #  mh1',
          '35':str(mh2)+'  #  mh2',
          '36':str(mh2)+'  #  mh2',
          '37':str(mhc)+'  #  mhc'}

decayss={'6': 'DECAY 6 1.508336e+00',
'37': 'DECAY 37 1.300000e+02 # whc'}
build_param_card(param_card_old='param_card.TMP.dat',param_card_new='param_card.dat',masses=masses,decays=decayss) 

print_cards()

runName='run_01'     


if do_newtheory:
    #copy file to running dir 
    if os.access(os.environ['PWD']+'/'+process_dir+'/SubProcesses/madfks_mcatnlo.inc',os.R_OK):
        shutil.copy(os.environ['PWD']+'/'+process_dir+'/SubProcesses/madfks_mcatnlo.inc','madfks_mcatnow.SM.inc')
    else:
        raise RuntimeError('Cannot find default NLO madfks_mcatnow.SM.inc!')
    #modify file

    build_madfks_inc(madfks_card_old='madfks_mcatnow.SM.inc', madfks_card_new='madfks_mcatnlo.inc.new')

    #copy it back
    if os.access(os.environ['PWD']+'/'+process_dir+'/SubProcesses/madfks_mcatnlo.inc',os.R_OK):
        shutil.copy('madfks_mcatnlo.inc.new',os.environ['PWD']+'/'+process_dir+'/SubProcesses/madfks_mcatnlo.inc')
    else:
        raise RuntimeError('Cannot find default NLO madfks_mcatnow.inc.new!')


generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,lhe_version=3,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')                                                                          

#### Shower     
#
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

evgenConfig.description = 'aMcAtNlo High mass charged Higgs NLO4FS'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['g.carrillo.montoya@cern.ch']


evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

# JOB OPTION NAME MUST CONTAIN THE DECAY MODE WE WANT TO SIMULATE IN FORMAT LIKE: *_taunu* or *_tb*
# Also double check the RunNumber associated with that decay is in the appropianted list
dotaunu=False
dotb=False
dowz=False
dodil=False
for s in mhc_str.split("_"):
    ss=s.replace(".py","")  
    print ss
    if ss=='taunu' and runArgs.runNumber in decay_taunu:
        dotaunu=True
    if ss=='tb' and runArgs.runNumber in decay_tb:
        dotb=True
    if ss=='wz' and runArgs.runNumber in decay_wz:
        dowz=True
    if ss=='dil':
        dodil=True

if dotaunu==False and dotb==False:
    raise RuntimeError("No decay mode was identified, check jobOption name %s, and/or runNumber %i."%(runArgs.jobConfig[0],runArgs.runNumber))

if dotaunu:
    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                                "37:onMode = off",                   # turn off all mhc decays
                                "37:onIfMatch = 15 16"]              # switch on H+ to taunu
    evgenConfig.keywords+=['tau','neutrino']

if dotb:
    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                                "37:onMode = off",                   # turn off all mhc decays
                                "37:onIfMatch = 5 6"]                # switch on H+ to tb
    evgenConfig.keywords+=['top','bottom']
    include('MC15JobOptions/TTbarWToLeptonFilter.py')                # lep filter   
    filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 
    filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


if dowz:
    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                                "37:onMode = off",                   # turn off all mhc decays
                                "37:onIfMatch = 23 24"]                # switch on H+ to wz

if dodil:
    include('MC15JobOptions/TTbarWToLeptonFilter.py')                # lep filter   
    filtSeq.TTbarWToLeptonFilter.NumLeptons = 2 
    filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
    evgenConfig.minevents=2000
