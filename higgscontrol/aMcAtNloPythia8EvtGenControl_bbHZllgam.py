evgenConfig.minevents = 5000

import math
import re

#parse channel, component and mass from joboption name
jo_name = runArgs.jobConfig[0]

if runArgs.maxEvents > 0:
    nEvents = 1.1*runArgs.maxEvents
else:
    nEvents = 1.1*evgenConfig.minevents

nLHEvents = nEvents

include("MC15JobOptions/aMcAtNloControl_bbH4FS.py")
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["25:onMode = off", # decay of Higgs
                            "25:onIfMatch = 22 23",
                            "23:onMode = off",
                            "23:mMin = 2.0",
                            "23:onIfAny = 11 13 15"]


