model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 330.
mZp = 300.
mHD = 125.
widthZp = 1.193662e+00
widthN2 = 1.977683e-02
filteff = 6.896552e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
