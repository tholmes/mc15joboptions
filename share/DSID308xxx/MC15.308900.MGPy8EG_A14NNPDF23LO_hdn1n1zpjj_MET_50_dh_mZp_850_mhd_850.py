model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 850.
mHD = 850.
widthZp = 4.051237e+00
widthhd = 3.381808e-01
filteff = 9.881423e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
