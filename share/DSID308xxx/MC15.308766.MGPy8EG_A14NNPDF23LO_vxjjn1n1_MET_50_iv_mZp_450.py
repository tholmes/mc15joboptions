model="InelasticVectorEFT"
mDM1 = 225.
mDM2 = 900.
mZp = 450.
mHD = 125.
widthZp = 2.088811e+00
widthN2 = 3.323737e-01
filteff = 9.803922e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
