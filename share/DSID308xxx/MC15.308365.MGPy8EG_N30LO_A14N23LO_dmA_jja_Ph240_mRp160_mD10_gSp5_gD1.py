model  = 'dmA'
mR     = 160
mDM    = 10000
gSM    = 0.50
gDM    = 1.00
widthR = 15.89809
phminpt= 240.000000
filteff = 0.075504

pta  = 100.0 # Matrix element-level photon pT cut
etaa =   3.0 # Matrix element-level photon eta cut

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma_boosted.py")
