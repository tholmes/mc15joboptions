model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 650.
mHD = 125.
widthZp = 3.086608e+00
widthhd = 1.340549e+00
filteff = 8.742787e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
