model="LightVector"
mDM1 = 250.
mDM2 = 1000.
mZp = 500.
mHD = 125.
widthZp = 2.346527e+00
widthN2 = 3.284799e+01
filteff = 9.940358e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
