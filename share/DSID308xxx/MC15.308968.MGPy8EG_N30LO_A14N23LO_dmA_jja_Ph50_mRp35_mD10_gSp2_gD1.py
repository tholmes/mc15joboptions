model       = 'dmA'
mR          = 350
mDM         = 10000
gSM         = 0.20
gDM         = 1.00
widthR      = 5.572384
phminpt     = 50
filteff     = 0.21600

evgenConfig.description = "Zprime sample - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>", "Chase Shimmin <cshimmin@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")
