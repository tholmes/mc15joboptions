model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 190.
mZp = 160.
mHD = 125.
widthZp = 6.366192e-01
widthN2 = 3.713497e-03
filteff = 4.955401e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
