model="LightVector"
mDM1 = 225.
mDM2 = 900.
mZp = 450.
mHD = 125.
widthZp = 2.088811e+00
widthN2 = 2.956319e+01
filteff = 9.689922e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
