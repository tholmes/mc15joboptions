model="LightVector"
mDM1 = 35.
mDM2 = 140.
mZp = 70.
mHD = 125.
widthZp = 2.785143e-01
widthN2 = 4.598718e+00
filteff = 5.458515e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
