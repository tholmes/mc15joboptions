include('MC15JobOptions/MadGraphControl_Wprime_tb.py')
evgenConfig.description    = "MadGraph5+Pythia8+EvtGen for Wprime->tb"
evgenConfig.keywords       = ["Wprime", "top", "singleTop", "sChannel", "BSMtop", "exotic", "BSM"]
