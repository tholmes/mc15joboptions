model="InelasticVectorEFT"
mDM1 = 300.
mDM2 = 1200.
mZp = 600.
mHD = 125.
widthZp = 2.842817e+00
widthN2 = 7.874134e-01
filteff = 9.920635e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
