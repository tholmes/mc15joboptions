model="LightVector"
mDM1 = 5.
mDM2 = 780.
mZp = 750.
mHD = 125.
widthZp = 3.570290e+00
widthN2 = 2.239132e-01
filteff = 8.196721e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
