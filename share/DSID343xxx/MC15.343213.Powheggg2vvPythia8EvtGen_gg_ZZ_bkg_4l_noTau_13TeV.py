######################################################################
#
# gg2VV 3.1.6/Powheg/Pythia8 gg -> ZZ, with ZZ -> 4l (background only)
#
# generator cuts on LHE level:
# m(12) > 4GeV ; m(34) > 4GeV ; m(14) > 4GeV ; m(32) > 4GeV
# m(1234) > 100 GeV
# pt(12) > 2GeV ; pt(34) > 2GeV
# NO taus
#

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'Powheg', 'gg2vv', 'Pythia8' ]
evgenConfig.description = 'gg2VV, ZZ-> 4l using CT10 PDF, PowhegPythia8 , m(l+l-)>4GeV (all possible permutations), pt(l+l-)>1GeV , m(4l)>100GeV , bkg only , no taus'
evgenConfig.keywords = ['diboson', '4lepton', 'electroweak', 'Higgs', 'ZZ']
evgenConfig.contact = ['jochen.meyer@cern.ch']
evgenConfig.inputfilecheck = 'gg2VV0316.343213.gg_ZZ_bkg_4l_noTau_13TeV'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]
