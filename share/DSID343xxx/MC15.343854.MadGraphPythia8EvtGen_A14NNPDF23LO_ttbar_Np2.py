include('MC15JobOptions/MadGraphControl_WWbb_LO_Pythia8_A14_CKKWLkTMerge_highMjj.py')

evgenConfig.generators = [ "MadGraph", "Pythia8", "EvtGen" ]
evgenConfig.description = "MadGraph ttbar with dilepton filter and high Mjj filter - Np2"

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

#--------------------------------------------------------------
# VBF Mjj Interval filter
#--------------------------------------------------------------
include("MC15JobOptions/VBFMjjIntervalFilter.py")
filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0
filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 15.*GeV
filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 15.*GeV
filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
filtSeq.VBFMjjIntervalFilter.NoJetProbability  = 0.0000252
filtSeq.VBFMjjIntervalFilter.OneJetProbability = 0.0000252
filtSeq.VBFMjjIntervalFilter.LowMjjProbability = 0.0000252
filtSeq.VBFMjjIntervalFilter.HighMjjProbability = 1.0
filtSeq.VBFMjjIntervalFilter.LowMjj = 100.*GeV
filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = False
filtSeq.VBFMjjIntervalFilter.HighMjj = 2500.*GeV
filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = True
