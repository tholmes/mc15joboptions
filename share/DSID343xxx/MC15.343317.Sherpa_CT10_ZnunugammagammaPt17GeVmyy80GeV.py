include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "nu nubar gamma gamma production with up to two jets in ME+PS."
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.keywords = [ "electroweak", "triboson", "diphoton", "neutrino", "SM" ]
evgenConfig.inputconfcheck = "ZnunugammagammaPt17GeVmyy80GeV"
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)
(model){
  MODEL         = SM
}(model)
(processes){
  Process 93 93 -> 91 91 22 22 93{2}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  End process;
}(processes)
(selector){
  Mass 12 -12 10 E_CMS
  Mass 14 -14 10 E_CMS
  Mass 16 -16 10 E_CMS
  PT 22 17 E_CMS
  DeltaR 22 90 0.2 1000
  DeltaR 22 22 0.2 1000
  DeltaR 93 90 0.2 1000
  IsolationCut  22  0.3  2  0.025
  Mass 22 -22 80.0 E_CMS
}(selector)
"""
