evgenConfig.description = 'MadGraph5_aMC@NLO+Pythia8 bbH production'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'MSSM', 'diphoton' ]
evgenConfig.contact     = [ 'lorenz.hauswald@cern.ch', 'niklaos.rompotis@cern.ch', 'samina.jabbar@cern.ch' ]

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbHgamgam.py")
evgenConfig.generators  = [ "aMcAtNlo", "Pythia8", "EvtGen"] 
