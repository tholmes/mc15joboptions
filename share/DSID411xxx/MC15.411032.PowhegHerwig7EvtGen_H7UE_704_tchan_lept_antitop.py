#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark t-channel (2->3) production (antitop),MadSpin, A14 tune, ME NNPDF3.04f NLO, H7UE'
evgenConfig.keywords    = ['SM', 'top', 'lepton']
evgenConfig.contact     = ['dominic.hirschbuehl@cern.ch' ]
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = "TXT"
evgenConfig.generators += ['Powheg','Herwig7']
#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118_nf_4")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune        = "MMHT2014"

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
""")

# run Herwig7
Herwig7Config.run()

