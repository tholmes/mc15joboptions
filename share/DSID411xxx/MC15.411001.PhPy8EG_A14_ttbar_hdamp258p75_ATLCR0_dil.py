evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, dilepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files + ATLAS CR0 settings '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch','deepak.kar@cern.ch']
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


#--------------------------------------------------------------
# Colour reconnection model
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["ColourReconnection:mode=0",
                            "MultipartonInteractions:pT0Ref = 2.15",
                            "MultipartonInteractions:expPow = 1.81",
                            "ColourReconnection:range = 2.92"
                           ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
