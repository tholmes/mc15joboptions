#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17311919E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03939641E+01   # W+
        25     1.24886598E+02   # h
        35     2.99998608E+03   # H
        36     2.99999995E+03   # A
        37     3.00109096E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04339325E+03   # ~d_L
   2000001     3.03820594E+03   # ~d_R
   1000002     3.04247953E+03   # ~u_L
   2000002     3.03938314E+03   # ~u_R
   1000003     3.04339325E+03   # ~s_L
   2000003     3.03820594E+03   # ~s_R
   1000004     3.04247953E+03   # ~c_L
   2000004     3.03938314E+03   # ~c_R
   1000005     1.09783041E+03   # ~b_1
   2000005     3.03529493E+03   # ~b_2
   1000006     1.09582974E+03   # ~t_1
   2000006     3.02479545E+03   # ~t_2
   1000011     3.00641381E+03   # ~e_L
   2000011     3.00198845E+03   # ~e_R
   1000012     3.00501593E+03   # ~nu_eL
   1000013     3.00641381E+03   # ~mu_L
   2000013     3.00198845E+03   # ~mu_R
   1000014     3.00501593E+03   # ~nu_muL
   1000015     2.98592986E+03   # ~tau_1
   2000015     3.02175362E+03   # ~tau_2
   1000016     3.00480266E+03   # ~nu_tauL
   1000021     2.35418405E+03   # ~g
   1000022     1.00685670E+02   # ~chi_10
   1000023     2.17573159E+02   # ~chi_20
   1000025    -2.99407513E+03   # ~chi_30
   1000035     2.99413325E+03   # ~chi_40
   1000024     2.17733192E+02   # ~chi_1+
   1000037     2.99504119E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891816E-01   # N_11
  1  2     7.63729350E-04   # N_12
  1  3    -1.46871523E-02   # N_13
  1  4     2.45472522E-04   # N_14
  2  1    -3.81808225E-04   # N_21
  2  2     9.99661873E-01   # N_22
  2  3     2.59962467E-02   # N_23
  2  4     4.34436566E-04   # N_24
  3  1     1.02223770E-02   # N_31
  3  2    -1.86832260E-02   # N_32
  3  3     7.06779563E-01   # N_33
  3  4     7.07113209E-01   # N_34
  4  1    -1.05694471E-02   # N_41
  4  2     1.80691211E-02   # N_42
  4  3    -7.06803461E-01   # N_43
  4  4     7.07100178E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99324718E-01   # U_11
  1  2     3.67438097E-02   # U_12
  2  1    -3.67438097E-02   # U_21
  2  2     9.99324718E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999812E-01   # V_11
  1  2    -6.12801085E-04   # V_12
  2  1    -6.12801085E-04   # V_21
  2  2    -9.99999812E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98415844E-01   # cos(theta_t)
  1  2    -5.62654641E-02   # sin(theta_t)
  2  1     5.62654641E-02   # -sin(theta_t)
  2  2     9.98415844E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99694330E-01   # cos(theta_b)
  1  2    -2.47234012E-02   # sin(theta_b)
  2  1     2.47234012E-02   # -sin(theta_b)
  2  2     9.99694330E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06920110E-01   # cos(theta_tau)
  1  2     7.07293403E-01   # sin(theta_tau)
  2  1    -7.07293403E-01   # -sin(theta_tau)
  2  2    -7.06920110E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00197967E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73119190E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43806115E+02   # higgs               
         4     1.00451379E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73119190E+03  # The gauge couplings
     1     3.62627682E-01   # gprime(Q) DRbar
     2     6.39923920E-01   # g(Q) DRbar
     3     1.02309794E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73119190E+03  # The trilinear couplings
  1  1     2.83724318E-06   # A_u(Q) DRbar
  2  2     2.83728746E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73119190E+03  # The trilinear couplings
  1  1     9.78116596E-07   # A_d(Q) DRbar
  2  2     9.78238691E-07   # A_s(Q) DRbar
  3  3     2.05078703E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73119190E+03  # The trilinear couplings
  1  1     4.46789438E-07   # A_e(Q) DRbar
  2  2     4.46811874E-07   # A_mu(Q) DRbar
  3  3     4.53217302E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73119190E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49314331E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73119190E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.83135347E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73119190E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03418579E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73119190E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.28635106E+04   # M^2_Hd              
        22    -9.01738102E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42172935E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.87942032E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47862259E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47862259E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52137741E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52137741E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.25152344E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.26963098E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.18599692E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.68703998E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10670900E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.98966745E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.98362472E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.00328351E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62219961E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.03065245E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.64705609E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59407411E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11864150E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.23790830E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.29893510E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.39680946E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.47329703E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.30871893E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.15039530E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.97224658E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.51188673E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.41234941E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.71895647E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.50722589E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.97771185E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01124849E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.46739632E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.05669455E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99812981E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65474471E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.33995381E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.90400652E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.31215925E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.37428586E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.97311372E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15559081E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.61488887E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.33790038E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.69719946E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.80988946E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.38511054E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.06166233E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.89956880E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65457822E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.37602981E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.91806096E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30640165E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.12207791E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.98001769E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64277008E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.60373147E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.66485653E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.60096458E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.90612915E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.53962669E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.05669455E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99812981E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65474471E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.33995381E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.90400652E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.31215925E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.37428586E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.97311372E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15559081E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.61488887E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.33790038E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.69719946E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.80988946E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.38511054E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.06166233E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.89956880E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65457822E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.37602981E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.91806096E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30640165E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.12207791E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.98001769E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64277008E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.60373147E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.66485653E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.60096458E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.90612915E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.53962669E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02420414E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.77444382E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00803414E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.39296520E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.95162284E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01452095E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.68557708E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56682151E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999849E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.44606406E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.90395027E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.05911204E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02420414E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.77444382E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00803414E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.39296520E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.95162284E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01452095E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.68557708E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56682151E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999849E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.44606406E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.90395027E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.05911204E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82372132E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.46843654E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17778664E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35377681E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76510554E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54943119E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15056653E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.71116537E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49728146E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.29950827E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.73161593E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02450006E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.71663101E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00898883E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.62617014E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.20225443E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01934788E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.00343108E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02450006E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.71663101E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00898883E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.62617014E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.20225443E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01934788E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.00343108E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02455271E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.71581121E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00873142E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.25573178E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.84625059E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01966190E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.53761549E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16402384E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.41075977E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.32231925E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.39479854E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.11371916E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.86021046E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.17337829E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.68234400E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.11286820E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.87796342E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.40933342E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.14635603E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.31662723E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.68796464E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.68796464E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.71436016E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.78084164E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.26852795E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.26852795E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.51910168E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.51910168E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.37596954E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.37596954E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.44663177E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.06798510E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.53370918E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.65787824E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.65787824E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.35183152E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.52768086E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.16099502E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.16099502E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.49155279E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.49155279E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.54480790E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.54480790E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.62275219E-03   # h decays
#          BR         NDA      ID1       ID2
     5.67435637E-01    2           5        -5   # BR(h -> b       bb     )
     7.15254129E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53202638E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.37519108E-04    2           3        -3   # BR(h -> s       sb     )
     2.33516228E-02    2           4        -4   # BR(h -> c       cb     )
     7.57021965E-02    2          21        21   # BR(h -> g       g      )
     2.58634286E-03    2          22        22   # BR(h -> gam     gam    )
     1.70702958E-03    2          22        23   # BR(h -> Z       gam    )
     2.28417803E-01    2          24       -24   # BR(h -> W+      W-     )
     2.84832340E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.40318973E+01   # H decays
#          BR         NDA      ID1       ID2
     8.93625720E-01    2           5        -5   # BR(H -> b       bb     )
     7.30692058E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.58355251E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.17262415E-04    2           3        -3   # BR(H -> s       sb     )
     8.89462057E-08    2           4        -4   # BR(H -> c       cb     )
     8.87294736E-03    2           6        -6   # BR(H -> t       tb     )
     1.49405179E-05    2          21        21   # BR(H -> g       g      )
     6.98846336E-08    2          22        22   # BR(H -> gam     gam    )
     3.62136519E-09    2          23        22   # BR(H -> Z       gam    )
     9.77326702E-07    2          24       -24   # BR(H -> W+      W-     )
     4.88060424E-07    2          23        23   # BR(H -> Z       Z      )
     6.29932069E-06    2          25        25   # BR(H -> h       h      )
     9.77151035E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.55805695E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.89013865E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.99286792E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20175074E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.10226592E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.32870961E+01   # A decays
#          BR         NDA      ID1       ID2
     9.13668944E-01    2           5        -5   # BR(A -> b       bb     )
     7.47050438E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.64138814E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.24417312E-04    2           3        -3   # BR(A -> s       sb     )
     9.15459670E-08    2           4        -4   # BR(A -> c       cb     )
     9.12727163E-03    2           6        -6   # BR(A -> t       tb     )
     2.68789414E-05    2          21        21   # BR(A -> g       g      )
     6.35589236E-08    2          22        22   # BR(A -> gam     gam    )
     2.64455720E-08    2          23        22   # BR(A -> Z       gam    )
     9.95535982E-07    2          23        25   # BR(A -> Z       h      )
     1.01713043E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.69686941E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.09000805E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     3.09028438E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.64006230E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.45852026E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.83399928E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.41633546E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.33451815E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.42001857E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.92143482E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.18442360E-01    2           6        -5   # BR(H+ -> t       bb     )
     9.11947482E-07    2          24        25   # BR(H+ -> W+      h      )
     5.58728423E-14    2          24        36   # BR(H+ -> W+      A      )
     5.63556583E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.24959755E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06373471E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
