#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13088415E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.77990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04176669E+01   # W+
        25     1.25662380E+02   # h
        35     4.00001283E+03   # H
        36     3.99999748E+03   # A
        37     4.00101932E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02610666E+03   # ~d_L
   2000001     4.02267509E+03   # ~d_R
   1000002     4.02545367E+03   # ~u_L
   2000002     4.02361364E+03   # ~u_R
   1000003     4.02610666E+03   # ~s_L
   2000003     4.02267509E+03   # ~s_R
   1000004     4.02545367E+03   # ~c_L
   2000004     4.02361364E+03   # ~c_R
   1000005     8.81825576E+02   # ~b_1
   2000005     4.02553029E+03   # ~b_2
   1000006     8.67800861E+02   # ~t_1
   2000006     2.14870804E+03   # ~t_2
   1000011     4.00468296E+03   # ~e_L
   2000011     4.00318492E+03   # ~e_R
   1000012     4.00356906E+03   # ~nu_eL
   1000013     4.00468296E+03   # ~mu_L
   2000013     4.00318492E+03   # ~mu_R
   1000014     4.00356906E+03   # ~nu_muL
   1000015     4.00393601E+03   # ~tau_1
   2000015     4.00814630E+03   # ~tau_2
   1000016     4.00497423E+03   # ~nu_tauL
   1000021     1.98060202E+03   # ~g
   1000022     4.45628525E+02   # ~chi_10
   1000023    -4.89742559E+02   # ~chi_20
   1000025     5.07196904E+02   # ~chi_30
   1000035     2.05219272E+03   # ~chi_40
   1000024     4.87581040E+02   # ~chi_1+
   1000037     2.05235727E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.89433271E-01   # N_11
  1  2    -1.93168536E-02   # N_12
  1  3    -4.54113520E-01   # N_13
  1  4    -4.12556517E-01   # N_14
  2  1    -3.48124402E-02   # N_21
  2  2     2.31914771E-02   # N_22
  2  3    -7.04836707E-01   # N_23
  2  4     7.08135203E-01   # N_24
  3  1     6.12847367E-01   # N_31
  3  2     2.80652330E-02   # N_32
  3  3     5.44909575E-01   # N_33
  3  4     5.71580267E-01   # N_34
  4  1    -1.14395730E-03   # N_41
  4  2     9.99150318E-01   # N_42
  4  3    -7.72541839E-03   # N_43
  4  4    -4.04679135E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.09314788E-02   # U_11
  1  2     9.99940250E-01   # U_12
  2  1    -9.99940250E-01   # U_21
  2  2     1.09314788E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.72392982E-02   # V_11
  1  2    -9.98360487E-01   # V_12
  2  1    -9.98360487E-01   # V_21
  2  2    -5.72392982E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94927089E-01   # cos(theta_t)
  1  2    -1.00598646E-01   # sin(theta_t)
  2  1     1.00598646E-01   # -sin(theta_t)
  2  2     9.94927089E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998819E-01   # cos(theta_b)
  1  2    -1.53687950E-03   # sin(theta_b)
  2  1     1.53687950E-03   # -sin(theta_b)
  2  2     9.99998819E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06211944E-01   # cos(theta_tau)
  1  2     7.08000487E-01   # sin(theta_tau)
  2  1    -7.08000487E-01   # -sin(theta_tau)
  2  2    -7.06211944E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00245063E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30884153E+03  # DRbar Higgs Parameters
         1    -4.77990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43708570E+02   # higgs               
         4     1.61836508E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30884153E+03  # The gauge couplings
     1     3.61879342E-01   # gprime(Q) DRbar
     2     6.35848134E-01   # g(Q) DRbar
     3     1.03002380E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30884153E+03  # The trilinear couplings
  1  1     1.38615811E-06   # A_u(Q) DRbar
  2  2     1.38617121E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30884153E+03  # The trilinear couplings
  1  1     5.12058313E-07   # A_d(Q) DRbar
  2  2     5.12105649E-07   # A_s(Q) DRbar
  3  3     9.14681390E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30884153E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13138905E-07   # A_mu(Q) DRbar
  3  3     1.14288361E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30884153E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66484075E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30884153E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86584347E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30884153E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02935080E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30884153E+03  # The soft SUSY breaking masses at the scale Q
         1     4.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56643994E+07   # M^2_Hd              
        22    -2.16054993E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40264059E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.46476635E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43740572E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43740572E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56259428E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56259428E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.72120122E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.89914862E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.15715759E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.79552042E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.14817336E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.07730821E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.84198254E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.24697037E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.10239965E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.47053092E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.95519839E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.76753107E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.00313909E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.02021255E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.10892980E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.59765015E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.48169740E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.23968208E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.76809704E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66904676E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57125319E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81312159E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.57255134E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.03933814E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63562469E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.05052585E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12899885E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.50114858E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.42082078E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.74464823E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.85489670E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78747478E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45010433E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.33293408E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76251788E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79666717E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.55757812E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58126397E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52428979E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60954594E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.39461587E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.56817928E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.03121523E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.01831330E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45675967E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78768048E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.43638307E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.52815959E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.58221697E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80215484E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.02712536E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61425000E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52645794E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54315347E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.84946233E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.71226200E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.29518000E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.04721153E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85838225E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78747478E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45010433E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.33293408E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76251788E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79666717E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.55757812E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58126397E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52428979E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60954594E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.39461587E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.56817928E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.03121523E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.01831330E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45675967E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78768048E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.43638307E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.52815959E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.58221697E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80215484E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.02712536E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61425000E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52645794E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54315347E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.84946233E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.71226200E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.29518000E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.04721153E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85838225E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13646494E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.25961404E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.68789388E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.06050573E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78376552E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.19110589E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58297452E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02885549E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.24945983E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20896317E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73844323E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.31194327E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13646494E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.25961404E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.68789388E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.06050573E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78376552E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.19110589E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58297452E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02885549E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.24945983E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20896317E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73844323E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.31194327E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06697759E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25175495E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49883704E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.50628778E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40925796E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56468036E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82634757E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05798408E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.26946254E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.91761668E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.31338534E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44424853E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.84696583E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89644533E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13751676E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09914543E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.20862978E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.11065975E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78821347E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.26367898E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55972971E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13751676E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09914543E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.20862978E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.11065975E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78821347E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.26367898E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55972971E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45839941E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97530332E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.35733050E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.63820794E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53166643E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.50416377E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04820874E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.08078961E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33657676E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33657676E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11220677E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11220677E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10243293E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.41860397E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.64487037E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.49994114E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.25397725E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.20013238E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.07062162E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.86301191E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.16414173E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.25108612E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.27488772E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18436671E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53641798E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18436671E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53641798E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38349094E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52380074E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52380074E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48436499E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04484132E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04484132E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04484119E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.95116674E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.95116674E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.95116674E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.95116674E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.83722980E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.83722980E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.83722980E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.83722980E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.22962164E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.22962164E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.55782184E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.19074849E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.67447009E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.40178038E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.20631861E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.40178038E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.20631861E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.14986462E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.66311055E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.66311055E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.67576301E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.58265217E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.58265217E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.58262190E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.74444053E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.26302662E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.74444053E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.26302662E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.09098764E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.19057146E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.19057146E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.89375734E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.03759993E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.03759993E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.03759994E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.06726627E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.06726627E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.06726627E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.06726627E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.55751115E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.55751115E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.55751115E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.55751115E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.41664445E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.41664445E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.41702777E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.70635277E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.96634314E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.51866787E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.07020679E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.07020679E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.46598504E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.25934279E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.41962536E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.78147830E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78147830E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.79147105E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.79147105E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02197064E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88762221E-01    2           5        -5   # BR(h -> b       bb     )
     6.48392165E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29529830E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86676266E-04    2           3        -3   # BR(h -> s       sb     )
     2.11388448E-02    2           4        -4   # BR(h -> c       cb     )
     6.91925939E-02    2          21        21   # BR(h -> g       g      )
     2.39508444E-03    2          22        22   # BR(h -> gam     gam    )
     1.64874909E-03    2          22        23   # BR(h -> Z       gam    )
     2.23052282E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82548028E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53534541E+01   # H decays
#          BR         NDA      ID1       ID2
     3.65193537E-01    2           5        -5   # BR(H -> b       bb     )
     5.98988056E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11787669E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50508251E-04    2           3        -3   # BR(H -> s       sb     )
     7.02563892E-08    2           4        -4   # BR(H -> c       cb     )
     7.04115307E-03    2           6        -6   # BR(H -> t       tb     )
     8.46607704E-07    2          21        21   # BR(H -> g       g      )
     2.74467114E-09    2          22        22   # BR(H -> gam     gam    )
     1.81232855E-09    2          23        22   # BR(H -> Z       gam    )
     1.65431061E-06    2          24       -24   # BR(H -> W+      W-     )
     8.26582266E-07    2          23        23   # BR(H -> Z       Z      )
     6.72218250E-06    2          25        25   # BR(H -> h       h      )
    -3.70434795E-25    2          36        36   # BR(H -> A       A      )
     2.46407439E-20    2          23        36   # BR(H -> Z       A      )
     1.86360946E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54181944E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.54181944E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.48882651E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.36644886E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.63461034E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.25853890E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.44657358E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.95361325E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.49310477E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.26333517E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.25421868E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.26459037E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.49714660E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.49714660E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.39105124E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53485847E+01   # A decays
#          BR         NDA      ID1       ID2
     3.65251056E-01    2           5        -5   # BR(A -> b       bb     )
     5.99042890E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11806890E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50571891E-04    2           3        -3   # BR(A -> s       sb     )
     7.07229155E-08    2           4        -4   # BR(A -> c       cb     )
     7.05587995E-03    2           6        -6   # BR(A -> t       tb     )
     1.44980448E-05    2          21        21   # BR(A -> g       g      )
     6.72456983E-08    2          22        22   # BR(A -> gam     gam    )
     1.59385524E-08    2          23        22   # BR(A -> Z       gam    )
     1.65096430E-06    2          23        25   # BR(A -> Z       h      )
     1.90735408E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.54168606E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.54168606E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.18340292E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.68096170E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41602558E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67753959E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.85425762E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.40766895E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.64538425E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.10569367E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.94998144E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     9.89310470E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     9.89310470E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.56003663E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.88792617E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.96482513E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10901603E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.76826957E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19479735E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45808238E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.74639731E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.64532341E-06    2          24        25   # BR(H+ -> W+      h      )
     2.63674405E-14    2          24        36   # BR(H+ -> W+      A      )
     4.83622509E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.99390392E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.44504482E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.53697287E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.34639704E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.52994557E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.18512699E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.24329210E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.93351013E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
