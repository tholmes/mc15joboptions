#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17311959E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03958733E+01   # W+
        25     1.24826135E+02   # h
        35     2.99998902E+03   # H
        36     2.99999996E+03   # A
        37     3.00108797E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04337485E+03   # ~d_L
   2000001     3.03820542E+03   # ~d_R
   1000002     3.04246426E+03   # ~u_L
   2000002     3.03938376E+03   # ~u_R
   1000003     3.04337485E+03   # ~s_L
   2000003     3.03820542E+03   # ~s_R
   1000004     3.04246426E+03   # ~c_L
   2000004     3.03938376E+03   # ~c_R
   1000005     1.09826884E+03   # ~b_1
   2000005     3.03533291E+03   # ~b_2
   1000006     1.09624842E+03   # ~t_1
   2000006     3.02480719E+03   # ~t_2
   1000011     3.00639552E+03   # ~e_L
   2000011     3.00198641E+03   # ~e_R
   1000012     3.00500169E+03   # ~nu_eL
   1000013     3.00639552E+03   # ~mu_L
   2000013     3.00198641E+03   # ~mu_R
   1000014     3.00500169E+03   # ~nu_muL
   1000015     2.98596512E+03   # ~tau_1
   2000015     3.02170227E+03   # ~tau_2
   1000016     3.00478975E+03   # ~nu_tauL
   1000021     2.35418368E+03   # ~g
   1000022     1.50925101E+02   # ~chi_10
   1000023     3.22995523E+02   # ~chi_20
   1000025    -2.99412248E+03   # ~chi_30
   1000035     2.99415091E+03   # ~chi_40
   1000024     3.23157759E+02   # ~chi_1+
   1000037     2.99507131E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891806E-01   # N_11
  1  2     3.81560198E-04   # N_12
  1  3    -1.47047989E-02   # N_13
  1  4     3.31793890E-07   # N_14
  2  1     1.66796542E-06   # N_21
  2  2     9.99659717E-01   # N_22
  2  3     2.60526228E-02   # N_23
  2  4     1.30829819E-03   # N_24
  3  1     1.04010998E-02   # N_31
  3  2    -1.93447736E-02   # N_32
  3  3     7.06764366E-01   # N_33
  3  4     7.07108003E-01   # N_34
  4  1    -1.04016258E-02   # N_41
  4  2     1.74952836E-02   # N_42
  4  3    -7.06816214E-01   # N_43
  4  4     7.07104350E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321493E-01   # U_11
  1  2     3.68314165E-02   # U_12
  2  1    -3.68314165E-02   # U_21
  2  2     9.99321493E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998292E-01   # V_11
  1  2    -1.84833617E-03   # V_12
  2  1    -1.84833617E-03   # V_21
  2  2    -9.99998292E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98415961E-01   # cos(theta_t)
  1  2    -5.62633879E-02   # sin(theta_t)
  2  1     5.62633879E-02   # -sin(theta_t)
  2  2     9.98415961E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99698094E-01   # cos(theta_b)
  1  2    -2.45707316E-02   # sin(theta_b)
  2  1     2.45707316E-02   # -sin(theta_b)
  2  2     9.99698094E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06934994E-01   # cos(theta_tau)
  1  2     7.07278527E-01   # sin(theta_tau)
  2  1    -7.07278527E-01   # -sin(theta_tau)
  2  2    -7.06934994E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00187969E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73119588E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43795490E+02   # higgs               
         4     1.00532189E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73119588E+03  # The gauge couplings
     1     3.62632278E-01   # gprime(Q) DRbar
     2     6.38971908E-01   # g(Q) DRbar
     3     1.02309619E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73119588E+03  # The trilinear couplings
  1  1     2.83976488E-06   # A_u(Q) DRbar
  2  2     2.83980866E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73119588E+03  # The trilinear couplings
  1  1     9.83426852E-07   # A_d(Q) DRbar
  2  2     9.83547965E-07   # A_s(Q) DRbar
  3  3     2.04681699E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73119588E+03  # The trilinear couplings
  1  1     4.40553969E-07   # A_e(Q) DRbar
  2  2     4.40575955E-07   # A_mu(Q) DRbar
  3  3     4.46824145E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73119588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49318770E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73119588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.80782100E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73119588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02951284E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73119588E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.40538528E+04   # M^2_Hd              
        22    -9.01719718E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41741800E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.87681513E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47859574E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47859574E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52140426E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52140426E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.12784287E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.36433084E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.17109005E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.69247687E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10493057E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.98590013E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.14643757E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.32894190E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62261554E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.03558231E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.64952685E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59232952E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11287330E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.11435936E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.41944999E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.40512030E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.45293470E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.28396204E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.15796517E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.66194311E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.45947917E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.36975038E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.59362928E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.53819722E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.92390451E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.00094715E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.45136114E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.00557352E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.00368743E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64287391E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.75600513E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.59065929E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28790037E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.25331435E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.00918780E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15402345E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.61178114E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.41188134E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.75425509E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.75229501E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.38821851E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.01056177E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.95305032E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64222942E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.96834650E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.47104638E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28216746E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.15860684E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.01606582E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64235155E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.59365598E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.25739130E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.75389736E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.74842981E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54063431E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.00557352E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.00368743E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64287391E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.75600513E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.59065929E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28790037E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.25331435E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.00918780E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15402345E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.61178114E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.41188134E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.75425509E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.75229501E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.38821851E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.01056177E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.95305032E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64222942E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.96834650E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.47104638E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28216746E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.15860684E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.01606582E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64235155E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.59365598E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.25739130E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.75389736E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.74842981E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54063431E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96649869E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87574722E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00554799E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.69250173E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73346898E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00687675E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.72188609E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56246245E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999994E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.73245136E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.97737905E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.95621709E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96649869E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87574722E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00554799E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.69250173E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73346898E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00687675E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.72188609E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56246245E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999994E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.73245136E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.97737905E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.95621709E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79202922E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.49689448E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16891395E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33419157E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73470108E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57816375E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14165255E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.71445898E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49638442E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.27968927E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.73349156E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96675094E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.84398274E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00388704E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.02448842E-08    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.75328256E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01171449E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     9.15232236E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96675094E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.84398274E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00388704E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.02448842E-08    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.75328256E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01171449E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     9.15232236E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96680095E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.84315733E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00362741E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.85087121E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.41575956E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01203132E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.53630011E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.43183802E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.37242490E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.31276645E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.42259170E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.10935905E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.79240747E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.16633559E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.61412202E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.07353231E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.32080403E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.22579025E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.77420975E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.32946342E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.11498110E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.07027900E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.65029692E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.65029692E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.46867416E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.57230570E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.29979073E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.29979073E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.49955528E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.49955528E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.38802432E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.38802432E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.45746928E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.09881786E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.70265787E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54901204E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.54901204E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.39437150E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.77708446E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.15618635E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.15618635E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.40264328E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.40264328E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.53135844E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.53135844E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.60940205E-03   # h decays
#          BR         NDA      ID1       ID2
     5.67795550E-01    2           5        -5   # BR(h -> b       bb     )
     7.17522577E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.54005975E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.39275649E-04    2           3        -3   # BR(h -> s       sb     )
     2.34288640E-02    2           4        -4   # BR(h -> c       cb     )
     7.58757060E-02    2          21        21   # BR(h -> g       g      )
     2.59125485E-03    2          22        22   # BR(h -> gam     gam    )
     1.70418169E-03    2          22        23   # BR(h -> Z       gam    )
     2.27686419E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83724850E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.39770331E+01   # H decays
#          BR         NDA      ID1       ID2
     8.93516052E-01    2           5        -5   # BR(H -> b       bb     )
     7.31872728E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.58772707E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.17775013E-04    2           3        -3   # BR(H -> s       sb     )
     8.90863482E-08    2           4        -4   # BR(H -> c       cb     )
     8.88692765E-03    2           6        -6   # BR(H -> t       tb     )
     1.49736034E-05    2          21        21   # BR(H -> g       g      )
     6.41527324E-08    2          22        22   # BR(H -> gam     gam    )
     3.62983753E-09    2          23        22   # BR(H -> Z       gam    )
     9.47287714E-07    2          24       -24   # BR(H -> W+      W-     )
     4.73059559E-07    2          23        23   # BR(H -> Z       Z      )
     6.30929231E-06    2          25        25   # BR(H -> h       h      )
     9.48673171E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.55105299E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.74375814E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.95141332E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20415816E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.05771944E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.32336210E+01   # A decays
#          BR         NDA      ID1       ID2
     9.13550820E-01    2           5        -5   # BR(A -> b       bb     )
     7.48252493E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.64563831E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.24939322E-04    2           3        -3   # BR(A -> s       sb     )
     9.16932707E-08    2           4        -4   # BR(A -> c       cb     )
     9.14195802E-03    2           6        -6   # BR(A -> t       tb     )
     2.69221913E-05    2          21        21   # BR(A -> g       g      )
     6.83351574E-08    2          22        22   # BR(A -> gam     gam    )
     2.64968440E-08    2          23        22   # BR(A -> Z       gam    )
     9.64934086E-07    2          23        25   # BR(A -> Z       h      )
     1.00695550E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.70051006E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.03487958E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     3.06947377E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.63944464E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.45851962E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.83515229E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.41674313E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.33451400E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.42025833E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.92192809E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.18443335E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.82642045E-07    2          24        25   # BR(H+ -> W+      h      )
     5.51187168E-14    2          24        36   # BR(H+ -> W+      A      )
     5.56423419E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.18585034E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06319118E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
