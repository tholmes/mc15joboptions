#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11628078E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.33990000E+02   # M_1(MX)             
         2     4.67990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04024279E+01   # W+
        25     1.24898763E+02   # h
        35     3.00022766E+03   # H
        36     2.99999958E+03   # A
        37     3.00097184E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02078189E+03   # ~d_L
   2000001     3.01565845E+03   # ~d_R
   1000002     3.01986354E+03   # ~u_L
   2000002     3.01778763E+03   # ~u_R
   1000003     3.02078189E+03   # ~s_L
   2000003     3.01565845E+03   # ~s_R
   1000004     3.01986354E+03   # ~c_L
   2000004     3.01778763E+03   # ~c_R
   1000005     5.08163615E+02   # ~b_1
   2000005     3.01520156E+03   # ~b_2
   1000006     5.04700180E+02   # ~t_1
   2000006     2.99399133E+03   # ~t_2
   1000011     3.00661514E+03   # ~e_L
   2000011     3.00098293E+03   # ~e_R
   1000012     3.00522278E+03   # ~nu_eL
   1000013     3.00661514E+03   # ~mu_L
   2000013     3.00098293E+03   # ~mu_R
   1000014     3.00522278E+03   # ~nu_muL
   1000015     2.98581456E+03   # ~tau_1
   2000015     3.02219619E+03   # ~tau_2
   1000016     3.00538529E+03   # ~nu_tauL
   1000021     2.32545717E+03   # ~g
   1000022     2.36413991E+02   # ~chi_10
   1000023     4.93227896E+02   # ~chi_20
   1000025    -3.00300619E+03   # ~chi_30
   1000035     3.00370124E+03   # ~chi_40
   1000024     4.93384015E+02   # ~chi_1+
   1000037     3.00429557E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886899E-01   # N_11
  1  2    -8.79944008E-04   # N_12
  1  3     1.48933192E-02   # N_13
  1  4    -1.89831615E-03   # N_14
  2  1     1.28938682E-03   # N_21
  2  2     9.99624824E-01   # N_22
  2  3    -2.68062868E-02   # N_23
  2  4     5.47460497E-03   # N_24
  3  1    -9.17285274E-03   # N_31
  3  2     1.50944469E-02   # N_32
  3  3     7.06858079E-01   # N_33
  3  4     7.07134834E-01   # N_34
  4  1    -1.18484226E-02   # N_41
  4  2     2.28384369E-02   # N_42
  4  3     7.06690363E-01   # N_43
  4  4    -7.07054985E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99280600E-01   # U_11
  1  2    -3.79246829E-02   # U_12
  2  1     3.79246829E-02   # U_21
  2  2     9.99280600E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99969995E-01   # V_11
  1  2    -7.74655050E-03   # V_12
  2  1     7.74655050E-03   # V_21
  2  2     9.99969995E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98899060E-01   # cos(theta_t)
  1  2    -4.69112772E-02   # sin(theta_t)
  2  1     4.69112772E-02   # -sin(theta_t)
  2  2     9.98899060E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99921072E-01   # cos(theta_b)
  1  2     1.25638279E-02   # sin(theta_b)
  2  1    -1.25638279E-02   # -sin(theta_b)
  2  2     9.99921072E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06923830E-01   # cos(theta_tau)
  1  2     7.07289685E-01   # sin(theta_tau)
  2  1    -7.07289685E-01   # -sin(theta_tau)
  2  2     7.06923830E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01954025E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.16280784E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44808649E+02   # higgs               
         4     6.34696979E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.16280784E+03  # The gauge couplings
     1     3.61323682E-01   # gprime(Q) DRbar
     2     6.37585966E-01   # g(Q) DRbar
     3     1.03203375E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.16280784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.22969244E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.16280784E+03  # The trilinear couplings
  1  1     3.18076342E-07   # A_d(Q) DRbar
  2  2     3.18136788E-07   # A_s(Q) DRbar
  3  3     7.15988053E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.16280784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.69164448E-08   # A_mu(Q) DRbar
  3  3     6.76979421E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.16280784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.57527707E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.16280784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13090479E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.16280784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06260758E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.16280784E+03  # The soft SUSY breaking masses at the scale Q
         1     2.33990000E+02   # M_1(Q)              
         2     4.67990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.09802649E+04   # M^2_Hd              
        22    -9.14662611E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41098775E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.39990338E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48253613E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48253613E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51746387E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51746387E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.45937969E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.58071982E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41928018E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05265346E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.13342495E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.17349714E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.37523060E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.16953767E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66295798E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52065951E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.02395361E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.56938861E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.66168230E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.33831770E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.26448855E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.83533155E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.22871379E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.08569117E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.91155557E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.14836916E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27263905E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.96629383E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.29042149E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.18114649E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.91534049E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.87003153E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59539927E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.46289643E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.77235342E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.19133966E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.56911643E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.15456058E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20294692E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55869736E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.49036884E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.53034692E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.32443460E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44130011E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.92054518E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.97786020E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59319614E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.84080034E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.61108310E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.18564333E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.87824314E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.16138101E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69436040E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42989994E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.07734513E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.21595490E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.80569065E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55700929E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.91534049E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.87003153E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59539927E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.46289643E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.77235342E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.19133966E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.56911643E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.15456058E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20294692E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55869736E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.49036884E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.53034692E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.32443460E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44130011E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.92054518E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.97786020E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59319614E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.84080034E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.61108310E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.18564333E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.87824314E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.16138101E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69436040E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42989994E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.07734513E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.21595490E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.80569065E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55700929E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.83545832E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00211691E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00366545E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.78695771E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.09320865E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99421761E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.16918349E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53925869E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998406E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.59399035E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.83545832E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00211691E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00366545E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.78695771E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.09320865E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99421761E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.16918349E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53925869E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998406E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.59399035E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.71523886E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54109316E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.15621240E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.30269444E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.65763255E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.62509680E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.12829927E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.40885586E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.53552237E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.24636084E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     8.36536724E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.83570440E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00781783E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99316340E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.83692762E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.83573751E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99901877E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.44738729E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.83570440E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00781783E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99316340E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.83692762E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.83573751E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99901877E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.44738729E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.83626644E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00772602E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99290463E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.26732488E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.47295094E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99936934E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.33721094E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.03158767E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.72008607E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.63159678E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.31860083E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.24332181E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.64474707E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.06121496E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.48347704E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.89975250E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.25132512E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.87069011E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.51293099E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.05484898E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.32806553E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.60456670E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.45647103E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.45647103E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.16757918E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.03792180E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.68871882E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.68871882E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26260834E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26260834E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.31511924E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.31511924E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.31511924E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.31511924E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.64599473E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.64599473E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.96989083E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.03633686E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.03335813E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.52047624E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.52047624E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.38192178E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.88207999E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.66404890E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.66404890E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28843848E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28843848E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.99636380E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.99636380E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.99636380E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.99636380E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.94394684E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.94394684E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.91501389E-03   # h decays
#          BR         NDA      ID1       ID2
     6.80109410E-01    2           5        -5   # BR(h -> b       bb     )
     5.30955527E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.87960199E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.98973226E-04    2           3        -3   # BR(h -> s       sb     )
     1.72130305E-02    2           4        -4   # BR(h -> c       cb     )
     5.64267124E-02    2          21        21   # BR(h -> g       g      )
     1.90360416E-03    2          22        22   # BR(h -> gam     gam    )
     1.25998566E-03    2          22        23   # BR(h -> Z       gam    )
     1.68378333E-01    2          24       -24   # BR(h -> W+      W-     )
     2.10264373E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38730737E+01   # H decays
#          BR         NDA      ID1       ID2
     7.40547581E-01    2           5        -5   # BR(H -> b       bb     )
     1.79256609E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.33808532E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.78313712E-04    2           3        -3   # BR(H -> s       sb     )
     2.19741832E-07    2           4        -4   # BR(H -> c       cb     )
     2.19206784E-02    2           6        -6   # BR(H -> t       tb     )
     3.45515390E-05    2          21        21   # BR(H -> g       g      )
     7.15663071E-08    2          22        22   # BR(H -> gam     gam    )
     8.39711666E-09    2          23        22   # BR(H -> Z       gam    )
     3.57296695E-05    2          24       -24   # BR(H -> W+      W-     )
     1.78428123E-05    2          23        23   # BR(H -> Z       Z      )
     8.92401355E-05    2          25        25   # BR(H -> h       h      )
    -5.63647180E-23    2          36        36   # BR(H -> A       A      )
     4.85711648E-17    2          23        36   # BR(H -> Z       A      )
     2.17199775E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.10835168E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.08301509E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.99835322E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.26132627E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.39952050E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31508737E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81271659E-01    2           5        -5   # BR(A -> b       bb     )
     1.89091137E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.68580140E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.21155257E-04    2           3        -3   # BR(A -> s       sb     )
     2.31718374E-07    2           4        -4   # BR(A -> c       cb     )
     2.31026730E-02    2           6        -6   # BR(A -> t       tb     )
     6.80351712E-05    2          21        21   # BR(A -> g       g      )
     3.63795398E-08    2          22        22   # BR(A -> gam     gam    )
     6.70359516E-08    2          23        22   # BR(A -> Z       gam    )
     3.75446974E-05    2          23        25   # BR(A -> Z       h      )
     2.67635274E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22996463E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33442616E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.05105422E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.79489207E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.07966113E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.53960897E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.97943788E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.90982027E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.27701320E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.08565130E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.04262171E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.04890651E-05    2          24        25   # BR(H+ -> W+      h      )
     1.16707584E-13    2          24        36   # BR(H+ -> W+      A      )
     2.06771591E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.39000838E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.65357876E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
