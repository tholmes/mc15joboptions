#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871741E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03961191E+01   # W+
        25     1.24921829E+02   # h
        35     3.00000906E+03   # H
        36     2.99999995E+03   # A
        37     3.00108994E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04194037E+03   # ~d_L
   2000001     3.03675347E+03   # ~d_R
   1000002     3.04102918E+03   # ~u_L
   2000002     3.03805945E+03   # ~u_R
   1000003     3.04194037E+03   # ~s_L
   2000003     3.03675347E+03   # ~s_R
   1000004     3.04102918E+03   # ~c_L
   2000004     3.03805945E+03   # ~c_R
   1000005     1.04775770E+03   # ~b_1
   2000005     3.03414563E+03   # ~b_2
   1000006     1.04569418E+03   # ~t_1
   2000006     3.02445701E+03   # ~t_2
   1000011     3.00645304E+03   # ~e_L
   2000011     3.00185546E+03   # ~e_R
   1000012     3.00505921E+03   # ~nu_eL
   1000013     3.00645304E+03   # ~mu_L
   2000013     3.00185546E+03   # ~mu_R
   1000014     3.00505921E+03   # ~nu_muL
   1000015     2.98599277E+03   # ~tau_1
   2000015     3.02174492E+03   # ~tau_2
   1000016     3.00489475E+03   # ~nu_tauL
   1000021     2.35219019E+03   # ~g
   1000022     1.50993989E+02   # ~chi_10
   1000023     3.22916813E+02   # ~chi_20
   1000025    -2.99500034E+03   # ~chi_30
   1000035     2.99502397E+03   # ~chi_40
   1000024     3.23079001E+02   # ~chi_1+
   1000037     2.99594687E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891816E-01   # N_11
  1  2     3.81600378E-04   # N_12
  1  3    -1.47041375E-02   # N_13
  1  4     3.31793294E-07   # N_14
  2  1     1.66814087E-06   # N_21
  2  2     9.99659614E-01   # N_22
  2  3     2.60565356E-02   # N_23
  2  4     1.30849468E-03   # N_24
  3  1     1.04006330E-02   # N_31
  3  2    -1.93476791E-02   # N_32
  3  3     7.06764294E-01   # N_33
  3  4     7.07108002E-01   # N_34
  4  1    -1.04011590E-02   # N_41
  4  2     1.74979114E-02   # N_42
  4  3    -7.06816156E-01   # N_43
  4  4     7.07104350E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321289E-01   # U_11
  1  2     3.68369444E-02   # U_12
  2  1    -3.68369444E-02   # U_21
  2  2     9.99321289E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998291E-01   # V_11
  1  2    -1.84861320E-03   # V_12
  2  1    -1.84861320E-03   # V_21
  2  2    -9.99998291E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98450414E-01   # cos(theta_t)
  1  2    -5.56486368E-02   # sin(theta_t)
  2  1     5.56486368E-02   # -sin(theta_t)
  2  2     9.98450414E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99702284E-01   # cos(theta_b)
  1  2    -2.43996591E-02   # sin(theta_b)
  2  1     2.43996591E-02   # -sin(theta_b)
  2  2     9.99702284E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06933046E-01   # cos(theta_tau)
  1  2     7.07280474E-01   # sin(theta_tau)
  2  1    -7.07280474E-01   # -sin(theta_tau)
  2  2    -7.06933046E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00178687E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68717409E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43842795E+02   # higgs               
         4     1.01425058E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68717409E+03  # The gauge couplings
     1     3.62545691E-01   # gprime(Q) DRbar
     2     6.38943962E-01   # g(Q) DRbar
     3     1.02368268E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68717409E+03  # The trilinear couplings
  1  1     2.70174339E-06   # A_u(Q) DRbar
  2  2     2.70178496E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68717409E+03  # The trilinear couplings
  1  1     9.37800822E-07   # A_d(Q) DRbar
  2  2     9.37916159E-07   # A_s(Q) DRbar
  3  3     1.95175402E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68717409E+03  # The trilinear couplings
  1  1     4.22363019E-07   # A_e(Q) DRbar
  2  2     4.22384109E-07   # A_mu(Q) DRbar
  3  3     4.28376496E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68717409E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50020566E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68717409E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.82660997E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68717409E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02938879E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68717409E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.49189632E+04   # M^2_Hd              
        22    -9.02634861E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41728316E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.15766856E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47891809E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47891809E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52108191E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52108191E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.05610561E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.37667768E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.15603579E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.70629644E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11771357E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.91399970E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.05358695E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.14220587E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.47650940E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.01328541E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.65914057E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59804877E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12745298E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.04151844E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.44611521E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.41938332E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.43600516E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.32588386E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.13030290E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.27032894E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.84512723E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.76130390E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.11966606E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.50190361E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.02572302E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01223119E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.46908450E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.00962075E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99420607E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64091640E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.23520826E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.17594962E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28398342E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.13768827E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.01515717E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16046149E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60773172E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.40172635E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.58985276E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.58832048E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39226797E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.01462002E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.94362986E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64027140E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.17058778E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.80740259E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27825348E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.69164775E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02203263E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64823684E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.58161244E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.25435773E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.27138723E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.26712730E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54183867E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.00962075E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99420607E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64091640E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.23520826E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.17594962E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28398342E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.13768827E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.01515717E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16046149E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60773172E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.40172635E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.58985276E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.58832048E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39226797E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.01462002E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.94362986E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64027140E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.17058778E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.80740259E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27825348E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.69164775E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02203263E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64823684E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.58161244E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.25435773E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.27138723E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.26712730E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54183867E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96611593E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87213368E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00566890E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.21829443E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.38376079E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00711727E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.06661357E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56164038E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999995E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.73306565E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.26324804E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.24791691E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96611593E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87213368E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00566890E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.21829443E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.38376079E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00711727E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.06661357E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56164038E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999995E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.73306565E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.26324804E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.24791691E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79144646E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.49598475E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16921769E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33479756E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73412738E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57722671E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14197528E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.61560240E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.40483737E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28033323E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.62737299E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96636942E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.84036979E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00400699E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.76145474E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.48967718E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01195586E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.71087190E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96636942E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.84036979E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00400699E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.76145474E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.48967718E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01195586E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.71087190E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96648336E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.83954315E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00374862E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.47798528E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.24678772E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01227547E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.14333630E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.42915707E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.55405255E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.32905337E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.45268249E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.46513903E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.65504951E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.12180406E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.48094652E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.92481665E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.31839159E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.22745473E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.77254527E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.51184256E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.09175061E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.00760700E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.51512414E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.51512414E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.26389931E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.51806260E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.31310315E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31310315E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.58191118E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.58191118E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.81179791E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.81179791E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.63930309E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07633567E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.62846032E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.41835242E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.41835242E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.36478313E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.67704225E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.17220660E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.17220660E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.48625208E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.48625208E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.87905499E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.87905499E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.62026276E-03   # h decays
#          BR         NDA      ID1       ID2
     5.66117059E-01    2           5        -5   # BR(h -> b       bb     )
     7.15893226E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53428707E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.37970009E-04    2           3        -3   # BR(h -> s       sb     )
     2.33729897E-02    2           4        -4   # BR(h -> c       cb     )
     7.58094482E-02    2          21        21   # BR(h -> g       g      )
     2.59179219E-03    2          22        22   # BR(h -> gam     gam    )
     1.71374251E-03    2          22        23   # BR(h -> Z       gam    )
     2.29385247E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86290002E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.45722361E+01   # H decays
#          BR         NDA      ID1       ID2
     8.94731012E-01    2           5        -5   # BR(H -> b       bb     )
     7.19277523E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.54319344E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.12305970E-04    2           3        -3   # BR(H -> s       sb     )
     8.75498797E-08    2           4        -4   # BR(H -> c       cb     )
     8.73365647E-03    2           6        -6   # BR(H -> t       tb     )
     1.35022108E-05    2          21        21   # BR(H -> g       g      )
     6.12375769E-08    2          22        22   # BR(H -> gam     gam    )
     3.56876310E-09    2          23        22   # BR(H -> Z       gam    )
     9.02603802E-07    2          24       -24   # BR(H -> W+      W-     )
     4.50745184E-07    2          23        23   # BR(H -> Z       Z      )
     6.12438258E-06    2          25        25   # BR(H -> h       h      )
    -2.13181829E-24    2          36        36   # BR(H -> A       A      )
     5.10793993E-20    2          23        36   # BR(H -> Z       A      )
     9.32666520E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.47217118E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.66371808E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.90090418E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.22808068E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.16463270E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.38073059E+01   # A decays
#          BR         NDA      ID1       ID2
     9.15017330E-01    2           5        -5   # BR(A -> b       bb     )
     7.35555202E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.60074379E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.19425342E-04    2           3        -3   # BR(A -> s       sb     )
     9.01373037E-08    2           4        -4   # BR(A -> c       cb     )
     8.98682576E-03    2           6        -6   # BR(A -> t       tb     )
     2.64653418E-05    2          21        21   # BR(A -> g       g      )
     6.71795292E-08    2          22        22   # BR(A -> gam     gam    )
     2.60483158E-08    2          23        22   # BR(A -> Z       gam    )
     9.19622995E-07    2          23        25   # BR(A -> Z       h      )
     9.90183166E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.62020934E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.95101612E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     3.01769288E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.70094513E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46038654E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.72157350E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.37658444E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.34646232E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.39665798E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.87337457E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.19472249E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.41497867E-07    2          24        25   # BR(H+ -> W+      h      )
     5.46976979E-14    2          24        36   # BR(H+ -> W+      A      )
     5.47231813E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.08895456E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07552473E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
