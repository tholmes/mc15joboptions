#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15008975E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.02990000E+02   # M_1(MX)             
         2     8.05990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04048837E+01   # W+
        25     1.23889924E+02   # h
        35     3.00014812E+03   # H
        36     2.99999980E+03   # A
        37     3.00092053E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03528300E+03   # ~d_L
   2000001     3.03029567E+03   # ~d_R
   1000002     3.03437324E+03   # ~u_L
   2000002     3.03160021E+03   # ~u_R
   1000003     3.03528300E+03   # ~s_L
   2000003     3.03029567E+03   # ~s_R
   1000004     3.03437324E+03   # ~c_L
   2000004     3.03160021E+03   # ~c_R
   1000005     8.50101293E+02   # ~b_1
   2000005     3.02944366E+03   # ~b_2
   1000006     8.48402891E+02   # ~t_1
   2000006     3.01565232E+03   # ~t_2
   1000011     3.00623053E+03   # ~e_L
   2000011     3.00181506E+03   # ~e_R
   1000012     3.00484231E+03   # ~nu_eL
   1000013     3.00623053E+03   # ~mu_L
   2000013     3.00181506E+03   # ~mu_R
   1000014     3.00484231E+03   # ~nu_muL
   1000015     2.98549179E+03   # ~tau_1
   2000015     3.02203847E+03   # ~tau_2
   1000016     3.00469911E+03   # ~nu_tauL
   1000021     2.34367972E+03   # ~g
   1000022     4.05564010E+02   # ~chi_10
   1000023     8.39599247E+02   # ~chi_20
   1000025    -2.99750745E+03   # ~chi_30
   1000035     2.99854871E+03   # ~chi_40
   1000024     8.39760906E+02   # ~chi_1+
   1000037     2.99903691E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99881714E-01   # N_11
  1  2    -6.78642490E-04   # N_12
  1  3     1.51143461E-02   # N_13
  1  4    -2.76666812E-03   # N_14
  2  1     1.12993727E-03   # N_21
  2  2     9.99560996E-01   # N_22
  2  3    -2.82451779E-02   # N_23
  2  4     8.87408788E-03   # N_24
  3  1    -8.71943576E-03   # N_31
  3  2     1.37067827E-02   # N_32
  3  3     7.06886614E-01   # N_33
  3  4     7.07140305E-01   # N_34
  4  1    -1.26195542E-02   # N_41
  4  2     2.62579478E-02   # N_42
  4  3     7.06601077E-01   # N_43
  4  4    -7.07012154E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99201424E-01   # U_11
  1  2    -3.99564143E-02   # U_12
  2  1     3.99564143E-02   # U_21
  2  2     9.99201424E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99921185E-01   # V_11
  1  2    -1.25548279E-02   # V_12
  2  1     1.25548279E-02   # V_21
  2  2     9.99921185E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98824878E-01   # cos(theta_t)
  1  2    -4.84650708E-02   # sin(theta_t)
  2  1     4.84650708E-02   # -sin(theta_t)
  2  2     9.98824878E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99911808E-01   # cos(theta_b)
  1  2     1.32806710E-02   # sin(theta_b)
  2  1    -1.32806710E-02   # -sin(theta_b)
  2  2     9.99911808E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06961336E-01   # cos(theta_tau)
  1  2     7.07252196E-01   # sin(theta_tau)
  2  1    -7.07252196E-01   # -sin(theta_tau)
  2  2     7.06961336E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01849456E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.50089752E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44280772E+02   # higgs               
         4     7.30380591E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.50089752E+03  # The gauge couplings
     1     3.62175325E-01   # gprime(Q) DRbar
     2     6.36736601E-01   # g(Q) DRbar
     3     1.02632665E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.50089752E+03  # The trilinear couplings
  1  1     2.14114596E-06   # A_u(Q) DRbar
  2  2     2.14117611E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.50089752E+03  # The trilinear couplings
  1  1     5.97254445E-07   # A_d(Q) DRbar
  2  2     5.97353542E-07   # A_s(Q) DRbar
  3  3     1.26986718E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.50089752E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.18852105E-07   # A_mu(Q) DRbar
  3  3     1.20234062E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.50089752E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51714426E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.50089752E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16504033E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.50089752E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07793723E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.50089752E+03  # The soft SUSY breaking masses at the scale Q
         1     4.02990000E+02   # M_1(Q)              
         2     8.05990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.65780743E+04   # M^2_Hd              
        22    -9.05669100E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40712329E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.15208149E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48220598E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48220598E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51779402E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51779402E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.47892326E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.57974861E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.20251394E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01096664E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.32099694E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.87380273E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.79026955E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.46723622E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.32513966E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.58299587E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.49627352E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.95128046E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.89708083E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.73014494E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.69855057E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.22293945E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.82381143E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.33194953E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55162532E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.47475230E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.17481043E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27806903E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.97312785E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.27751428E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.14180469E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56821685E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.11628507E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.51890703E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.73865047E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.44672817E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.03851015E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.47439443E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.38141881E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15906036E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55502989E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.76013265E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.10548292E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.40127004E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44496814E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.57362818E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.20142904E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.51697143E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.95696221E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.74432721E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.03287001E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.69316615E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.38813930E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66343560E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.41143704E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.99273989E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.87018024E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.67914646E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55885574E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56821685E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.11628507E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.51890703E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.73865047E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.44672817E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.03851015E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.47439443E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.38141881E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15906036E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55502989E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.76013265E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.10548292E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.40127004E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44496814E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.57362818E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.20142904E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.51697143E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.95696221E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.74432721E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.03287001E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.69316615E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.38813930E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66343560E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.41143704E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.99273989E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.87018024E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.67914646E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55885574E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.46599563E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08802349E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97476029E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.98759746E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.31743126E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93721592E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.55204088E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.50965627E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998873E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12578908E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.48965966E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.81873709E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.46599563E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08802349E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97476029E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.98759746E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.31743126E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93721592E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.55204088E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.50965627E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998873E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12578908E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.48965966E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.81873709E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51237957E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75178542E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08574277E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16247181E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46078457E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.83999311E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05634364E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.34930943E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49672583E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10322609E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.52552912E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.46601968E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09267920E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96526467E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.89648107E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.84450965E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94205601E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.64249256E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.46601968E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09267920E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96526467E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.89648107E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.84450965E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94205601E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.64249256E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46613612E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09258656E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96497589E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.78465958E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.53698628E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94242719E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.02636666E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.74036784E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.38409414E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.85335613E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.14663129E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.79902521E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.07143723E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.22603679E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.88642059E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.49638834E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.51335303E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.81965084E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.41803492E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.39477701E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.55543138E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.53930415E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.86550670E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.86550670E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.68144647E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.53640913E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61187119E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61187119E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.31609770E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.31609770E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.90002839E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.90002839E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.32297775E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.49204993E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.52670630E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.92998154E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.92998154E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.63282835E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.99444026E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.57746615E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.57746615E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.34370793E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.34370793E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.91239119E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.91239119E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.68477047E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88148481E-01    2           5        -5   # BR(h -> b       bb     )
     5.52310095E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.95523666E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.15686704E-04    2           3        -3   # BR(h -> s       sb     )
     1.79415649E-02    2           4        -4   # BR(h -> c       cb     )
     5.74327586E-02    2          21        21   # BR(h -> g       g      )
     1.93393573E-03    2          22        22   # BR(h -> gam     gam    )
     1.20622320E-03    2          22        23   # BR(h -> Z       gam    )
     1.58079766E-01    2          24       -24   # BR(h -> W+      W-     )
     1.94150509E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39520482E+01   # H decays
#          BR         NDA      ID1       ID2
     7.46246159E-01    2           5        -5   # BR(H -> b       bb     )
     1.78237403E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30204865E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.73891156E-04    2           3        -3   # BR(H -> s       sb     )
     2.18402024E-07    2           4        -4   # BR(H -> c       cb     )
     2.17870111E-02    2           6        -6   # BR(H -> t       tb     )
     2.82502233E-05    2          21        21   # BR(H -> g       g      )
     4.95899108E-08    2          22        22   # BR(H -> gam     gam    )
     8.38848297E-09    2          23        22   # BR(H -> Z       gam    )
     3.24589977E-05    2          24       -24   # BR(H -> W+      W-     )
     1.62094956E-05    2          23        23   # BR(H -> Z       Z      )
     8.38410791E-05    2          25        25   # BR(H -> h       h      )
    -4.61597860E-24    2          36        36   # BR(H -> A       A      )
     5.56840076E-18    2          23        36   # BR(H -> Z       A      )
     1.60134938E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.04488103E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.98857323E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.04210235E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.90493861E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.00386845E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32881108E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83601695E-01    2           5        -5   # BR(A -> b       bb     )
     1.87138253E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61675218E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12674571E-04    2           3        -3   # BR(A -> s       sb     )
     2.29325245E-07    2           4        -4   # BR(A -> c       cb     )
     2.28640744E-02    2           6        -6   # BR(A -> t       tb     )
     6.73325149E-05    2          21        21   # BR(A -> g       g      )
     6.51045040E-08    2          22        22   # BR(A -> gam     gam    )
     6.63716920E-08    2          23        22   # BR(A -> Z       gam    )
     3.39533555E-05    2          23        25   # BR(A -> Z       h      )
     2.60775847E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22780623E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30074169E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.88700792E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01850043E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10331347E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.44229356E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.63535433E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.06119517E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.07481430E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04405249E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.17856239E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.43665755E-05    2          24        25   # BR(H+ -> W+      h      )
     8.54836396E-14    2          24        36   # BR(H+ -> W+      A      )
     1.85371019E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.20429965E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.29476157E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
