#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13015583E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.23990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04192269E+01   # W+
        25     1.25979687E+02   # h
        35     4.00000717E+03   # H
        36     3.99999733E+03   # A
        37     4.00102896E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02592942E+03   # ~d_L
   2000001     4.02256861E+03   # ~d_R
   1000002     4.02527941E+03   # ~u_L
   2000002     4.02333463E+03   # ~u_R
   1000003     4.02592942E+03   # ~s_L
   2000003     4.02256861E+03   # ~s_R
   1000004     4.02527941E+03   # ~c_L
   2000004     4.02333463E+03   # ~c_R
   1000005     9.71807238E+02   # ~b_1
   2000005     4.02546815E+03   # ~b_2
   1000006     9.49470635E+02   # ~t_1
   2000006     1.90163625E+03   # ~t_2
   1000011     4.00454826E+03   # ~e_L
   2000011     4.00332522E+03   # ~e_R
   1000012     4.00343768E+03   # ~nu_eL
   1000013     4.00454826E+03   # ~mu_L
   2000013     4.00332522E+03   # ~mu_R
   1000014     4.00343768E+03   # ~nu_muL
   1000015     4.00375079E+03   # ~tau_1
   2000015     4.00836598E+03   # ~tau_2
   1000016     4.00485336E+03   # ~nu_tauL
   1000021     1.97812658E+03   # ~g
   1000022     4.94454333E+02   # ~chi_10
   1000023    -5.35921535E+02   # ~chi_20
   1000025     5.55152731E+02   # ~chi_30
   1000035     2.05209347E+03   # ~chi_40
   1000024     5.33861267E+02   # ~chi_1+
   1000037     2.05225743E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.69583593E-01   # N_11
  1  2    -2.09033920E-02   # N_12
  1  3    -4.69345009E-01   # N_13
  1  4    -4.32457402E-01   # N_14
  2  1    -3.15794211E-02   # N_21
  2  2     2.27565358E-02   # N_22
  2  3    -7.05144582E-01   # N_23
  2  4     7.07994349E-01   # N_24
  3  1     6.37763600E-01   # N_31
  3  2     2.82200113E-02   # N_32
  3  3     5.31422141E-01   # N_33
  3  4     5.56822889E-01   # N_34
  4  1    -1.19319551E-03   # N_41
  4  2     9.99124026E-01   # N_42
  4  3    -8.76867468E-03   # N_43
  4  4    -4.09007015E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.24069411E-02   # U_11
  1  2     9.99923031E-01   # U_12
  2  1    -9.99923031E-01   # U_21
  2  2     1.24069411E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.78504675E-02   # V_11
  1  2    -9.98325259E-01   # V_12
  2  1    -9.98325259E-01   # V_21
  2  2    -5.78504675E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89848668E-01   # cos(theta_t)
  1  2    -1.42125348E-01   # sin(theta_t)
  2  1     1.42125348E-01   # -sin(theta_t)
  2  2     9.89848668E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998529E-01   # cos(theta_b)
  1  2    -1.71522530E-03   # sin(theta_b)
  2  1     1.71522530E-03   # -sin(theta_b)
  2  2     9.99998529E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06302603E-01   # cos(theta_tau)
  1  2     7.07910046E-01   # sin(theta_tau)
  2  1    -7.07910046E-01   # -sin(theta_tau)
  2  2    -7.06302603E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00246871E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30155825E+03  # DRbar Higgs Parameters
         1    -5.23990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43668791E+02   # higgs               
         4     1.61474861E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30155825E+03  # The gauge couplings
     1     3.61855289E-01   # gprime(Q) DRbar
     2     6.35671660E-01   # g(Q) DRbar
     3     1.03008293E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30155825E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37040424E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30155825E+03  # The trilinear couplings
  1  1     5.06670774E-07   # A_d(Q) DRbar
  2  2     5.06717284E-07   # A_s(Q) DRbar
  3  3     9.05086717E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30155825E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13044965E-07   # A_mu(Q) DRbar
  3  3     1.14197998E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30155825E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67917569E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30155825E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88561747E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30155825E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02841303E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30155825E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56182055E+07   # M^2_Hd              
        22    -2.35481579E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40179401E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.93566998E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.39997943E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.39997943E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.60002057E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.60002057E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.45845117E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.95348892E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.06338080E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.81189273E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.17123755E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12678002E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.09789915E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05032526E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.19695320E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.06082201E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.72033819E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.30585469E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.53317461E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.01187960E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.58556070E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.37144033E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.04874536E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.79942536E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66433709E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58776641E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83279151E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56879927E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.64578828E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67222663E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.25808970E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11953818E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.02063718E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16933657E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.95853006E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     7.58931357E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78133477E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.33056556E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.37601577E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87801130E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81004090E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.66026829E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60798989E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52021328E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60295784E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.22062566E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.39461849E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.19457116E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.38864715E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45794042E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78153562E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.36838960E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.98779172E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.26164168E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81572767E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.60363437E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64142166E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52238039E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53712921E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.39465565E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.40612036E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.72019710E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14362472E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85871075E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78133477E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.33056556E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.37601577E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87801130E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81004090E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.66026829E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60798989E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52021328E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60295784E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.22062566E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.39461849E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.19457116E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.38864715E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45794042E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78153562E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.36838960E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.98779172E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.26164168E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81572767E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.60363437E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64142166E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52238039E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53712921E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.39465565E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.40612036E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.72019710E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14362472E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85871075E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13175990E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.67433115E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.13262862E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.56867809E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78606122E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.52656216E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58799803E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01590576E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.94209861E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.95134703E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.04794204E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.00619990E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13175990E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.67433115E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.13262862E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.56867809E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78606122E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.52656216E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58799803E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01590576E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.94209861E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.95134703E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.04794204E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.00619990E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05572772E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.13357589E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51792804E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60969448E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41247402E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.59452274E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83301053E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04519985E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.14409216E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.86320588E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.43060230E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45063748E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.78888762E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.90945871E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13281377E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04933740E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.21963755E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.53908628E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79083007E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.31683216E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56453594E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13281377E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04933740E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.21963755E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.53908628E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79083007E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.31683216E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56453594E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45150476E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.52805619E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.46351688E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.02955795E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53531438E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.46336661E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05512403E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.77959279E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33701837E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33701837E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11235347E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11235347E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10125632E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.07701077E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.57514333E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.90026759E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.39667974E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.69219820E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.57821256E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.49538401E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91326461E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.59780969E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.95826679E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.06102414E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18658588E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53935005E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18658588E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53935005E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36745298E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53083962E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53083962E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48747116E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.05894500E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.05894500E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.05894489E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.92633931E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.92633931E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.92633931E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.92633931E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.75447119E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.75447119E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.75447119E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.75447119E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.20074188E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.20074188E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.74132792E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.00688674E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.82841631E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.60567008E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.60654585E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.60567008E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.60654585E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.40989063E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.00196008E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.00196008E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.01112806E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.14000087E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.14000087E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.13998446E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.02581720E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.62813561E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.02581720E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.62813561E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.46767404E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.02849255E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.02849255E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.74304644E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.20511859E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.20511859E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.20511860E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.15274272E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.15274272E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.15274272E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.15274272E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.84242682E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.84242682E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.84242682E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.84242682E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.71271303E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.71271303E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.07536438E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.41512638E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.42407498E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.42868611E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.49655769E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.49655769E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.77870439E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.24078833E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.55900772E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.74325853E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.74325853E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.74344699E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.74344699E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06036418E-03   # h decays
#          BR         NDA      ID1       ID2
     5.83376544E-01    2           5        -5   # BR(h -> b       bb     )
     6.43891470E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27935219E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83058108E-04    2           3        -3   # BR(h -> s       sb     )
     2.09815162E-02    2           4        -4   # BR(h -> c       cb     )
     6.87640859E-02    2          21        21   # BR(h -> g       g      )
     2.40042596E-03    2          22        22   # BR(h -> gam     gam    )
     1.67933597E-03    2          22        23   # BR(h -> Z       gam    )
     2.28596759E-01    2          24       -24   # BR(h -> W+      W-     )
     2.91011922E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.58097830E+01   # H decays
#          BR         NDA      ID1       ID2
     3.69557374E-01    2           5        -5   # BR(H -> b       bb     )
     5.94089576E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10055685E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48459658E-04    2           3        -3   # BR(H -> s       sb     )
     6.96823548E-08    2           4        -4   # BR(H -> c       cb     )
     6.98362275E-03    2           6        -6   # BR(H -> t       tb     )
     4.32106561E-07    2          21        21   # BR(H -> g       g      )
     9.79009236E-09    2          22        22   # BR(H -> gam     gam    )
     1.79805749E-09    2          23        22   # BR(H -> Z       gam    )
     1.64976370E-06    2          24       -24   # BR(H -> W+      W-     )
     8.24310465E-07    2          23        23   # BR(H -> Z       Z      )
     6.72629024E-06    2          25        25   # BR(H -> h       h      )
     2.41679826E-24    2          36        36   # BR(H -> A       A      )
     3.24806556E-20    2          23        36   # BR(H -> Z       A      )
     1.85689719E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.50923450E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.50923450E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.48717597E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.82421737E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64587021E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.13764578E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.12285338E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.03019521E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.56554853E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.23002311E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.88439258E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.78703884E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.64190616E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.27064326E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.27064326E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.36567372E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.58080398E+01   # A decays
#          BR         NDA      ID1       ID2
     3.69594775E-01    2           5        -5   # BR(A -> b       bb     )
     5.94111082E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10063123E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48508980E-04    2           3        -3   # BR(A -> s       sb     )
     7.01406672E-08    2           4        -4   # BR(A -> c       cb     )
     6.99779024E-03    2           6        -6   # BR(A -> t       tb     )
     1.43786859E-05    2          21        21   # BR(A -> g       g      )
     6.91758975E-08    2          22        22   # BR(A -> gam     gam    )
     1.58116276E-08    2          23        22   # BR(A -> Z       gam    )
     1.64631544E-06    2          23        25   # BR(A -> Z       h      )
     1.91555091E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.50890903E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.50890903E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.20278625E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.03804331E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.44570774E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.50284328E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.76672408E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.61647765E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.70951502E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.82874760E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.69098366E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.37915389E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.37915389E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.60927725E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.96249431E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.91247761E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09050723E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.81599315E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18431141E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43650942E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.79236235E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.63979977E-06    2          24        25   # BR(H+ -> W+      h      )
     2.74120441E-14    2          24        36   # BR(H+ -> W+      A      )
     4.48148282E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.43588326E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.61678529E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.50360388E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.62883159E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.49799217E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.55482728E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     9.16091657E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.65834136E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
