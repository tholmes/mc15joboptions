#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13058363E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.69990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04187805E+01   # W+
        25     1.25835680E+02   # h
        35     4.00001066E+03   # H
        36     3.99999761E+03   # A
        37     4.00101594E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02603411E+03   # ~d_L
   2000001     4.02263386E+03   # ~d_R
   1000002     4.02538286E+03   # ~u_L
   2000002     4.02346617E+03   # ~u_R
   1000003     4.02603411E+03   # ~s_L
   2000003     4.02263386E+03   # ~s_R
   1000004     4.02538286E+03   # ~c_L
   2000004     4.02346617E+03   # ~c_R
   1000005     9.27940653E+02   # ~b_1
   2000005     4.02552228E+03   # ~b_2
   1000006     9.10167485E+02   # ~t_1
   2000006     2.01969779E+03   # ~t_2
   1000011     4.00460461E+03   # ~e_L
   2000011     4.00322528E+03   # ~e_R
   1000012     4.00349282E+03   # ~nu_eL
   1000013     4.00460461E+03   # ~mu_L
   2000013     4.00322528E+03   # ~mu_R
   1000014     4.00349282E+03   # ~nu_muL
   1000015     4.00351177E+03   # ~tau_1
   2000015     4.00852411E+03   # ~tau_2
   1000016     4.00489569E+03   # ~nu_tauL
   1000021     1.97936940E+03   # ~g
   1000022     5.43363731E+02   # ~chi_10
   1000023    -5.82013950E+02   # ~chi_20
   1000025     6.02897589E+02   # ~chi_30
   1000035     2.05209840E+03   # ~chi_40
   1000024     5.79975093E+02   # ~chi_1+
   1000037     2.05226284E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48259442E-01   # N_11
  1  2    -2.25860925E-02   # N_12
  1  3    -4.85025706E-01   # N_13
  1  4    -4.52048383E-01   # N_14
  2  1    -2.89048607E-02   # N_21
  2  2     2.23493735E-02   # N_22
  2  3    -7.05385219E-01   # N_23
  2  4     7.07881845E-01   # N_24
  3  1     6.62775043E-01   # N_31
  3  2     2.83562018E-02   # N_32
  3  3     5.16802349E-01   # N_33
  3  4     5.41147393E-01   # N_34
  4  1    -1.24865321E-03   # N_41
  4  2     9.99092738E-01   # N_42
  4  3    -9.85340901E-03   # N_43
  4  4    -4.14131796E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.39410144E-02   # U_11
  1  2     9.99902819E-01   # U_12
  2  1    -9.99902819E-01   # U_21
  2  2     1.39410144E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.85742509E-02   # V_11
  1  2    -9.98283055E-01   # V_12
  2  1    -9.98283055E-01   # V_21
  2  2    -5.85742509E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92870803E-01   # cos(theta_t)
  1  2    -1.19195506E-01   # sin(theta_t)
  2  1     1.19195506E-01   # -sin(theta_t)
  2  2     9.92870803E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998273E-01   # cos(theta_b)
  1  2    -1.85849321E-03   # sin(theta_b)
  2  1     1.85849321E-03   # -sin(theta_b)
  2  2     9.99998273E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06369593E-01   # cos(theta_tau)
  1  2     7.07843202E-01   # sin(theta_tau)
  2  1    -7.07843202E-01   # -sin(theta_tau)
  2  2    -7.06369593E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00232191E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30583628E+03  # DRbar Higgs Parameters
         1    -5.69990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43710107E+02   # higgs               
         4     1.61875123E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30583628E+03  # The gauge couplings
     1     3.61842400E-01   # gprime(Q) DRbar
     2     6.35616280E-01   # g(Q) DRbar
     3     1.03004015E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30583628E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37980703E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30583628E+03  # The trilinear couplings
  1  1     5.11114140E-07   # A_d(Q) DRbar
  2  2     5.11161065E-07   # A_s(Q) DRbar
  3  3     9.12098923E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30583628E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.14159536E-07   # A_mu(Q) DRbar
  3  3     1.15319823E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30583628E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67101059E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30583628E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.89042863E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30583628E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02675048E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30583628E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55690659E+07   # M^2_Hd              
        22    -2.97958777E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40156925E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.19539136E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42094813E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42094813E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57905187E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57905187E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.35770236E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.36745000E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.99050209E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.41522406E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.22682385E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14048507E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.49255257E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08383290E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.31011100E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.14013899E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.71267959E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.22670213E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.45638003E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.81101120E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.12965766E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.72551621E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.00367300E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.71411531E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66789612E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58996301E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82447886E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54443792E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.28806439E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65334968E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.53704758E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12169116E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.60147421E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.98419549E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.34806661E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.39036590E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78491608E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.20154247E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38408030E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98513171E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79942015E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.75372903E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58676428E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52352268E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60603042E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.01979200E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.48211756E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34932273E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.79689415E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46263984E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78512064E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28277911E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.49483689E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.93045299E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80529176E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25950910E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62053581E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52568356E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54058335E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.86842241E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.16786569E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.12141437E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24954716E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85998472E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78491608E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.20154247E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38408030E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98513171E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79942015E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.75372903E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58676428E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52352268E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60603042E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.01979200E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.48211756E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34932273E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.79689415E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46263984E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78512064E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28277911E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.49483689E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.93045299E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80529176E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25950910E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62053581E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52568356E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54058335E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.86842241E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.16786569E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.12141437E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24954716E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85998472E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12792853E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.06649891E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71292838E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.08728257E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78872206E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91689982E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59381160E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.00170803E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.62032559E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.34053273E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.37132505E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.82871816E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12792853E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.06649891E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71292838E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.08728257E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78872206E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91689982E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59381160E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.00170803E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.62032559E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.34053273E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.37132505E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.82871816E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.04350965E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.00898585E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52758322E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.71859054E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41639243E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.62161254E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84111160E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.03152229E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.01461716E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.80999409E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.54941985E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45802549E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.72433497E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92450460E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12897543E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96797626E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.41877910E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.98027809E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79385230E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.38179979E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.57008549E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12897543E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96797626E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.41877910E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.98027809E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79385230E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.38179979E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.57008549E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44507004E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05679765E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.74065078E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.43365112E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53966529E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.41165242E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06338394E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.54359054E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33760989E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33760989E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11255007E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11255007E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09968008E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.23101041E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.62041272E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     9.06279812E-07    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.45651926E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.42835249E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.73433096E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.23693101E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.50227802E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.32869715E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.75229588E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.59566144E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18917488E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54272352E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18917488E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54272352E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34746804E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53866565E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53866565E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49017980E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07456268E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07456268E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07456258E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.67086559E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.67086559E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.67086559E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.67086559E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.22362267E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.22362267E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.22362267E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.22362267E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.19606091E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.19606091E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.15912450E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.45513285E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.29961757E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.20675666E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.53065089E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.20675666E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.53065089E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.49795515E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.24709600E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.24709600E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.29923716E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.15538752E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.15538752E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.15530699E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.19033345E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.84158798E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.19033345E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.84158798E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.75260017E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.51823985E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.51823985E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.25474132E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.30302620E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.30302620E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.30302621E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.19690406E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.19690406E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.19690406E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.19690406E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.98962668E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.98962668E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.98962668E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.98962668E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.87303228E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.87303228E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.22945070E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.29330022E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.38587478E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.21462848E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.23920440E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.23920440E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.94657324E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.01593168E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.34800588E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.76686123E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76686123E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.77220113E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.77220113E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03332396E-03   # h decays
#          BR         NDA      ID1       ID2
     5.84865397E-01    2           5        -5   # BR(h -> b       bb     )
     6.47427543E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29187601E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85820687E-04    2           3        -3   # BR(h -> s       sb     )
     2.11027433E-02    2           4        -4   # BR(h -> c       cb     )
     6.91235412E-02    2          21        21   # BR(h -> g       g      )
     2.40380164E-03    2          22        22   # BR(h -> gam     gam    )
     1.66939548E-03    2          22        23   # BR(h -> Z       gam    )
     2.26594505E-01    2          24       -24   # BR(h -> W+      W-     )
     2.87828533E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.58301141E+01   # H decays
#          BR         NDA      ID1       ID2
     3.72156530E-01    2           5        -5   # BR(H -> b       bb     )
     5.93873839E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.09979405E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48369405E-04    2           3        -3   # BR(H -> s       sb     )
     6.96529476E-08    2           4        -4   # BR(H -> c       cb     )
     6.98067561E-03    2           6        -6   # BR(H -> t       tb     )
     9.54207297E-07    2          21        21   # BR(H -> g       g      )
     8.79173522E-09    2          22        22   # BR(H -> gam     gam    )
     1.79825063E-09    2          23        22   # BR(H -> Z       gam    )
     1.57693553E-06    2          24       -24   # BR(H -> W+      W-     )
     7.87921580E-07    2          23        23   # BR(H -> Z       Z      )
     6.52819283E-06    2          25        25   # BR(H -> h       h      )
    -4.89855141E-24    2          36        36   # BR(H -> A       A      )
    -1.17785960E-20    2          23        36   # BR(H -> Z       A      )
     1.86787621E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.48709090E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.48709090E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.48850902E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.40715422E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.65260712E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.02703292E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.10522862E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.12515245E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.65366799E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.24905825E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.54831945E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.61964034E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.56059406E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.56059406E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40628554E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.58396307E+01   # A decays
#          BR         NDA      ID1       ID2
     3.72118690E-01    2           5        -5   # BR(A -> b       bb     )
     5.93775009E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09944296E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48368402E-04    2           3        -3   # BR(A -> s       sb     )
     7.01009898E-08    2           4        -4   # BR(A -> c       cb     )
     6.99383171E-03    2           6        -6   # BR(A -> t       tb     )
     1.43705508E-05    2          21        21   # BR(A -> g       g      )
     7.15424527E-08    2          22        22   # BR(A -> gam     gam    )
     1.58014542E-08    2          23        22   # BR(A -> Z       gam    )
     1.57332904E-06    2          23        25   # BR(A -> Z       h      )
     1.94433000E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.48632793E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.48632793E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.22897129E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.55037826E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.47360582E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.34084513E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.18518258E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.86272471E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.78570838E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.59913729E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.44934033E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.65243809E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.65243809E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.61843548E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.01305419E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.90282087E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08709284E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.84835145E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18237758E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43253091E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.82382760E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.56541780E-06    2          24        25   # BR(H+ -> W+      h      )
     2.56484027E-14    2          24        36   # BR(H+ -> W+      A      )
     4.15472358E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.20156749E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.81203769E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.47966406E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.96272474E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.47507569E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.97135346E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.26890666E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.20891099E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
