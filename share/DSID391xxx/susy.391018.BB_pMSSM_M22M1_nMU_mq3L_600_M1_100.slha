#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13385107E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03960835E+01   # W+
        25     1.25867577E+02   # h
        35     3.00013900E+03   # H
        36     2.99999994E+03   # A
        37     3.00110223E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02889391E+03   # ~d_L
   2000001     3.02347678E+03   # ~d_R
   1000002     3.02797473E+03   # ~u_L
   2000002     3.02607065E+03   # ~u_R
   1000003     3.02889391E+03   # ~s_L
   2000003     3.02347678E+03   # ~s_R
   1000004     3.02797473E+03   # ~c_L
   2000004     3.02607065E+03   # ~c_R
   1000005     6.84200487E+02   # ~b_1
   2000005     3.02346590E+03   # ~b_2
   1000006     6.81212692E+02   # ~t_1
   2000006     3.02116066E+03   # ~t_2
   1000011     3.00708941E+03   # ~e_L
   2000011     3.00054758E+03   # ~e_R
   1000012     3.00569198E+03   # ~nu_eL
   1000013     3.00708941E+03   # ~mu_L
   2000013     3.00054758E+03   # ~mu_R
   1000014     3.00569198E+03   # ~nu_muL
   1000015     2.98619938E+03   # ~tau_1
   2000015     3.02224038E+03   # ~tau_2
   1000016     3.00598096E+03   # ~nu_tauL
   1000021     2.33559187E+03   # ~g
   1000022     1.01144126E+02   # ~chi_10
   1000023     2.17072598E+02   # ~chi_20
   1000025    -3.00320451E+03   # ~chi_30
   1000035     3.00329580E+03   # ~chi_40
   1000024     2.17232088E+02   # ~chi_1+
   1000037     3.00418670E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891890E-01   # N_11
  1  2     7.64638444E-04   # N_12
  1  3    -1.46820598E-02   # N_13
  1  4     2.45387554E-04   # N_14
  2  1    -3.82262277E-04   # N_21
  2  2     9.99660833E-01   # N_22
  2  3     2.60361917E-02   # N_23
  2  4     4.35105914E-04   # N_24
  3  1     1.02188651E-02   # N_31
  3  2    -1.87119388E-02   # N_32
  3  3     7.06778844E-01   # N_33
  3  4     7.07113219E-01   # N_34
  4  1    -1.05658146E-02   # N_41
  4  2     1.80968891E-02   # N_42
  4  3    -7.06802816E-01   # N_43
  4  4     7.07100167E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99322642E-01   # U_11
  1  2     3.68002360E-02   # U_12
  2  1    -3.68002360E-02   # U_21
  2  2     9.99322642E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999812E-01   # V_11
  1  2    -6.13740870E-04   # V_12
  2  1    -6.13740870E-04   # V_21
  2  2    -9.99999812E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98614009E-01   # cos(theta_t)
  1  2    -5.26313692E-02   # sin(theta_t)
  2  1     5.26313692E-02   # -sin(theta_t)
  2  2     9.98614009E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99713499E-01   # cos(theta_b)
  1  2    -2.39357456E-02   # sin(theta_b)
  2  1     2.39357456E-02   # -sin(theta_b)
  2  2     9.99713499E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06901337E-01   # cos(theta_tau)
  1  2     7.07312166E-01   # sin(theta_tau)
  2  1    -7.07312166E-01   # -sin(theta_tau)
  2  2    -7.06901337E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00153553E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33851073E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44303571E+02   # higgs               
         4     1.10293073E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.33851073E+03  # The gauge couplings
     1     3.61765287E-01   # gprime(Q) DRbar
     2     6.39602934E-01   # g(Q) DRbar
     3     1.02890287E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33851073E+03  # The trilinear couplings
  1  1     1.68397196E-06   # A_u(Q) DRbar
  2  2     1.68399834E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33851073E+03  # The trilinear couplings
  1  1     5.92739006E-07   # A_d(Q) DRbar
  2  2     5.92811598E-07   # A_s(Q) DRbar
  3  3     1.24237751E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33851073E+03  # The trilinear couplings
  1  1     2.83181752E-07   # A_e(Q) DRbar
  2  2     2.83195967E-07   # A_mu(Q) DRbar
  3  3     2.87255341E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33851073E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55736887E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33851073E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.99769995E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33851073E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03273405E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33851073E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.05427552E+04   # M^2_Hd              
        22    -9.11098621E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42020966E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.85577255E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48045585E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48045585E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51954415E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51954415E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.74172020E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.30566642E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.96198862E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.90744474E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20352939E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.47462914E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.45711949E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.94446912E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.88847288E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.88737969E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70848053E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63288051E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.21590592E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.48333113E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.53354122E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.62390579E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.22274009E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.64793927E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.93458419E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -4.09934934E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62714417E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.57096253E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.48794622E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.22378299E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.71896340E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.10181262E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.60873172E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07444351E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.92335277E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64021953E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46135139E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.33563584E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28309180E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.61154101E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.01745487E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19886401E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58288687E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.29702981E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.44437223E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.71442157E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41711280E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.07949113E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.82569614E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64004447E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.36928068E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.22926803E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27735174E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.39649211E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02434497E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67937241E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.51193898E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.54748263E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.00375998E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.06368320E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54880602E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07444351E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.92335277E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64021953E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46135139E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.33563584E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28309180E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.61154101E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.01745487E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19886401E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58288687E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.29702981E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.44437223E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.71442157E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41711280E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.07949113E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.82569614E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64004447E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.36928068E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.22926803E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27735174E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.39649211E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02434497E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67937241E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.51193898E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.54748263E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.00375998E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.06368320E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54880602E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01977316E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.74081220E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00916012E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.39442851E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.84348334E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01675862E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.07009863E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55859431E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.44959395E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01977316E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.74081220E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00916012E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.39442851E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.84348334E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01675862E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.07009863E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55859431E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.44959395E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81754067E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45990996E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18063346E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35945658E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75909793E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54063407E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15359114E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.32555054E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.83019408E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30554384E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.93958668E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02008184E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.68300862E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01010450E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.99574304E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.47873195E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02159463E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.29836399E-13    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02008184E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.68300862E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01010450E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.99574304E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.47873195E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02159463E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.29836399E-13    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02081020E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.68218958E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00985465E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.22159786E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.62204561E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02192626E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.20802946E-08    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.12575316E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.70721565E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.44956739E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.55690228E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.08205808E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.00652361E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.89709601E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.85583399E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.17577885E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.81594980E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.71178335E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99103851E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.90203194E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.85695720E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.85695720E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.39594911E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.41491092E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34042378E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34042378E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.16090196E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.16090196E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.62991238E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.62991238E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.62991238E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.62991238E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.22364907E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.22364907E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.74479394E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.31790478E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.09524671E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.83682353E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.83682353E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16980035E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.92978960E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.24618694E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.24618694E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.13792626E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.13792626E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.99767547E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.99767547E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.99767547E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.99767547E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     6.72642594E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.72642594E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.74707204E-03   # h decays
#          BR         NDA      ID1       ID2
     5.51059816E-01    2           5        -5   # BR(h -> b       bb     )
     6.96844904E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.46681064E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.22878839E-04    2           3        -3   # BR(h -> s       sb     )
     2.27195087E-02    2           4        -4   # BR(h -> c       cb     )
     7.47260297E-02    2          21        21   # BR(h -> g       g      )
     2.58457468E-03    2          22        22   # BR(h -> gam     gam    )
     1.80050434E-03    2          22        23   # BR(h -> Z       gam    )
     2.45552155E-01    2          24       -24   # BR(h -> W+      W-     )
     3.11033609E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.97597658E+01   # H decays
#          BR         NDA      ID1       ID2
     9.06150829E-01    2           5        -5   # BR(H -> b       bb     )
     6.25459310E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.21147466E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.71569109E-04    2           3        -3   # BR(H -> s       sb     )
     7.61223039E-08    2           4        -4   # BR(H -> c       cb     )
     7.59369046E-03    2           6        -6   # BR(H -> t       tb     )
     5.64801033E-06    2          21        21   # BR(H -> g       g      )
     4.29166565E-08    2          22        22   # BR(H -> gam     gam    )
     3.10731342E-09    2          23        22   # BR(H -> Z       gam    )
     7.19998449E-07    2          24       -24   # BR(H -> W+      W-     )
     3.59555174E-07    2          23        23   # BR(H -> Z       Z      )
     5.05110667E-06    2          25        25   # BR(H -> h       h      )
     8.39562089E-25    2          36        36   # BR(H -> A       A      )
     1.35347318E-18    2          23        36   # BR(H -> Z       A      )
     8.39166093E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.89794019E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.19959447E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.56464175E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.16449464E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.41706455E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.89024366E+01   # A decays
#          BR         NDA      ID1       ID2
     9.26126044E-01    2           5        -5   # BR(A -> b       bb     )
     6.39218050E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.26011911E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.77589559E-04    2           3        -3   # BR(A -> s       sb     )
     7.83318388E-08    2           4        -4   # BR(A -> c       cb     )
     7.80980302E-03    2           6        -6   # BR(A -> t       tb     )
     2.29991226E-05    2          21        21   # BR(A -> g       g      )
     5.44079899E-08    2          22        22   # BR(A -> gam     gam    )
     2.26365764E-08    2          23        22   # BR(A -> Z       gam    )
     7.32998762E-07    2          23        25   # BR(A -> Z       h      )
     8.73077875E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.01528074E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.36912857E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.64715543E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.22884894E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47599171E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.88251719E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.07991460E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.44633536E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22231204E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51468891E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.28204988E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.75473891E-07    2          24        25   # BR(H+ -> W+      h      )
     5.06313624E-14    2          24        36   # BR(H+ -> W+      A      )
     4.85629895E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.12662477E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05264131E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
