#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15977945E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.53990000E+02   # M_1(MX)             
         2     9.07990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04053373E+01   # W+
        25     1.23665788E+02   # h
        35     3.00011712E+03   # H
        36     2.99999991E+03   # A
        37     3.00090396E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03887787E+03   # ~d_L
   2000001     3.03382608E+03   # ~d_R
   1000002     3.03797034E+03   # ~u_L
   2000002     3.03492511E+03   # ~u_R
   1000003     3.03887787E+03   # ~s_L
   2000003     3.03382608E+03   # ~s_R
   1000004     3.03797034E+03   # ~c_L
   2000004     3.03492511E+03   # ~c_R
   1000005     9.55079762E+02   # ~b_1
   2000005     3.03283547E+03   # ~b_2
   1000006     9.53572525E+02   # ~t_1
   2000006     3.01868650E+03   # ~t_2
   1000011     3.00623517E+03   # ~e_L
   2000011     3.00203464E+03   # ~e_R
   1000012     3.00484810E+03   # ~nu_eL
   1000013     3.00623517E+03   # ~mu_L
   2000013     3.00203464E+03   # ~mu_R
   1000014     3.00484810E+03   # ~nu_muL
   1000015     2.98544827E+03   # ~tau_1
   2000015     3.02205444E+03   # ~tau_2
   1000016     3.00462164E+03   # ~nu_tauL
   1000021     2.34815851E+03   # ~g
   1000022     4.56422134E+02   # ~chi_10
   1000023     9.43394325E+02   # ~chi_20
   1000025    -2.99585541E+03   # ~chi_30
   1000035     2.99701488E+03   # ~chi_40
   1000024     9.43556426E+02   # ~chi_1+
   1000037     2.99747091E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99879608E-01   # N_11
  1  2    -6.48895365E-04   # N_12
  1  3     1.52028082E-02   # N_13
  1  4    -3.03696039E-03   # N_14
  2  1     1.11811677E-03   # N_21
  2  2     9.99532212E-01   # N_22
  2  3    -2.88731322E-02   # N_23
  2  4     1.00224041E-02   # N_24
  3  1    -8.59159157E-03   # N_31
  3  2     1.33388311E-02   # N_32
  3  3     7.06893977E-01   # N_33
  3  4     7.07141545E-01   # N_34
  4  1    -1.28726043E-02   # N_41
  4  2     2.75138338E-02   # N_42
  4  3     7.06566431E-01   # N_43
  4  4    -7.06994458E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99165525E-01   # U_11
  1  2    -4.08442498E-02   # U_12
  2  1     4.08442498E-02   # U_21
  2  2     9.99165525E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99899471E-01   # V_11
  1  2    -1.41791212E-02   # V_12
  2  1     1.41791212E-02   # V_21
  2  2     9.99899471E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98784972E-01   # cos(theta_t)
  1  2    -4.92806220E-02   # sin(theta_t)
  2  1     4.92806220E-02   # -sin(theta_t)
  2  2     9.98784972E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99907550E-01   # cos(theta_b)
  1  2     1.35974797E-02   # sin(theta_b)
  2  1    -1.35974797E-02   # -sin(theta_b)
  2  2     9.99907550E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06970117E-01   # cos(theta_tau)
  1  2     7.07243419E-01   # sin(theta_tau)
  2  1    -7.07243419E-01   # -sin(theta_tau)
  2  2     7.06970117E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01816436E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59779449E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44162326E+02   # higgs               
         4     7.51814659E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59779449E+03  # The gauge couplings
     1     3.62384637E-01   # gprime(Q) DRbar
     2     6.36549840E-01   # g(Q) DRbar
     3     1.02491167E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59779449E+03  # The trilinear couplings
  1  1     2.43316083E-06   # A_u(Q) DRbar
  2  2     2.43319445E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59779449E+03  # The trilinear couplings
  1  1     6.93393432E-07   # A_d(Q) DRbar
  2  2     6.93503628E-07   # A_s(Q) DRbar
  3  3     1.45095980E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59779449E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.35826605E-07   # A_mu(Q) DRbar
  3  3     1.37400235E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59779449E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50142867E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59779449E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.17571833E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59779449E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.08190004E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59779449E+03  # The soft SUSY breaking masses at the scale Q
         1     4.53990000E+02   # M_1(Q)              
         2     9.07990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.69340697E+04   # M^2_Hd              
        22    -9.03521594E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40627810E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.63811327E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48191135E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48191135E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51808865E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51808865E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.57059657E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.54960181E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.50398191E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.91954544E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.39610858E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.13128531E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.31137682E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.12211065E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.37603663E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.56247020E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.48572572E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.92280171E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.81763306E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.72831102E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.71688976E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.20640662E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.81225747E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.41073564E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.10679929E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.00435942E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.44231651E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28521364E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.95591842E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.26272613E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.11282192E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.43517738E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.20231802E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.48623540E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.16413918E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.39930931E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.97317804E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.88352676E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.47856164E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14333930E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55014693E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.66117569E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.02242007E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.91028244E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44985112E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.44067067E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.28448036E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.48434894E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.68266476E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.39868482E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.96756550E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.09087475E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.48523391E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65273602E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.39409348E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.70825462E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.42146105E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.11800046E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.56059011E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.43517738E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.20231802E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.48623540E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.16413918E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.39930931E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.97317804E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.88352676E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.47856164E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14333930E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55014693E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.66117569E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.02242007E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.91028244E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44985112E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.44067067E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.28448036E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.48434894E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.68266476E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.39868482E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.96756550E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.09087475E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.48523391E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65273602E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.39409348E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.70825462E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.42146105E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.11800046E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.56059011E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.32510833E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.12436016E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.96265474E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.23579107E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.56970807E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.91298461E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.12146405E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.49657257E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998932E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.06433549E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.30848210E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.93918149E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.32510833E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.12436016E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.96265474E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.23579107E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.56970807E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.91298461E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.12146405E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.49657257E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998932E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.06433549E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.30848210E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.93918149E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43404179E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.83840326E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.05685261E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.10474413E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.38489307E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.92803039E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.02695278E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.59939168E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.76025864E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04449968E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.81174338E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.32505335E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12894710E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95320692E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.32369264E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.19884023E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.91784578E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.52247160E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.32505335E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12894710E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95320692E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.32369264E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.19884023E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.91784578E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.52247160E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.32503642E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12885973E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95290204E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.10931011E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.13070086E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.91822065E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.74280085E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.74488952E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.11250835E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.88290475E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.07405022E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.90404752E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.26031623E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.30212776E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.06746910E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.74664090E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.47581706E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.07167586E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.39283241E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.11927852E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.64531008E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.86168324E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.04825404E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.04825404E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.59695863E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.40410039E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.57910275E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.57910275E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.32529418E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.32529418E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.95179126E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.95179126E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.05161168E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.39335840E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.39392059E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.11376401E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.11376401E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.73096584E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.38186056E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.54095090E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.54095090E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.35363703E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.35363703E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.85959428E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.85959428E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.62887188E-03   # h decays
#          BR         NDA      ID1       ID2
     6.89415047E-01    2           5        -5   # BR(h -> b       bb     )
     5.57892690E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.97500846E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.20039322E-04    2           3        -3   # BR(h -> s       sb     )
     1.81318044E-02    2           4        -4   # BR(h -> c       cb     )
     5.77736534E-02    2          21        21   # BR(h -> g       g      )
     1.94302309E-03    2          22        22   # BR(h -> gam     gam    )
     1.19580499E-03    2          22        23   # BR(h -> Z       gam    )
     1.56040022E-01    2          24       -24   # BR(h -> W+      W-     )
     1.90938353E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39702131E+01   # H decays
#          BR         NDA      ID1       ID2
     7.48467496E-01    2           5        -5   # BR(H -> b       bb     )
     1.78003867E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29379138E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72878226E-04    2           3        -3   # BR(H -> s       sb     )
     2.18087410E-07    2           4        -4   # BR(H -> c       cb     )
     2.17556214E-02    2           6        -6   # BR(H -> t       tb     )
     2.51451691E-05    2          21        21   # BR(H -> g       g      )
     4.11401546E-08    2          22        22   # BR(H -> gam     gam    )
     8.38855997E-09    2          23        22   # BR(H -> Z       gam    )
     3.14777392E-05    2          24       -24   # BR(H -> W+      W-     )
     1.57194714E-05    2          23        23   # BR(H -> Z       Z      )
     8.19914214E-05    2          25        25   # BR(H -> h       h      )
    -3.59357620E-23    2          36        36   # BR(H -> A       A      )
     1.83324782E-18    2          23        36   # BR(H -> Z       A      )
     1.37528510E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.02076060E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.86145640E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.66994891E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.74798602E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.79490228E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33318497E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84381472E-01    2           5        -5   # BR(A -> b       bb     )
     1.86524300E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59504430E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.10008387E-04    2           3        -3   # BR(A -> s       sb     )
     2.28572886E-07    2           4        -4   # BR(A -> c       cb     )
     2.27890631E-02    2           6        -6   # BR(A -> t       tb     )
     6.71116107E-05    2          21        21   # BR(A -> g       g      )
     7.83913493E-08    2          22        22   # BR(A -> gam     gam    )
     6.61591104E-08    2          23        22   # BR(A -> Z       gam    )
     3.28632616E-05    2          23        25   # BR(A -> Z       h      )
     2.55626859E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22690285E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.27512207E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.81223295E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03011320E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.11059970E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.41474754E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.53795833E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.10782698E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.01758033E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03227762E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.22073132E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.25970976E-05    2          24        25   # BR(H+ -> W+      h      )
     7.71396374E-14    2          24        36   # BR(H+ -> W+      A      )
     1.77733847E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.99915131E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.15782192E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
