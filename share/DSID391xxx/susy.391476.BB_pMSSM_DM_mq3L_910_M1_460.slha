#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13015889E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.77990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04189973E+01   # W+
        25     1.25980936E+02   # h
        35     4.00000799E+03   # H
        36     3.99999721E+03   # A
        37     4.00103419E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02593248E+03   # ~d_L
   2000001     4.02257292E+03   # ~d_R
   1000002     4.02528235E+03   # ~u_L
   2000002     4.02334391E+03   # ~u_R
   1000003     4.02593248E+03   # ~s_L
   2000003     4.02257292E+03   # ~s_R
   1000004     4.02528235E+03   # ~c_L
   2000004     4.02334391E+03   # ~c_R
   1000005     9.70992381E+02   # ~b_1
   2000005     4.02546130E+03   # ~b_2
   1000006     9.48660707E+02   # ~t_1
   2000006     1.90142132E+03   # ~t_2
   1000011     4.00455404E+03   # ~e_L
   2000011     4.00335195E+03   # ~e_R
   1000012     4.00344319E+03   # ~nu_eL
   1000013     4.00455404E+03   # ~mu_L
   2000013     4.00335195E+03   # ~mu_R
   1000014     4.00344319E+03   # ~nu_muL
   1000015     4.00397314E+03   # ~tau_1
   2000015     4.00818829E+03   # ~tau_2
   1000016     4.00486291E+03   # ~nu_tauL
   1000021     1.97812856E+03   # ~g
   1000022     4.45467375E+02   # ~chi_10
   1000023    -4.89758925E+02   # ~chi_20
   1000025     5.07399871E+02   # ~chi_30
   1000035     2.05210454E+03   # ~chi_40
   1000024     4.87637160E+02   # ~chi_1+
   1000037     2.05226855E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.89459864E-01   # N_11
  1  2    -1.93093864E-02   # N_12
  1  3    -4.54084748E-01   # N_13
  1  4    -4.12537648E-01   # N_14
  2  1    -3.48040167E-02   # N_21
  2  2     2.31834409E-02   # N_22
  2  3    -7.04838030E-01   # N_23
  2  4     7.08134563E-01   # N_24
  3  1     6.12813590E-01   # N_31
  3  2     2.80561312E-02   # N_32
  3  3     5.44931877E-01   # N_33
  3  4     5.71595667E-01   # N_34
  4  1    -1.14328673E-03   # N_41
  4  2     9.99150904E-01   # N_42
  4  3    -7.72276464E-03   # N_43
  4  4    -4.04539570E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.09277181E-02   # U_11
  1  2     9.99940291E-01   # U_12
  2  1    -9.99940291E-01   # U_21
  2  2     1.09277181E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.72195437E-02   # V_11
  1  2    -9.98361620E-01   # V_12
  2  1    -9.98361620E-01   # V_21
  2  2    -5.72195437E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89864751E-01   # cos(theta_t)
  1  2    -1.42013291E-01   # sin(theta_t)
  2  1     1.42013291E-01   # -sin(theta_t)
  2  2     9.89864751E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998784E-01   # cos(theta_b)
  1  2    -1.55948662E-03   # sin(theta_b)
  2  1     1.55948662E-03   # -sin(theta_b)
  2  2     9.99998784E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06218962E-01   # cos(theta_tau)
  1  2     7.07993487E-01   # sin(theta_tau)
  2  1    -7.07993487E-01   # -sin(theta_tau)
  2  2    -7.06218962E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00255511E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30158885E+03  # DRbar Higgs Parameters
         1    -4.77990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43652894E+02   # higgs               
         4     1.61350115E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30158885E+03  # The gauge couplings
     1     3.61874161E-01   # gprime(Q) DRbar
     2     6.35773160E-01   # g(Q) DRbar
     3     1.03008352E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30158885E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37046407E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30158885E+03  # The trilinear couplings
  1  1     5.05898242E-07   # A_d(Q) DRbar
  2  2     5.05944710E-07   # A_s(Q) DRbar
  3  3     9.04300514E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30158885E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.12409144E-07   # A_mu(Q) DRbar
  3  3     1.13556912E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30158885E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67992687E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30158885E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87557849E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30158885E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02987444E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30158885E+03  # The soft SUSY breaking masses at the scale Q
         1     4.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56636868E+07   # M^2_Hd              
        22    -1.89254416E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40225173E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.94072142E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40010481E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40010481E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59989519E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59989519E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.61599946E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.71326574E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.12565903E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.01841538E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.14265984E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14096207E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.95890353E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.06669331E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.55283813E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.09282531E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.69763298E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.28963813E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.50203611E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.16429320E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.32520206E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.25097762E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.15266281E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.82711575E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66394844E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58074914E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83018270E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58426487E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.07118581E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66977263E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.11385630E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12173180E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.49597616E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.42825154E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.72306002E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.21249997E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78146216E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45526625E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.33817649E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76786995E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81155542E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.57097974E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61101499E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51970680E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60352167E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.40737852E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.58910514E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.03840005E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.02818542E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45476283E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78166229E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.44438884E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.54703887E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.60474154E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81706315E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.03202208E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64409244E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52187790E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53728369E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.88367111E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.71789921E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.31447419E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.04995363E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85784665E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78146216E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45526625E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.33817649E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76786995E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81155542E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.57097974E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61101499E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51970680E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60352167E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.40737852E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.58910514E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.03840005E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.02818542E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45476283E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78166229E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.44438884E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.54703887E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.60474154E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81706315E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.03202208E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64409244E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52187790E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53728369E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.88367111E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.71789921E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.31447419E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.04995363E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85784665E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13576784E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.26232200E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.66980022E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.06027623E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78368592E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.19022149E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58280734E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02889060E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.24998102E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20837369E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73792794E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.30422465E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13576784E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.26232200E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.66980022E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.06027623E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78368592E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.19022149E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58280734E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02889060E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.24998102E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20837369E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73792794E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.30422465E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06701610E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25207247E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.50154925E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.50606364E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40905636E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56713158E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82593945E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05800297E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.26965636E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.92022881E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.31340156E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44399839E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.84980974E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89593983E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13682478E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09936666E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.20354838E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.11110613E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78813059E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.26124554E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55957613E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13682478E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09936666E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.20354838E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.11110613E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78813059E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.26124554E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55957613E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45788475E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97663332E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.35215189E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.63829835E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53143187E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.50971178E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04775163E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.24453549E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33654277E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33654277E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11219542E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11219542E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10252363E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.08997576E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.56968088E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.02315982E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.39308254E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.71413968E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.33968531E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.51871559E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.17062263E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.62417944E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.37928003E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.41995344E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18436861E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53644504E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18436861E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53644504E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38406734E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52400666E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52400666E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48480438E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04529862E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04529862E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04529850E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.63440098E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.63440098E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.63440098E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.63440098E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.78134315E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.78134315E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.78134315E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.78134315E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.14869170E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.14869170E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.77571871E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.14018382E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.57282014E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.43932946E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.21121388E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.43932946E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.21121388E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.15526462E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.67450133E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.67450133E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.68712220E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.60533578E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.60533578E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.60530521E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.77975857E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.30888093E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.77975857E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.30888093E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.13261533E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.29596087E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.29596087E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.99935704E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.05867428E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.05867428E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.05867429E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.07181090E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.07181090E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.07181090E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.07181090E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.57265974E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.57265974E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.57265974E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.57265974E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.43322666E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.43322666E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.08836576E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.19597036E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.28166660E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.61493765E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.51826052E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.51826052E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.55994217E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.40433918E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63992091E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.74098218E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.74098218E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.74117159E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.74117159E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06690208E-03   # h decays
#          BR         NDA      ID1       ID2
     5.83996881E-01    2           5        -5   # BR(h -> b       bb     )
     6.42884934E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27578904E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82301831E-04    2           3        -3   # BR(h -> s       sb     )
     2.09479519E-02    2           4        -4   # BR(h -> c       cb     )
     6.86569816E-02    2          21        21   # BR(h -> g       g      )
     2.39655782E-03    2          22        22   # BR(h -> gam     gam    )
     1.67680577E-03    2          22        23   # BR(h -> Z       gam    )
     2.28267588E-01    2          24       -24   # BR(h -> W+      W-     )
     2.90588600E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56207042E+01   # H decays
#          BR         NDA      ID1       ID2
     3.66359791E-01    2           5        -5   # BR(H -> b       bb     )
     5.96109214E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10769780E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49304302E-04    2           3        -3   # BR(H -> s       sb     )
     6.99216610E-08    2           4        -4   # BR(H -> c       cb     )
     7.00760623E-03    2           6        -6   # BR(H -> t       tb     )
     4.33106636E-07    2          21        21   # BR(H -> g       g      )
     7.16703811E-09    2          22        22   # BR(H -> gam     gam    )
     1.80345645E-09    2          23        22   # BR(H -> Z       gam    )
     1.69880438E-06    2          24       -24   # BR(H -> W+      W-     )
     8.48813806E-07    2          23        23   # BR(H -> Z       Z      )
     6.85800873E-06    2          25        25   # BR(H -> h       h      )
     6.47823636E-26    2          36        36   # BR(H -> A       A      )
     2.67965041E-20    2          23        36   # BR(H -> Z       A      )
     1.85339410E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.53453882E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.53453882E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.47650419E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.34269132E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.62644545E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.24763787E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.43138249E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.93970072E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.48556951E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.22434604E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.23397557E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.43697782E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.71901189E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     9.93703563E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.93703563E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.34956273E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56171684E+01   # A decays
#          BR         NDA      ID1       ID2
     3.66408861E-01    2           5        -5   # BR(A -> b       bb     )
     5.96149981E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10784028E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49361826E-04    2           3        -3   # BR(A -> s       sb     )
     7.03813796E-08    2           4        -4   # BR(A -> c       cb     )
     7.02180562E-03    2           6        -6   # BR(A -> t       tb     )
     1.44280319E-05    2          21        21   # BR(A -> g       g      )
     6.69025412E-08    2          22        22   # BR(A -> gam     gam    )
     1.58652571E-08    2          23        22   # BR(A -> Z       gam    )
     1.69530711E-06    2          23        25   # BR(A -> Z       h      )
     1.89688212E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.53437406E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.53437406E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.17244988E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.64995589E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.40899897E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.66458639E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.84168169E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.39103104E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.63700898E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.07212699E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.92697118E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.07839041E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.07839041E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.58521663E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.90318234E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.93795581E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09951570E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.77803351E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18941468E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44700849E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.75561427E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.69007857E-06    2          24        25   # BR(H+ -> W+      h      )
     2.82512663E-14    2          24        36   # BR(H+ -> W+      A      )
     4.81436485E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.95728638E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.42855188E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.53019040E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.31794596E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.52321437E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.14489880E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     7.38275750E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.07907420E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
