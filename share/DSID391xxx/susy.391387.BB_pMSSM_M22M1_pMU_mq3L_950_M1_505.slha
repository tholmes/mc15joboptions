#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16891462E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.04990000E+02   # M_1(MX)             
         2     1.00990000E+03   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04057198E+01   # W+
        25     1.23473844E+02   # h
        35     3.00008506E+03   # H
        36     2.99999999E+03   # A
        37     3.00088675E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04211726E+03   # ~d_L
   2000001     3.03694422E+03   # ~d_R
   1000002     3.04121181E+03   # ~u_L
   2000002     3.03786209E+03   # ~u_R
   1000003     3.04211726E+03   # ~s_L
   2000003     3.03694422E+03   # ~s_R
   1000004     3.04121181E+03   # ~c_L
   2000004     3.03786209E+03   # ~c_R
   1000005     1.05836184E+03   # ~b_1
   2000005     3.03581387E+03   # ~b_2
   1000006     1.05696816E+03   # ~t_1
   2000006     3.02112205E+03   # ~t_2
   1000011     3.00630569E+03   # ~e_L
   2000011     3.00223582E+03   # ~e_R
   1000012     3.00491977E+03   # ~nu_eL
   1000013     3.00630569E+03   # ~mu_L
   2000013     3.00223582E+03   # ~mu_R
   1000014     3.00491977E+03   # ~nu_muL
   1000015     2.98543652E+03   # ~tau_1
   2000015     3.02210603E+03   # ~tau_2
   1000016     3.00461645E+03   # ~nu_tauL
   1000021     2.35229090E+03   # ~g
   1000022     5.07226515E+02   # ~chi_10
   1000023     1.04677045E+03   # ~chi_20
   1000025    -2.99433848E+03   # ~chi_30
   1000035     2.99562385E+03   # ~chi_40
   1000024     1.04693288E+03   # ~chi_1+
   1000037     2.99604783E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99877243E-01   # N_11
  1  2    -6.26071871E-04   # N_12
  1  3     1.53015688E-02   # N_13
  1  4    -3.31200691E-03   # N_14
  2  1     1.11618269E-03   # N_21
  2  2     9.99497702E-01   # N_22
  2  3    -2.96070438E-02   # N_23
  2  4     1.12481816E-02   # N_24
  3  1    -8.46753692E-03   # N_31
  3  2     1.29911527E-02   # N_32
  3  3     7.06900860E-01   # N_33
  3  4     7.07142634E-01   # N_34
  4  1    -1.31359933E-02   # N_41
  4  2     2.88995239E-02   # N_42
  4  3     7.06527041E-01   # N_43
  4  4    -7.06973694E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99122559E-01   # U_11
  1  2    -4.18821315E-02   # U_12
  2  1     4.18821315E-02   # U_21
  2  2     9.99122559E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99873380E-01   # V_11
  1  2    -1.59130257E-02   # V_12
  2  1     1.59130257E-02   # V_21
  2  2     9.99873380E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98736108E-01   # cos(theta_t)
  1  2    -5.02611836E-02   # sin(theta_t)
  2  1     5.02611836E-02   # -sin(theta_t)
  2  2     9.98736108E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99902532E-01   # cos(theta_b)
  1  2     1.39616081E-02   # sin(theta_b)
  2  1    -1.39616081E-02   # -sin(theta_b)
  2  2     9.99902532E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06977977E-01   # cos(theta_tau)
  1  2     7.07235562E-01   # sin(theta_tau)
  2  1    -7.07235562E-01   # -sin(theta_tau)
  2  2     7.06977977E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01784518E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68914621E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44061775E+02   # higgs               
         4     7.69861537E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68914621E+03  # The gauge couplings
     1     3.62570839E-01   # gprime(Q) DRbar
     2     6.36378852E-01   # g(Q) DRbar
     3     1.02364741E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68914621E+03  # The trilinear couplings
  1  1     2.71941067E-06   # A_u(Q) DRbar
  2  2     2.71944733E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68914621E+03  # The trilinear couplings
  1  1     7.91153648E-07   # A_d(Q) DRbar
  2  2     7.91274928E-07   # A_s(Q) DRbar
  3  3     1.63037387E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68914621E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.52610771E-07   # A_mu(Q) DRbar
  3  3     1.54370678E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68914621E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.48683148E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68914621E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.18633437E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68914621E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.08557237E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68914621E+03  # The soft SUSY breaking masses at the scale Q
         1     5.04990000E+02   # M_1(Q)              
         2     1.00990000E+03   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.78697078E+04   # M^2_Hd              
        22    -9.01626956E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40550596E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.08742285E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48154597E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48154597E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51845403E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51845403E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.50160133E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.63655634E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.63443655E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.71641476E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.47307618E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.40716974E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.87057549E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.29879498E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.43086908E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.54123693E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.47415690E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.89185293E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.65010299E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.77969863E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.20301365E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.18701554E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.79786236E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.51684820E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.68551565E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.55422085E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.76245302E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29462291E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.93075668E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.24333704E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.07654231E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.28985278E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.29511168E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.44907938E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.57143113E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.31098779E-07    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.89886930E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.35178935E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.58909770E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.12547289E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54387972E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.58660228E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.08529473E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.46428208E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.45611834E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.29543486E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.37518757E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.44723721E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.38456952E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.92306275E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.89329123E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.72455547E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.59571059E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64035964E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.37253614E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.49296507E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.95104321E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.69208851E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.56274584E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.28985278E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.29511168E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.44907938E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.57143113E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.31098779E-07    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.89886930E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.35178935E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.58909770E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.12547289E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54387972E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.58660228E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.08529473E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.46428208E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.45611834E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.29543486E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.37518757E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.44723721E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.38456952E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.92306275E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.89329123E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.72455547E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.59571059E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64035964E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.37253614E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.49296507E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.95104321E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.69208851E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.56274584E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.17257695E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.16670344E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.94857239E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.60735258E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.80941339E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.88472345E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.21533110E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.48159942E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998975E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.01893950E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.09779470E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.54046954E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.17257695E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.16670344E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.94857239E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.60735258E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.80941339E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.88472345E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.21533110E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.48159942E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998975E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.01893950E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.09779470E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.54046954E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.34879548E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.93733470E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.02387139E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.03879392E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.30234595E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.02843922E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.99345054E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.87228684E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.04739977E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.97750570E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.12583510E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.17243992E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17129062E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.93910050E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.98557133E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.92760315E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.88960856E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.71822984E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.17243992E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17129062E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.93910050E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.98557133E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.92760315E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.88960856E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.71822984E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.17229375E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17121249E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.93877533E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.64872826E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.80430955E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.88998541E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.65451891E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.00408848E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.81422730E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.90671719E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.99261622E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.14999784E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.47461108E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.39167276E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.27357089E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.02723438E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.69231775E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.29688998E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.37031100E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.81722492E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.74676226E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     6.20214687E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.25596972E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.25596972E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.53641140E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.27874528E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54226960E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54226960E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.33066481E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.33066481E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.16600504E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.16600504E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.75364545E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.32018426E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.26855781E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.32298049E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.32298049E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.84127038E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.79301972E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49996662E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.49996662E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.35988049E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.35988049E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.96962021E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.96962021E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.57892362E-03   # h decays
#          BR         NDA      ID1       ID2
     6.90303674E-01    2           5        -5   # BR(h -> b       bb     )
     5.63029246E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.99320016E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.24037522E-04    2           3        -3   # BR(h -> s       sb     )
     1.83067108E-02    2           4        -4   # BR(h -> c       cb     )
     5.81061440E-02    2          21        21   # BR(h -> g       g      )
     1.95189837E-03    2          22        22   # BR(h -> gam     gam    )
     1.18751419E-03    2          22        23   # BR(h -> Z       gam    )
     1.54385860E-01    2          24       -24   # BR(h -> W+      W-     )
     1.88319173E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39809583E+01   # H decays
#          BR         NDA      ID1       ID2
     7.51089045E-01    2           5        -5   # BR(H -> b       bb     )
     1.77865217E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28888903E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72277310E-04    2           3        -3   # BR(H -> s       sb     )
     2.17890078E-07    2           4        -4   # BR(H -> c       cb     )
     2.17359311E-02    2           6        -6   # BR(H -> t       tb     )
     2.15882765E-05    2          21        21   # BR(H -> g       g      )
     3.41955880E-08    2          22        22   # BR(H -> gam     gam    )
     8.39243188E-09    2          23        22   # BR(H -> Z       gam    )
     3.05596024E-05    2          24       -24   # BR(H -> W+      W-     )
     1.52609690E-05    2          23        23   # BR(H -> Z       Z      )
     8.02101779E-05    2          25        25   # BR(H -> h       h      )
    -3.56811563E-23    2          36        36   # BR(H -> A       A      )
     2.38961405E-19    2          23        36   # BR(H -> Z       A      )
     1.12585695E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.94510971E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.61760227E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.26002947E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.54421722E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.51810471E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33750912E+01   # A decays
#          BR         NDA      ID1       ID2
     7.85193668E-01    2           5        -5   # BR(A -> b       bb     )
     1.85921274E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.57372278E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.07389661E-04    2           3        -3   # BR(A -> s       sb     )
     2.27833918E-07    2           4        -4   # BR(A -> c       cb     )
     2.27153868E-02    2           6        -6   # BR(A -> t       tb     )
     6.68946391E-05    2          21        21   # BR(A -> g       g      )
     9.42165370E-08    2          22        22   # BR(A -> gam     gam    )
     6.59495641E-08    2          23        22   # BR(A -> Z       gam    )
     3.18275575E-05    2          23        25   # BR(A -> Z       h      )
     2.47595781E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22580502E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.23510851E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.72151529E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04107407E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.11795657E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38931034E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.44801859E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.15491092E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96472830E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02140426E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.26359407E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.09519663E-05    2          24        25   # BR(H+ -> W+      h      )
     6.93030446E-14    2          24        36   # BR(H+ -> W+      A      )
     1.69598729E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.80937450E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.99316533E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
