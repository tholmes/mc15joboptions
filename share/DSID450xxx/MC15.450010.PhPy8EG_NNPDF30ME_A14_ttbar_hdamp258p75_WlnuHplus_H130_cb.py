include("MC15JobOptions/PowhegControl_ttHplus_NLO.py")
genSeq.Pythia8.Commands += [
    "Higgs:useBSM = on",
    "37:onMode = off",
    '37:addChannel = 1 1. 0 4 -5' #because there are no default decays to cb
]

