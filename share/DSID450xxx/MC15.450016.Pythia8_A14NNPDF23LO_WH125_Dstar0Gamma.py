evgenConfig.description = "PYTHIA8, WH, W->any, H->Dstar0Gamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "mH125" ]
evgenConfig.contact = ["Rhys Owen <rhys.owen@cern.ch>"]
evgenConfig.generators  = [ "Pythia8" ]

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]

genSeq.Pythia8.UserModes += [ 'WeakZ0:gmZmode = 2']
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 423 22',
                             '25:addChannel = 1 0.002 100 -423 22',
                             '421:onMode = off',
                             '421:onIfMatch = -321 211',
                             '-421:onMode = off',
                             '-421:onIfMatch = 321 -211',
                             ]
