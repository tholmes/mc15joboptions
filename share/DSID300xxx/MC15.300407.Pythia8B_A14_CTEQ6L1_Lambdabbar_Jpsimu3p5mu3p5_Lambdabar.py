#######################################################################
# Job options for Pythia8B_i generation of Lambda_b->J/psi(mumu)Lambda
#######################################################################
evgenConfig.description = "Signal anti-Lambda_b->J/psi(mumu)anti-Lambda"
evgenConfig.keywords = ["exclusive","Jpsi","2muon","Lambda_b0","Lambda"]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands += ['5122:m0 = 5.61951'] #PDG2014
genSeq.Pythia8B.Commands += ['5122:tau0 = 0.4395'] #PDG2014
genSeq.Pythia8B.Commands += ['5122:17:onMode = on']
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [-5122,443,-13,13,-3122]
genSeq.Pythia8B.OutputLevel = INFO

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.TriggerOppositeCharges = True
genSeq.Pythia8B.MinimumCountPerCut = [2]

include("MC15JobOptions/ParentChildwStatusFilter.py")
filtSeq.ParentChildwStatusFilter.PDGParent  = [5122]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
filtSeq.ParentChildwStatusFilter.PtMinParent =  0.0
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 1000.
filtSeq.ParentChildwStatusFilter.PDGChild = [3122]
filtSeq.ParentChildwStatusFilter.PtMinChild = 2000.
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 1000.

