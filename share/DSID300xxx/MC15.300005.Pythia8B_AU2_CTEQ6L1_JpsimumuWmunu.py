##############################################################
# J/psi(mu2.5mu2.5)X + W->munu  
# C. Burton
# For the study of associated production W+J/\psi at 13TeV
##############################################################

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")

evgenConfig.description = "pp->Jpsi(mumu) + W->munu via second hard process"
evgenConfig.keywords = ["charmonium","2muon","Jpsi","W"]
evgenConfig.minevents = 5000

include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.Commands += ['24:onMode = off']
genSeq.Pythia8B.Commands += ['24:7:onMode = on']
genSeq.Pythia8B.Commands += ['SecondHard:generate = on']
genSeq.Pythia8B.Commands += ['SecondHard:SingleW = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
genSeq.Pythia8B.SignalPtCuts = [0.0,2.0,2.0]
genSeq.Pythia8B.SignalEtaCuts = [102.5,2.5,2.5]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [20.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [1]
