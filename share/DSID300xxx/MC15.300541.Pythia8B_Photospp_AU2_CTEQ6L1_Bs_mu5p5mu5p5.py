##############################################################
# JO intended for electron decay channel studies.
# Pythia8B_i generation with ISR/FSR for decay:
# Bs -> (mu5.5mu5.5) (flat angles)
##############################################################

evgenConfig.description = "Exclusive Bs->mu5p5mu5p5 production"
evgenConfig.keywords    = ["exclusive", "Bs", "2muon", "rareDecay"]
evgenConfig.minevents   = 1000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 11.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands      += ['531:addChannel = 2 1.0 0 -13 13']
genSeq.Pythia8B.SignalPDGCodes = [531, -13, 13]

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [5.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]
