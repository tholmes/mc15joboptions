##############################################################
# Sample for B to lepton-lepton X study
# Pythia8B_i generation with ISR/FSR for decay:
#  B- -> pi- pi0(eeg) 
##############################################################
f = open("Bm_Pim_PI0_EEG_USER.DEC","w") 
f.write("noPhotos\n") 

f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_pi0 pi0\n")
f.write("Decay  my_pi0\n")
f.write("1.0000   e+  e-  gamma    PI0_DALITZ; #[Reconstructed PDG2011]\n")
f.write("Enddecay\n")
f.write("Decay B-\n")
f.write("1.0000   my_pi0   pi-   PHSP;\n")
f.write("Enddecay\n") 
f.write("End\n") 
f.close() 
##############################################################
evgenConfig.description = "Exclusive B- -> pi- pi0(eeg)  production"
evgenConfig.keywords    = ["exclusive", "bottom", "2electron"]
evgenConfig.minevents   = 100

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7']
genSeq.Pythia8B.NDecayLoops = 1
genSeq.Pythia8B.NHadronizationLoops = 2
        
genSeq.Pythia8B.QuarkPtCut      = 7.5
genSeq.Pythia8B.QuarkEtaCut     = 3.0
genSeq.Pythia8B.AntiQuarkPtCut  = 0.0
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [-521]
genSeq.EvtInclusiveDecay.userDecayFile = "Bm_Pim_PI0_EEG_USER.DEC"
filtSeq.BSignalFilter.LVL1MuonCutOn  = False
filtSeq.BSignalFilter.LVL2MuonCutOn  = False

if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 2
filtSeq.MultiElectronFilter.Ptcut      = 4000.0
filtSeq.MultiElectronFilter.Etacut     = 2.7
filtSeq.BSignalFilter.B_PDGCode = -521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7
