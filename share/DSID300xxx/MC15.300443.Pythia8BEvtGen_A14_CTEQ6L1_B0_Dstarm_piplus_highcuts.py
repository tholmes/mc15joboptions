##############################################################
f = open("B0_DSTARMPI_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias myD*- D*-\n")
f.write("Alias myanti-D0 anti-D0\n")
f.write("Decay B0\n")
f.write("1.0000 myD*-   pi+       SVS;\n")
f.write("Enddecay\n")
f.write("Decay myD*-\n")
f.write("1.0000    myanti-D0 pi-            VSS;\n")
f.write("Enddecay\n")
f.write("Decay myanti-D0\n")
f.write("1.0000    K+  pi-            PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive B0 -> D*- (anti-D0 (K+pi-) pi-) pi+ production"
evgenConfig.keywords    = ["exclusive","Bplus"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']

genSeq.Pythia8B.QuarkPtCut      = 0.0
genSeq.Pythia8B.AntiQuarkPtCut  = 9.0
genSeq.Pythia8B.QuarkEtaCut     = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [511]

genSeq.EvtInclusiveDecay.userDecayFile = "B0_DSTARMPI_USER.DEC"

filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 800. 
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6
filtSeq.BSignalFilter.B_PDGCode = 511
