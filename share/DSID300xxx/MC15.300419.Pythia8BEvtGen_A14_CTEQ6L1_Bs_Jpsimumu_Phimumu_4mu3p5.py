##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B_s -> (Jpsi->mumu)(Phi->mumu)
##############################################################
f = open("Bs_JPsiPhi_4mu_USER.DEC","w")
f.write("Define dm_incohMix_B0 0.0\n")
f.write("Define dm_incohMix_B_s0 0.0\n")
f.write("Define Hp 0.49\n")
f.write("Define Hz 0.775\n")
f.write("Define Hm 0.4\n")
f.write("Define pHp 2.50\n")
f.write("Define pHz 0.0\n")
f.write("Define pHm -0.17\n")
f.write("Alias      MyJ/psi  J/psi\n")
f.write("Alias      MyPhi    phi\n")
f.write("Decay B_s0\n")
f.write("    1.000         MyJ/psi     MyPhi        PVV_CPLH 0.02 1 Hp pHp Hz pHz Hm pHm;\n")
f.write("Enddecay\n")
f.write("Decay MyJ/psi\n")
f.write("    1.000         mu+         mu-          VLL;\n")
f.write("Enddecay\n")
f.write("Decay MyPhi\n")
f.write("    1.000         mu+         mu-          VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
##############################################################

evgenConfig.description = "Exclusive Bs->Jpsi(mumu)Phi(mumu) production"
evgenConfig.keywords = ["exclusive","Bs","4muon"]
evgenConfig.minevents = 100

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")

### Put the content of MC15JobOptions/Pythia8B_exclusiveB_Common.py
### except actual closing B decays

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")
###
###

include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 11.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 5

# Make Pythia select the events with Bs
genSeq.Pythia8B.SignalPDGCodes = [531]

genSeq.EvtInclusiveDecay.userDecayFile = "Bs_JPsiPhi_4mu_USER.DEC"

# Final state selections
filtSeq.BSignalFilter.B_PDGCode = 531
filtSeq.BSignalFilter.Cuts_Final_mu_switch = True
filtSeq.BSignalFilter.Cuts_Final_mu_pT     = 3500.0
filtSeq.BSignalFilter.Cuts_Final_mu_eta    = 2.6
#filtSeq.BSignalFilter.LVL1MuonCutOn = True
#filtSeq.BSignalFilter.LVL2MuonCutOn = True
#filtSeq.BSignalFilter.LVL1MuonCutPT = 3500 
#filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
#filtSeq.BSignalFilter.LVL2MuonCutPT = 3500
#filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

