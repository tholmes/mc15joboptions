######################################################################
# SM QCD diphoton with Pythia 8 (binned in m_gg)
######################################################################

evgenConfig.description = "Pythia8 diphoton sample. gammagamma events with at least two photons with pT > 20 GeV, with mass cut 0<Mgg<55 GeV"
evgenConfig.process = "QCD direct diphoton production"
evgenConfig.keywords = ["diphoton","SM","egamma"]
evgenConfig.contact = ["frank.siegert@cern.ch"]
evgenConfig.minevents = 5000

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

## Configure Pythia
genSeq.Pythia8.Commands += ["PromptPhoton:ffbar2gammagamma = on",
                            "PromptPhoton:gg2gammagamma = on",
                            "PhaseSpace:pTHatMin = 18",
                            "PhaseSpace:mHatMin = 0.",
                            "PhaseSpace:mHatMax = 55."]
                            
#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

## Filter
if not hasattr( filtSeq, "DirectPhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
    filtSeq += DirectPhotonFilter()
    pass

DirectPhotonFilter = filtSeq.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 20000.
DirectPhotonFilter.Etacut = 2.7
DirectPhotonFilter.NPhotons = 2


