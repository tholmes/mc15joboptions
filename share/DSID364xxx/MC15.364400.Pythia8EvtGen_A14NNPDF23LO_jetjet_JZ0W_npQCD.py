# JO for Pythia 8 jet jet JZ0W slice

evgenConfig.description = "Dijet truth jet slice JZ0W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"
                            "HadronLevel:all = off",
                            "BeamRemnants:primordialKT = off",
                            "PartonLevel:MPI = off"]

include("MC15JobOptions/JetFilter_JZ0W.py")
