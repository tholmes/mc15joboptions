#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410049E+01   # W+
        25     1.26000000E+02   # h
        35     2.00398460E+03   # H
        36     2.00000000E+03   # A
        37     2.00152335E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.38578487E+03   # ~d_L
   2000001     4.38566225E+03   # ~d_R
   1000002     4.38551071E+03   # ~u_L
   2000002     4.38557551E+03   # ~u_R
   1000003     4.38578487E+03   # ~s_L
   2000003     4.38566225E+03   # ~s_R
   1000004     4.38551071E+03   # ~c_L
   2000004     4.38557551E+03   # ~c_R
   1000005     4.38514816E+03   # ~b_1
   2000005     4.38630048E+03   # ~b_2
   1000006     4.52193053E+03   # ~t_1
   2000006     4.85168521E+03   # ~t_2
   1000011     5.00008219E+03   # ~e_L
   2000011     5.00007608E+03   # ~e_R
   1000012     4.99984172E+03   # ~nu_eL
   1000013     5.00008219E+03   # ~mu_L
   2000013     5.00007608E+03   # ~mu_R
   1000014     4.99984172E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984172E+03   # ~nu_tauL
   1000021     4.51225453E+03   # ~g
   1000022     1.28271034E+03   # ~chi_10
   1000023    -1.33115983E+03   # ~chi_20
   1000025     1.36989715E+03   # ~chi_30
   1000035     3.12828027E+03   # ~chi_40
   1000024     1.32778589E+03   # ~chi_1+
   1000037     3.12827839E+03   # ~chi_2+
   1000039     5.62500000E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.18593319E-01   # N_11
  1  2    -2.92227348E-02   # N_12
  1  3     4.93124217E-01   # N_13
  1  4    -4.89487671E-01   # N_14
  2  1     3.28661037E-03   # N_21
  2  2    -3.48271373E-03   # N_22
  2  3    -7.06988870E-01   # N_23
  2  4    -7.07208460E-01   # N_24
  3  1     6.95421970E-01   # N_31
  3  2     3.17354198E-02   # N_32
  3  3    -5.06171032E-01   # N_33
  3  4     5.09089415E-01   # N_34
  4  1     1.05979294E-03   # N_41
  4  2    -9.99062944E-01   # N_42
  4  3    -2.80380217E-02   # N_43
  4  4     3.29542193E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.96268987E-02   # U_11
  1  2     9.99214546E-01   # U_12
  2  1     9.99214546E-01   # U_21
  2  2     3.96268987E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.65805363E-02   # V_11
  1  2     9.98914538E-01   # V_12
  2  1     9.98914538E-01   # V_21
  2  2     4.65805363E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99380764E-01   # cos(theta_t)
  1  2    -3.51864824E-02   # sin(theta_t)
  2  1     3.51864824E-02   # -sin(theta_t)
  2  2     9.99380764E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68426626E-01   # cos(theta_b)
  1  2     7.43778089E-01   # sin(theta_b)
  2  1    -7.43778089E-01   # -sin(theta_b)
  2  2     6.68426626E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03859140E-01   # cos(theta_tau)
  1  2     7.10339574E-01   # sin(theta_tau)
  2  1    -7.10339574E-01   # -sin(theta_tau)
  2  2     7.03859140E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90167592E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52145970E+02   # vev(Q)              
         4     3.25292320E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52785240E-01   # gprime(Q) DRbar
     2     6.27190509E-01   # g(Q) DRbar
     3     1.06610830E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02405147E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.70987322E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79811257E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.30743777E+06   # M^2_Hd              
        22    -6.07379498E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37055545E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.73281767E+00   # gluino decays
#          BR         NDA      ID1       ID2
     4.98989757E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.98989757E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99944048E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99944048E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.01124632E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.01124632E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.00619637E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.00619637E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.98989757E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.98989757E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99944048E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99944048E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.01124632E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.01124632E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.00619637E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.00619637E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     4.91433322E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     4.91433322E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.07208567E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.07208567E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.92786238E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     6.46920915E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.83272542E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.03052376E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.28544289E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     7.34998988E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.35623535E-03    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.46029720E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
    -1.62271000E-02    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
    -1.95279616E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     1.28621108E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.50041523E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23718793E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.39961078E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.64437401E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.46079480E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.31534873E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.88593598E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.66185859E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.32461665E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.28715871E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.57797605E-03    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.41651048E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.82020534E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.34831725E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.07297049E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.44506552E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     7.46303093E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.29679662E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     4.10982361E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.15714271E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.00566312E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.38374944E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.09718687E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.90337695E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.22780949E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     1.27457325E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.21256180E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.82308267E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.78903966E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.18415009E-01    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.66443298E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.36895720E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     7.71142427E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     5.22902710E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.07833353E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.77086174E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.33260438E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
#
#         PDG            Width
DECAY   1000001     1.27361086E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.93041543E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.80560620E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.03357648E-02    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.19007351E-01    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.37855773E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.37956116E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
#
#         PDG            Width
DECAY   2000001     1.92790269E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.22902422E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.07833361E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.77086462E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.33284816E-07    2     1000035         1   # BR(~d_R -> ~chi_40 d)
#
#         PDG            Width
DECAY   1000004     1.27457325E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.21256180E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.82308267E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.78903966E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.18415009E-01    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.66443298E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.36895720E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     7.71142427E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.22902710E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.07833353E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.77086174E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.33260438E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
#
#         PDG            Width
DECAY   1000003     1.27361086E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.93041543E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.80560620E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.03357648E-02    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.19007351E-01    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.37855773E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.37956116E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
#
#         PDG            Width
DECAY   2000003     1.92790269E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.22902422E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.07833361E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.77086462E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.33284816E-07    2     1000035         3   # BR(~s_R -> ~chi_40 s)
#
#         PDG            Width
DECAY   1000011     2.71402038E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.84580050E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.66164156E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.10296865E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.66163041E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.95593762E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.33124489E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.14026300E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.21345343E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07878029E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.78643388E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.81225273E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.71402038E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.84580050E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.66164156E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.10296865E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.66163041E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.95593762E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.33124489E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.14026300E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.21345343E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07878029E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.78643388E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.81225273E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43209150E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.86780241E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.70240950E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.68751559E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.46774017E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.23442719E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.93989468E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     4.67413392E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43052060E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.71453644E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.82723905E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.76302660E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.50341555E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.86956075E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.01130721E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.71410018E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18171226E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.76867945E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.96661660E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.66745367E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.70236373E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.32697190E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.71410018E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18171226E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.76867945E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.96661660E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.66745367E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.70236373E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.32697190E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.71687567E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18050505E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.76687261E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.95847810E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.66472867E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.71996874E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.32154210E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.17029839E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.33643136E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33615056E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33615056E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11205125E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11205125E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10353301E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.84094119E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53099800E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12491266E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46282854E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34231309E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53894768E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.82838957E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.03033317E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.47501391E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99444574E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89334565E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12208610E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.36151581E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.56583010E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49569658E-11    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.11371226E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.18516703E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18574717E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53533096E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18574717E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53533096E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40011369E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50628975E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50628975E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47138750E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00216128E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00216128E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00216128E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18461276E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18461276E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18461276E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18461276E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94870944E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94870944E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94870944E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94870944E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.96341906E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.96341906E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18075607E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.74343050E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     9.29484469E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.11100621E-06    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.73771695E-06    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.76633512E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.02655165E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.18017031E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.02655165E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.18017031E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.23784278E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.04507096E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.04507096E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.67666698E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.81519005E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.81519005E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.81519005E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.43504007E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.15294809E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.43504007E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.15294809E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.73762845E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.20051201E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.20051201E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.10049286E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.43795417E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.43795417E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.43795417E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.32753518E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.32753518E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.32753518E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.32753518E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.42510894E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.42510894E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.42510894E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.42510894E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.38620452E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.38620452E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.84096678E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.56960295E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51371235E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.73670058E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46374071E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46374071E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15172805E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.60568432E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37471500E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     6.78285706E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.14980841E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.95537627E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.97314315E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.42156740E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22310974E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02304695E-01    2           5        -5   # BR(h -> b       bb     )
     6.21623621E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20027661E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65536895E-04    2           3        -3   # BR(h -> s       sb     )
     2.00871450E-02    2           4        -4   # BR(h -> c       cb     )
     6.63585190E-02    2          21        21   # BR(h -> g       g      )
     2.30447926E-03    2          22        22   # BR(h -> gam     gam    )
     1.62479737E-03    2          22        23   # BR(h -> Z       gam    )
     2.16423074E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80493643E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78049848E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46418101E-03    2           5        -5   # BR(H -> b       bb     )
     2.46469327E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71360831E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11566317E-06    2           3        -3   # BR(H -> s       sb     )
     1.00670385E-05    2           4        -4   # BR(H -> c       cb     )
     9.96086190E-01    2           6        -6   # BR(H -> t       tb     )
     7.97613197E-04    2          21        21   # BR(H -> g       g      )
     2.71145511E-06    2          22        22   # BR(H -> gam     gam    )
     1.16017469E-06    2          23        22   # BR(H -> Z       gam    )
     3.24263882E-04    2          24       -24   # BR(H -> W+      W-     )
     1.61689706E-04    2          23        23   # BR(H -> Z       Z      )
     9.03666849E-04    2          25        25   # BR(H -> h       h      )
     7.09821297E-24    2          36        36   # BR(H -> A       A      )
     2.86691770E-11    2          23        36   # BR(H -> Z       A      )
     6.28127965E-12    2          24       -37   # BR(H -> W+      H-     )
     6.28127965E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82375731E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46691033E-03    2           5        -5   # BR(A -> b       bb     )
     2.43901891E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62281292E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13679340E-06    2           3        -3   # BR(A -> s       sb     )
     9.96193517E-06    2           4        -4   # BR(A -> c       cb     )
     9.97013040E-01    2           6        -6   # BR(A -> t       tb     )
     9.43691783E-04    2          21        21   # BR(A -> g       g      )
     3.17099746E-06    2          22        22   # BR(A -> gam     gam    )
     1.35259323E-06    2          23        22   # BR(A -> Z       gam    )
     3.15971521E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74467616E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34892391E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49242379E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81161846E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49332251E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45704275E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08733646E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99412744E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.23840353E-04    2          24        25   # BR(H+ -> W+      h      )
     2.88095297E-13    2          24        36   # BR(H+ -> W+      A      )
