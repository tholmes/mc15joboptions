#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419684E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399756E+03   # H
        36     2.00000000E+03   # A
        37     2.00151776E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.94269495E+03   # ~d_L
   2000001     4.94258484E+03   # ~d_R
   1000002     4.94244872E+03   # ~u_L
   2000002     4.94250685E+03   # ~u_R
   1000003     4.94269495E+03   # ~s_L
   2000003     4.94258484E+03   # ~s_R
   1000004     4.94244872E+03   # ~c_L
   2000004     4.94250685E+03   # ~c_R
   1000005     4.94187529E+03   # ~b_1
   2000005     4.94340586E+03   # ~b_2
   1000006     5.05426863E+03   # ~t_1
   2000006     5.34442760E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.06305949E+03   # ~g
   1000022     1.90930252E+03   # ~chi_10
   1000023    -1.96180158E+03   # ~chi_20
   1000025     1.99666393E+03   # ~chi_30
   1000035     3.12547633E+03   # ~chi_40
   1000024     1.95661634E+03   # ~chi_1+
   1000037     3.12546929E+03   # ~chi_2+
   1000039     2.71968750E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.35556101E-01   # N_11
  1  2    -4.32631954E-02   # N_12
  1  3     4.79354745E-01   # N_13
  1  4    -4.76764666E-01   # N_14
  2  1     2.23208743E-03   # N_21
  2  2    -3.05250477E-03   # N_22
  2  3    -7.07041518E-01   # N_23
  2  4    -7.07161928E-01   # N_24
  3  1     6.77455555E-01   # N_31
  3  2     5.06542918E-02   # N_32
  3  3    -5.17970781E-01   # N_33
  3  4     5.19802255E-01   # N_34
  4  1     2.49225813E-03   # N_41
  4  2    -9.97774083E-01   # N_42
  4  3    -4.49175963E-02   # N_43
  4  4     4.92247634E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.34344928E-02   # U_11
  1  2     9.97986004E-01   # U_12
  2  1     9.97986004E-01   # U_21
  2  2     6.34344928E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.95234433E-02   # V_11
  1  2     9.97580318E-01   # V_12
  2  1     9.97580318E-01   # V_21
  2  2     6.95234433E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99845487E-01   # cos(theta_t)
  1  2     1.75784563E-02   # sin(theta_t)
  2  1    -1.75784563E-02   # -sin(theta_t)
  2  2     9.99845487E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81195258E-01   # cos(theta_b)
  1  2     7.32101783E-01   # sin(theta_b)
  2  1    -7.32101783E-01   # -sin(theta_b)
  2  2     6.81195258E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954927E-01   # cos(theta_tau)
  1  2     7.09252107E-01   # sin(theta_tau)
  2  1    -7.09252107E-01   # -sin(theta_tau)
  2  2     7.04954927E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200215E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305867E+02   # vev(Q)              
         4     3.11222351E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52717859E-01   # gprime(Q) DRbar
     2     6.26763462E-01   # g(Q) DRbar
     3     1.08044698E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02490862E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71424567E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738296E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.86477537E+05   # M^2_Hd              
        22    -8.57550630E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36864870E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.22456075E-08   # gluino decays
#          BR         NDA      ID1       ID2
     5.14130237E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.37948714E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     5.27648844E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     6.30610877E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     9.97196411E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.06859736E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.05150507E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.86911433E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.87344777E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     4.32920062E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     9.97196411E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.06859736E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.05150507E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.86911433E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.87344777E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.32920062E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     9.75491354E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     4.36524407E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.10458983E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.34623859E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.34623859E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.34623859E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.34623859E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.61600007E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.62580484E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.97333056E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.51103242E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.83339291E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.11719928E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.57841085E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.05527399E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.45092762E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.72078584E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.24710484E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.49556286E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.88220064E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.97107636E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.34713597E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.77822111E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.71504615E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.17948921E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.81342689E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.16163086E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.40517754E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.60351246E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.11490643E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.39064743E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.45284622E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.85643349E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.41463673E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.04641306E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.87485887E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.46142128E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.19328966E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.54872940E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.09648508E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.49052970E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.70206823E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.27759218E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.45972573E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.15492619E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.99695713E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.06294361E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.59717070E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.06567153E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.36055099E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.41907696E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.72483222E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.95831835E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.86383038E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.20572207E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.99179955E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.81037340E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.26603339E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59838720E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.99703986E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.22008063E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.98564252E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.81970617E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.36732666E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.34417641E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.73124506E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.95877015E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.80774259E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.68554738E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.13413684E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.66648835E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.26347222E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89647880E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.99695713E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.06294361E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.59717070E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.06567153E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.36055099E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.41907696E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.72483222E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.95831835E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.86383038E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.20572207E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.99179955E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.81037340E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.26603339E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59838720E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.99703986E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.22008063E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.98564252E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.81970617E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.36732666E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.34417641E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.73124506E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.95877015E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.80774259E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.68554738E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.13413684E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.66648835E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.26347222E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89647880E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.62939460E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.44956364E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71641750E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.79286793E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73835213E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.28888559E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49449869E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.77963513E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.49032689E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.96002010E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.50959144E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.20673592E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.62939460E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.44956364E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71641750E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.79286793E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73835213E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.28888559E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49449869E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.77963513E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.49032689E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.96002010E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.50959144E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.20673592E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.20915550E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.72545947E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45419263E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.36634858E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61319947E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.36167893E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.23692148E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.20085918E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.20677493E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.59167763E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02319022E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.44021486E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64789354E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.82194880E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.30636883E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.62965005E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.13313138E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.87258551E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.73650670E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75305282E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.15083788E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.48855803E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.62965005E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.13313138E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.87258551E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.73650670E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75305282E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.15083788E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.48855803E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.63194932E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13214147E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.86396082E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.73149529E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75064776E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01702875E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48379231E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.47508556E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.50953161E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33589845E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33589845E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196731E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196731E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10425338E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.65784141E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53814456E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.05672515E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46116779E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.41820402E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52575847E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.26316099E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.28339743E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     3.02994511E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00720615E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88566308E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.07130770E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02550034E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.09040996E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.48503943E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.26395007E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.05690351E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18401803E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53321770E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18401803E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53321770E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41631767E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50205046E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50205046E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47088930E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99407331E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99407331E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99407331E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.79873465E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.79873465E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.79873465E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.79873465E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.26624504E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.26624504E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.26624504E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.26624504E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.29250083E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.29250083E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.68589260E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.98184867E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.25133309E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.10896808E-06    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.93885080E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.97305299E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.78701973E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.12715163E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.78701973E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.12715163E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.92149047E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.72466453E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.72466453E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.10164453E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.03736482E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.03736482E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.03736482E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.92629818E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.49441936E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.92629818E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.49441936E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.09703674E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.69755395E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.69755395E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.60392187E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.13787427E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.13787427E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.13787427E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.39947205E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.39947205E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.39947205E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.39947205E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.66490014E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.66490014E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.66490014E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.66490014E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.61952729E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.61952729E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.65776749E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.40554129E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52475285E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.91178616E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46910027E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46910027E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07850376E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.16278644E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.44806275E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.01213778E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.61773529E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.59376710E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.19597041E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.82318565E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22181264E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02231504E-01    2           5        -5   # BR(h -> b       bb     )
     6.21875181E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20116702E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65724437E-04    2           3        -3   # BR(h -> s       sb     )
     2.00924384E-02    2           4        -4   # BR(h -> c       cb     )
     6.63554682E-02    2          21        21   # BR(h -> g       g      )
     2.30451575E-03    2          22        22   # BR(h -> gam     gam    )
     1.62535603E-03    2          22        23   # BR(h -> Z       gam    )
     2.16459380E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80579782E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78094603E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46648948E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430976E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71225245E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548765E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668835E-05    2           4        -4   # BR(H -> c       cb     )
     9.96071210E-01    2           6        -6   # BR(H -> t       tb     )
     7.97751428E-04    2          21        21   # BR(H -> g       g      )
     2.71749526E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030514E-06    2          23        22   # BR(H -> Z       gam    )
     3.34076988E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66582920E-04    2          23        23   # BR(H -> Z       Z      )
     9.01526633E-04    2          25        25   # BR(H -> h       h      )
     7.23683071E-24    2          36        36   # BR(H -> A       A      )
     2.91355358E-11    2          23        36   # BR(H -> Z       A      )
     6.52076864E-12    2          24       -37   # BR(H -> W+      H-     )
     6.52076864E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380349E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46941150E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898945E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62270879E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677967E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181486E-06    2           4        -4   # BR(A -> c       cb     )
     9.97001000E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680387E-04    2          21        21   # BR(A -> g       g      )
     3.13388802E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290158E-06    2          23        22   # BR(A -> Z       gam    )
     3.25562003E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471600E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35413814E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239032E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150011E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49665974E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697152E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732227E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402916E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33667285E-04    2          24        25   # BR(H+ -> W+      h      )
     2.82845455E-13    2          24        36   # BR(H+ -> W+      A      )
