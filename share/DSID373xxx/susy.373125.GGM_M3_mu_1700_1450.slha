#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413780E+01   # W+
        25     1.26000000E+02   # h
        35     2.00401339E+03   # H
        36     2.00000000E+03   # A
        37     2.00151076E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.97172756E+03   # ~d_L
   2000001     4.97161782E+03   # ~d_R
   1000002     4.97148220E+03   # ~u_L
   2000002     4.97154016E+03   # ~u_R
   1000003     4.97172756E+03   # ~s_L
   2000003     4.97161782E+03   # ~s_R
   1000004     4.97148220E+03   # ~c_L
   2000004     4.97154016E+03   # ~c_R
   1000005     4.97107447E+03   # ~b_1
   2000005     4.97227232E+03   # ~b_2
   1000006     5.08973151E+03   # ~t_1
   2000006     5.38333949E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.79543334E+03   # ~g
   1000022     1.49247894E+03   # ~chi_10
   1000023    -1.54194611E+03   # ~chi_20
   1000025     1.57970203E+03   # ~chi_30
   1000035     3.12742737E+03   # ~chi_40
   1000024     1.53812967E+03   # ~chi_1+
   1000037     3.12742461E+03   # ~chi_2+
   1000039     2.42500000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.23038675E-01   # N_11
  1  2    -3.27792467E-02   # N_12
  1  3     4.89510298E-01   # N_13
  1  4    -4.86333490E-01   # N_14
  2  1     2.83789454E-03   # N_21
  2  2    -3.32576358E-03   # N_22
  2  3    -7.07012506E-01   # N_23
  2  4    -7.07187530E-01   # N_24
  3  1     6.90800392E-01   # N_31
  3  2     3.62837141E-02   # N_32
  3  3    -5.09387023E-01   # N_33
  3  4     5.11862453E-01   # N_34
  4  1     1.35633263E-03   # N_41
  4  2    -9.98798254E-01   # N_42
  4  3    -3.22155905E-02   # N_43
  4  4     3.69102116E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.55240405E-02   # U_11
  1  2     9.98963243E-01   # U_12
  2  1     9.98963243E-01   # U_21
  2  2     4.55240405E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.21635821E-02   # V_11
  1  2     9.98638554E-01   # V_12
  2  1     9.98638554E-01   # V_21
  2  2     5.21635821E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99956199E-01   # cos(theta_t)
  1  2     9.35949152E-03   # sin(theta_t)
  2  1    -9.35949152E-03   # -sin(theta_t)
  2  2     9.99956199E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73940012E-01   # cos(theta_b)
  1  2     7.38786072E-01   # sin(theta_b)
  2  1    -7.38786072E-01   # -sin(theta_b)
  2  2     6.73940012E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04328371E-01   # cos(theta_tau)
  1  2     7.09874317E-01   # sin(theta_tau)
  2  1    -7.09874317E-01   # -sin(theta_tau)
  2  2     7.04328371E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203813E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52209087E+02   # vev(Q)              
         4     3.32590749E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52758880E-01   # gprime(Q) DRbar
     2     6.27023328E-01   # g(Q) DRbar
     3     1.08305266E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02526701E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71668371E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781272E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.91096146E+05   # M^2_Hd              
        22    -7.39437861E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36980900E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.08008286E-06   # gluino decays
#          BR         NDA      ID1       ID2
     2.75003364E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.57450206E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.18550442E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     3.38183389E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.76286137E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.02963837E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.64053211E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.27292588E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.10413843E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.03090912E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.76286137E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.02963837E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.64053211E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.27292588E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.10413843E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.03090912E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.76224907E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.81512946E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.76001281E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.84092326E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.84092326E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.84092326E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.84092326E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.99204998E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.99204998E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.89765599E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.97269533E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.03870774E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.59028774E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.60459047E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.77962745E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.16098663E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.01407964E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.34628395E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     6.07158072E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.59089334E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.76950301E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.10951807E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.00096973E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.52852475E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.82487257E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.16662822E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.08519598E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -4.33282932E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.39083394E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.91377066E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     4.65682164E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.32737797E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.39988479E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.71983727E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.16913043E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.26858677E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.05736032E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.56101554E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.49321731E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.44326783E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.37277137E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.62743282E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.73149769E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.60499097E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.37649935E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.24083111E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.08691301E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.18879997E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.50694750E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.46871532E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.90682090E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.13663779E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.81756688E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.27447934E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02849009E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.06267399E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.21686780E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.36982440E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.97577154E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.45558947E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58073235E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.18886424E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.01336288E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.04303447E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.03139296E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.14029965E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.90771140E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.27949013E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02893786E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.99786293E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.72208381E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.69804978E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.09978071E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.91972476E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89178040E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.18879997E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.50694750E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.46871532E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.90682090E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.13663779E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.81756688E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.27447934E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02849009E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.06267399E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.21686780E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.36982440E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.97577154E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.45558947E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58073235E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.18886424E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.01336288E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.04303447E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.03139296E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.14029965E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.90771140E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.27949013E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02893786E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.99786293E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.72208381E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.69804978E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.09978071E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.91972476E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89178040E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.68799935E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.44299729E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78114174E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06438743E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68552443E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.47160319E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.38105457E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03123710E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.28687867E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.03756965E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.71303265E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.30969297E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.68799935E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.44299729E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78114174E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06438743E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68552443E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.47160319E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.38105457E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03123710E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.28687867E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.03756965E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.71303265E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.30969297E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36454831E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.82749525E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.65536598E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.59482686E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51005448E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.72163102E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.02575171E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.58661179E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.36258560E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.68151882E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.57007582E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67180454E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54413388E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.00084385E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.09397184E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.68813428E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16610300E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44313918E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.31795347E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69314600E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.24474476E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37636389E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.68813428E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16610300E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44313918E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.31795347E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69314600E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.24474476E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37636389E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.69076597E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.16496250E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.44172773E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.31079618E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69051198E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.21804023E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.37112133E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.24587871E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.70827169E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33609669E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33609669E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11203348E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11203348E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10373293E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.87876255E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53246299E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.10778557E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46242151E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.36092084E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53640909E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.50452541E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.08457998E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.09504996E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99260923E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89592646E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11464311E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.51197416E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.11496115E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     8.01405821E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.14495825E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.28756519E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18537846E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53490314E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18537846E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53490314E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40468701E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50554192E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50554192E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47166212E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00081641E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00081641E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00081641E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.97296892E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.97296892E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.97296892E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.97296892E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.57656345E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.57656345E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.57656345E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.57656345E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.28911065E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.28911065E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.04605701E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.10001966E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.67963835E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.61656648E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.20676342E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     9.22217739E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.10173632E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.30303083E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.10173632E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.30303083E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.99160232E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.23874188E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.23874188E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.81699991E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.39968344E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.39968344E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.39968344E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.30904536E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.98990193E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.30904536E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.98990193E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.57742576E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.82860224E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.82860224E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.72985948E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.36371307E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.36371307E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.36371307E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.34538685E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.34538685E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.34538685E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.34538685E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.48461646E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.48461646E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.48461646E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.48461646E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.44415315E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.44415315E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.87877179E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.36970214E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51848053E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.24828012E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46471572E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46471572E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13465386E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.14007209E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.39441545E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.60433705E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.14392253E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.21700096E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.03914863E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.69282273E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22008741E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02050502E-01    2           5        -5   # BR(h -> b       bb     )
     6.22136095E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20209055E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65919742E-04    2           3        -3   # BR(h -> s       sb     )
     2.01005556E-02    2           4        -4   # BR(h -> c       cb     )
     6.63814403E-02    2          21        21   # BR(h -> g       g      )
     2.30597464E-03    2          22        22   # BR(h -> gam     gam    )
     1.62598711E-03    2          22        23   # BR(h -> Z       gam    )
     2.16566354E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80694482E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78103590E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46926306E-03    2           5        -5   # BR(H -> b       bb     )
     2.46425877E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71207221E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546325E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668209E-05    2           4        -4   # BR(H -> c       cb     )
     9.96065452E-01    2           6        -6   # BR(H -> t       tb     )
     7.97760096E-04    2          21        21   # BR(H -> g       g      )
     2.71479699E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010709E-06    2          23        22   # BR(H -> Z       gam    )
     3.35171913E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67128873E-04    2          23        23   # BR(H -> Z       Z      )
     9.02869720E-04    2          25        25   # BR(H -> h       h      )
     7.41657602E-24    2          36        36   # BR(H -> A       A      )
     2.97158140E-11    2          23        36   # BR(H -> Z       A      )
     6.82630132E-12    2          24       -37   # BR(H -> W+      H-     )
     6.82630132E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381828E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47218820E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898002E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62267544E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677527E-06    2           3        -3   # BR(A -> s       sb     )
     9.96177633E-06    2           4        -4   # BR(A -> c       cb     )
     9.96997143E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676737E-04    2          21        21   # BR(A -> g       g      )
     3.15260649E-06    2          22        22   # BR(A -> gam     gam    )
     1.35269738E-06    2          23        22   # BR(A -> Z       gam    )
     3.26627683E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472288E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36027081E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237702E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145311E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50058469E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45694499E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731698E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401821E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34756649E-04    2          24        25   # BR(H+ -> W+      h      )
     2.76383704E-13    2          24        36   # BR(H+ -> W+      A      )
