#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376839E+01   # W+
        25     1.26000000E+02   # h
        35     2.00415128E+03   # H
        36     2.00000000E+03   # A
        37     2.00209664E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.01677132E+03   # ~d_L
   2000001     5.01666165E+03   # ~d_R
   1000002     5.01652628E+03   # ~u_L
   2000002     5.01658455E+03   # ~u_R
   1000003     5.01677132E+03   # ~s_L
   2000003     5.01666165E+03   # ~s_R
   1000004     5.01652628E+03   # ~c_L
   2000004     5.01658455E+03   # ~c_R
   1000005     5.01663492E+03   # ~b_1
   2000005     5.01679953E+03   # ~b_2
   1000006     5.14688164E+03   # ~t_1
   2000006     5.44359489E+03   # ~t_2
   1000011     5.00008276E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984124E+03   # ~nu_eL
   1000013     5.00008276E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984124E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011982E+03   # ~tau_2
   1000016     4.99984124E+03   # ~nu_tauL
   1000021     1.24136834E+03   # ~g
   1000022     1.46512389E+02   # ~chi_10
   1000023    -1.62935257E+02   # ~chi_20
   1000025     2.94796279E+02   # ~chi_30
   1000035     3.12909418E+03   # ~chi_40
   1000024     1.60723623E+02   # ~chi_1+
   1000037     3.12909379E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08694364E-01   # N_11
  1  2    -2.47366661E-02   # N_12
  1  3     6.79341668E-01   # N_13
  1  4    -6.65274969E-01   # N_14
  2  1     2.00320307E-02   # N_21
  2  2    -4.81328654E-03   # N_22
  2  3    -7.04243205E-01   # N_23
  2  4    -7.09659819E-01   # N_24
  3  1     9.50950227E-01   # N_31
  3  2     8.56781151E-03   # N_32
  3  3    -2.05683971E-01   # N_33
  3  4     2.30899030E-01   # N_34
  4  1     4.15220102E-04   # N_41
  4  2    -9.99645698E-01   # N_42
  4  3    -1.51825644E-02   # N_43
  4  4     2.18585247E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14641223E-02   # U_11
  1  2     9.99769619E-01   # U_12
  2  1     9.99769619E-01   # U_21
  2  2     2.14641223E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09090003E-02   # V_11
  1  2     9.99522203E-01   # V_12
  2  1     9.99522203E-01   # V_21
  2  2     3.09090003E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99863131E-01   # cos(theta_t)
  1  2    -1.65444633E-02   # sin(theta_t)
  2  1     1.65444633E-02   # -sin(theta_t)
  2  2     9.99863131E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08500994E-01   # cos(theta_b)
  1  2     9.12757875E-01   # sin(theta_b)
  2  1    -9.12757875E-01   # -sin(theta_b)
  2  2     4.08500994E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76625360E-01   # cos(theta_tau)
  1  2     7.36327456E-01   # sin(theta_tau)
  2  1    -7.36327456E-01   # -sin(theta_tau)
  2  2     6.76625360E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201648E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51741654E+02   # vev(Q)              
         4     3.88412679E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146798E-01   # gprime(Q) DRbar
     2     6.29569958E-01   # g(Q) DRbar
     3     1.09013025E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02625148E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72297085E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79977702E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04075809E+06   # M^2_Hd              
        22    -5.51972501E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111701E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.55497673E-04   # gluino decays
#          BR         NDA      ID1       ID2
     2.06848711E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.33375537E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.06499150E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.32832021E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.02684739E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.23065240E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.44543776E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.77552599E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.37859378E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.24661291E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.02684739E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.23065240E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.44543776E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.77552599E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.37859378E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.24661291E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.22853434E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.36987885E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.47149764E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.69685104E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.84220533E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.36431776E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.08545878E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.08545878E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.08545878E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.08545878E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.39704515E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.39704515E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.38009097E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.57906371E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.09874782E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.44759576E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.31868369E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.39703336E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.62334416E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.94124072E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     2.71269037E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.34618796E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.16691313E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.65792955E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.46617524E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.42662391E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -6.71083863E-06    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.88204516E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.33452792E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.96069255E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.58661669E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.61047282E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.65076631E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.82252514E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.90594570E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.49218590E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.51627945E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.56769387E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.12638695E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.85037693E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.26072055E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.20158802E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.64534795E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.02526980E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.64066981E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.07173040E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.55014570E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.94260025E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.11598166E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.28716941E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.51244376E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.26129497E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.65469623E-08    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.62463602E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.84403585E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.45058376E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.68755159E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.11831732E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.39720437E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.24267168E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.78593549E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.00597900E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.87724056E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.55679676E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.51247301E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.16348419E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.56115102E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.16427704E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.84537655E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.99544747E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.69128183E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.11877275E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.31756552E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.09713280E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.61833622E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.03592547E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.44068234E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88538993E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.51244376E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.26129497E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.65469623E-08    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.62463602E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.84403585E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.45058376E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.68755159E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.11831732E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.39720437E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.24267168E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.78593549E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.00597900E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.87724056E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.55679676E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.51247301E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.16348419E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.56115102E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.16427704E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.84537655E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.99544747E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.69128183E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.11877275E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.31756552E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.09713280E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.61833622E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.03592547E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.44068234E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88538993E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762143E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54407119E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89085862E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04823992E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563275E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45632103E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497481E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513763E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57452905E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03026166E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03851619E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42226175E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762143E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54407119E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89085862E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04823992E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563275E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45632103E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497481E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513763E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57452905E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03026166E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03851619E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42226175E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957303E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08304847E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84772069E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56354218E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26706454E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289936E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53594346E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825920E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319027E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61282724E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12665092E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05234738E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49093188E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85198131E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98398631E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747260E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74502508E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80489405E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92077966E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774200E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33884630E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178248E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747260E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74502508E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80489405E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92077966E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774200E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33884630E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178248E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068715E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74188562E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80282981E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91858288E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477099E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48069716E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584776E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23355861E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87423539E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36074002E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36074002E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024722E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024722E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03763808E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36648320E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99038563E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71248075E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94609437E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26171083E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99433714E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31626893E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33933874E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91682064E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29772038E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34138565E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02294149E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82543621E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.02090684E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03353760E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96112259E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.33981166E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.25758440E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.51107409E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65849179E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18177527E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01390152E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27777986E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65372799E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27777986E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65372799E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83496472E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77318403E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77318403E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52635571E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53271621E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53271621E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53271621E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.36144335E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.36144335E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.36144335E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.36144335E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.87147945E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.87147945E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.87147945E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.87147945E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36083877E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36083877E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63952315E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83684288E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19053806E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99721146E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99721146E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79625168E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.18856437E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.83891403E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47472263E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.37877496E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36651877E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03778957E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93136223E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02308889E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94055426E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94055426E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71045961E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76523381E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63790921E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36478988E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01160815E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77918484E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22304899E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48828776E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19270064E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33297270E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33297270E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44994608E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77868124E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80603287E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81041807E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61830285E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21660829E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01587545E-01    2           5        -5   # BR(h -> b       bb     )
     6.22645394E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20389324E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66301215E-04    2           3        -3   # BR(h -> s       sb     )
     2.01171989E-02    2           4        -4   # BR(h -> c       cb     )
     6.64350119E-02    2          21        21   # BR(h -> g       g      )
     2.32844650E-03    2          22        22   # BR(h -> gam     gam    )
     1.62711638E-03    2          22        23   # BR(h -> Z       gam    )
     2.16860843E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80926086E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01471433E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38954113E-03    2           5        -5   # BR(H -> b       bb     )
     2.32099179E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20557009E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05060250E-06    2           3        -3   # BR(H -> s       sb     )
     9.48137730E-06    2           4        -4   # BR(H -> c       cb     )
     9.38142095E-01    2           6        -6   # BR(H -> t       tb     )
     7.51344499E-04    2          21        21   # BR(H -> g       g      )
     2.62941609E-06    2          22        22   # BR(H -> gam     gam    )
     1.09159287E-06    2          23        22   # BR(H -> Z       gam    )
     3.15108149E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57124272E-04    2          23        23   # BR(H -> Z       Z      )
     8.52400634E-04    2          25        25   # BR(H -> h       h      )
     8.41674870E-24    2          36        36   # BR(H -> A       A      )
     3.31322695E-11    2          23        36   # BR(H -> Z       A      )
     2.39830593E-12    2          24       -37   # BR(H -> W+      H-     )
     2.39830593E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71026260E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52102073E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36088586E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64251519E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94542050E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53665535E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87760724E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05931975E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39244361E-03    2           5        -5   # BR(A -> b       bb     )
     2.29748258E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12243086E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082525E-06    2           3        -3   # BR(A -> s       sb     )
     9.38384381E-06    2           4        -4   # BR(A -> c       cb     )
     9.39156347E-01    2           6        -6   # BR(A -> t       tb     )
     8.88929324E-04    2          21        21   # BR(A -> g       g      )
     2.69764488E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304833E-06    2          23        22   # BR(A -> Z       gam    )
     3.07073422E-04    2          23        25   # BR(A -> Z       h      )
     5.85736809E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28902574E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65400105E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84234912E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65217019E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36578645E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93383757E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97901471E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23443740E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630781E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29504567E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42063706E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13692964E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02355259E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40805857E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.14705566E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33871754E-12    2          24        36   # BR(H+ -> W+      A      )
     5.57823751E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23534181E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29296695E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
