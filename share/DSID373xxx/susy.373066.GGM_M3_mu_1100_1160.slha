#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.16000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.16000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05408381E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402669E+03   # H
        36     2.00000000E+03   # A
        37     2.00150805E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.01677079E+03   # ~d_L
   2000001     5.01666165E+03   # ~d_R
   1000002     5.01652679E+03   # ~u_L
   2000002     5.01658447E+03   # ~u_R
   1000003     5.01677079E+03   # ~s_L
   2000003     5.01666165E+03   # ~s_R
   1000004     5.01652679E+03   # ~c_L
   2000004     5.01658447E+03   # ~c_R
   1000005     5.01623926E+03   # ~b_1
   2000005     5.01719460E+03   # ~b_2
   1000006     5.13772179E+03   # ~t_1
   2000006     5.43152978E+03   # ~t_2
   1000011     5.00008221E+03   # ~e_L
   2000011     5.00007608E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008221E+03   # ~mu_L
   2000013     5.00007608E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99977002E+03   # ~tau_1
   2000015     5.00038888E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.24135416E+03   # ~g
   1000022     1.18816701E+03   # ~chi_10
   1000023    -1.23642786E+03   # ~chi_20
   1000025     1.27535595E+03   # ~chi_30
   1000035     3.12857323E+03   # ~chi_40
   1000024     1.23321947E+03   # ~chi_1+
   1000037     3.12857162E+03   # ~chi_2+
   1000039     2.17057143E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.18390310E-01   # N_11
  1  2    -2.77998281E-02   # N_12
  1  3     4.93448311E-01   # N_13
  1  4    -4.89541925E-01   # N_14
  2  1     3.53892727E-03   # N_21
  2  2    -3.55845108E-03   # N_22
  2  3    -7.06974831E-01   # N_23
  2  4    -7.07220900E-01   # N_24
  3  1     6.95630593E-01   # N_31
  3  2     3.01027257E-02   # N_32
  3  3    -5.05959954E-01   # N_33
  3  4     5.09113376E-01   # N_34
  4  1     9.57466880E-04   # N_41
  4  2    -9.99153808E-01   # N_42
  4  3    -2.64552028E-02   # N_43
  4  4     3.14781292E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.73918417E-02   # U_11
  1  2     9.99300681E-01   # U_12
  2  1     9.99300681E-01   # U_21
  2  2     3.73918417E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.44966897E-02   # V_11
  1  2     9.99009532E-01   # V_12
  2  1     9.99009532E-01   # V_21
  2  2     4.44966897E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99970215E-01   # cos(theta_t)
  1  2     7.71810293E-03   # sin(theta_t)
  2  1    -7.71810293E-03   # -sin(theta_t)
  2  2     9.99970215E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.65493857E-01   # cos(theta_b)
  1  2     7.46403327E-01   # sin(theta_b)
  2  1    -7.46403327E-01   # -sin(theta_b)
  2  2     6.65493857E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03595188E-01   # cos(theta_tau)
  1  2     7.10601021E-01   # sin(theta_tau)
  2  1    -7.10601021E-01   # -sin(theta_tau)
  2  2     7.03595188E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199071E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.16000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52126245E+02   # vev(Q)              
         4     3.48869179E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52796766E-01   # gprime(Q) DRbar
     2     6.27263484E-01   # g(Q) DRbar
     3     1.09012941E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02489833E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71821660E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79817890E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.16000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.54164046E+06   # M^2_Hd              
        22    -6.74120197E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37088151E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.92955549E-09   # gluino decays
#          BR         NDA      ID1       ID2
     9.45601775E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.67774703E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
     3.73301289E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.71690264E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     8.23360752E-13    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.23412251E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.48533234E-13    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.71690264E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     8.23360752E-13    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.23412251E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.48533234E-13    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.48827797E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     5.18311836E-09    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.18311836E-09    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.18311836E-09    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.18311836E-09    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.27056286E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.84938135E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.97955300E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.29816165E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.37189766E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.12412558E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.71135152E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.16326977E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.19373014E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     6.26342850E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.98561875E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.59867826E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.80143780E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.78964819E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.20354136E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.76282987E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.46816733E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.21640827E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -8.60096105E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.38922114E-06    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.31529964E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.88846457E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.65419174E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.14247150E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.23800731E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.19664490E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.10872321E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.69412948E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.23268661E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.63233110E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.80601560E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.12373083E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.28009409E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.62557059E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.47950227E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.34192096E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.97983971E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.16372382E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.51038820E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.69520723E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.58974534E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.70430962E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.82338075E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.64289124E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.64716712E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.12555843E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.38517098E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.05766292E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.94424342E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.89455407E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.53523182E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60477320E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.51043079E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.77842088E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.18889337E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.65632711E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.82584706E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.86636613E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.65133237E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.12596327E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.31451461E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.30129677E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.27381943E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.88107125E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.95546110E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89817501E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.51038820E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.69520723E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.58974534E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.70430962E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.82338075E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.64289124E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.64716712E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.12555843E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.38517098E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.05766292E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.94424342E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.89455407E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.53523182E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60477320E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.51043079E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.77842088E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.18889337E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.65632711E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.82584706E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.86636613E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.65133237E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.12596327E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.31451461E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.30129677E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.27381943E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.88107125E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.95546110E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89817501E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.72488185E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.05099651E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.55642893E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.11451900E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65178760E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.77133475E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.31086484E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.18510559E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.20652301E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.25098122E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.79334804E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.84655853E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.72488185E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.05099651E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.55642893E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.11451900E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65178760E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.77133475E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.31086484E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.18510559E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.20652301E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.25098122E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.79334804E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.84655853E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.45991589E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.89584531E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.71042215E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.71284459E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.45068118E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.05582445E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.90536021E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.10361702E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.45857605E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.73874826E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.94232695E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.78697739E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.48756461E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.77842899E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.97918957E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.72493793E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.19226741E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.94894168E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.18707113E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65699924E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.50822848E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30674906E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.72493793E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.19226741E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.94894168E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.18707113E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65699924E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.50822848E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30674906E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.72777210E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.19102863E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.94691672E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.17856472E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65423861E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.54354750E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.30124612E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16329495E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.95153293E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33617351E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33617351E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11205914E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11205914E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10353173E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.79809431E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53053210E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12595606E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46302410E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34081619E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53967155E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.92198627E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.85170567E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     4.33060448E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.01323095E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87532270E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11446350E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.32936100E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.95385071E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     9.53923550E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.00238024E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.45154237E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18573163E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53528979E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18573163E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53528979E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.39911831E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50609481E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50609481E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47099627E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00170728E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00170728E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00170728E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     9.43861981E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     9.43861981E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     9.43861981E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     9.43861981E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.14620679E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.14620679E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.14620679E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.14620679E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.74289195E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.74289195E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18968944E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.84523722E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.74902557E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.45241459E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.28036887E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.61978249E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.08439637E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29321256E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.08439637E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29321256E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.25946962E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26619603E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26619603E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.75326557E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.31951167E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.31951167E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.31951167E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.48006682E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.21120460E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.48006682E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.21120460E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.79192568E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.33334817E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.33334817E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.23225254E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.46446765E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.46446765E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.46446765E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.32117519E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.32117519E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.32117519E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.32117519E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.40391091E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.40391091E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.40391091E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.40391091E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.36523873E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.36523873E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     4.02552893E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.05522938E-08    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     4.02552893E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.05522938E-08    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     7.73679326E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.79812463E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.70865035E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51118165E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.01270106E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46346610E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46346610E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15214410E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.85624964E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37234387E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.61069590E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.46072169E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.25152483E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.81892049E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.44199170E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21832287E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01868151E-01    2           5        -5   # BR(h -> b       bb     )
     6.22387525E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20298050E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66108163E-04    2           3        -3   # BR(h -> s       sb     )
     2.01090915E-02    2           4        -4   # BR(h -> c       cb     )
     6.64083822E-02    2          21        21   # BR(h -> g       g      )
     2.30752855E-03    2          22        22   # BR(h -> gam     gam    )
     1.62663567E-03    2          22        23   # BR(h -> Z       gam    )
     2.16673862E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80811904E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78101084E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47202701E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430711E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71224311E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548423E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668026E-05    2           4        -4   # BR(H -> c       cb     )
     9.96064012E-01    2           6        -6   # BR(H -> t       tb     )
     7.97770599E-04    2          21        21   # BR(H -> g       g      )
     2.71083917E-06    2          22        22   # BR(H -> gam     gam    )
     1.15996212E-06    2          23        22   # BR(H -> Z       gam    )
     3.33738269E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66413990E-04    2          23        23   # BR(H -> Z       Z      )
     9.03683397E-04    2          25        25   # BR(H -> h       h      )
     7.56093357E-24    2          36        36   # BR(H -> A       A      )
     3.02112352E-11    2          23        36   # BR(H -> Z       A      )
     7.04747267E-12    2          24       -37   # BR(H -> W+      H-     )
     7.04747267E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382340E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47489988E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897675E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266389E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677375E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176299E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995808E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675473E-04    2          21        21   # BR(A -> g       g      )
     3.18423885E-06    2          22        22   # BR(A -> gam     gam    )
     1.35251365E-06    2          23        22   # BR(A -> Z       gam    )
     3.25221436E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472703E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36627282E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237088E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143140E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50442599E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693255E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731450E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403259E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33314133E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73913586E-13    2          24        36   # BR(H+ -> W+      A      )
