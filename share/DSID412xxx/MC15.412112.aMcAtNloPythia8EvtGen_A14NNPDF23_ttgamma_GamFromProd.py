from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(2*1.1*runArgs.maxEvents)
name='ttgamma_nonallhad'
stringy = 'madgraph.'+str(runArgs.runNumber)+'.aMCatNLO_'+str(name)
parton_shower='PYTHIA8'

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
generate p p > t t~ a QED=1 QCD=2 [QCD]
output -f
""")
fcard.close()

dec = """
define w+child = e+ mu+ ta+ ve vm vt u c d~ s~
define w-child = e- mu- ta- ve~ vm~ vt~ u~ c~ d s
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
"""

# Decay with MadSpin
madspin_card_loc='madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set Nevents_for_max_weigth 500
set BW_cut 15
set seed %i
%s
launch
"""%(runArgs.randomSeed, dec))
mscard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'   :'3.0',
           'lhaid'         :260000,
           'pdlabel'       :"'lhapdf'",
           'maxjetflavor'  :5,
	   'parton_shower' :parton_shower,
           'ptl'           :20.,
	   'ptgmin'	   :15.,
           'ptj'           :1.,
           'etal'          :5.0,
	   'etagamma'	   :5.0,
	   'reweight_scale':'.true.',
           'rw_Rscale_down':0.5,
           'rw_Rscale_up'  :2.0,
           'rw_Fscale_down':0.5,
           'rw_Fscale_up'  :2.0,
           'reweight_PDF'  :'.true.',
           'PDF_set_min'   :260001,
           'PDF_set_max'   :260100,
	   'dynamical_scale_choice':'3'}

process_dir = new_process()
# Run card
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',  nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, xqcut=0., extras=extras)
# Param card
paramNameToCopy      = 'aMcAtNlo_param_card_loopsmnobmass.dat'
paramNameDestination = 'param_card.dat'
paramcard            = subprocess.Popen(['get_files','-data',paramNameToCopy])
paramcard.wait()
if not os.access(paramNameToCopy,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%(paramNameToCopy))
shutil.copy(paramNameToCopy,paramNameDestination)
# Print cards
print_cards()
# set up
generate(run_card_loc='run_card.dat', param_card_loc='param_card.dat', madspin_card_loc=madspin_card_loc, mode=0, proc_dir=process_dir)
# run
outputDS=arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.tar.gz',lhe_version=3)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

## pythia shower
keyword=['SM','top', 'ttV', 'photon']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = 'aMcAtNlo_'+str(name)+'_GamFromProd'
evgenConfig.keywords += keyword
evgenConfig.contact = ["amartya.rej@cern.ch","atlas-generators-madgraphcontrol@cern.ch"]
runArgs.inputGeneratorFile=outputDS
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
#include("MC15JobOptions/Pythia8_ShowerWeights.py")
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
