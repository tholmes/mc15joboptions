#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.36958918E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05368948E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414639E+03   # H
        36     2.00000000E+03   # A
        37     2.00203181E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013333E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989199E+03   # ~u_L
   2000002     4.99994937E+03   # ~u_R
   1000003     5.00013333E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989199E+03   # ~c_L
   2000004     4.99994937E+03   # ~c_R
   1000005     4.99999894E+03   # ~b_1
   2000005     5.00016115E+03   # ~b_2
   1000006     5.00071127E+03   # ~t_1
   2000006     5.00375095E+03   # ~t_2
   1000011     5.00008270E+03   # ~e_L
   2000011     5.00007595E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008270E+03   # ~mu_L
   2000013     5.00007595E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00003952E+03   # ~tau_1
   2000015     5.00011976E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     1.20000000E+03   # ~g
   1000022     1.49120493E+02   # ~chi_10
   1000023    -1.56739959E+02   # ~chi_20
   1000025     2.42579113E+02   # ~chi_30
   1000035     3.00199927E+03   # ~chi_40
   1000024     1.51829568E+02   # ~chi_1+
   1000037     3.00199892E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.15242370E-02   # N_11
  1  2    -5.02689423E-03   # N_12
  1  3    -7.14404585E-01   # N_13
  1  4    -6.93703203E-01   # N_14
  2  1     1.10068446E-01   # N_21
  2  2    -2.44706867E-02   # N_22
  2  3     6.99502997E-01   # N_23
  2  4    -7.05678170E-01   # N_24
  3  1     9.89701009E-01   # N_31
  3  2     3.59592252E-03   # N_32
  3  3    -1.17231062E-02   # N_33
  3  4     1.42623811E-01   # N_34
  4  1     4.05484197E-04   # N_41
  4  2    -9.99681442E-01   # N_42
  4  3    -1.35725614E-02   # N_43
  4  4     2.12752436E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.91871440E-02   # U_11
  1  2     9.99815910E-01   # U_12
  2  1     9.99815910E-01   # U_21
  2  2     1.91871440E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.00852215E-02   # V_11
  1  2    -9.99547337E-01   # V_12
  2  1     9.99547337E-01   # V_21
  2  2     3.00852215E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13746734E-01   # cos(theta_t)
  1  2    -7.00403883E-01   # sin(theta_t)
  2  1     7.00403883E-01   # -sin(theta_t)
  2  2     7.13746734E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -4.08738692E-01   # cos(theta_b)
  1  2     9.12651457E-01   # sin(theta_b)
  2  1    -9.12651457E-01   # -sin(theta_b)
  2  2    -4.08738692E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -6.76716525E-01   # cos(theta_tau)
  1  2     7.36243672E-01   # sin(theta_tau)
  2  1    -7.36243672E-01   # -sin(theta_tau)
  2  2    -6.76716525E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201749E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51664706E+02   # vev(Q)              
         4     3.99149533E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53144539E-01   # gprime(Q) DRbar
     2     6.29521326E-01   # g(Q) DRbar
     3     1.08870456E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02673411E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72452776E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79995910E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.36958918E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04101732E+06   # M^2_Hd              
        22    -5.51978171E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38093751E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.58037004E-04   # gluino decays
#          BR         NDA      ID1       ID2
     2.40363140E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.45895477E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     9.32220262E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.10953200E-05    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.02746631E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.48207238E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.06759006E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.87430920E-04    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.22630868E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     7.10953200E-05    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.02746631E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.48207238E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.06759006E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.87430920E-04    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.22630868E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.79514084E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.89714318E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.48111358E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.93368762E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.81679486E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     2.71504175E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     3.65114171E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.65114171E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.65114171E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.65114171E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35659121E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35659121E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.52972067E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.47415706E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.55827599E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.87214103E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.15847050E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.04514142E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.31219905E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.41733421E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.57418483E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.01741387E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.07810024E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.08138901E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.04947761E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.97506288E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.09335835E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.48455710E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.96299132E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.52381220E-04    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
    -1.15234063E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.03224147E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.49178840E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.76519879E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.99195458E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.73373302E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.61539616E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
    -7.91293704E-05    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.92186879E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.62033888E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.29296675E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.68756315E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.59048504E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.77156454E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.53376464E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.09656519E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.13486797E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.66386437E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.07783183E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.35857656E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.15494745E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.04860385E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.39883328E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.71517777E-04    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.37224891E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.33199596E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.99281100E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.55771295E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.53378626E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.67951537E-05    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52318575E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.46473958E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.07922084E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.52606234E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.15916582E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.04907019E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.31930407E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.60657620E-05    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.38913726E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.12015240E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.73896790E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88563496E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.53376464E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.09656519E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.13486797E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.66386437E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.07783183E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.35857656E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.15494745E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.04860385E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.39883328E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.71517777E-04    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.37224891E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.33199596E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.99281100E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.55771295E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.53378626E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.67951537E-05    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52318575E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.46473958E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.07922084E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.52606234E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.15916582E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.04907019E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.31930407E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.60657620E-05    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.38913726E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.12015240E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.73896790E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88563496E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03798088E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.38930481E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.99681470E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.01635078E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65071645E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.76828935E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30527462E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46956253E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.40080739E-03    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.21476822E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.79451443E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.75604799E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03798088E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.38930481E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.99681470E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.01635078E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65071645E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.76828935E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30527462E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46956253E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.40080739E-03    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.21476822E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.79451443E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.75604799E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.73319617E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.59035210E-03    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.23446040E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.82508040E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.35082295E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     9.35915890E-05    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.70359622E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31638458E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.78435638E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.67703056E-03    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.74822177E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.16913601E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56616828E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.58330784E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.13461012E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03780550E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.05794212E-03    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     4.81327058E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.96480018E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65286646E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.17233678E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30189786E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03780550E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.05794212E-03    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     4.81327058E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.96480018E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65286646E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.17233678E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30189786E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.04102182E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.05576555E-03    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.80817984E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.96272212E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65006066E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.22852380E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29629253E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16423709E-10   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.11987492E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     4.55447683E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.14793233E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.51816026E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.50671104E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.52844614E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.94904107E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.80522700E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.72350753E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.73414069E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.62588989E-03    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.80833251E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.14471053E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.17188004E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     7.15709639E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.06815139E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     8.25464363E-04    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     9.48917164E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.41262951E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.75840825E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.29419810E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     2.94739365E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.46428576E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.21869430E-05    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.20121738E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.55303840E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09780517E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.42081709E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.64520489E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.41358914E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.24183653E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.23865525E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.24131318E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.47202292E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.47202292E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.47202292E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.25086829E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.25086829E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.85409577E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.85409577E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.41695645E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.41695645E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.41368551E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.41368551E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.56961731E-03    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.56961731E-03    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.49095268E-02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.06815216E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.96592380E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.96592380E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     2.07234666E-08    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.60368191E-09    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.05350342E-11    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     2.94907647E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.69756293E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.63564238E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.31846231E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.75997083E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.75997083E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.91091204E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.91908115E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.25313824E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.69655145E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76490221E-03    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     6.89439910E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.47931391E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.80773297E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.47676318E-04    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.16296423E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.16296423E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.27355058E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.21480433E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.84267309E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.40086720E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.17643960E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08181533E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17303522E-01    2           5        -5   # BR(h -> b       bb     )
     6.38090020E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25860367E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78618507E-04    2           3        -3   # BR(h -> s       sb     )
     2.06486589E-02    2           4        -4   # BR(h -> c       cb     )
     6.70476774E-02    2          21        21   # BR(h -> g       g      )
     2.27843153E-03    2          22        22   # BR(h -> gam     gam    )
     1.53872896E-03    2          22        23   # BR(h -> Z       gam    )
     2.01028053E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56414469E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.00902918E+01   # H decays
#          BR         NDA      ID1       ID2
     1.39243507E-03    2           5        -5   # BR(H -> b       bb     )
     2.32427718E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21718515E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05208999E-06    2           3        -3   # BR(H -> s       sb     )
     9.49480563E-06    2           4        -4   # BR(H -> c       cb     )
     9.39470644E-01    2           6        -6   # BR(H -> t       tb     )
     7.52327776E-04    2          21        21   # BR(H -> g       g      )
     2.50776325E-06    2          22        22   # BR(H -> gam     gam    )
     1.09292349E-06    2          23        22   # BR(H -> Z       gam    )
     3.15581723E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57360380E-04    2          23        23   # BR(H -> Z       Z      )
     8.53958378E-04    2          25        25   # BR(H -> h       h      )
     8.32640223E-24    2          36        36   # BR(H -> A       A      )
     3.29837613E-11    2          23        36   # BR(H -> Z       A      )
     2.77301401E-12    2          24       -37   # BR(H -> W+      H-     )
     2.77301401E-12    2         -24        37   # BR(H -> W-      H+     )
     8.42646856E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13582216E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.00118852E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.13541429E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.06384274E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.16578006E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.25490473E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05319669E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39546619E-03    2           5        -5   # BR(A -> b       bb     )
     2.30095332E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.13470120E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07244291E-06    2           3        -3   # BR(A -> s       sb     )
     9.39801973E-06    2           4        -4   # BR(A -> c       cb     )
     9.40575105E-01    2           6        -6   # BR(A -> t       tb     )
     8.90272206E-04    2          21        21   # BR(A -> g       g      )
     3.07014966E-06    2          22        22   # BR(A -> gam     gam    )
     1.27472077E-06    2          23        22   # BR(A -> Z       gam    )
     3.07624062E-04    2          23        25   # BR(A -> Z       h      )
     5.28223863E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.24960694E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.70332830E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.76856542E-04    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.45826539E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.67654548E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.95025315E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97309735E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23974488E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34972621E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30713094E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42402010E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14443632E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02504834E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.42181411E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.15232036E-04    2          24        25   # BR(H+ -> W+      h      )
     1.14588818E-12    2          24        36   # BR(H+ -> W+      A      )
     4.25409322E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.78211191E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.61513772E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
