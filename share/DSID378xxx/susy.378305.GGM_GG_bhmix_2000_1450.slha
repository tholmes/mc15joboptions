#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05415450E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414674E+03   # H
        36     2.00000000E+03   # A
        37     2.00175539E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013260E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989271E+03   # ~u_L
   2000002     4.99994938E+03   # ~u_R
   1000003     5.00013260E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989271E+03   # ~c_L
   2000004     4.99994938E+03   # ~c_R
   1000005     4.99949077E+03   # ~b_1
   2000005     5.00066852E+03   # ~b_2
   1000006     4.98750661E+03   # ~t_1
   2000006     5.01692659E+03   # ~t_2
   1000011     5.00008198E+03   # ~e_L
   2000011     5.00007594E+03   # ~e_R
   1000012     4.99984208E+03   # ~nu_eL
   1000013     5.00008198E+03   # ~mu_L
   2000013     5.00007594E+03   # ~mu_R
   1000014     4.99984208E+03   # ~nu_muL
   1000015     4.99969305E+03   # ~tau_1
   2000015     5.00046546E+03   # ~tau_2
   1000016     4.99984208E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.44323180E+03   # ~chi_10
   1000023    -1.45200043E+03   # ~chi_20
   1000025     1.46129295E+03   # ~chi_30
   1000035     3.00150233E+03   # ~chi_40
   1000024     1.45119255E+03   # ~chi_1+
   1000037     3.00150205E+03   # ~chi_2+
   1000039     1.16148254E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.05060497E-01   # N_11
  1  2    -7.45717505E-03   # N_12
  1  3    -5.69504984E-01   # N_13
  1  4    -5.56336462E-01   # N_14
  2  1     1.49897169E-02   # N_21
  2  2    -1.73908309E-02   # N_22
  2  3     7.06822921E-01   # N_23
  2  4    -7.07017840E-01   # N_24
  3  1     7.96038193E-01   # N_31
  3  2     6.68148436E-03   # N_32
  3  3     4.19568758E-01   # N_33
  3  4     4.36165805E-01   # N_34
  4  1     5.46101029E-04   # N_41
  4  2    -9.99798633E-01   # N_42
  4  3    -5.24305321E-03   # N_43
  4  4     1.93624600E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.40444941E-03   # U_11
  1  2     9.99972587E-01   # U_12
  2  1     9.99972587E-01   # U_21
  2  2     7.40444941E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.73835279E-02   # V_11
  1  2    -9.99625001E-01   # V_12
  2  1     9.99625001E-01   # V_21
  2  2     2.73835279E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07787128E-01   # cos(theta_t)
  1  2    -7.06425779E-01   # sin(theta_t)
  2  1     7.06425779E-01   # -sin(theta_t)
  2  2     7.07787128E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.74130514E-01   # cos(theta_b)
  1  2     7.38612246E-01   # sin(theta_b)
  2  1    -7.38612246E-01   # -sin(theta_b)
  2  2    -6.74130514E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04336196E-01   # cos(theta_tau)
  1  2     7.09866553E-01   # sin(theta_tau)
  2  1    -7.09866553E-01   # -sin(theta_tau)
  2  2    -7.04336196E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199876E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51926117E+02   # vev(Q)              
         4     4.55028270E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52751672E-01   # gprime(Q) DRbar
     2     6.26976873E-01   # g(Q) DRbar
     3     1.08044372E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02711607E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72862594E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79695563E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.96313585E+05   # M^2_Hd              
        22    -7.38002795E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36959893E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.30060032E-05   # gluino decays
#          BR         NDA      ID1       ID2
     5.57693664E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     8.68554188E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.16544024E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.91641658E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.05184463E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     7.04037180E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.61537871E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.11072227E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.44970342E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.91641658E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.05184463E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     7.04037180E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.61537871E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.11072227E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.44970342E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.16028946E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.72569396E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.06914241E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     8.51803689E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     8.45317676E-02    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     7.30015647E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.85035559E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.85035559E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.85035559E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.85035559E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.32537558E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.32537558E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.86615252E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.08831934E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01536199E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.23291219E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.44791385E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.01256960E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.89145101E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.30600877E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.00517532E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.78249280E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05389522E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.82667299E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.20615812E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.06067644E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.40763329E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.36313262E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.26694474E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.19760609E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.20668961E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.33777292E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.58710845E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.25234578E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.17968062E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.20520085E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.32447541E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51288147E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.73216143E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.24419581E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.78660079E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.39215986E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.57932782E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.01194435E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09936067E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.48885613E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60190754E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.82898255E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.68991369E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.13042309E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.37877751E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86506158E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.95010534E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.67512880E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02583808E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.88626569E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.67088736E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.54375790E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09941187E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.10457301E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.07575103E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.52907276E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69190910E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.26543691E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38487633E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86559477E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88342659E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.33617324E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.65544476E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.47127622E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.72684888E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88189893E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09936067E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.48885613E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60190754E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.82898255E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.68991369E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.13042309E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.37877751E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86506158E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.95010534E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.67512880E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02583808E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.88626569E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.67088736E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.54375790E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09941187E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.10457301E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.07575103E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.52907276E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69190910E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.26543691E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38487633E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86559477E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88342659E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.33617324E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.65544476E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.47127622E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.72684888E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88189893E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.91933234E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.23924620E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.50532991E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.15747779E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73741903E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.15856882E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.48011217E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07422265E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.67162355E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.24845757E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.32612653E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.45629960E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.91933234E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.23924620E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.50532991E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.15747779E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73741903E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.15856882E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.48011217E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07422265E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.67162355E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.24845757E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.32612653E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.45629960E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.49637431E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.95508731E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.73383912E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.27200649E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.58837059E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.00654726E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.17979512E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.06799210E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50526262E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.82580293E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.14034451E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.32984264E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60712498E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.47391784E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21735209E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.91914286E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.81070642E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.74485632E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.09042159E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.74053610E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.42311520E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.47580369E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.91914286E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.81070642E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.74485632E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.09042159E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.74053610E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.42311520E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.47580369E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92183693E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.80442662E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.74140338E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.08941617E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73800920E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.76374068E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.47075316E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.11247339E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.28507670E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.54200711E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03994343E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18067552E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17963694E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.29229334E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.98923285E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53373736E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48667338E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46344375E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.91817270E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52432822E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.46115936E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.43152752E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.09380659E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50917458E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.39701883E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.92654194E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.40970482E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01458622E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91577538E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40209335E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81553187E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17691798E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80856360E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.14653062E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.14345737E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.18776125E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.28103874E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.28103874E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.28103874E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.95239539E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.95239539E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.65079857E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.65079857E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.51860125E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.51860125E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.77124556E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.01894168E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.14865825E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.97326599E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.11720267E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     9.90043383E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32113385E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.14462325E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.31943932E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.30300373E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.34012088E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.33841635E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.90283493E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.86177530E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.86177530E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.86177530E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.17228183E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.10772941E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.71809308E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.09368417E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.38181565E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.37561284E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.45669956E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.87358408E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.87358408E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.87358408E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.62314738E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.62314738E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.47521589E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.47521589E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.41043711E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.41043711E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.40747936E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.40747936E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.65760583E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.65760583E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.98941745E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.20743342E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.23188364E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.06640461E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46674398E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46674398E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16272836E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.90293642E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.10040498E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     8.32627561E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.62800072E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.98902817E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.74760877E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.74671744E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07696737E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16963732E-01    2           5        -5   # BR(h -> b       bb     )
     6.38845206E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26127674E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79185007E-04    2           3        -3   # BR(h -> s       sb     )
     2.06732643E-02    2           4        -4   # BR(h -> c       cb     )
     6.71262027E-02    2          21        21   # BR(h -> g       g      )
     2.30348952E-03    2          22        22   # BR(h -> gam     gam    )
     1.54081543E-03    2          22        23   # BR(h -> Z       gam    )
     2.01130725E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56719375E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78126745E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48288668E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428483E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71216434E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546484E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666631E-05    2           4        -4   # BR(H -> c       cb     )
     9.96053523E-01    2           6        -6   # BR(H -> t       tb     )
     7.97607025E-04    2          21        21   # BR(H -> g       g      )
     2.73785274E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010933E-06    2          23        22   # BR(H -> Z       gam    )
     3.34020636E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66554871E-04    2          23        23   # BR(H -> Z       Z      )
     9.03028071E-04    2          25        25   # BR(H -> h       h      )
     8.64991128E-24    2          36        36   # BR(H -> A       A      )
     3.49887987E-11    2          23        36   # BR(H -> Z       A      )
     5.43759215E-12    2          24       -37   # BR(H -> W+      H-     )
     5.43759215E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386522E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48567751E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895008E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62256960E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676132E-06    2           3        -3   # BR(A -> s       sb     )
     9.96165406E-06    2           4        -4   # BR(A -> c       cb     )
     9.96984905E-01    2           6        -6   # BR(A -> t       tb     )
     9.43665154E-04    2          21        21   # BR(A -> g       g      )
     3.02532222E-06    2          22        22   # BR(A -> gam     gam    )
     1.35273706E-06    2          23        22   # BR(A -> Z       gam    )
     3.25518216E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74519293E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39029674E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49236880E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81142403E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51980144E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45683671E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729535E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402844E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33705295E-04    2          24        25   # BR(H+ -> W+      h      )
     5.85193665E-13    2          24        36   # BR(H+ -> W+      A      )
