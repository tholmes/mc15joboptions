#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.25384476E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05411846E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414818E+03   # H
        36     2.00000000E+03   # A
        37     2.00177085E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013263E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989268E+03   # ~u_L
   2000002     4.99994938E+03   # ~u_R
   1000003     5.00013263E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989268E+03   # ~c_L
   2000004     4.99994938E+03   # ~c_R
   1000005     4.99957148E+03   # ~b_1
   2000005     5.00058786E+03   # ~b_2
   1000006     4.98954185E+03   # ~t_1
   2000006     5.01490160E+03   # ~t_2
   1000011     5.00008201E+03   # ~e_L
   2000011     5.00007592E+03   # ~e_R
   1000012     4.99984206E+03   # ~nu_eL
   1000013     5.00008201E+03   # ~mu_L
   2000013     5.00007592E+03   # ~mu_R
   1000014     4.99984206E+03   # ~nu_muL
   1000015     4.99974631E+03   # ~tau_1
   2000015     5.00041224E+03   # ~tau_2
   1000016     4.99984206E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.24321555E+03   # ~chi_10
   1000023    -1.25216826E+03   # ~chi_20
   1000025     1.26124909E+03   # ~chi_30
   1000035     3.00154838E+03   # ~chi_40
   1000024     1.25127389E+03   # ~chi_1+
   1000037     3.00154810E+03   # ~chi_2+
   1000039     7.60097687E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.07049158E-01   # N_11
  1  2    -6.52938325E-03   # N_12
  1  3    -5.69485854E-01   # N_13
  1  4    -5.54197212E-01   # N_14
  2  1     1.73803354E-02   # N_21
  2  2    -1.82094404E-02   # N_22
  2  3     7.06760193E-01   # N_23
  2  4    -7.07005212E-01   # N_24
  3  1     7.94474037E-01   # N_31
  3  2     6.01441787E-03   # N_32
  3  3     4.19680777E-01   # N_33
  3  4     4.38911012E-01   # N_34
  4  1     4.98258310E-04   # N_41
  4  2    -9.99794784E-01   # N_42
  4  3    -6.62854092E-03   # N_43
  4  4     1.91364566E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.36500530E-03   # U_11
  1  2     9.99956147E-01   # U_12
  2  1     9.99956147E-01   # U_21
  2  2     9.36500530E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.70631745E-02   # V_11
  1  2    -9.99633725E-01   # V_12
  2  1     9.99633725E-01   # V_21
  2  2     2.70631745E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07896518E-01   # cos(theta_t)
  1  2    -7.06316161E-01   # sin(theta_t)
  2  1     7.06316161E-01   # -sin(theta_t)
  2  2     7.07896518E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.68734463E-01   # cos(theta_b)
  1  2     7.43501323E-01   # sin(theta_b)
  2  1    -7.43501323E-01   # -sin(theta_b)
  2  2    -6.68734463E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03868164E-01   # cos(theta_tau)
  1  2     7.10330633E-01   # sin(theta_tau)
  2  1    -7.10330633E-01   # -sin(theta_tau)
  2  2    -7.03868164E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200877E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51889101E+02   # vev(Q)              
         4     4.45828493E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52777886E-01   # gprime(Q) DRbar
     2     6.27143490E-01   # g(Q) DRbar
     3     1.08044418E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02717399E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72801638E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79736417E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.25384476E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.30266627E+06   # M^2_Hd              
        22    -6.87798354E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37034231E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.01580280E-04   # gluino decays
#          BR         NDA      ID1       ID2
     2.43713502E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.86210169E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.44896154E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.88577833E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.00426466E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.79475455E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.28235664E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.85100372E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.01177413E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.88577833E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.00426466E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.79475455E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.28235664E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.85100372E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.01177413E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.06817771E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.22983986E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.83212444E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.11656127E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.44829445E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.01647488E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.20924348E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.20924348E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.20924348E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.20924348E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34431100E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.34431100E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.91335006E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.19648318E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.04898463E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.51040915E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.42569848E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.04535527E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.84734087E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.20766693E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.05014262E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.02939679E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08604204E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.95406293E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.18846311E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.09362302E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.37254563E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.26588810E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.28042131E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.34517458E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.00275088E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.48063365E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.57002250E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.30293627E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.14516319E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.15688680E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.34167100E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57192413E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.88252731E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.39584797E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.78253606E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.45017492E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.57084121E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.95292710E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09975652E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.10858235E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.76513669E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.89315965E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.69109034E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.15700365E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.38118339E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86339893E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.95436583E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.76218517E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.44177237E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.00675265E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.54177094E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.52296198E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09980563E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.14611220E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.63980531E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.61075303E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69295742E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.38551019E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38692007E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86394107E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88449307E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.56889580E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.73814752E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.79574217E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.43688245E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87631622E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09975652E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.10858235E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.76513669E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.89315965E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.69109034E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.15700365E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.38118339E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86339893E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.95436583E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.76218517E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.44177237E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.00675265E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.54177094E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.52296198E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09980563E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.14611220E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.63980531E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.61075303E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69295742E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.38551019E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38692007E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86394107E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88449307E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.56889580E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.73814752E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.79574217E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.43688245E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87631622E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.94557840E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.55781166E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.14878252E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.19447549E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71450168E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.02355914E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43380323E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.17397292E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.69406024E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.02235069E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.30291625E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.15681686E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.94557840E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.55781166E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.14878252E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.19447549E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71450168E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.02355914E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43380323E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.17397292E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.69406024E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.02235069E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.30291625E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.15681686E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.55922808E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01444189E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.65867602E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.33350359E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.54837295E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.54496512E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.09947770E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.43264331E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.56879480E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.87846782E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.23631016E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.38660313E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57005155E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.62982230E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14288458E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.94539084E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.07915036E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     4.56952820E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.13187403E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.71729600E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.54782465E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.42979758E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.94539084E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.07915036E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     4.56952820E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.13187403E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.71729600E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.54782465E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.42979758E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.94821474E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.07236969E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.56515133E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.13078988E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71469327E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.81190211E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42459571E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.24122210E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.34240324E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.53408490E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04417874E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17803495E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17702360E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.32437485E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     7.65983038E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53215492E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48842177E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46181872E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.88250799E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52935373E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     6.31959835E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.61569207E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.14605062E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50447432E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.34947506E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.80331543E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     8.34125399E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01521405E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.89984929E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.39900087E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81146910E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18335084E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80479741E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.13697718E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.13403377E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.22034383E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.26179554E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.26179554E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.26179554E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.44518114E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.44518114E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.48172720E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.48172720E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.31790913E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.31790913E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.85504908E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.80978705E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.41590556E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.08206464E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.21506974E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.25425377E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.66644123E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15739913E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.66428661E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.66422403E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.15227866E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.15015334E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.60405987E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.42325549E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.42325549E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.42325549E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.77200765E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.58930947E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.35653828E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.57646020E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     8.19725510E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     8.19158386E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.43356056E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.63698356E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.63698356E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.63698356E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.49574333E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.49574333E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.35618942E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.35618942E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.98576039E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.98576039E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.98296601E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.98296601E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.27606155E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.27606155E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     7.66005167E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.28975120E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.80314459E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.65723430E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46516757E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46516757E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.36420077E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.04981949E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.47636139E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.52008439E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     4.79863677E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.69179908E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.80217269E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.87676352E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07765231E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17017498E-01    2           5        -5   # BR(h -> b       bb     )
     6.38739805E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26090367E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79105922E-04    2           3        -3   # BR(h -> s       sb     )
     2.06697640E-02    2           4        -4   # BR(h -> c       cb     )
     6.71151847E-02    2          21        21   # BR(h -> g       g      )
     2.30271049E-03    2          22        22   # BR(h -> gam     gam    )
     1.54053683E-03    2          22        23   # BR(h -> Z       gam    )
     2.01107504E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56676252E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78128156E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48189575E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427410E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71212641E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11545984E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666619E-05    2           4        -4   # BR(H -> c       cb     )
     9.96053439E-01    2           6        -6   # BR(H -> t       tb     )
     7.97614811E-04    2          21        21   # BR(H -> g       g      )
     2.74090472E-06    2          22        22   # BR(H -> gam     gam    )
     1.16000000E-06    2          23        22   # BR(H -> Z       gam    )
     3.34324556E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66706403E-04    2          23        23   # BR(H -> Z       Z      )
     9.03637553E-04    2          25        25   # BR(H -> h       h      )
     8.79910797E-24    2          36        36   # BR(H -> A       A      )
     3.50491792E-11    2          23        36   # BR(H -> Z       A      )
     5.28005706E-12    2          24       -37   # BR(H -> W+      H-     )
     5.28005706E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386256E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48470242E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895178E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62257559E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676211E-06    2           3        -3   # BR(A -> s       sb     )
     9.96166098E-06    2           4        -4   # BR(A -> c       cb     )
     9.96985598E-01    2           6        -6   # BR(A -> t       tb     )
     9.43665810E-04    2          21        21   # BR(A -> g       g      )
     3.00985496E-06    2          22        22   # BR(A -> gam     gam    )
     1.35261655E-06    2          23        22   # BR(A -> Z       gam    )
     3.25815108E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74521456E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38813816E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237365E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81144119E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51841994E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45684163E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729633E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402535E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34015332E-04    2          24        25   # BR(H+ -> W+      h      )
     6.11413511E-13    2          24        36   # BR(H+ -> W+      A      )
