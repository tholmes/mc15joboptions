#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.25384476E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05411829E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414167E+03   # H
        36     2.00000000E+03   # A
        37     2.00177630E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013262E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989268E+03   # ~u_L
   2000002     4.99994938E+03   # ~u_R
   1000003     5.00013262E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989268E+03   # ~c_L
   2000004     4.99994938E+03   # ~c_R
   1000005     4.99957182E+03   # ~b_1
   2000005     5.00058751E+03   # ~b_2
   1000006     4.98954817E+03   # ~t_1
   2000006     5.01488809E+03   # ~t_2
   1000011     5.00008201E+03   # ~e_L
   2000011     5.00007592E+03   # ~e_R
   1000012     4.99984207E+03   # ~nu_eL
   1000013     5.00008201E+03   # ~mu_L
   2000013     5.00007592E+03   # ~mu_R
   1000014     4.99984207E+03   # ~nu_muL
   1000015     4.99974630E+03   # ~tau_1
   2000015     5.00041224E+03   # ~tau_2
   1000016     4.99984207E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.24321572E+03   # ~chi_10
   1000023    -1.25216817E+03   # ~chi_20
   1000025     1.26124889E+03   # ~chi_30
   1000035     3.00154832E+03   # ~chi_40
   1000024     1.25127384E+03   # ~chi_1+
   1000037     3.00154804E+03   # ~chi_2+
   1000039     7.60097687E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.07047811E-01   # N_11
  1  2    -6.52927182E-03   # N_12
  1  3    -5.69486401E-01   # N_13
  1  4    -5.54198126E-01   # N_14
  2  1     1.73799624E-02   # N_21
  2  2    -1.82090711E-02   # N_22
  2  3     7.06760207E-01   # N_23
  2  4    -7.07005217E-01   # N_24
  3  1     7.94475074E-01   # N_31
  3  2     6.01427099E-03   # N_32
  3  3     4.19680013E-01   # N_33
  3  4     4.38909867E-01   # N_34
  4  1     4.98237497E-04   # N_41
  4  2    -9.99794792E-01   # N_42
  4  3    -6.62840621E-03   # N_43
  4  4     1.91360682E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.36481535E-03   # U_11
  1  2     9.99956149E-01   # U_12
  2  1     9.99956149E-01   # U_21
  2  2     9.36481535E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.70626252E-02   # V_11
  1  2    -9.99633740E-01   # V_12
  2  1     9.99633740E-01   # V_21
  2  2     2.70626252E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07897107E-01   # cos(theta_t)
  1  2    -7.06315571E-01   # sin(theta_t)
  2  1     7.06315571E-01   # -sin(theta_t)
  2  2     7.07897107E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.68708970E-01   # cos(theta_b)
  1  2     7.43524252E-01   # sin(theta_b)
  2  1    -7.43524252E-01   # -sin(theta_b)
  2  2    -6.68708970E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03868153E-01   # cos(theta_tau)
  1  2     7.10330644E-01   # sin(theta_tau)
  2  1    -7.10330644E-01   # -sin(theta_tau)
  2  2    -7.03868153E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90194280E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51883570E+02   # vev(Q)              
         4     4.42546974E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52778045E-01   # gprime(Q) DRbar
     2     6.27144520E-01   # g(Q) DRbar
     3     1.08619146E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02639257E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72618570E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79740467E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.25384476E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.30250329E+06   # M^2_Hd              
        22    -6.93958083E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37034688E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.36401133E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.64279122E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.49836623E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.33839184E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.47785555E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.13910051E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.71696726E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.50060593E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.42631843E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.33839184E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.47785555E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.13910051E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.71696726E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.50060593E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.42631843E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.50419612E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.00584666E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.08316792E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.03643735E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.03643735E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.03643735E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.03643735E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.26883628E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.63579488E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.35885463E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.70176782E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.27096604E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.32069249E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.53831143E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.61736127E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.37454519E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.16040539E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.81106744E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.66874146E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.07293209E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.88512639E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.14191764E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.62598096E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.61362939E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.93724105E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.47839222E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.07088452E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.35055590E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.11562436E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.70549918E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.41834104E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.68814137E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.35832959E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.64333665E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.80758308E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.57026286E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.28446405E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.14561842E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.19064536E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43946430E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.85353849E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52185540E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63227762E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.17564974E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.97538739E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.35044151E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02206484E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29432265E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.50363451E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.23017277E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.56556010E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.71808832E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59295747E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43949709E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.88199394E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.13817142E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.38880635E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.17728777E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.19456285E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.35544038E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02252385E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22432638E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.87745377E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.17227247E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.61586434E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.21669971E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89503508E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43946430E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.85353849E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52185540E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63227762E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.17564974E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.97538739E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.35044151E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02206484E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29432265E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.50363451E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.23017277E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.56556010E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.71808832E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59295747E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43949709E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.88199394E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.13817142E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.38880635E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.17728777E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.19456285E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.35544038E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02252385E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22432638E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.87745377E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.17227247E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.61586434E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.21669971E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89503508E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.94558685E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.55777291E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.14863886E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.19447553E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71450304E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.02351806E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43380576E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.17397489E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.69404369E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.02222100E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.30293293E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.15672026E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.94558685E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.55777291E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.14863886E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.19447553E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71450304E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.02351806E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43380576E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.17397489E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.69404369E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.02222100E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.30293293E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.15672026E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.55923343E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01443224E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.65884254E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.33350719E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.54837487E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.54517505E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.09948144E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.43263821E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.56880025E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.87845612E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.23632790E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.38660852E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57005356E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.63003918E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14288848E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.94539930E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.07910113E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     4.56933726E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.13187557E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.71729724E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.54748131E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.42980027E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.94539930E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.07910113E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     4.56933726E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.13187557E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.71729724E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.54748131E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.42980027E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.94822333E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.07232022E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.56496040E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.13079137E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71469440E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.81190816E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42459817E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.24093247E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.34257733E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.53409165E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04416009E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17803720E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17702580E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.32427532E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     7.65964234E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53212115E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48840828E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46178753E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.88232270E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52945071E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     6.31975361E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.61540159E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.14604517E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50446973E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.34948510E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.80404800E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     8.34203587E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01535214E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.90063978E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.39900084E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81146870E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18333825E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80479662E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.13697459E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.13403101E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.22028701E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.26178926E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.26178926E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.26178926E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.44458158E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.44458158E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.48152735E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.48152735E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.31770849E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.31770849E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.85514092E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.80949260E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.41586967E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.08205108E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.21504498E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.25409896E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.66623533E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15725273E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.66408089E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.66310362E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.15176518E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.14964003E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.60359408E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.42233350E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.42233350E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.42233350E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.77180945E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.58905211E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.35635951E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.57620345E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     8.19666403E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     8.19099306E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.43305253E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.63686531E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.63686531E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.63686531E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.49561501E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.49561501E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.35606913E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.35606913E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.98533267E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.98533267E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.98253845E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.98253845E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.27567528E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.27567528E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     7.65986367E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.28973744E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.80308069E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.65709602E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46513616E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46513616E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.36446590E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.04987146E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.47654463E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.52011892E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     4.79875672E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.69180529E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.80213085E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.87663575E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07852475E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17099761E-01    2           5        -5   # BR(h -> b       bb     )
     6.38590594E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26037551E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78994178E-04    2           3        -3   # BR(h -> s       sb     )
     2.06655252E-02    2           4        -4   # BR(h -> c       cb     )
     6.71015276E-02    2          21        21   # BR(h -> g       g      )
     2.30221235E-03    2          22        22   # BR(h -> gam     gam    )
     1.54020639E-03    2          22        23   # BR(h -> Z       gam    )
     2.01064541E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56621354E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78117915E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48044071E-03    2           5        -5   # BR(H -> b       bb     )
     2.46435462E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71241106E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11549698E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667081E-05    2           4        -4   # BR(H -> c       cb     )
     9.96057829E-01    2           6        -6   # BR(H -> t       tb     )
     7.97620527E-04    2          21        21   # BR(H -> g       g      )
     2.74087859E-06    2          22        22   # BR(H -> gam     gam    )
     1.16003273E-06    2          23        22   # BR(H -> Z       gam    )
     3.32326642E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65710168E-04    2          23        23   # BR(H -> Z       Z      )
     9.03682984E-04    2          25        25   # BR(H -> h       h      )
     8.70188613E-24    2          36        36   # BR(H -> A       A      )
     3.47759968E-11    2          23        36   # BR(H -> Z       A      )
     5.14873076E-12    2          24       -37   # BR(H -> W+      H-     )
     5.14873076E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82384939E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48321583E-03    2           5        -5   # BR(A -> b       bb     )
     2.43896018E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62260529E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676602E-06    2           3        -3   # BR(A -> s       sb     )
     9.96169529E-06    2           4        -4   # BR(A -> c       cb     )
     9.96989032E-01    2           6        -6   # BR(A -> t       tb     )
     9.43669060E-04    2          21        21   # BR(A -> g       g      )
     3.00986686E-06    2          22        22   # BR(A -> gam     gam    )
     1.35262064E-06    2          23        22   # BR(A -> Z       gam    )
     3.23863567E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74520845E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38488979E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238450E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147954E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51634095E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45686337E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08730066E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404536E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32016814E-04    2          24        25   # BR(H+ -> W+      h      )
     6.20879945E-13    2          24        36   # BR(H+ -> W+      A      )
