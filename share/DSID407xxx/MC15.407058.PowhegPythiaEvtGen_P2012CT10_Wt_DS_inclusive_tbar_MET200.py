#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 Wt production (antitop), MET filtered, with CT10 and Perugia2012 tune and diagram subtraction (DS), EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', 'inclusive', 'systematic']
evgenConfig.contact     = [ 'christian.ohm@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
# the MET-filtered DS samples go over the 24h hard limit on wall time for 5k events
evgenConfig.minevents = 2000

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_Wt_DS_Common.py')

  PowhegConfig.topdecaymode = 11111 # inclusive W-from-top decays
  PowhegConfig.wdecaymode = 11111 # inclusive W decays
  PowhegConfig.ttype  = -1 # anti-top
  PowhegConfig.nEvents *= 150
  PowhegConfig.PDF     = 10800
  PowhegConfig.mu_F    = 1.00000000000000000000
  PowhegConfig.mu_R    = 1.00000000000000000000
  PowhegConfig.hdamp   = -1
  PowhegConfig.hfact   = -1
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')

  include('MC15JobOptions/MissingEtFilter.py')
  filtSeq.MissingEtFilter.METCut = 200*GeV
  
#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')



