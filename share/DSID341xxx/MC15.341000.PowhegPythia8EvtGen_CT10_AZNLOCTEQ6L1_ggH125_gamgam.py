#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->gamgam"
evgenConfig.keywords = ["SM", "Higgs", "SMHiggs", "diphoton", "mH125"]
evgenConfig.contact = ["Junichi.Tanaka@cern.ch"]

#--------------------------------------------------------------
# Powheg ggH setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H = 125.
PowhegConfig.width_H = 0.00407

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 3

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
    PowhegConfig.ew = 1
else:
    PowhegConfig.ew = 0

# Set scaling and masswindow parameters
hfact_scale = 1.2
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
if PowhegConfig.mass_H <= 700.:
    masswindow = min((1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow_max)
else:
    masswindow = min((1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow_max)
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Generate events
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += ['Main31:NFinal = 1']

#--------------------------------------------------------------
# Pythia8 Higgs decays
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["25:onMode = off",      # disable Higgs decays
                            "25:onIfMatch = 22 22"] # enable H->yy
