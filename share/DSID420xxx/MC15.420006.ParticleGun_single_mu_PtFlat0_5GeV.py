evgenConfig.description = "Single muons with flat phi, eta in [-3.0, 3.0], and flat pT [0,5] GeV"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[0.0,5000.0], eta=[-3.0, 3.0])

