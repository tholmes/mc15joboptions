evgenConfig.description = "Pythia6 Perugia2012 tune + Tauola mu mu ->gamma gamma ->tau tau->X, pT > 5 GeV"
evgenConfig.tune = "Perugia2012"
evgenConfig.keywords=["tau","coherent"]
include("MC15JobOptions/Pythia_Perugia2012_Common.py")

genSeq.Pythia.PythiaCommand += ["pyinit use_PYINIT CMS gamma/mu+ gamma/mu+ 5020"]
genSeq.Pythia.PythiaCommand += ["pysubs msel 0",
                                "pysubs msub 85 1",
                                "pypars mstp 7 15", #explicitly select flavor for process 85
                                "pysubs ckin 3 5.", 
                                "pysubs ckin 61 0.00001", #min x for photon, beam1
                                "pysubs ckin 62 0.05", #max x for photon, beam1
                                "pysubs ckin 63 0.00001", #min x for photon, beam2
                                "pysubs ckin 64 0.05", #max x for photon, beam2
                                "pysubs ckin 66 0.001", #max virtuality on photon, beam1
                                "pysubs ckin 68 0.001", #max virtuality on phton, beam2
                                "pysubs ckin 77 10", #minimum of photon-hadron invariant mass
                                "pypars mstp 14 0", #production mode, mix of direct/VMD/GVMD
                                "pypars mstp 55 5", #PDF set for photons, re-set to default
                                "pypars mstp 56 1"] #PDF library for photons, re-set to default


include("MC15JobOptions/Pythia_Tauola.py")

import AthenaCommon.AppMgr as acam
testSeq = acam.athFilterSeq.EvgenTestSeq
if hasattr(testSeq, "TestHepMC") :
    evgenLog.info("Removing TestHepMC sanity checker")
    del testSeq.TestHepMC
