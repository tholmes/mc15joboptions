evgenConfig.description = "Single phi, pT and y taken from STARlight, mass is Breit-Wigner";
evgenConfig.keywords = ["singleParticle","resonance","exclusive"]
include("MC15JobOptions/ParticleGun_Common.py")

resonance_name='phi'
resonance_id=333
resonance_mass=1019.460
resonance_width=4.247


import ParticleGun as PG
import random, math
from ROOT import gRandom

class BreitWignerSampler(PG.ContinuousSampler):
    def __init__(self, mean, sigma):
        self.mean=float(mean)
        self.sigma=float(sigma)
        #for reproducibility
        gRandom.SetSeed(random.randint(1,1e9))
    def shoot(self):
        return math.fabs(gRandom.BreitWigner(self.mean, self.sigma))


# get kinematics file
from PyJobTransformsCore.trfutil import get_files
get_files( "kinematics.STARlight.root", keepDir=False, errorIfNotFound=True )

genSeq.ParticleGun.sampler.pid = (resonance_id)
genSeq.ParticleGun.sampler.mom = PG.PtRapMPhiSampler(pt=PG.TH1Sampler("kinematics.STARlight.root","h_"+resonance_name+"_pT"), 
                                                     rap=PG.TH1Sampler("kinematics.STARlight.root","h_"+resonance_name+"_y"), 
                                                     mass=BreitWignerSampler(resonance_mass,resonance_width))

# Use EvtGen to decay the resonance
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 0.
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = -1
filtSeq.ParticleFilter.PDG = 310

