# JO for Pythia 8 jet jet JZ1 slice with eta* < 2.7

evgenConfig.description = "Dijet truth jet slice JZ1, R04, with the A14 NNPDF23 LO tune, eta* < 2.7"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ1R04.py")
include("MC15JobOptions/JetFilterFragment_MaxEta_m3p0.py")

evgenConfig.minevents = 1000
