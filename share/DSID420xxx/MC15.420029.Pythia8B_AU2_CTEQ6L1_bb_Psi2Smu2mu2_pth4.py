##############################################################
# bb->J/psi(mu2mu2)X  
##############################################################
evgenConfig.description = "Inclusive bb->J/psi(mu2mu2) production"
evgenConfig.keywords = ["charmonium","2muon"]
evgenConfig.minevents = 500

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')

# Hard process
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.'] # Equivalent of CKIN3
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
	
# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# Close B decays and open antiB decays
include("MC15JobOptions/Pythia8B_CloseBDecays.py")

# Open inclusive B->J/psi decays
# B0
genSeq.Pythia8B.Commands += ['511:onIfAny = 100443']
# B+/-
genSeq.Pythia8B.Commands += ['521:onIfAny = 100443']
# Bs
genSeq.Pythia8B.Commands += ['531:onIfAny = 100443']
# Bc
genSeq.Pythia8B.Commands += ['541:onIfAny = 100443']
# LambdaB
genSeq.Pythia8B.Commands += ['5122:onIfAny = 100443']
# Xb+/-
genSeq.Pythia8B.Commands += ['5132:onIfAny = 100443']
# Xb
genSeq.Pythia8B.Commands += ['5232:onIfAny = 100443']
# Omega_b+/-
genSeq.Pythia8B.Commands += ['5332:onIfAny = 100443']

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

# Quark cuts
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 2.0
genSeq.Pythia8B.QuarkEtaCut = 103.1
genSeq.Pythia8B.AntiQuarkEtaCut = 3.1
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->mumu
genSeq.Pythia8B.Commands += ['100443:onMode = off']
genSeq.Pythia8B.Commands += ['100443:1:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
genSeq.Pythia8B.SignalPDGCodes = [100443,-13,13]

# Number of repeat-hadronization loops
genSeq.Pythia8B.NHadronizationLoops = 2

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [2.0]
genSeq.Pythia8B.TriggerStateEtaCut = 3.1
genSeq.Pythia8B.MinimumCountPerCut = [2]

