# Pythia8 minimum bias A14 inclusive

evgenConfig.description = "Incluisve minimum bias, with the A14 NNPDF23LO tune, single muon"
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.contact = ["Andrzej Olszewski <Andrzej.Olszewski@ifj.edu.pl>"]
evgenConfig.keywords = ["QCD", "minBias", "SM"]

include ("MC15JobOptions/nonStandard//Pythia8_A14_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/LowPtMuonFilter.py")

evgenConfig.minevents = 1000
