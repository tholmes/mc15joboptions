evgenConfig.description = "Central Exclusive Production (CEP) at 5020 GeV to 2 gamma (p-p), pT(gamma)>2GeV, |eta(gamma)|<2.6"
evgenConfig.keywords = ["2photon"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["mateusz.dyndal@cern.ch"]

# TODO: Sort out proper param setting based on runArgs.ecmEnergy
if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)


evgenConfig.inputfilecheck = 'cepTOgg_5TeV_4M'

include("MC15JobOptions/HepMCReadFromFile_Common.py")

evgenConfig.tune = "none"
