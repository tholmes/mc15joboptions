# JO for Pythia 8 jet jet 0 slice with lower bound moved to 4 GeV

evgenConfig.description = "Dijet truth jet slice JZ0 from 4 GeV, R04, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilterAkt4.py")

filtSeq.QCDTruthJetFilter.MinPt = 4*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 20*GeV

evgenConfig.minevents = 1000
