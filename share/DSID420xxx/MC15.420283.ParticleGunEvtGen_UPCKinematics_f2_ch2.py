evgenConfig.description = "Single f2(1270), pT and y taken from STARlight, mass is Breit-Wigner, require 2 charged particles in ID acceptance"
evgenConfig.keywords = ["singleParticle","resonance","exclusive"]
include("MC15JobOptions/ParticleGun_Common.py")

resonance_name='f2'
resonance_id=225
resonance_mass=1275.1
resonance_width=185.1


import ParticleGun as PG
import random, math
from ROOT import gRandom

class BreitWignerSampler(PG.ContinuousSampler):
    def __init__(self, mean, sigma):
        self.mean=float(mean)
        self.sigma=float(sigma)
        #for reproducibility
        gRandom.SetSeed(random.randint(1,1e9))
    def shoot(self):
        mass_count=0
        while mass_count < 10:
            mass=math.fabs(gRandom.BreitWigner(self.mean, self.sigma))
            mass_count += 1
            if(math.fabs(mass-self.mean) < 4*self.sigma):
                return mass


# get kinematics file
from PyJobTransformsCore.trfutil import get_files
get_files( "kinematics.STARlight.root", keepDir=False, errorIfNotFound=True )

genSeq.ParticleGun.sampler.pid = (resonance_id)
genSeq.ParticleGun.sampler.mom = PG.PtRapMPhiSampler(pt=PG.TH1Sampler("kinematics.STARlight.root","h_"+resonance_name+"_pT"), 
                                                     rap=PG.TH1Sampler("kinematics.STARlight.root","h_"+resonance_name+"_y"), 
                                                     mass=BreitWignerSampler(resonance_mass,resonance_width))

# Use EvtGen to decay the resonance
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"

include("MC15JobOptions/ChargedTrackFilter.py")
filtSeq.ChargedTracksFilter.NTracks = 1 #Ntracks > 1, i.e. 2 or more tracks
#filtSeq.ChargedTracksFilter.OutputLevel=1
# filtSeq.ChargedTracksFilter.Ptcut = 100
# filtSeq.ChargedTracksFilter.Etacut = 2.7



