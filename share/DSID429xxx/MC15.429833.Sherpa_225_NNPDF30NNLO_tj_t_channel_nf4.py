# SHERPA run card for t-channel single top-quark production at MC@NLO
# and N_f = 4
include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa tj Production t-channel with NF4"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "kai.chung.tam@cern.ch" ]
evgenConfig.minevents = 100000

genSeq.Sherpa_i.RunCard="""
(run){
  # general setting
  #EVENTS 50000

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  #EVENT_GENERATION_MODE Weighted
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  # scales, tags for scale variations
  #   muR = transverse momentum of the bottom
  #   muF = muQ = transverse momentum of the top
  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=SCF
  rscl2:=MPerp2(p[3])
  fscl2:=MPerp2(p[2])
  SCALES VAR{FSF*fscl2}{RSF*rscl2}{QSF*fscl2}

  # collider setup
  #BEAM_1 2212; BEAM_ENERGY_1 6500
  #BEAM_2 2212; BEAM_ENERGY_2 6500

  # disable hadronic W decays
  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  # choose EW Gmu input scheme
  #EW_SCHEME 3

  # required for using top-quark in ME
  #WIDTH[6]  0.

  # configure for N_f = 4
  #PDF_LIBRARY LHAPDFSherpa
  #PDF_SET NNPDF30_nlo_as_0118_nf_4
  #USE_PDF_ALPHAS 1
  MASS[5] 4.18  # as in NNPDF30_nlo_as_0118_nf_4
  MASSIVE[5] 1
}(run)

(processes){
  Process 93 93 -> 6 -5 93
  NLO_QCD_Mode MC@NLO
  Order (*,2)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Min_N_TChannels 1  # require t-channel W
  End process
}(processes)
"""
genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptj", "pptjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "EW_SCHEME=3", "PDF_SET=NNPDF30_nlo_as_0118_nf_4" ]
