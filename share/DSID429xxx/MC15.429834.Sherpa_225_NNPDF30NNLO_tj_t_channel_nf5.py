# SHERPA run card for t-channel single top-quark production at MC@NLO
# and N_f = 5
include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa tj Production t-channel with NF5"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "kai.chung.tam@cern.ch" ]
evgenConfig.minevents = 100000

genSeq.Sherpa_i.RunCard="""
(run){
  # general setting
  #EVENTS 50000

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  #EVENT_GENERATION_MODE Weighted
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  # scales, tags for scale variations
  # SCALES STRICT_METS:
  #   use CKKW clustering scale for real/MC@NLO emission
  # CORESCALE SingleTop:
  #   use Mandelstam \hat{t} for t-channel 2->2 core process
  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=SCF
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2}
  CORE_SCALE SingleTop

  # collider setup
  #BEAM_1 2212; BEAM_ENERGY_1 6500
  #BEAM_2 2212; BEAM_ENERGY_2 6500

  # disable hadronic W decays
  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  # choose EW Gmu input scheme
  #EW_SCHEME 3

  # required for using top-quark in ME
  #WIDTH[6]  0.
}(run)

(processes){
  Process 93 93 -> 6 93
  NLO_QCD_Mode MC@NLO
  Order (*,2)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Min_N_TChannels 1  # require t-channel W
  End process
}(processes)
"""

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptj", "pptjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "EW_SCHEME=3" ]
