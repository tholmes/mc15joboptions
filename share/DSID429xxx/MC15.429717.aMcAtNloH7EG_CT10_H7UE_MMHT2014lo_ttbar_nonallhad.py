# based on the JobOptions MC15.410003 and MC15.429303
# using LHE files from group.phys-gener.MG5_aMCatNLO221.410003.ttbar_inclusive_CT10_13TeV.TXT.mc15_v3

# Provide config information
evgenConfig.generators    += ["aMcAtNlo", "Herwig7", "EvtGen"] 
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "MG5_aMC@NLO+Herwig7 ttbar non-allhad, H7-UE-MMHT tune, with EvtGen"
evgenConfig.keywords       = ['SM', 'top', 'lepton']
evgenConfig.contact        = ['paolo.francavilla@cern.ch', 'dominic.hirschbuehl@cern.ch', 'daniel.rauch@desy.de']

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

# NonAllHad filter
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(non-all had)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
