include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa W+/W- -> enu + 0,1,2j@NLO + 3,4,5j@LO. Validation sample."
evgenConfig.keywords = ["SM","W","electron"]
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=5; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  ABS_ERROR=50.
  ERROR=0.99

  %model setup
  MASSIVE[15] 1;
}(run)

(processes){
  Process 93 93 -> 11 -12 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;

  Process 93 93 -> -11 12 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;
}(processes)

(selector){
  Mass 11 -12 2.0 E_CMS
  Mass -11 12 2.0 E_CMS
}(selector)
"""
