# based on MC15.410141....
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.generators  += ["aMcAtNlo", "Herwig7"]
evgenConfig.description  = 'MG5_aMC@NLO+Herwig7 t-channel single top, CT10f4 ME, H7UE tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'lepton']
evgenConfig.contact      = [ 'dominic.hirschbuehl@cern.ch', 'orel.gueta@cern.ch' ]
#--------------------------------------------------------------
# Showering with Herwig7, H7-UE-MMHT tune
#--------------------------------------------------------------

include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10f4ME_LHEF_EvtGen_Common.py")

## commands specific to showering MG5_aMC@NLO events
genSeq.Herwig7.Commands += hw.mg5amc_cmds().splitlines()
