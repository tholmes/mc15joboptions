from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME

genSeq += Herwig7()

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "MMHT2014"
evgenConfig.description = "Herwig7 dijet sample with MMHT2014 PDF and corresponding tune"
evgenConfig.keywords    = ["SM","QCD", "dijet"]
evgenConfig.contact     = ["Daniel Rauch (daniel.rauch@desy.de)"]

## Configure Herwig/Matchbox
## These are the commands corresponding to what would go
## into the regular Herwig infile

generator = Hw7ConfigBuiltinME(genSeq, runArgs, run_name="HerwigBuiltinME")
generator.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
generator.shower_pdf_commands(order="LO", name="MMHT2014lo68cl")
generator.tune_commands(ps_tune_name = "H7-PS-MMHT2014LO", ue_tune_name = "H7-UE-MMHT")

import os
if "HERWIG7VER" in os.environ:
   version = os.getenv("HERWIG7VER")
   verh7 = version.split(".")[1]
else:
   verh7 = 0

#print os.environ['HERWIG7VER']
#print 'version ',verh7

#if int(verh7 == 0):
#  generator.add_commands("""
#  insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
#  set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

#  set /Herwig/Generators/LHCGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
# The default MinBias cuts of the Herwig authors are X1Min == X2Min == 0.055, but to be consistent $
#  set /Herwig/Cuts/MinBiasCuts:X1Min 0.01
#  set /Herwig/Cuts/MinBiasCuts:X2Min 0.01

# These settings have no effect on the MinBiasCuts, it is set here just to avoid confusion in the l$
#  set /Herwig/Cuts/JetKtCut:MinKT 0*GeV
#  set /Herwig/Cuts/LeptonKtCut:MinKT 0.0*GeV
#  set /Herwig/Cuts/PhotonKtCut:MinKT 0.0*GeV
#  """)
## ------------------
## Hard process setup
## ------------------

#else:
generator.add_commands("""
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

set /Herwig/Generators/EventGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
# The default MinBias cuts of the Herwig authors are X1Min == X2Min == 0.055, but to be consistent with previous productions it is set to 0.01 here.
set /Herwig/Cuts/MinBiasCuts:X1Min 0.01
set /Herwig/Cuts/MinBiasCuts:X2Min 0.01

# These settings have no effect on the MinBiasCuts, it is set here just to avoid confusion in the log file.
set /Herwig/Cuts/JetKtCut:MinKT 0*GeV
set /Herwig/Cuts/LeptonKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/PhotonKtCut:MinKT 0.0*GeV
""")

## run the generator
generator.run()



