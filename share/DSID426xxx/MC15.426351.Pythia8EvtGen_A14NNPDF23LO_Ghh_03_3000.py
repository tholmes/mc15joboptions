######################################################################
# Graviton to mu mu decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton flat in pT"
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = [" fdibello@cern.ch"]
evgenConfig.process = "RS Graviton -> qq"
evgenConfig.generators += [ 'Pythia8' ]

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=["ExtraDimensionsG*:gg2G* = off"]
genSeq.Pythia8.Commands +=["ExtraDimensionsG*:ffbar2G* = on"]
genSeq.Pythia8.Commands +=["5100039:m0 = 1000000."]
genSeq.Pythia8.Commands +=["ExtraDimensionsG*:kappaMG = 1.084"]
genSeq.Pythia8.Commands +=["ExtraDimensionsG*:Ghh = 1"]
genSeq.Pythia8.Commands +=["5100039:mMin = 50."]
genSeq.Pythia8.Commands +=["PhaseSpace:mHatMin = 50."]
genSeq.Pythia8.Commands +=["5100039:onMode = off"]
genSeq.Pythia8.Commands +=["5100039:onIfAny = 25"]
genSeq.Pythia8.Commands +=["25:onMode = off"]
genSeq.Pythia8.Commands +=["25:onIfAny = 5"]

genSeq.Pythia8.UserHooks += ["GravFlat"]
genSeq.Pythia8.Commands += ["GravFlat:EnergyMode = 13"]
genSeq.Pythia8.Commands += ["GravFlat:FlatPt = on"]
