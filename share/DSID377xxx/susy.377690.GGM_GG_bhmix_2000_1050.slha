#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05405919E+01   # W+
        25     1.25000000E+02   # h
        35     2.00410350E+03   # H
        36     2.00000000E+03   # A
        37     2.00157958E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013294E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989241E+03   # ~u_L
   2000002     4.99994929E+03   # ~u_R
   1000003     5.00013294E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989241E+03   # ~c_L
   2000004     4.99994929E+03   # ~c_R
   1000005     4.99965313E+03   # ~b_1
   2000005     5.00050658E+03   # ~b_2
   1000006     4.99157786E+03   # ~t_1
   2000006     5.01287060E+03   # ~t_2
   1000011     5.00008223E+03   # ~e_L
   2000011     5.00007607E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008223E+03   # ~mu_L
   2000013     5.00007607E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99979938E+03   # ~tau_1
   2000015     5.00035953E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.00239653E+03   # ~chi_10
   1000023    -1.05009581E+03   # ~chi_20
   1000025     1.08955961E+03   # ~chi_30
   1000035     3.00313967E+03   # ~chi_40
   1000024     1.04698056E+03   # ~chi_1+
   1000037     3.00313821E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.14711554E-01   # N_11
  1  2    -2.71048293E-02   # N_12
  1  3     4.96479763E-01   # N_13
  1  4    -4.91894875E-01   # N_14
  2  1     4.16760217E-03   # N_21
  2  2    -3.83358484E-03   # N_22
  2  3    -7.06934140E-01   # N_23
  2  4    -7.07256712E-01   # N_24
  3  1     6.99406330E-01   # N_31
  3  2     2.90079896E-02   # N_32
  3  3    -5.03099183E-01   # N_33
  3  4     5.06833834E-01   # N_34
  4  1     9.00976995E-04   # N_41
  4  2    -9.99204268E-01   # N_42
  4  3    -2.53609836E-02   # N_43
  4  4     3.07707711E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.58460380E-02   # U_11
  1  2     9.99357324E-01   # U_12
  2  1     9.99357324E-01   # U_21
  2  2     3.58460380E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.34985797E-02   # V_11
  1  2     9.99053489E-01   # V_12
  2  1     9.99053489E-01   # V_21
  2  2     4.34985797E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08050082E-01   # cos(theta_t)
  1  2     7.06162220E-01   # sin(theta_t)
  2  1    -7.06162220E-01   # -sin(theta_t)
  2  2     7.08050082E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.61037889E-01   # cos(theta_b)
  1  2     7.50352523E-01   # sin(theta_b)
  2  1    -7.50352523E-01   # -sin(theta_b)
  2  2     6.61037889E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03207286E-01   # cos(theta_tau)
  1  2     7.10984889E-01   # sin(theta_tau)
  2  1    -7.10984889E-01   # -sin(theta_tau)
  2  2     7.03207286E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90207148E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52100052E+02   # vev(Q)              
         4     3.49385261E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52814126E-01   # gprime(Q) DRbar
     2     6.27373539E-01   # g(Q) DRbar
     3     1.08044635E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02585858E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71818904E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79826943E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.81566612E+06   # M^2_Hd              
        22    -6.43043923E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37137309E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.14283468E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.11348831E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.08880497E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     9.79729201E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.52991686E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.22667945E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.17802720E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.68956481E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.39830139E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.20685272E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.52991686E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.22667945E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.17802720E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.68956481E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.39830139E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.20685272E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.60812079E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.83381761E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.27490634E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.04884270E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.64920040E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.06623921E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.50820424E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.50820424E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.50820424E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.50820424E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.18380731E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.18380731E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.07459634E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.35870783E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.09225730E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.23596382E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.11790452E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.13774410E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.22054676E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.17668632E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.96500274E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.88935140E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.09680065E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.76938347E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.48126651E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.05217576E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.94616133E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.14240732E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.22085045E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.11815005E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.78459060E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.85402215E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.37040235E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.12338393E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.75711460E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.38326419E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.42634762E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.41829257E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.12527337E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.07614741E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.93697675E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.69187728E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.89835072E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.66852030E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.10024657E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.46038442E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.65461929E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.09156015E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.68364707E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11080265E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.36756933E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86138392E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.95823518E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.55135990E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.60512940E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.40652466E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.80668375E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.50420276E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.10030005E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.14925002E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.74977003E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.50483048E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.68661919E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.11260585E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.37301272E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86190937E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88546894E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.62468105E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.23434774E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.24861508E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.69125479E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87126476E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.10024657E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.46038442E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.65461929E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.09156015E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.68364707E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11080265E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.36756933E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86138392E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.95823518E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.55135990E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.60512940E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.40652466E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.80668375E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.50420276E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.10030005E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.14925002E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.74977003E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.50483048E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.68661919E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.11260585E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.37301272E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86190937E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88546894E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.62468105E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.23434774E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.24861508E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.69125479E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87126476E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.97059006E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.52932669E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.33659152E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06644228E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68600765E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.54830181E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.37912101E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26448765E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.14624210E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73560170E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85358071E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.62774873E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.97059006E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.52932669E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.33659152E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06644228E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68600765E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.54830181E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.37912101E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26448765E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.14624210E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73560170E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85358071E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.62774873E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62157217E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78730863E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47904549E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.66953762E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.50165481E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.83561756E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.00729161E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.37210542E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62268208E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62888280E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.88916465E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.73585684E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54131032E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.89585854E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08667129E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97059731E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.11742108E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.29762093E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.93522895E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69103300E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.27980483E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37499521E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.97059731E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.11742108E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.29762093E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.93522895E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69103300E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.27980483E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37499521E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97353572E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11631686E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.29535045E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.92738746E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.68837376E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.26476546E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.36969345E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.10630740E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.40727391E-03    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.32036934E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.30388538E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10679113E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10676024E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09812117E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55737765E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53026466E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13651070E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46355972E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.33022580E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53936033E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.87779938E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.55085106E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00992212E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87916889E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10908992E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.25615974E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.35685851E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.59816484E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.19160516E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.31909056E-03    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18010438E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52797122E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17205130E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52775306E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38954848E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48923107E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48910521E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45370194E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.96793405E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.96793405E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.96793405E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.62233148E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.62233148E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.12717795E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.12717795E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.87411069E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.87411069E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.85769017E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.85769017E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.53834091E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.53834091E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.29046984E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.61510994E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.31814098E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.00373707E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.59933232E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.28649590E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18420106E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42253659E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.39112942E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42641182E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.14947799E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55198312E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55195353E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.68109030E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.90926792E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.90926792E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.90926792E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.50139513E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.23875278E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.47811477E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.23809481E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.82612644E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.39594490E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.39559225E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.29610460E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.47694630E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.47694630E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.47694630E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31135587E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31135587E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30420790E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30420790E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37117938E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37117938E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37104539E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37104539E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33359337E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33359337E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55741533E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07447625E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.50711558E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24635637E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46339912E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46339912E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16089725E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.12160372E-07    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.29061088E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.35899060E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89033950E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.98662284E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.14711524E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.45802955E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.16562356E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08524740E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17705111E-01    2           5        -5   # BR(h -> b       bb     )
     6.37564230E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25674256E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78223978E-04    2           3        -3   # BR(h -> s       sb     )
     2.06311625E-02    2           4        -4   # BR(h -> c       cb     )
     6.69900538E-02    2          21        21   # BR(h -> g       g      )
     2.30485414E-03    2          22        22   # BR(h -> gam     gam    )
     1.53764081E-03    2          22        23   # BR(h -> Z       gam    )
     2.00750952E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56199045E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78124554E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47072010E-03    2           5        -5   # BR(H -> b       bb     )
     2.46422194E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71194198E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11543951E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667529E-05    2           4        -4   # BR(H -> c       cb     )
     9.96061214E-01    2           6        -6   # BR(H -> t       tb     )
     7.97644234E-04    2          21        21   # BR(H -> g       g      )
     2.70401029E-06    2          22        22   # BR(H -> gam     gam    )
     1.15982819E-06    2          23        22   # BR(H -> Z       gam    )
     3.36215399E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67649202E-04    2          23        23   # BR(H -> Z       Z      )
     9.04217186E-04    2          25        25   # BR(H -> h       h      )
     8.30758140E-24    2          36        36   # BR(H -> A       A      )
     3.32019898E-11    2          23        36   # BR(H -> Z       A      )
     7.12119952E-12    2          24       -37   # BR(H -> W+      H-     )
     7.12119952E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382834E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47366037E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897361E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62265277E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677228E-06    2           3        -3   # BR(A -> s       sb     )
     9.96175014E-06    2           4        -4   # BR(A -> c       cb     )
     9.96994522E-01    2           6        -6   # BR(A -> t       tb     )
     9.43674256E-04    2          21        21   # BR(A -> g       g      )
     3.25325236E-06    2          22        22   # BR(A -> gam     gam    )
     1.35242895E-06    2          23        22   # BR(A -> Z       gam    )
     3.27679645E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74484814E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36349343E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237935E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81146134E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50264721E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45692469E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731292E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99400715E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35859317E-04    2          24        25   # BR(H+ -> W+      h      )
     3.45314128E-13    2          24        36   # BR(H+ -> W+      A      )
