#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413771E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401300E+03   # H
        36     2.00000000E+03   # A
        37     2.00151023E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994926E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994926E+03   # ~c_R
   1000005     4.99949282E+03   # ~b_1
   2000005     5.00066685E+03   # ~b_2
   1000006     4.98751389E+03   # ~t_1
   2000006     5.01691216E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.60000000E+03   # ~g
   1000022     1.40098581E+03   # ~chi_10
   1000023    -1.45008040E+03   # ~chi_20
   1000025     1.48816879E+03   # ~chi_30
   1000035     3.00392579E+03   # ~chi_40
   1000024     1.44618521E+03   # ~chi_1+
   1000037     3.00392286E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19596903E-01   # N_11
  1  2    -3.36076784E-02   # N_12
  1  3     4.92117300E-01   # N_13
  1  4    -4.88744703E-01   # N_14
  2  1     3.01756562E-03   # N_21
  2  2    -3.48702406E-03   # N_22
  2  3    -7.07001241E-01   # N_23
  2  4    -7.07197271E-01   # N_24
  3  1     6.94384040E-01   # N_31
  3  2     3.68757176E-02   # N_32
  3  3    -5.06846763E-01   # N_33
  3  4     5.09487335E-01   # N_34
  4  1     1.41317476E-03   # N_41
  4  2    -9.98748490E-01   # N_42
  4  3    -3.28049835E-02   # N_43
  4  4     3.77265149E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.63552858E-02   # U_11
  1  2     9.98925016E-01   # U_12
  2  1     9.98925016E-01   # U_21
  2  2     4.63552858E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.33159169E-02   # V_11
  1  2     9.98577695E-01   # V_12
  2  1     9.98577695E-01   # V_21
  2  2     5.33159169E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07789339E-01   # cos(theta_t)
  1  2     7.06423564E-01   # sin(theta_t)
  2  1    -7.06423564E-01   # -sin(theta_t)
  2  2     7.07789339E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73942022E-01   # cos(theta_b)
  1  2     7.38784238E-01   # sin(theta_b)
  2  1    -7.38784238E-01   # -sin(theta_b)
  2  2     6.73942022E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04328426E-01   # cos(theta_tau)
  1  2     7.09874262E-01   # sin(theta_tau)
  2  1    -7.09874262E-01   # -sin(theta_tau)
  2  2     7.04328426E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203262E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52208574E+02   # vev(Q)              
         4     3.33225807E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52758846E-01   # gprime(Q) DRbar
     2     6.27023025E-01   # g(Q) DRbar
     3     1.08402976E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02520813E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71683733E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781662E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.91000424E+05   # M^2_Hd              
        22    -7.40535124E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36980772E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.23184482E-05   # gluino decays
#          BR         NDA      ID1       ID2
     8.45279442E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     7.94565583E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.78376420E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.89883250E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.85019852E-09    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.88473296E-06    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.65728488E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.19865263E-09    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.47372595E-05    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.89883250E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.85019852E-09    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.88473296E-06    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.65728488E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.19865263E-09    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.47372595E-05    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.87748832E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.79524573E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     9.34890323E-06    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.71084549E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.71084549E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.71084549E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.71084549E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.21599849E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.30219879E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.61257301E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.77621015E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03301362E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.01283770E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.04447817E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.61031493E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.12765543E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.32877031E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.52044472E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.66498155E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.44780363E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.01413622E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.87153658E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.61523270E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.47243202E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.24985565E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.45328234E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.13223534E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.34553390E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.03998515E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.71588832E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.48930639E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.59260699E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.29295089E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.44421579E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.68443423E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.69573346E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.34416289E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.42351383E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.09319411E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.33684288E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.05036063E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.78726038E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87517086E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.30082051E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.85958318E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.60290141E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.97996037E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.18783802E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.13660037E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.71167639E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.94627712E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.97400638E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59170814E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.33689264E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.95054799E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11409650E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.85193625E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.30477073E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.91772606E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.60838509E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.98039813E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.12088927E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.51022546E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.57230103E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.01939138E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.02491340E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89470277E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.33684288E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.05036063E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.78726038E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87517086E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.30082051E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.85958318E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.60290141E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.97996037E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.18783802E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.13660037E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.71167639E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.94627712E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.97400638E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59170814E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.33689264E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.95054799E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11409650E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.85193625E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.30477073E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.91772606E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.60838509E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.98039813E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.12088927E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.51022546E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.57230103E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.01939138E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.02491340E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89470277E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.92196240E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.83150287E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.79744721E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01610831E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72198542E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.41499311E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45458809E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08016191E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.23307664E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.09072645E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76682274E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.70692284E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.92196240E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.83150287E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.79744721E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01610831E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72198542E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.41499311E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45458809E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08016191E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.23307664E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.09072645E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76682274E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.70692284E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.50562071E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.70309561E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45630754E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.53789529E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.57006333E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.67852285E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.14626879E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.43544845E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50495381E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.56059730E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40234635E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61066284E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60466851E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.12583601E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21554316E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.92206604E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09230920E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.50889998E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.95706445E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.73014161E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19438469E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.44974801E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.92206604E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09230920E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.50889998E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.95706445E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.73014161E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19438469E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.44974801E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92476218E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09130227E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.50750903E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.95065121E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72762488E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.11167550E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44474023E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22358856E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.94757227E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.24305224E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.22739804E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08101867E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08098935E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07278447E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.72295363E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53265179E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11771047E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46263180E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35136727E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53556333E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.53393853E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.54050632E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95905705E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92842560E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12517352E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.49670694E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.72947512E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.17580780E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.35784999E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.30348054E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.15800604E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.49945965E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15046187E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.49925674E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37050967E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.42459332E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.42447478E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.39113970E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.83915746E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.83915746E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.83915746E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.20658967E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.20658967E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.16819089E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.16819089E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.35529937E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.35529937E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.32836249E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.32836249E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.67695476E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.67695476E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18669297E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.77358148E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.82205170E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.49281205E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.30081434E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.72500444E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.03526159E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22791656E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.44123967E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.23551382E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.67447289E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.11907726E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.11905311E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.33003995E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.02268973E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.02268973E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.02268973E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.23441491E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.89326582E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.21234555E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.89263770E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.50038203E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.60789754E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.60756490E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.51365401E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.31963687E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.31963687E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.31963687E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31133633E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31133633E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30398252E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30398252E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37111466E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37111466E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37097679E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37097679E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33244969E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33244969E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.72296455E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.71061639E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51809873E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54809843E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46485260E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46485260E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14407306E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20313954E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38375756E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80480198E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.72823270E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.43276944E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.35021691E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.47339489E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08573602E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17775646E-01    2           5        -5   # BR(h -> b       bb     )
     6.37480586E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25644649E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78161343E-04    2           3        -3   # BR(h -> s       sb     )
     2.06288026E-02    2           4        -4   # BR(h -> c       cb     )
     6.69817824E-02    2          21        21   # BR(h -> g       g      )
     2.30358813E-03    2          22        22   # BR(h -> gam     gam    )
     1.53749973E-03    2          22        23   # BR(h -> Z       gam    )
     2.00703975E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56168411E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78103161E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46994119E-03    2           5        -5   # BR(H -> b       bb     )
     2.46426290E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71208682E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546516E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668140E-05    2           4        -4   # BR(H -> c       cb     )
     9.96064764E-01    2           6        -6   # BR(H -> t       tb     )
     7.97663870E-04    2          21        21   # BR(H -> g       g      )
     2.71270210E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010847E-06    2          23        22   # BR(H -> Z       gam    )
     3.35004314E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67045301E-04    2          23        23   # BR(H -> Z       Z      )
     9.03228644E-04    2          25        25   # BR(H -> h       h      )
     7.46520661E-24    2          36        36   # BR(H -> A       A      )
     2.97014141E-11    2          23        36   # BR(H -> Z       A      )
     6.82821657E-12    2          24       -37   # BR(H -> W+      H-     )
     6.82821657E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382050E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47285791E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897861E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62267044E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677461E-06    2           3        -3   # BR(A -> s       sb     )
     9.96177056E-06    2           4        -4   # BR(A -> c       cb     )
     9.96996566E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676190E-04    2          21        21   # BR(A -> g       g      )
     3.16289877E-06    2          22        22   # BR(A -> gam     gam    )
     1.35269630E-06    2          23        22   # BR(A -> Z       gam    )
     3.26526084E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472480E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36163424E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237508E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81144625E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50145730E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45694094E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731618E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401925E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34652090E-04    2          24        25   # BR(H+ -> W+      h      )
     2.75898968E-13    2          24        36   # BR(H+ -> W+      A      )
