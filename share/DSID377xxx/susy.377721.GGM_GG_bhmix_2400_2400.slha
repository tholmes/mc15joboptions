#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.37400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.40000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05425881E+01   # W+
        25     1.25000000E+02   # h
        35     2.00397947E+03   # H
        36     2.00000000E+03   # A
        37     2.00152376E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013293E+03   # ~d_L
   2000001     5.00002540E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994920E+03   # ~u_R
   1000003     5.00013293E+03   # ~s_L
   2000003     5.00002540E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994920E+03   # ~c_R
   1000005     4.99911224E+03   # ~b_1
   2000005     5.00104734E+03   # ~b_2
   1000006     4.97784781E+03   # ~t_1
   2000006     5.02650208E+03   # ~t_2
   1000011     5.00008213E+03   # ~e_L
   2000011     5.00007620E+03   # ~e_R
   1000012     4.99984167E+03   # ~nu_eL
   1000013     5.00008213E+03   # ~mu_L
   2000013     5.00007620E+03   # ~mu_R
   1000014     4.99984167E+03   # ~nu_muL
   1000015     4.99943900E+03   # ~tau_1
   2000015     5.00071988E+03   # ~tau_2
   1000016     4.99984167E+03   # ~nu_tauL
   1000021     2.40000000E+03   # ~g
   1000022     2.33801350E+03   # ~chi_10
   1000023    -2.40006051E+03   # ~chi_20
   1000025     2.42609764E+03   # ~chi_30
   1000035     3.00994936E+03   # ~chi_40
   1000024     2.39018721E+03   # ~chi_1+
   1000037     3.00990185E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.69269585E-01   # N_11
  1  2    -7.42864387E-02   # N_12
  1  3     4.49896223E-01   # N_13
  1  4    -4.47548007E-01   # N_14
  2  1     1.82906540E-03   # N_21
  2  2    -2.87249368E-03   # N_22
  2  3    -7.07058111E-01   # N_23
  2  4    -7.07147249E-01   # N_24
  3  1     6.38861993E-01   # N_31
  3  2     1.03021451E-01   # N_32
  3  3    -5.38509371E-01   # N_33
  3  4     5.39675450E-01   # N_34
  4  1     8.73572252E-03   # N_41
  4  2    -9.91897099E-01   # N_42
  4  3    -8.75778195E-02   # N_43
  4  4     9.16185477E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.23270682E-01   # U_11
  1  2     9.92373085E-01   # U_12
  2  1     9.92373085E-01   # U_21
  2  2     1.23270682E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.28968027E-01   # V_11
  1  2     9.91648752E-01   # V_12
  2  1     9.91648752E-01   # V_21
  2  2     1.28968027E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07518715E-01   # cos(theta_t)
  1  2     7.06694607E-01   # sin(theta_t)
  2  1    -7.06694607E-01   # -sin(theta_t)
  2  2     7.07518715E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87179908E-01   # cos(theta_b)
  1  2     7.26487284E-01   # sin(theta_b)
  2  1    -7.26487284E-01   # -sin(theta_b)
  2  2     6.87179908E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05468567E-01   # cos(theta_tau)
  1  2     7.08741209E-01   # sin(theta_tau)
  2  1    -7.08741209E-01   # -sin(theta_tau)
  2  2     7.05468567E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90190469E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.40000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52420744E+02   # vev(Q)              
         4     2.78847207E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52674931E-01   # gprime(Q) DRbar
     2     6.26491445E-01   # g(Q) DRbar
     3     1.07754733E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02423580E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71043682E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79684472E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.37400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.39215549E+06   # M^2_Hd              
        22    -1.06670170E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36743463E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.16771511E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.13424660E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.53829288E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.96849411E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.53829288E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.96849411E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.38904000E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.88444859E-12    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.88444859E-12    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.88444859E-12    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.88444859E-12    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.42449689E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.31296843E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.03983499E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.28233974E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09115872E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.03327073E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.05936740E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.68816234E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.32100380E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.45679267E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.90201281E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.51273909E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.39437596E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     7.51160134E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.61678778E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.66056903E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.89634009E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.81624788E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.06006792E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.43549394E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.82263489E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01487345E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.81171445E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.35846819E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.95078104E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.11608073E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.49609661E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.95652803E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.11904878E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.17260639E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.43134212E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.12077882E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.82084562E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.10179837E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.92232899E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.00693890E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.14001211E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.04956822E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.30359518E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.70196848E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.64823538E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.32727190E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.27664610E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.53736070E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.00525203E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61351541E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.82093969E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.02062070E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.34056678E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.72006064E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.16790756E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.87258298E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.31660292E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.70243857E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60051266E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.99185494E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.28689022E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.95813564E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.16286357E-07    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90049460E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.82084562E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.10179837E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.92232899E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.00693890E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.14001211E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.04956822E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.30359518E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.70196848E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.64823538E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.32727190E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.27664610E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.53736070E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.00525203E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61351541E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.82093969E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.02062070E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.34056678E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.72006064E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.16790756E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.87258298E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.31660292E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.70243857E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60051266E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.99185494E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.28689022E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.95813564E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.16286357E-07    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90049460E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.76934825E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.53915156E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.41786082E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.82040510E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.79170943E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.27508055E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.64481267E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.48446778E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.02238329E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.30292391E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.97706650E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.17172776E-05    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.76934825E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.53915156E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.41786082E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.82040510E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.79170943E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.27508055E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.64481267E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.48446778E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.02238329E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.30292391E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.97706650E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.17172776E-05    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.13100007E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.51925101E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.87669946E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.93491595E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.79186127E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.25129884E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.62327782E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.68736606E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.12888210E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.40185894E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.09463787E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98892131E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.83842263E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.94352605E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.71626722E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.76973168E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.10742505E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.35558516E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.71287254E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.84678338E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.39532822E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.63490794E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.76973168E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.10742505E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.35558516E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.71287254E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.84678338E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.39532822E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.63490794E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.77163279E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10666544E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.35122574E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.71101173E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.84483072E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.46201276E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.63113788E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.78048589E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.63317249E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     1.26684849E-03    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     2.78981570E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.77981032E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     9.29939481E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     9.29920760E-02    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.24672753E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.50519750E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.57160741E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.08119511E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.45282156E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.60405223E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.46328688E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.12329264E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.59346198E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.04425551E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.85161737E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     9.15425911E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
     1.20679503E-03    2     1000039        35   # BR(~chi_10 -> ~G        H)
     5.16578595E-05    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     5.06254005E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.56834378E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     6.18282261E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.46693221E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.62046495E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.86319580E-05    2     1000039        35   # BR(~chi_20 -> ~G        H)
     7.59292976E-04    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.07390717E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.39070409E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.06902670E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.39058185E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.31222370E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.17687362E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.17679331E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.15423692E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.34486884E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.34486884E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.34486884E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.74548494E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.74548494E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.10393186E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.10393186E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.24849524E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.24849524E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.24720859E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.24720859E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.92233156E-05    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.92233156E-05    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
     4.39134190E-27    3     1000021        -2         2   # BR(~chi_20 -> ~g      ub      u)
     5.73147612E-27    3     1000021        -1         1   # BR(~chi_20 -> ~g      db      d)
     4.39134190E-27    3     1000021        -4         4   # BR(~chi_20 -> ~g      cb      c)
     5.73147612E-27    3     1000021        -3         3   # BR(~chi_20 -> ~g      sb      s)
#
#         PDG            Width
DECAY   1000025     1.64545486E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.07995024E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.74469683E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.16858560E-01    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.92722787E-01    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     6.43219268E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.67510556E-03    2     1000039        35   # BR(~chi_30 -> ~G        H)
     6.85392202E-05    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     5.44454138E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.72913340E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15921597E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.84408903E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.87397510E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.29714961E-05    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.29713505E-05    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.64142856E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.17658758E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.17658758E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.17658758E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.79452149E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.20888463E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.70148372E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.20609692E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.53802488E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.41833567E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.41820155E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.37986269E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.83269649E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.83269649E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.83269649E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.06556664E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.06556664E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.99597697E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.99597697E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.02185152E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.02185152E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.02172080E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.02172080E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.98530264E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.98530264E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     2.02260592E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     4.17822460E-09    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     2.02260592E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     4.17822460E-09    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     2.40234494E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     2.50445301E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.54069681E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52475487E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.80956457E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.49777091E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.49777091E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.94393180E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.22394911E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.58263871E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.60536342E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.62594208E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.22803408E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.35420896E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.68561443E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.09022560E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18234733E-01    2           5        -5   # BR(h -> b       bb     )
     6.36756541E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25388364E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77618593E-04    2           3        -3   # BR(h -> s       sb     )
     2.06065128E-02    2           4        -4   # BR(h -> c       cb     )
     6.69071602E-02    2          21        21   # BR(h -> g       g      )
     2.30002968E-03    2          22        22   # BR(h -> gam     gam    )
     1.53587721E-03    2          22        23   # BR(h -> Z       gam    )
     2.00448301E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55887245E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78076628E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46282473E-03    2           5        -5   # BR(H -> b       bb     )
     2.46443685E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71270178E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11554685E-06    2           3        -3   # BR(H -> s       sb     )
     1.00669908E-05    2           4        -4   # BR(H -> c       cb     )
     9.96081336E-01    2           6        -6   # BR(H -> t       tb     )
     7.97632399E-04    2          21        21   # BR(H -> g       g      )
     2.71798286E-06    2          22        22   # BR(H -> gam     gam    )
     1.16054144E-06    2          23        22   # BR(H -> Z       gam    )
     3.31127006E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65111968E-04    2          23        23   # BR(H -> Z       Z      )
     8.99592199E-04    2          25        25   # BR(H -> h       h      )
     6.97959444E-24    2          36        36   # BR(H -> A       A      )
     2.84840872E-11    2          23        36   # BR(H -> Z       A      )
     6.21047483E-12    2          24       -37   # BR(H -> W+      H-     )
     6.21047483E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82377849E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46571058E-03    2           5        -5   # BR(A -> b       bb     )
     2.43900540E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62276517E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678710E-06    2           3        -3   # BR(A -> s       sb     )
     9.96188000E-06    2           4        -4   # BR(A -> c       cb     )
     9.97007519E-01    2           6        -6   # BR(A -> t       tb     )
     9.43686558E-04    2          21        21   # BR(A -> g       g      )
     3.12521073E-06    2          22        22   # BR(A -> gam     gam    )
     1.35311927E-06    2          23        22   # BR(A -> Z       gam    )
     3.22744198E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74469554E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34578920E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49241140E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81157465E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49131636E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45701547E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08733102E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99405807E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.30781770E-04    2          24        25   # BR(H+ -> W+      h      )
     2.88481648E-13    2          24        36   # BR(H+ -> W+      A      )
