#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.93500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.95000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05420951E+01   # W+
        25     1.25000000E+02   # h
        35     2.00399431E+03   # H
        36     2.00000000E+03   # A
        37     2.00151888E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002539E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994923E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002539E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994923E+03   # ~c_R
   1000005     4.99929237E+03   # ~b_1
   2000005     5.00086726E+03   # ~b_2
   1000006     4.98242656E+03   # ~t_1
   2000006     5.02196522E+03   # ~t_2
   1000011     5.00008214E+03   # ~e_L
   2000011     5.00007616E+03   # ~e_R
   1000012     4.99984169E+03   # ~nu_eL
   1000013     5.00008214E+03   # ~mu_L
   2000013     5.00007616E+03   # ~mu_R
   1000014     4.99984169E+03   # ~nu_muL
   1000015     4.99955915E+03   # ~tau_1
   2000015     5.00059974E+03   # ~tau_2
   1000016     4.99984169E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.89589546E+03   # ~chi_10
   1000023    -1.95006821E+03   # ~chi_20
   1000025     1.98342061E+03   # ~chi_30
   1000035     3.00575214E+03   # ~chi_40
   1000024     1.94435441E+03   # ~chi_1+
   1000037     3.00574274E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.43705988E-01   # N_11
  1  2    -4.68022394E-02   # N_12
  1  3     4.72870719E-01   # N_13
  1  4    -4.70217224E-01   # N_14
  2  1     2.24726113E-03   # N_21
  2  2    -3.13393060E-03   # N_22
  2  3    -7.07039688E-01   # N_23
  2  4    -7.07163353E-01   # N_24
  3  1     6.68496269E-01   # N_31
  3  2     5.65765253E-02   # N_32
  3  3    -5.23470579E-01   # N_33
  3  4     5.25252690E-01   # N_34
  4  1     3.01520146E-03   # N_41
  4  2    -9.97295756E-01   # N_42
  4  3    -4.96660508E-02   # N_43
  4  4     5.40866555E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.01204556E-02   # U_11
  1  2     9.97538531E-01   # U_12
  2  1     9.97538531E-01   # U_21
  2  2     7.01204556E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.63687899E-02   # V_11
  1  2     9.97079640E-01   # V_12
  2  1     9.97079640E-01   # V_21
  2  2     7.63687899E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07613910E-01   # cos(theta_t)
  1  2     7.06599288E-01   # sin(theta_t)
  2  1    -7.06599288E-01   # -sin(theta_t)
  2  2     7.07613910E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.82539979E-01   # cos(theta_b)
  1  2     7.30848259E-01   # sin(theta_b)
  2  1    -7.30848259E-01   # -sin(theta_b)
  2  2     6.82539979E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05070494E-01   # cos(theta_tau)
  1  2     7.09137221E-01   # sin(theta_tau)
  2  1    -7.09137221E-01   # -sin(theta_tau)
  2  2     7.05070494E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90198941E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.95000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52328199E+02   # vev(Q)              
         4     3.06197358E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52709043E-01   # gprime(Q) DRbar
     2     6.26707604E-01   # g(Q) DRbar
     3     1.08044708E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02479818E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71379352E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79728068E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.93500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.21115055E+06   # M^2_Hd              
        22    -8.92649181E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36839937E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.68639136E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.58398062E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.18548235E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.48614158E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.54383064E-06    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.74607613E-12    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.37135572E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.00757319E-05    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.86235879E-12    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     9.98289824E-10    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.54383064E-06    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.74607613E-12    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.37135572E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.00757319E-05    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.86235879E-12    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     9.98289824E-10    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.38609926E-06    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.81965096E-09    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.17648431E-10    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.54465657E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.54465657E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.54465657E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.54465657E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.84520834E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.81009592E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.33639842E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.97396882E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.08825055E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.00808606E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.13112220E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.65793035E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.74659769E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.93511495E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.21965167E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.66590189E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.76044799E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.46473657E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.46580874E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.64883382E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.21008434E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.40979227E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.28456775E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.73745569E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.55822252E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.04135874E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.17278705E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.42333936E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.28788999E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.61664180E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.95439602E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.36941743E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.85528733E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.25252291E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.77844775E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.13334755E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09852085E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.66477131E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.75444452E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.11404113E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.65476428E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     7.55403189E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.31470740E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86868886E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.93792945E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.22248008E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.99012783E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.73940813E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.03943069E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60380715E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09859279E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28777488E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.17024889E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.02447264E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.66351163E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.36879698E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.32233389E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86913626E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88039537E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.72634832E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.12768286E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.48169060E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.25483670E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89791857E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09852085E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.66477131E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.75444452E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.11404113E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.65476428E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     7.55403189E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.31470740E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86868886E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.93792945E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.22248008E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.99012783E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.73940813E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.03943069E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60380715E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09859279E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28777488E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.17024889E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.02447264E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.66351163E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.36879698E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.32233389E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86913626E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88039537E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.72634832E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.12768286E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.48169060E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.25483670E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89791857E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.84693197E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.95213450E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.72351669E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.12649632E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77394309E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.86120980E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56956450E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.78893763E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.60990810E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.02302044E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.38999037E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.12985304E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.84693197E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.95213450E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.72351669E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.12649632E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77394309E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.86120980E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56956450E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.78893763E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.60990810E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.02302044E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.38999037E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.12985304E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.32244640E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.65411456E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.24023539E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.21907204E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.68296951E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.89121307E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.37914307E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.54845740E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.32071277E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.52567643E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.76841441E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.28834572E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.71878495E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.05959607E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.45082852E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.84717384E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.08920777E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.54302323E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.97737353E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79221196E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.76519577E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56309553E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.84717384E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.08920777E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.54302323E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.97737353E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79221196E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.76519577E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56309553E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.84948290E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.08832514E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.53529010E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.97334015E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.78994930E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.56745117E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.55862168E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.79853583E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.88159966E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.04376499E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03103109E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.01458947E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.01456563E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.00788886E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.43941879E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.54067589E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.02286500E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46081823E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.45506686E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52049232E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.16815555E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.04840026E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.05363267E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.84307152E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.03295806E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.49987734E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.51896921E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.30568796E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.47979115E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     6.14736208E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.10834084E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.43523649E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10214945E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.43507448E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33179441E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.27832181E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.27822268E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.25036943E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.54730348E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.54730348E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.54730348E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.93895821E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.93895821E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.65854119E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.65854119E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.97965296E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.97965296E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.97457023E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.97457023E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.87688950E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.87688950E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.74177227E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.76841688E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.85861101E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.55759504E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.88367079E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.10128042E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     2.45375784E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.97169500E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.36273557E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.00739967E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.80301403E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     5.43454057E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     5.43447868E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.58211505E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.43111227E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.43111227E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.43111227E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.51482457E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.96161106E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.49600929E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.96106360E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.62309871E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.48065401E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.48037541E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.40145359E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.94850211E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.94850211E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.94850211E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.21556955E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.21556955E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.20768787E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.20768787E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.05189299E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.05189299E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.05174509E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.05174509E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.01047216E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.01047216E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.43930823E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.04043410E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52555038E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.58261266E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47114467E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47114467E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.04114018E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.47044678E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.48184491E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.94379287E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.22311801E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.21997108E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.99114560E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.07655151E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08791686E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18002568E-01    2           5        -5   # BR(h -> b       bb     )
     6.37132281E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25521362E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77900201E-04    2           3        -3   # BR(h -> s       sb     )
     2.06179168E-02    2           4        -4   # BR(h -> c       cb     )
     6.69453648E-02    2          21        21   # BR(h -> g       g      )
     2.30169287E-03    2          22        22   # BR(h -> gam     gam    )
     1.53671853E-03    2          22        23   # BR(h -> Z       gam    )
     2.00575914E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56031754E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78092327E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46647684E-03    2           5        -5   # BR(H -> b       bb     )
     2.46432480E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71230565E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11549475E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668917E-05    2           4        -4   # BR(H -> c       cb     )
     9.96071937E-01    2           6        -6   # BR(H -> t       tb     )
     7.97649951E-04    2          21        21   # BR(H -> g       g      )
     2.71637533E-06    2          22        22   # BR(H -> gam     gam    )
     1.16034906E-06    2          23        22   # BR(H -> Z       gam    )
     3.33690112E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66390013E-04    2          23        23   # BR(H -> Z       Z      )
     9.01493368E-04    2          25        25   # BR(H -> h       h      )
     7.11218448E-24    2          36        36   # BR(H -> A       A      )
     2.90175656E-11    2          23        36   # BR(H -> Z       A      )
     6.46356235E-12    2          24       -37   # BR(H -> W+      H-     )
     6.46356235E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380222E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46939089E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899027E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62271167E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678005E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181819E-06    2           4        -4   # BR(A -> c       cb     )
     9.97001333E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680702E-04    2          21        21   # BR(A -> g       g      )
     3.13644559E-06    2          22        22   # BR(A -> gam     gam    )
     1.35294473E-06    2          23        22   # BR(A -> Z       gam    )
     3.25246593E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471617E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35394031E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239160E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150463E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49653313E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697391E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732274E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403239E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33344272E-04    2          24        25   # BR(H+ -> W+      h      )
     2.83890973E-13    2          24        36   # BR(H+ -> W+      A      )
