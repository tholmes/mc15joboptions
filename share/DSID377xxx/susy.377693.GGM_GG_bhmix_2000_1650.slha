#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.64000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416906E+01   # W+
        25     1.25000000E+02   # h
        35     2.00400486E+03   # H
        36     2.00000000E+03   # A
        37     2.00151513E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994925E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994925E+03   # ~c_R
   1000005     4.99941275E+03   # ~b_1
   2000005     5.00074689E+03   # ~b_2
   1000006     4.98547847E+03   # ~t_1
   2000006     5.01893601E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007613E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007613E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99963923E+03   # ~tau_1
   2000015     5.00051965E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.59914936E+03   # ~chi_10
   1000023    -1.65007489E+03   # ~chi_20
   1000025     1.68643106E+03   # ~chi_30
   1000035     3.00449447E+03   # ~chi_40
   1000024     1.64561340E+03   # ~chi_1+
   1000037     3.00449002E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.29212190E-01   # N_11
  1  2    -3.77918366E-02   # N_12
  1  3     4.84635799E-01   # N_13
  1  4    -4.81611360E-01   # N_14
  2  1     2.65349318E-03   # N_21
  2  2    -3.33657464E-03   # N_22
  2  3    -7.07020116E-01   # N_23
  2  4    -7.07180586E-01   # N_24
  3  1     6.84279999E-01   # N_31
  3  2     4.29859592E-02   # N_32
  3  3    -5.13613968E-01   # N_33
  3  4     5.15862173E-01   # N_34
  4  1     1.85035434E-03   # N_41
  4  2    -9.98355073E-01   # N_42
  4  3    -3.80971076E-02   # N_43
  4  4     4.28057813E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.38207763E-02   # U_11
  1  2     9.98550612E-01   # U_12
  2  1     9.98550612E-01   # U_21
  2  2     5.38207763E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.04792979E-02   # V_11
  1  2     9.98169452E-01   # V_12
  2  1     9.98169452E-01   # V_21
  2  2     6.04792979E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07706329E-01   # cos(theta_t)
  1  2     7.06506725E-01   # sin(theta_t)
  2  1    -7.06506725E-01   # -sin(theta_t)
  2  2     7.07706329E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.78010028E-01   # cos(theta_b)
  1  2     7.35052652E-01   # sin(theta_b)
  2  1    -7.35052652E-01   # -sin(theta_b)
  2  2     6.78010028E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04680561E-01   # cos(theta_tau)
  1  2     7.09524705E-01   # sin(theta_tau)
  2  1    -7.09524705E-01   # -sin(theta_tau)
  2  2     7.04680561E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202542E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52259207E+02   # vev(Q)              
         4     3.21094874E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52737117E-01   # gprime(Q) DRbar
     2     6.26885455E-01   # g(Q) DRbar
     3     1.08044678E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02513404E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71517281E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79759197E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.64000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.34134036E+03   # M^2_Hd              
        22    -7.92870234E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36919336E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.14952788E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.85589255E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.77020523E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.10009676E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.85364290E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.98428932E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.12779528E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.44186883E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.81937265E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.65244421E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.85364290E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.98428932E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.12779528E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.44186883E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.81937265E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.65244421E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.86071468E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.54331674E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.30092110E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.29677977E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     6.89350484E-06    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.52444953E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.52444953E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.52444953E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.52444953E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.56397013E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.56397013E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.93336612E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.46068893E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.97865711E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.04163606E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.10797852E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.05921786E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.18633196E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.46325288E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.83022432E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.34982860E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.91689881E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.09614749E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.63339287E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.30552486E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.23258200E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.44656254E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.22615083E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.59321175E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.89465852E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.10673946E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.51640689E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.11089690E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.06846022E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.36282741E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.32902495E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.56563930E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.83166603E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.73353330E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.86227059E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.39343086E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.76924786E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.98944240E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09912632E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.09613653E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.75204648E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.07238074E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.66860717E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.22807171E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.33961256E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86612426E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.94571629E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.33887747E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.05163760E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.00743084E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.65810681E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.56536535E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09919120E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.20725834E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.04932333E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.14410807E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.67417139E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.14043242E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.34613769E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86660147E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88234102E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.04419343E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.88613229E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.18766417E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.97908334E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88768044E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09912632E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.09613653E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.75204648E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.07238074E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.66860717E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.22807171E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.33961256E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86612426E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.94571629E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.33887747E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.05163760E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.00743084E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.65810681E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.56536535E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09919120E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.20725834E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.04932333E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.14410807E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.67417139E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.14043242E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.34613769E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86660147E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88234102E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.04419343E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.88613229E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.18766417E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.97908334E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88768044E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.89372795E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.55397766E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.82276187E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.71906198E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.74264461E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.11132741E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49891992E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.97114189E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.38149649E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.02107524E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.61841575E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.75525257E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.89372795E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.55397766E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.82276187E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.71906198E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.74264461E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.11132741E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49891992E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.97114189E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.38149649E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.02107524E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.61841575E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.75525257E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43704309E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.69621641E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.39330558E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.41171361E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61163565E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.32577658E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.23130749E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.47576271E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43584292E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.55963663E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.15839407E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48453638E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64581002E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.13929917E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.29971928E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.89388442E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09289533E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.25088956E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.20603762E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75352873E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.92830104E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.49356408E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.89388442E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09289533E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.25088956E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.20603762E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75352873E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.92830104E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.49356408E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.89643581E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09193263E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.24978769E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.20057090E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75110323E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.80360456E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48874603E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.41824616E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.86840068E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.17843704E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.16393740E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.05948023E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.05945308E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.05185218E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.68718137E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53480074E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08123403E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46211415E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.39016235E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53161259E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.61129389E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.14533926E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00381170E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88709892E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09089387E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.80732807E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.55626358E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.56454499E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.48332036E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.66435961E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.14037148E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.47666416E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.13335892E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.47647738E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35775391E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.37271331E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.37260237E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.34141398E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.73566652E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.73566652E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.73566652E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.59764988E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.59764988E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.21987233E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.21987233E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.19921671E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.19921671E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.19586582E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.19586582E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.59597259E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.59597259E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.96960295E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.20452168E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.53591616E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.03971584E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.70976291E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.94715688E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.48909165E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.78260333E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.29532578E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.79768638E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.66154374E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.15676797E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.15673189E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.54501462E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.67471425E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.67471425E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.67471425E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.00695076E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.59879845E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.98548963E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.59818254E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.21498150E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.93568527E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.93536390E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.84453538E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.18541332E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.18541332E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.18541332E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.29357777E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.29357777E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.28589906E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.28589906E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.31191978E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.31191978E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.31177577E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.31177577E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.27155374E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.27155374E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.68716379E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.90525922E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52181160E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.65614523E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46644731E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46644731E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10586612E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.36760626E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42242253E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82020583E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.79009402E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     7.82966345E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.11691206E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.32586649E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08703410E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17906967E-01    2           5        -5   # BR(h -> b       bb     )
     6.37276746E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25572498E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78008466E-04    2           3        -3   # BR(h -> s       sb     )
     2.06222706E-02    2           4        -4   # BR(h -> c       cb     )
     6.69601682E-02    2          21        21   # BR(h -> g       g      )
     2.30254219E-03    2          22        22   # BR(h -> gam     gam    )
     1.53702860E-03    2          22        23   # BR(h -> Z       gam    )
     2.00631062E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56087050E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78099794E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46789067E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427722E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71213742E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547229E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668468E-05    2           4        -4   # BR(H -> c       cb     )
     9.96067783E-01    2           6        -6   # BR(H -> t       tb     )
     7.97659603E-04    2          21        21   # BR(H -> g       g      )
     2.71456335E-06    2          22        22   # BR(H -> gam     gam    )
     1.16020886E-06    2          23        22   # BR(H -> Z       gam    )
     3.34783984E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66935446E-04    2          23        23   # BR(H -> Z       Z      )
     9.02591707E-04    2          25        25   # BR(H -> h       h      )
     7.31576145E-24    2          36        36   # BR(H -> A       A      )
     2.94019089E-11    2          23        36   # BR(H -> Z       A      )
     6.65226418E-12    2          24       -37   # BR(H -> W+      H-     )
     6.65226418E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381182E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47081842E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898414E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269002E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677719E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179318E-06    2           4        -4   # BR(A -> c       cb     )
     9.96998829E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678332E-04    2          21        21   # BR(A -> g       g      )
     3.14917092E-06    2          22        22   # BR(A -> gam     gam    )
     1.35280501E-06    2          23        22   # BR(A -> Z       gam    )
     3.26313192E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472151E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35711944E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238337E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147555E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49856780E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695728E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731943E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402145E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34435904E-04    2          24        25   # BR(H+ -> W+      h      )
     2.80403331E-13    2          24        36   # BR(H+ -> W+      A      )
