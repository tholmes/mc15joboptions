#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413738E+01   # W+
        25     1.25000000E+02   # h
        35     2.00400695E+03   # H
        36     2.00000000E+03   # A
        37     2.00151687E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994926E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994926E+03   # ~c_R
   1000005     4.99949348E+03   # ~b_1
   2000005     5.00066618E+03   # ~b_2
   1000006     4.98751432E+03   # ~t_1
   2000006     5.01691132E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.40098579E+03   # ~chi_10
   1000023    -1.45008040E+03   # ~chi_20
   1000025     1.48816878E+03   # ~chi_30
   1000035     3.00392583E+03   # ~chi_40
   1000024     1.44618518E+03   # ~chi_1+
   1000037     3.00392290E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19596765E-01   # N_11
  1  2    -3.36078339E-02   # N_12
  1  3     4.92117397E-01   # N_13
  1  4    -4.88744798E-01   # N_14
  2  1     3.01756601E-03   # N_21
  2  2    -3.48703959E-03   # N_22
  2  3    -7.07001240E-01   # N_23
  2  4    -7.07197272E-01   # N_24
  3  1     6.94384183E-01   # N_31
  3  2     3.68758737E-02   # N_32
  3  3    -5.06846661E-01   # N_33
  3  4     5.09487231E-01   # N_34
  4  1     1.41318114E-03   # N_41
  4  2    -9.98748479E-01   # N_42
  4  3    -3.28051282E-02   # N_43
  4  4     3.77266814E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.63554903E-02   # U_11
  1  2     9.98925006E-01   # U_12
  2  1     9.98925006E-01   # U_21
  2  2     4.63554903E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.33161523E-02   # V_11
  1  2     9.98577682E-01   # V_12
  2  1     9.98577682E-01   # V_21
  2  2     5.33161523E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07789381E-01   # cos(theta_t)
  1  2     7.06423522E-01   # sin(theta_t)
  2  1    -7.06423522E-01   # -sin(theta_t)
  2  2     7.07789381E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73903310E-01   # cos(theta_b)
  1  2     7.38819551E-01   # sin(theta_b)
  2  1    -7.38819551E-01   # -sin(theta_b)
  2  2     6.73903310E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04327947E-01   # cos(theta_tau)
  1  2     7.09874738E-01   # sin(theta_tau)
  2  1    -7.09874738E-01   # -sin(theta_tau)
  2  2     7.04327947E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199662E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52208217E+02   # vev(Q)              
         4     3.24713818E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52759388E-01   # gprime(Q) DRbar
     2     6.27026699E-01   # g(Q) DRbar
     3     1.07402834E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02516490E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71375046E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781869E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.94071510E+05   # M^2_Hd              
        22    -7.16806882E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36982391E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.10730052E-03   # gluino decays
#          BR         NDA      ID1       ID2
     3.44685543E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.77573808E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.35354932E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.81919631E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.04175189E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.00978530E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.43582576E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.34649616E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.17677304E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.81919631E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.04175189E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.00978530E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.43582576E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.34649616E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.17677304E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.89397554E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.68494377E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.09972869E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.10468540E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.00811923E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.23329847E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.15045713E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.15045713E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.15045713E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.15045713E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.22080718E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.22080718E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.24478541E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.03356901E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.35795623E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.33229309E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.48000276E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.43013038E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.92936629E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.20417816E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.11320293E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.83376551E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.38479698E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.11761711E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.13387516E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.31180015E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.23262046E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.06575964E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.47905367E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.09582586E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.22390821E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.61240617E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.19040642E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.65050580E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.42044266E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.58010307E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.61940536E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.60578083E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.48516687E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.20097840E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.75870522E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.17402021E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.56945478E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.91361103E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.35807527E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.20461765E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     9.88971446E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.20473225E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.66797826E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.59545565E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.13381206E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.24869127E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.20898575E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.83930143E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.67052869E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.49818173E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.17667591E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.26624430E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.35815424E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.33259912E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.90377387E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.00006988E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.67455347E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.98578145E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.13471213E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.24950102E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.14250913E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01569361E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.76470021E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.25450488E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.89865611E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.80588364E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.35807527E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.20461765E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     9.88971446E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.20473225E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.66797826E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.59545565E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.13381206E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.24869127E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.20898575E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.83930143E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.67052869E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.49818173E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.17667591E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.26624430E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.35815424E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.33259912E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.90377387E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.00006988E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.67455347E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.98578145E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.13471213E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.24950102E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.14250913E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01569361E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.76470021E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.25450488E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.89865611E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.80588364E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.92199212E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.83143134E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.79749549E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01610298E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72198948E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.41501818E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45459625E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08016830E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.23307466E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.09072880E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76682472E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.70701020E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.92199212E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.83143134E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.79749549E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01610298E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72198948E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.41501818E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45459625E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08016830E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.23307466E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.09072880E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76682472E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.70701020E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.50563815E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.70308568E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45628105E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.53788926E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.57006857E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.67854587E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.14627931E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.43543846E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50497249E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.56058198E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40231983E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61065137E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60467740E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.12593726E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21556099E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.92209578E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09230255E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.50890720E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.95700581E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.73014568E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19441785E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.44975612E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.92209578E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09230255E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.50890720E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.95700581E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.73014568E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19441785E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.44975612E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92479192E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09129564E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.50751626E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.95059267E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72762896E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.11170140E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44474836E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22361571E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.94750630E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.24305445E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.22740023E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08101941E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08099008E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07278520E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.72301893E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53265434E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11771448E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46263956E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35137105E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53554522E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.53380661E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.54025221E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95906380E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92841467E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12521533E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.49674112E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.72953439E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.17572218E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.35763647E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.30342466E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.15800801E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.49946112E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15046383E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.49925821E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37051104E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.42459164E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.42447310E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.39113804E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.83915080E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.83915080E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.83915080E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.20667626E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.20667626E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.16824844E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.16824844E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.35558800E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.35558800E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.32865047E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.32865047E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.67710376E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.67710376E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18674504E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.77356605E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.82195973E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.49278014E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.30078010E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.72504787E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.03526750E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22792472E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.44124252E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.23552179E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.67110354E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.11909808E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.11907393E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.33000844E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.02271265E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.02271265E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.02271265E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.23440682E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.89325325E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.21233747E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.89262513E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.50037066E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.60785909E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.60752646E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.51361603E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.31962855E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.31962855E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.31962855E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31133897E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31133897E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30398516E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30398516E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37112347E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37112347E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37098560E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37098560E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33245845E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33245845E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.72302985E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.71062437E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51810122E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54810230E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46486042E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46486042E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14406513E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20317641E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38374698E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80475847E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.72814445E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.43288462E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.35029084E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.47338225E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08771126E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17960458E-01    2           5        -5   # BR(h -> b       bb     )
     6.37165698E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25533190E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77925247E-04    2           3        -3   # BR(h -> s       sb     )
     2.06189339E-02    2           4        -4   # BR(h -> c       cb     )
     6.69497962E-02    2          21        21   # BR(h -> g       g      )
     2.30247203E-03    2          22        22   # BR(h -> gam     gam    )
     1.53675616E-03    2          22        23   # BR(h -> Z       gam    )
     2.00607092E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56044630E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78096225E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46671142E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431256E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71226235E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548821E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668643E-05    2           4        -4   # BR(H -> c       cb     )
     9.96069572E-01    2           6        -6   # BR(H -> t       tb     )
     7.97669805E-04    2          21        21   # BR(H -> g       g      )
     2.71269831E-06    2          22        22   # BR(H -> gam     gam    )
     1.16012936E-06    2          23        22   # BR(H -> Z       gam    )
     3.33912212E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66500737E-04    2          23        23   # BR(H -> Z       Z      )
     9.03275975E-04    2          25        25   # BR(H -> h       h      )
     7.27715592E-24    2          36        36   # BR(H -> A       A      )
     2.94787729E-11    2          23        36   # BR(H -> Z       A      )
     6.65700439E-12    2          24       -37   # BR(H -> W+      H-     )
     6.65700439E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380405E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46963053E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898910E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62270753E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677950E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181341E-06    2           4        -4   # BR(A -> c       cb     )
     9.97000854E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680249E-04    2          21        21   # BR(A -> g       g      )
     3.16291272E-06    2          22        22   # BR(A -> gam     gam    )
     1.35270100E-06    2          23        22   # BR(A -> Z       gam    )
     3.25460017E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471523E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35460475E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238972E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81149801E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49695838E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697055E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732207E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403020E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33562267E-04    2          24        25   # BR(H+ -> W+      h      )
     2.82017419E-13    2          24        36   # BR(H+ -> W+      A      )
