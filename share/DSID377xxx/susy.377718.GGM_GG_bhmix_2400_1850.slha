#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419663E+01   # W+
        25     1.25000000E+02   # h
        35     2.00399567E+03   # H
        36     2.00000000E+03   # A
        37     2.00151953E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994923E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994923E+03   # ~c_R
   1000005     4.99933281E+03   # ~b_1
   2000005     5.00082683E+03   # ~b_2
   1000006     4.98344457E+03   # ~t_1
   2000006     5.02095485E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.40000000E+03   # ~g
   1000022     1.79827731E+03   # ~chi_10
   1000023    -1.85007025E+03   # ~chi_20
   1000025     1.88553248E+03   # ~chi_30
   1000035     3.00526046E+03   # ~chi_40
   1000024     1.84484589E+03   # ~chi_1+
   1000037     3.00525327E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.30160595E-01   # N_11
  1  2    -4.39670663E-02   # N_12
  1  3     4.83512654E-01   # N_13
  1  4    -4.80778448E-01   # N_14
  2  1     2.36659308E-03   # N_21
  2  2    -3.19866990E-03   # N_22
  2  3    -7.07034052E-01   # N_23
  2  4    -7.07168309E-01   # N_24
  3  1     6.83266778E-01   # N_31
  3  2     5.06939356E-02   # N_32
  3  3    -5.14080684E-01   # N_33
  3  4     5.16040391E-01   # N_34
  4  1     2.53261436E-03   # N_41
  4  2    -9.97740843E-01   # N_42
  4  3    -4.51598607E-02   # N_43
  4  4     4.96727527E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.37750518E-02   # U_11
  1  2     9.97964299E-01   # U_12
  2  1     9.97964299E-01   # U_21
  2  2     6.37750518E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.01550482E-02   # V_11
  1  2     9.97536099E-01   # V_12
  2  1     9.97536099E-01   # V_21
  2  2     7.01550482E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07641403E-01   # cos(theta_t)
  1  2     7.06571755E-01   # sin(theta_t)
  2  1    -7.06571755E-01   # -sin(theta_t)
  2  2     7.07641403E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81184455E-01   # cos(theta_b)
  1  2     7.32111835E-01   # sin(theta_b)
  2  1    -7.32111835E-01   # -sin(theta_b)
  2  2     6.81184455E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954786E-01   # cos(theta_tau)
  1  2     7.09252247E-01   # sin(theta_tau)
  2  1    -7.09252247E-01   # -sin(theta_tau)
  2  2     7.04954786E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199033E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305637E+02   # vev(Q)              
         4     3.08112539E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52718071E-01   # gprime(Q) DRbar
     2     6.26764873E-01   # g(Q) DRbar
     3     1.07754645E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02487001E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71314063E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738428E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.85039840E+05   # M^2_Hd              
        22    -8.51006785E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36865496E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.46754020E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.74830747E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.97914698E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.34497846E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.15423254E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.24170793E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.37266981E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.19042284E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.49076333E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     5.64396048E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.15423254E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.24170793E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.37266981E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.19042284E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.49076333E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     5.64396048E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.18773512E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     9.75303581E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.42397408E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.55895768E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     3.14513452E-02    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     2.27967669E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.41838810E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.41838810E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.41838810E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.41838810E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     8.59689751E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     8.59689751E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.60259240E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.86565030E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.05301356E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.28793013E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.21248029E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.12937318E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.38198708E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.24280848E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.48883520E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.52261557E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05081538E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.58433070E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.90741309E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.73245898E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.76321611E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.19818117E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.93699697E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.03059413E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.54132691E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.20730240E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.76055054E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.20951513E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.57544104E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.18365261E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.02524213E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.87440995E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.04701983E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.89526518E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.11572447E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.47831824E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.29791813E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.82157373E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.82199690E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.77776749E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.84443047E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.43401735E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.21455960E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     7.59458523E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.43373500E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.69645217E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.66382439E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.57583053E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.65952170E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.18989096E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.67590065E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.52342352E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.82207639E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.56549110E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.02194017E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.56709345E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.22306947E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.27629168E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.44192655E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.69699188E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60440744E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.67821987E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.89520571E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.67762057E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.34512790E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87644047E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.82199690E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.77776749E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.84443047E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.43401735E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.21455960E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     7.59458523E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.43373500E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.69645217E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.66382439E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.57583053E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.65952170E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.18989096E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.67590065E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.52342352E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.82207639E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.56549110E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.02194017E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.56709345E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.22306947E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.27629168E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.44192655E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.69699188E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60440744E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.67821987E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.89520571E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.67762057E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.34512790E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87644047E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.86272741E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.96596480E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77190360E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.51195370E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76413909E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.14315526E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.54661979E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.85052535E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.40534001E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.58033523E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.59456918E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.50020551E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.86272741E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.96596480E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77190360E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.51195370E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76413909E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.14315526E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.54661979E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.85052535E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.40534001E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.58033523E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.59456918E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.50020551E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36118646E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.60923079E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.30075238E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.34503778E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.65875851E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25843068E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.32856477E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.52309247E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35960732E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47807007E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.90462092E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.41603733E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.69370655E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.76103827E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.39852039E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.86294046E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07030846E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04339180E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.59544439E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77933542E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.01280778E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54057926E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.86294046E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07030846E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04339180E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.59544439E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77933542E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.01280778E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54057926E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86533338E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06941462E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.04252044E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.59077147E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77701432E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.84087366E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.53598093E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.49289564E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.21504595E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.06658951E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.05266475E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.02219765E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.02217157E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.01487194E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55083116E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53819737E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07438320E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46137027E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.40076837E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52520168E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.90886421E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.64964508E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95462420E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93612292E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09252881E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02955843E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.32176056E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.99678194E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.34559954E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.80902157E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11355235E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44196935E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10688366E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44179255E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32932314E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.29362642E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.29352058E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.26376897E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.57782132E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.57782132E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.57782132E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.04293960E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.04293960E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.95344745E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.95344745E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.34764670E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.34764670E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.34285852E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.34285852E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.35026896E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.35026896E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.00296968E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.12794212E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.04219569E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.27115719E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.61296548E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.36929611E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.42697379E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.70190561E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.42894389E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.72064865E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.28223861E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.97457192E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.97453803E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.42728107E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.30248636E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.30248636E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.30248636E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.74196628E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.25572205E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.72252357E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.25516170E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.90726860E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.15233793E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.15204774E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.06998129E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02898737E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02898737E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02898737E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26072815E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26072815E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25319586E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25319586E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.20242123E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.20242123E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.20227996E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.20227996E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.16282573E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.16282573E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55076186E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.64042849E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52456104E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.10977358E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46911621E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46911621E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09607212E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.58522128E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42971988E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88599529E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.02172690E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.02772568E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.60507046E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.84500057E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08836270E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18040103E-01    2           5        -5   # BR(h -> b       bb     )
     6.37062975E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25496830E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77848214E-04    2           3        -3   # BR(h -> s       sb     )
     2.06156658E-02    2           4        -4   # BR(h -> c       cb     )
     6.69383094E-02    2          21        21   # BR(h -> g       g      )
     2.30154559E-03    2          22        22   # BR(h -> gam     gam    )
     1.53654386E-03    2          22        23   # BR(h -> Z       gam    )
     2.00557806E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56003833E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78092559E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46574073E-03    2           5        -5   # BR(H -> b       bb     )
     2.46432466E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71230514E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11549457E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668942E-05    2           4        -4   # BR(H -> c       cb     )
     9.96072219E-01    2           6        -6   # BR(H -> t       tb     )
     7.97655560E-04    2          21        21   # BR(H -> g       g      )
     2.71586108E-06    2          22        22   # BR(H -> gam     gam    )
     1.16031115E-06    2          23        22   # BR(H -> Z       gam    )
     3.33718558E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66404193E-04    2          23        23   # BR(H -> Z       Z      )
     9.01899374E-04    2          25        25   # BR(H -> h       h      )
     7.21001741E-24    2          36        36   # BR(H -> A       A      )
     2.90668931E-11    2          23        36   # BR(H -> Z       A      )
     6.47283257E-12    2          24       -37   # BR(H -> W+      H-     )
     6.47283257E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82379954E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46866187E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899197E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62271770E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678084E-06    2           3        -3   # BR(A -> s       sb     )
     9.96182516E-06    2           4        -4   # BR(A -> c       cb     )
     9.97002030E-01    2           6        -6   # BR(A -> t       tb     )
     9.43681362E-04    2          21        21   # BR(A -> g       g      )
     3.14001190E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290227E-06    2          23        22   # BR(A -> Z       gam    )
     3.25274069E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471369E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35237551E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239406E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81151333E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49553165E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697906E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732377E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403212E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33372754E-04    2          24        25   # BR(H+ -> W+      h      )
     2.84498849E-13    2          24        36   # BR(H+ -> W+      A      )
