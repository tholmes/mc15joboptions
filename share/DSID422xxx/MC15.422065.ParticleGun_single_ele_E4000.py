# Generates fixed energy 4 TeV electron gun

evgenConfig.description = "Single ele with phi in [0.0, 0.2], eta in [0.0, 2.5], and E = 4 TeV"
evgenConfig.keywords = ["singleParticle", "electron"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 11
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=4000000,eta=[0.0,2.5],phi=[0.0, 0.2])
