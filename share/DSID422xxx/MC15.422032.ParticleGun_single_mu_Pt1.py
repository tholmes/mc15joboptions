evgenConfig.description = "Single muons with flat eta-phi and fixed pT of 1 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]
	
include("MC15JobOptions/ParticleGun_Common.py")
	
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=1000, eta=[-4.2, 4.2])
