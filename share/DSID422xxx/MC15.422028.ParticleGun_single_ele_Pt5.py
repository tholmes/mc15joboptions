evgenConfig.description = "Single electrons with flat eta-phi and fixed pT of 5 GeV"
evgenConfig.keywords = ["singleParticle", "electron"]
	
include("MC15JobOptions/ParticleGun_Common.py")
	
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-11, 11)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=5000, eta=[-4.2, 4.2])
