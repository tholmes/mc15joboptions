model="LightVector"
mDM1 = 5.
mDM2 = 210.
mZp = 180.
mHD = 125.
widthZp = 8.594363e+01
widthN2 = 8.173004e-01
filteff = 9.529255e-02

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]
evgenConfig.minevents = 500

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
