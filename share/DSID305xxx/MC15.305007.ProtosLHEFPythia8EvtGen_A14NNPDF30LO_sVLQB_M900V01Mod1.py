evgenConfig.description = "Protos VLB single production (B singlet, M = 900 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["You Zhou <y.zhou@cern.ch>"]
evgenConfig.inputfilecheck = "VLQ_Bbj_M900V01MOD1"
evgenConfig.process = "pp>Bbq"

include("MC15JobOptions/ProtosLHEF_Common.py") 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

