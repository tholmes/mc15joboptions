model="InelasticVectorEFT"
mDM1 = 90.
mDM2 = 360.
mZp = 180.
mHD = 125.
widthZp = 7.161969e+01
widthN2 = 1.182535e+09
filteff = 9.124088e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
