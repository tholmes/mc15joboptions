mchi = 100
mphi = 500
gx = 1.0
filter_string = "T"
evt_multiplier = 10
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
