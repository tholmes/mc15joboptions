######################################################################
# Graviton to e e decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 4000. GeV, kappaMG = 1.626."
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = [" sebastien.rettie@cern.ch,thrynova@mail.cern.ch"]
evgenConfig.process = "RS Graviton -> e e"

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
"ExtraDimensionsG*:gg2G* = on",
"ExtraDimensionsG*:ffbar2G* = on",
"5100039:m0 = 4000.",
"5100039:onMode = off",
"5100039:onIfAny = 11",
"ExtraDimensionsG*:kappaMG = 1.626",
"5100039:mMin = 50.",
"PhaseSpace:mHatMin = 50.",
]

genSeq.Pythia8.UserModes += ["GravFlat:EnergyMode = 13"]
