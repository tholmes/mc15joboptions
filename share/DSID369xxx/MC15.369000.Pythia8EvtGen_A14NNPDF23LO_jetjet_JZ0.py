# JO for Pythia 8 jet jet JZ0 slice

evgenConfig.description = "Dijet truth jet slice JZ0, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.generators += ["Pythia8"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

#include("MC15JobOptions/JetFilter_JZ0.py")
include ("GeneratorFilters/FindJets.py")
evgenConfig.minevents = 1000
