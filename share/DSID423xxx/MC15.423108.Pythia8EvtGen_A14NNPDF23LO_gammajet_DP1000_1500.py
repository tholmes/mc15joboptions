evgenConfig.description = "Pythia8 gammajet sample. jetjet, gamma+jet events with at least one hard process or parton shower photon with 1000 < pT < 1500 (GeV)"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.minevents = 100

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 500"]

include("MC15JobOptions/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.Ptmin = 1000000.
filtSeq.DirectPhotonFilter.Ptmax = 1500000.
