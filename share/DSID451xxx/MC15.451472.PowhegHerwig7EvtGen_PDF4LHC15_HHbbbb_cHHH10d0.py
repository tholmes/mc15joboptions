#-------------------------------------------------------------
# Herwig 7 showering setup
#-------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_30_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7.1-Default"

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# HW7 settings and Higgs BR
Herwig7Config.add_commands("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes
do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
""")


# EVGEN Configuration
evgenConfig.generators  = [ "Powheg", "Herwig7", "EvtGen"]
evgenConfig.description = "DiHiggs production with cHHH +10.0, decay to 4b, with Powheg Box V2, at NLO + full top mass"
evgenConfig.keywords    = ["hh", "BSM", "SMHiggs", "nonResonant", "bbbar", "bottom"]
evgenConfig.contact     = ['Shota Hayashida <shota.hayashida@cern.ch>']
evgenConfig.minevents   = 10000
evgenConfig.inputFilesPerJob = 2

# run Herwig7 
Herwig7Config.run()
