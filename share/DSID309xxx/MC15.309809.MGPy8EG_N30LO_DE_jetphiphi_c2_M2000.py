# Inverse of M scale
invMscale=0.0005

# Wilson coefficients
c1=0.000000e+00
c2=1.000000e+00

evt_multiplier=1
filter_string="MET150"

include("MC15JobOptions/MadGraphControl_DarkEnergy_jetphiphi_c2_M2000.py")
