include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "JHU+Pythia8, Graviton Longitudinal Bulk, mVV = 3000, @LO"
evgenConfig.process = "gg -> G -> WW -> jet+jet"
evgenConfig.keywords = ["BSM", "spin2", "diboson", "jets" ]
evgenConfig.contact  = [ "sebastian.andres.olivares.pino@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.generators = ["JHU", "Pythia8", "EvtGen"]
evgenConfig.inputfilecheck = 'gg_GravitonLongitudinal_WW4q_m3000'
  
