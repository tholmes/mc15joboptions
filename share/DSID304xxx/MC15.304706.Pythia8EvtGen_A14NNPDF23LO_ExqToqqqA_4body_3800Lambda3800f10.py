######################################################################
# Excited quarks to gamm+jet with Pythia 8
######################################################################

evgenConfig.description = "PYTHIA 8 q* -> gamma+3 jets, q* mass = lambda = 3800 GeV"
evgenConfig.keywords = ["exotic", "excitedQuark", "photon", "jets", "BSM"] 
evgenConfig.contact = ["amoroso@cern.ch"]
evgenConfig.process = "q* -> gamma+3 jets"

#Excited Quark Mass (in GeV)
M_ExQ = 3800.

#Mass Scale parameter (Lambda, in GeV)
M_Lam = M_ExQ

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
                               "ExcitedFermion:ug2uStar = on",             #switch on ug -> u*
                               "ExcitedFermion:Lambda = 3800",      # Compositness scale
                               "4000002:m0=3800",                   # u* mass
                               "4000002:oneChannel = 1 1 0  1 -1 2 22",
                               "ExcitedFermion:coupF = 1.",                #coupling strength of SU(2)
                               "ExcitedFermion:coupFprime = 1.",           #coupling strength of U(1)
                               "ExcitedFermion:coupFcol = 1."              #coupling strength of SU(3)
                              ]
