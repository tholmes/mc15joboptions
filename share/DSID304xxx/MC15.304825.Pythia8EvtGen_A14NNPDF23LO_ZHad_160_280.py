evgenConfig.description = "High pT Hadronic Z   160 < pT < 280"
evgenConfig.process = "Z + jets (Z -> qqbar)"
evgenConfig.keywords = ["SM","Z","jets"] 
evgenConfig.contact  = ["craig.sawyer@cern.ch", "alexander.tuna@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")	 

genSeq.Pythia8.Commands += ["PartonLevel:FSR = on"] # turn on FSR (no Photons interface to Pythia8)
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = 160."]
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMax = 280."]
genSeq.Pythia8.Commands += ["WeakBosonAndParton:qqbar2gmZg = on"]
genSeq.Pythia8.Commands += ["WeakBosonAndParton:qg2gmZq = on"]
genSeq.Pythia8.Commands += ["23:onMode = off"] # switch off all Z decays
genSeq.Pythia8.Commands += ["23:onIfAny = 1 2 3 4 5"] # switch on Z->had

