evgenConfig.description = "Single muon with flat eta (between 2.3 and 4.3), flat phi and fixed pT = 1 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 13
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=1000.0, eta=[[-4.3, -2.3], [2.3, 4.3]])
