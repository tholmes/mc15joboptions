evgenConfig.description = "Single electron with flat eta-phi and flat pT = [20, 100] GeV"
evgenConfig.keywords = ["singleParticle", "electron"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 11
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[20000.0, 100000.0], eta=[2.3, 4.3])

