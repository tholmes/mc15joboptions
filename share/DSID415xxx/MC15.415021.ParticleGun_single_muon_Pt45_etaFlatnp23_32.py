evgenConfig.description = "Single muon with flat eta-phi and fixed pT = 45 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 13
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=45000.0, eta=[[-3.2, -2.3], [2.3, 3.2]])

