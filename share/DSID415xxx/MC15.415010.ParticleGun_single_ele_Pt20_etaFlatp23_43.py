evgenConfig.description = "Single electron with flat eta-phi and fixed pT = 20 GeV"
evgenConfig.keywords = ["singleParticle", "electron"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 11
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=20000.0, eta=[2.3, 4.3])

