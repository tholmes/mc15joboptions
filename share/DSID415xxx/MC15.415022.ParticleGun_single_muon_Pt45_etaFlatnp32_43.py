evgenConfig.description = "Single muon with flat eta(between 3.2 and 4.3) positive and negative, flat phi and fixed pT = 45 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 13
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=45000.0, eta=[[-4.3, -3.2], [3.2, 4.3]])

