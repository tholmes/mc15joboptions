#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12256101E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03973623E+01   # W+
        25     1.24852206E+02   # h
        35     3.00020529E+03   # H
        36     2.99999959E+03   # A
        37     3.00096522E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02401675E+03   # ~d_L
   2000001     3.01869824E+03   # ~d_R
   1000002     3.02309313E+03   # ~u_L
   2000002     3.02070147E+03   # ~u_R
   1000003     3.02401675E+03   # ~s_L
   2000003     3.01869824E+03   # ~s_R
   1000004     3.02309313E+03   # ~c_L
   2000004     3.02070147E+03   # ~c_R
   1000005     5.65729570E+02   # ~b_1
   2000005     3.01821362E+03   # ~b_2
   1000006     5.62788971E+02   # ~t_1
   2000006     3.00306916E+03   # ~t_2
   1000011     3.00678150E+03   # ~e_L
   2000011     3.00114024E+03   # ~e_R
   1000012     3.00538081E+03   # ~nu_eL
   1000013     3.00678150E+03   # ~mu_L
   2000013     3.00114024E+03   # ~mu_R
   1000014     3.00538081E+03   # ~nu_muL
   1000015     2.98599868E+03   # ~tau_1
   2000015     3.02218886E+03   # ~tau_2
   1000016     3.00549357E+03   # ~nu_tauL
   1000021     2.32930058E+03   # ~g
   1000022     1.00672393E+02   # ~chi_10
   1000023     2.14830844E+02   # ~chi_20
   1000025    -3.00221058E+03   # ~chi_30
   1000035     3.00267526E+03   # ~chi_40
   1000024     2.14990250E+02   # ~chi_1+
   1000037     3.00336682E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888473E-01   # N_11
  1  2    -1.53910704E-03   # N_12
  1  3     1.48039751E-02   # N_13
  1  4    -1.23069414E-03   # N_14
  2  1     1.93066521E-03   # N_21
  2  2     9.99649740E-01   # N_22
  2  3    -2.62182746E-02   # N_23
  2  4     3.04482139E-03   # N_24
  3  1    -9.56945789E-03   # N_31
  3  2     1.64030070E-02   # N_32
  3  3     7.06830112E-01   # N_33
  3  4     7.07128390E-01   # N_34
  4  1    -1.13021744E-02   # N_41
  4  2     2.07115629E-02   # N_42
  4  3     7.06742271E-01   # N_43
  4  4    -7.07077545E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99311325E-01   # U_11
  1  2    -3.71062730E-02   # U_12
  2  1     3.71062730E-02   # U_21
  2  2     9.99311325E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990710E-01   # V_11
  1  2    -4.31052679E-03   # V_12
  2  1     4.31052679E-03   # V_21
  2  2     9.99990710E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98890940E-01   # cos(theta_t)
  1  2    -4.70838612E-02   # sin(theta_t)
  2  1     4.70838612E-02   # -sin(theta_t)
  2  2     9.98890940E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99921734E-01   # cos(theta_b)
  1  2     1.25110301E-02   # sin(theta_b)
  2  1    -1.25110301E-02   # -sin(theta_b)
  2  2     9.99921734E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06895250E-01   # cos(theta_tau)
  1  2     7.07318249E-01   # sin(theta_tau)
  2  1    -7.07318249E-01   # -sin(theta_tau)
  2  2     7.06895250E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01918268E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.22561005E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44701302E+02   # higgs               
         4     6.70773420E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.22561005E+03  # The gauge couplings
     1     3.61492960E-01   # gprime(Q) DRbar
     2     6.39616545E-01   # g(Q) DRbar
     3     1.03087507E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.22561005E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37919372E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.22561005E+03  # The trilinear couplings
  1  1     3.40766068E-07   # A_d(Q) DRbar
  2  2     3.40835669E-07   # A_s(Q) DRbar
  3  3     7.94284744E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.22561005E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.49530590E-08   # A_mu(Q) DRbar
  3  3     7.58487652E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.22561005E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.56435519E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.22561005E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11140894E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.22561005E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05049967E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.22561005E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.85056432E+04   # M^2_Hd              
        22    -9.12880853E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42021354E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.24547480E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48255801E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48255801E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51744199E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51744199E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.91663151E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.30180344E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.73475202E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.13506764E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05739162E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.18649522E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.75574142E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.52990237E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.19373586E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.63031683E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52266849E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.02634366E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.57991837E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.79458903E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.86933270E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.95120839E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24994251E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.89115599E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.87864097E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.63500306E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.52004928E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -4.24298719E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28725386E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.91105643E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.22727693E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.09724201E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07967690E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.75678649E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63783189E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.93955918E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.23096147E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27546950E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.23875568E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.02913053E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20354334E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57568346E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.83373970E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.57580732E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.41920620E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42431065E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08476822E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.94564871E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63479795E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.34171905E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.02510514E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26971635E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.00251004E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.03602787E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69055100E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48369050E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.66000520E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.86957560E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.74381456E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55162928E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07967690E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.75678649E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63783189E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.93955918E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.23096147E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27546950E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.23875568E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.02913053E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20354334E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57568346E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.83373970E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.57580732E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.41920620E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42431065E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08476822E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.94564871E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63479795E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.34171905E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.02510514E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26971635E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.00251004E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.03602787E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69055100E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48369050E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.66000520E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.86957560E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.74381456E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55162928E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01961559E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.64666429E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01755178E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.39641393E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.65297686E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01778174E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.31960927E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55658528E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996302E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.69849409E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01961559E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.64666429E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01755178E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.39641393E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.65297686E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01778174E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.31960927E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55658528E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996302E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.69849409E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81716876E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.44999691E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18757583E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36242726E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75719051E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53132875E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16063460E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.71985545E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.95362173E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30778207E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     8.78424240E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01995969E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.74693438E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00272003E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.43529307E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.23622785E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02258651E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.02951630E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.01995969E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.74693438E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00272003E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.43529307E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.23622785E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02258651E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.02951630E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02044967E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.74611379E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00246905E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.89986185E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.85042403E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02291886E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.02151888E-08    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.31307082E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.93254481E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.62601896E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.31084878E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.30570654E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.71796369E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.07388937E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.56882697E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.90474014E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.70086512E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.96390885E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.24762156E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.10485446E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.53183594E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.53183594E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.15500353E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.61290128E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.68040027E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.68040027E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.22365548E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.22365548E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.05242118E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.05242118E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.05242118E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.05242118E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     6.70263970E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.70263970E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.87141249E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.03706168E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.60311923E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.60160464E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.60160464E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.29309568E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.29933574E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.65996444E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.65996444E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.24903501E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.24903501E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.17440725E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.17440725E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.17440725E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.17440725E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     8.75775096E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     8.75775096E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.89805958E-03   # h decays
#          BR         NDA      ID1       ID2
     6.80067512E-01    2           5        -5   # BR(h -> b       bb     )
     5.32518496E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.88513666E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.00177843E-04    2           3        -3   # BR(h -> s       sb     )
     1.72674370E-02    2           4        -4   # BR(h -> c       cb     )
     5.64535752E-02    2          21        21   # BR(h -> g       g      )
     1.90926920E-03    2          22        22   # BR(h -> gam     gam    )
     1.25884510E-03    2          22        23   # BR(h -> Z       gam    )
     1.68226471E-01    2          24       -24   # BR(h -> W+      W-     )
     2.09763497E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39215803E+01   # H decays
#          BR         NDA      ID1       ID2
     7.41475844E-01    2           5        -5   # BR(H -> b       bb     )
     1.78630763E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31595691E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75597124E-04    2           3        -3   # BR(H -> s       sb     )
     2.18943605E-07    2           4        -4   # BR(H -> c       cb     )
     2.18410465E-02    2           6        -6   # BR(H -> t       tb     )
     3.38376727E-05    2          21        21   # BR(H -> g       g      )
     3.57731826E-08    2          22        22   # BR(H -> gam     gam    )
     8.36931149E-09    2          23        22   # BR(H -> Z       gam    )
     3.45381818E-05    2          24       -24   # BR(H -> W+      W-     )
     1.72477932E-05    2          23        23   # BR(H -> Z       Z      )
     8.82002257E-05    2          25        25   # BR(H -> h       h      )
     6.72672765E-24    2          36        36   # BR(H -> A       A      )
     2.85162038E-17    2          23        36   # BR(H -> Z       A      )
     2.40595312E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13391460E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19791084E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.38715489E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.20088524E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.24377032E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31994403E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82101767E-01    2           5        -5   # BR(A -> b       bb     )
     1.88395387E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.66120138E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.18133863E-04    2           3        -3   # BR(A -> s       sb     )
     2.30865780E-07    2           4        -4   # BR(A -> c       cb     )
     2.30176680E-02    2           6        -6   # BR(A -> t       tb     )
     6.77848395E-05    2          21        21   # BR(A -> g       g      )
     3.35801882E-08    2          22        22   # BR(A -> gam     gam    )
     6.67310189E-08    2          23        22   # BR(A -> Z       gam    )
     3.62865110E-05    2          23        25   # BR(A -> Z       h      )
     2.65167484E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22149538E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32022532E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.02471010E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.86255350E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08360079E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.52218058E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.91781534E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.93503405E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.24080053E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07820119E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.06511848E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.86409837E-05    2          24        25   # BR(H+ -> W+      h      )
     1.12009142E-13    2          24        36   # BR(H+ -> W+      A      )
     2.10861051E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.85501068E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.59999077E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
