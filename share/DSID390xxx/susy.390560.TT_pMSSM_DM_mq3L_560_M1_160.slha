#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11088090E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.86900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.20485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04151437E+01   # W+
        25     1.24091993E+02   # h
        35     4.00000325E+03   # H
        36     3.99999610E+03   # A
        37     4.00103686E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02013861E+03   # ~d_L
   2000001     4.01791354E+03   # ~d_R
   1000002     4.01947480E+03   # ~u_L
   2000002     4.01921476E+03   # ~u_R
   1000003     4.02013861E+03   # ~s_L
   2000003     4.01791354E+03   # ~s_R
   1000004     4.01947480E+03   # ~c_L
   2000004     4.01921476E+03   # ~c_R
   1000005     6.21060499E+02   # ~b_1
   2000005     4.02209890E+03   # ~b_2
   1000006     6.13406615E+02   # ~t_1
   2000006     2.22286734E+03   # ~t_2
   1000011     4.00360699E+03   # ~e_L
   2000011     4.00293099E+03   # ~e_R
   1000012     4.00248200E+03   # ~nu_eL
   1000013     4.00360699E+03   # ~mu_L
   2000013     4.00293099E+03   # ~mu_R
   1000014     4.00248200E+03   # ~nu_muL
   1000015     4.00513901E+03   # ~tau_1
   2000015     4.00723983E+03   # ~tau_2
   1000016     4.00443223E+03   # ~nu_tauL
   1000021     1.97171630E+03   # ~g
   1000022     1.46701593E+02   # ~chi_10
   1000023    -1.96727409E+02   # ~chi_20
   1000025     2.10022464E+02   # ~chi_30
   1000035     2.05496296E+03   # ~chi_40
   1000024     1.92957122E+02   # ~chi_1+
   1000037     2.05512761E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.19558587E-01   # N_11
  1  2    -1.33158640E-02   # N_12
  1  3    -4.59869516E-01   # N_13
  1  4    -3.41564691E-01   # N_14
  2  1    -9.35718882E-02   # N_21
  2  2     2.64336654E-02   # N_22
  2  3    -6.95961054E-01   # N_23
  2  4     7.11465933E-01   # N_24
  3  1     5.65302717E-01   # N_31
  3  2     2.53212521E-02   # N_32
  3  3     5.51502832E-01   # N_33
  3  4     6.12891751E-01   # N_34
  4  1    -9.28298431E-04   # N_41
  4  2     9.99241104E-01   # N_42
  4  3    -1.69278518E-03   # N_43
  4  4    -3.89035918E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.39868650E-03   # U_11
  1  2     9.99997123E-01   # U_12
  2  1    -9.99997123E-01   # U_21
  2  2     2.39868650E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50309579E-02   # V_11
  1  2    -9.98484649E-01   # V_12
  2  1    -9.98484649E-01   # V_21
  2  2    -5.50309579E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97253912E-01   # cos(theta_t)
  1  2    -7.40583216E-02   # sin(theta_t)
  2  1     7.40583216E-02   # -sin(theta_t)
  2  2     9.97253912E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999833E-01   # cos(theta_b)
  1  2    -5.77927307E-04   # sin(theta_b)
  2  1     5.77927307E-04   # -sin(theta_b)
  2  2     9.99999833E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04436173E-01   # cos(theta_tau)
  1  2     7.09767341E-01   # sin(theta_tau)
  2  1    -7.09767341E-01   # -sin(theta_tau)
  2  2    -7.04436173E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00287399E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10880895E+03  # DRbar Higgs Parameters
         1    -1.86900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44134267E+02   # higgs               
         4     1.61532154E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10880895E+03  # The gauge couplings
     1     3.61508597E-01   # gprime(Q) DRbar
     2     6.36724142E-01   # g(Q) DRbar
     3     1.03386088E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10880895E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.49975066E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10880895E+03  # The trilinear couplings
  1  1     3.50125944E-07   # A_d(Q) DRbar
  2  2     3.50159366E-07   # A_s(Q) DRbar
  3  3     6.23255893E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10880895E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.57856504E-08   # A_mu(Q) DRbar
  3  3     7.65585956E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10880895E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68402960E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10880895E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81114315E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10880895E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03700589E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10880895E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58442821E+07   # M^2_Hd              
        22    -5.43668957E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.20485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40661863E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.74914524E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46308918E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46308918E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53691082E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53691082E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.85773898E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.20247270E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.35389493E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.31950428E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.12412809E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21649557E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.89077042E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.32032215E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.06434010E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.59344529E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.13547300E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.60605615E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.23141280E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.07470152E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.83332653E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.17675674E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.45034396E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.29550891E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.36465286E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.78894943E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66658367E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65141268E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.73297615E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.50777008E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.30360368E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.54375811E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.48235450E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15478680E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.76418215E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.61019975E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.79785117E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.90433361E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78684063E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75191108E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.62057405E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.52155036E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78566153E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.26871174E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55912205E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52745211E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61264881E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.71633550E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.83512385E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.76364057E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.62312910E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44716701E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78704735E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47195783E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69482987E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.79711673E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79038273E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.00106147E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59081584E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52965861E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54465844E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.69657180E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.26156714E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.60164037E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.84112509E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85575624E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78684063E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75191108E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.62057405E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.52155036E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78566153E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.26871174E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55912205E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52745211E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61264881E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.71633550E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.83512385E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.76364057E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.62312910E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44716701E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78704735E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47195783E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69482987E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.79711673E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79038273E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.00106147E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59081584E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52965861E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54465844E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.69657180E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.26156714E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.60164037E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.84112509E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85575624E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14790837E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04505919E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.63686986E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.11634357E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77532584E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.87474714E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56428500E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07399663E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.72295286E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.74495925E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.18959285E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.69069570E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14790837E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04505919E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.63686986E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.11634357E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77532584E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.87474714E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56428500E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07399663E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.72295286E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.74495925E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.18959285E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.69069570E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10575932E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52974368E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.14737563E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.32180475E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39503087E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41770438E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79691270E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10988257E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.46902909E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73135508E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.07186827E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42050197E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.17480373E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84798479E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14896972E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17111067E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.22891134E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.45539301E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77835102E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09020116E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54180788E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14896972E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17111067E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.22891134E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.45539301E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77835102E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09020116E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54180788E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48136566E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05981358E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92205625E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.03198362E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51605564E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.73093311E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01861854E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.43358310E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33598773E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33598773E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11200649E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11200649E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10401157E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.33208216E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.77722306E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.67795786E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.47342875E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.32295129E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.35436598E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.94239536E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.35504944E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.07868079E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.18729855E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18104835E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53149708E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18104835E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53149708E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40912720E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50892226E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50892226E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47486025E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01479357E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01479357E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01479324E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.60540557E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.60540557E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.60540557E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.60540557E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.20180391E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.20180391E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.20180391E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.20180391E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.06962268E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.06962268E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.73243409E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.44958933E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.15399259E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.93902056E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.02678484E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.93902056E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.02678484E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.68833692E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.33121872E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.33121872E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.31375464E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.71321468E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.71321468E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.71320626E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.10179025E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.02243447E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.10179025E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.02243447E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.11116220E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     9.21784098E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.21784098E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.30844312E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.84225054E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.84225054E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.84225058E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.66153319E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.66153319E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.66153319E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.66153319E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.22049557E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.22049557E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.22049557E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.22049557E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.15716371E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.15716371E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.33090883E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.77949219E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.55626052E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.23822473E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.31654489E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.31654489E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.65909486E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.89657038E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.77482827E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85531492E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85531492E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.87254346E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.87254346E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.86399505E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16977905E-01    2           5        -5   # BR(h -> b       bb     )
     6.66559214E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.35968128E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.01553556E-04    2           3        -3   # BR(h -> s       sb     )
     2.17815239E-02    2           4        -4   # BR(h -> c       cb     )
     7.01626094E-02    2          21        21   # BR(h -> g       g      )
     2.35745404E-03    2          22        22   # BR(h -> gam     gam    )
     1.49037755E-03    2          22        23   # BR(h -> Z       gam    )
     1.95683155E-01    2          24       -24   # BR(h -> W+      W-     )
     2.41535320E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45022664E+01   # H decays
#          BR         NDA      ID1       ID2
     3.42727171E-01    2           5        -5   # BR(H -> b       bb     )
     6.08341021E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15094650E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54419916E-04    2           3        -3   # BR(H -> s       sb     )
     7.13655352E-08    2           4        -4   # BR(H -> c       cb     )
     7.15231239E-03    2           6        -6   # BR(H -> t       tb     )
     9.76243443E-07    2          21        21   # BR(H -> g       g      )
     1.26350990E-11    2          22        22   # BR(H -> gam     gam    )
     1.83645080E-09    2          23        22   # BR(H -> Z       gam    )
     1.90221047E-06    2          24       -24   # BR(H -> W+      W-     )
     9.50446182E-07    2          23        23   # BR(H -> Z       Z      )
     7.21364614E-06    2          25        25   # BR(H -> h       h      )
    -1.42603602E-24    2          36        36   # BR(H -> A       A      )
     2.72072581E-20    2          23        36   # BR(H -> Z       A      )
     1.86377404E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65661509E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65661509E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.85860564E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56124190E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.58851015E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.11905663E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.24965738E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.58858829E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.59797950E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.77856106E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.24775569E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.90527188E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.69165673E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.69165673E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40273133E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44995445E+01   # A decays
#          BR         NDA      ID1       ID2
     3.42770075E-01    2           5        -5   # BR(A -> b       bb     )
     6.08375079E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15106523E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54475436E-04    2           3        -3   # BR(A -> s       sb     )
     7.18246754E-08    2           4        -4   # BR(A -> c       cb     )
     7.16580028E-03    2           6        -6   # BR(A -> t       tb     )
     1.47239097E-05    2          21        21   # BR(A -> g       g      )
     4.71040082E-08    2          22        22   # BR(A -> gam     gam    )
     1.61797469E-08    2          23        22   # BR(A -> Z       gam    )
     1.89844189E-06    2          23        25   # BR(A -> Z       h      )
     1.86507228E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65673470E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65673470E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.47704455E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.93487989E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.28569607E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.69993778E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.91873627E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.45278984E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.79750252E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.30897894E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.84954999E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.79825754E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.79825754E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44461375E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.47387992E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09130292E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15373548E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.50328013E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22013109E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51020203E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.48927034E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.90245334E-06    2          24        25   # BR(H+ -> W+      h      )
     2.95137264E-14    2          24        36   # BR(H+ -> W+      A      )
     5.62913586E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.43984655E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.27112962E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66049021E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.05682418E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61176224E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.01204643E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.29058434E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     9.40871868E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
