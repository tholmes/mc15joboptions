#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13089553E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04155804E+01   # W+
        25     1.25675835E+02   # h
        35     4.00000379E+03   # H
        36     3.99999668E+03   # A
        37     4.00105786E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02612687E+03   # ~d_L
   2000001     4.02269507E+03   # ~d_R
   1000002     4.02547225E+03   # ~u_L
   2000002     4.02364463E+03   # ~u_R
   1000003     4.02612687E+03   # ~s_L
   2000003     4.02269507E+03   # ~s_R
   1000004     4.02547225E+03   # ~c_L
   2000004     4.02364463E+03   # ~c_R
   1000005     8.77783215E+02   # ~b_1
   2000005     4.02545942E+03   # ~b_2
   1000006     8.63848119E+02   # ~t_1
   2000006     2.14834110E+03   # ~t_2
   1000011     4.00470874E+03   # ~e_L
   2000011     4.00330074E+03   # ~e_R
   1000012     4.00359183E+03   # ~nu_eL
   1000013     4.00470874E+03   # ~mu_L
   2000013     4.00330074E+03   # ~mu_R
   1000014     4.00359183E+03   # ~nu_muL
   1000015     4.00531913E+03   # ~tau_1
   2000015     4.00696730E+03   # ~tau_2
   1000016     4.00501779E+03   # ~nu_tauL
   1000021     1.98060980E+03   # ~g
   1000022     1.45999385E+02   # ~chi_10
   1000023    -1.96003365E+02   # ~chi_20
   1000025     2.09555081E+02   # ~chi_30
   1000035     2.05228408E+03   # ~chi_40
   1000024     1.92203002E+02   # ~chi_1+
   1000037     2.05244921E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15360033E-01   # N_11
  1  2    -1.34605875E-02   # N_12
  1  3    -4.64053214E-01   # N_13
  1  4    -3.45921153E-01   # N_14
  2  1    -9.37775172E-02   # N_21
  2  2     2.63947357E-02   # N_22
  2  3    -6.95907484E-01   # N_23
  2  4     7.11492704E-01   # N_24
  3  1     5.71308093E-01   # N_31
  3  2     2.51625578E-02   # N_32
  3  3     5.48055289E-01   # N_33
  3  4     6.10417324E-01   # N_34
  4  1    -9.25814810E-04   # N_41
  4  2     9.99244202E-01   # N_42
  4  3    -1.66986973E-03   # N_43
  4  4    -3.88249688E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36624114E-03   # U_11
  1  2     9.99997200E-01   # U_12
  2  1    -9.99997200E-01   # U_21
  2  2     2.36624114E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49196133E-02   # V_11
  1  2    -9.98490779E-01   # V_12
  2  1    -9.98490779E-01   # V_21
  2  2    -5.49196133E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94979762E-01   # cos(theta_t)
  1  2    -1.00076337E-01   # sin(theta_t)
  2  1     1.00076337E-01   # -sin(theta_t)
  2  2     9.94979762E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999829E-01   # cos(theta_b)
  1  2    -5.84807636E-04   # sin(theta_b)
  2  1     5.84807636E-04   # -sin(theta_b)
  2  2     9.99999829E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04631114E-01   # cos(theta_tau)
  1  2     7.09573811E-01   # sin(theta_tau)
  2  1    -7.09573811E-01   # -sin(theta_tau)
  2  2    -7.04631114E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00310047E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30895532E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43588858E+02   # higgs               
         4     1.60946253E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30895532E+03  # The gauge couplings
     1     3.62068968E-01   # gprime(Q) DRbar
     2     6.36898500E-01   # g(Q) DRbar
     3     1.03002606E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30895532E+03  # The trilinear couplings
  1  1     1.38737543E-06   # A_u(Q) DRbar
  2  2     1.38738893E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30895532E+03  # The trilinear couplings
  1  1     5.08550604E-07   # A_d(Q) DRbar
  2  2     5.08598283E-07   # A_s(Q) DRbar
  3  3     9.10749557E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30895532E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09269450E-07   # A_mu(Q) DRbar
  3  3     1.10388639E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30895532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67040151E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30895532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80623769E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30895532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03986171E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30895532E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58555426E+07   # M^2_Hd              
        22    -2.12745309E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40736996E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.48864219E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43793005E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43793005E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56206995E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56206995E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.19062275E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.07416525E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.41348044E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.48997943E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.02237488E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13204811E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.01933552E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.31655140E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03991650E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.57349722E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.95496373E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.66944795E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.63182164E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.93546726E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.22744014E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.01888189E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.82883765E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.09270596E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.90595745E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66592998E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.66850701E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.75191294E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.51727756E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.30474732E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58307286E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.48444324E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14636691E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.61011497E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.42683270E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.47023082E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.55027915E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78854888E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.73593220E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.47769745E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.54940374E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81069592E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25451494E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60918423E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51983933E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61178732E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.69671393E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.88070252E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.81031025E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63089825E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44441662E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78875093E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.46196871E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69886426E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.01045228E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81541260E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.75481877E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64087812E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52203217E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54380195E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.64643882E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.27360021E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.72393230E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.86299523E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85502262E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78854888E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.73593220E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.47769745E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.54940374E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81069592E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25451494E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60918423E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51983933E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61178732E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.69671393E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.88070252E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.81031025E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63089825E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44441662E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78875093E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.46196871E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69886426E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.01045228E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81541260E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.75481877E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64087812E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52203217E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54380195E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.64643882E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.27360021E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.72393230E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.86299523E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85502262E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15775473E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03376491E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.68970262E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.23231123E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77522209E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70398116E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56403514E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08063752E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65437619E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.78369778E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25778216E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.67491866E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15775473E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03376491E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.68970262E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.23231123E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77522209E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70398116E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56403514E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08063752E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65437619E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.78369778E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25778216E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.67491866E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11476895E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.50701512E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.14720942E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34413890E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39531291E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41355279E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79745686E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11796416E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44427865E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73433120E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.09972792E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41921021E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.17976825E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84537328E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15880940E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16042331E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23344737E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.56586401E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77826661E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.07077314E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54168147E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15880940E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16042331E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23344737E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.56586401E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77826661E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.07077314E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54168147E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49128613E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05029163E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92657673E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13254227E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51586273E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.73054108E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01827154E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.53309376E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33599492E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33599492E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11200887E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11200887E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10399242E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.50244987E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.61308800E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.47461118E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.39983067E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.55356036E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.25943854E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.60356925E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.30660941E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.23471946E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.17926893E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18090858E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53162210E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18090858E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53162210E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40723909E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51093770E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51093770E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47681955E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01944169E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01944169E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01944145E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.68308053E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.68308053E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.68308053E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.68308053E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.22769561E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.22769561E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.22769561E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.22769561E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.23333520E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.23333520E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.77159443E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.76902348E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.13404294E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.68982698E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.94675845E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.68982698E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.94675845E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.40655255E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.25879682E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.25879682E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.24195556E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.56880004E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.56880004E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.56879389E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.36459540E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.36411541E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.36459540E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.36411541E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.06809632E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.00057704E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.00057704E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.05606147E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.99990009E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.99990009E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.99990012E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.92326765E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.92326765E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.92326765E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.92326765E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.30773901E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.30773901E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.30773901E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.30773901E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.24202124E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.24202124E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.50125205E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.96386075E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.06186960E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.54072686E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.21603027E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.21603027E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.87105244E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.33219224E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.16663159E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.76729665E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76729665E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.77685474E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.77685474E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06686870E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92804972E-01    2           5        -5   # BR(h -> b       bb     )
     6.41469254E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27079072E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81468221E-04    2           3        -3   # BR(h -> s       sb     )
     2.09072612E-02    2           4        -4   # BR(h -> c       cb     )
     6.84651270E-02    2          21        21   # BR(h -> g       g      )
     2.36775334E-03    2          22        22   # BR(h -> gam     gam    )
     1.63235916E-03    2          22        23   # BR(h -> Z       gam    )
     2.20977792E-01    2          24       -24   # BR(h -> W+      W-     )
     2.79892629E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44951881E+01   # H decays
#          BR         NDA      ID1       ID2
     3.42111036E-01    2           5        -5   # BR(H -> b       bb     )
     6.08419982E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15122569E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54452932E-04    2           3        -3   # BR(H -> s       sb     )
     7.13812702E-08    2           4        -4   # BR(H -> c       cb     )
     7.15388938E-03    2           6        -6   # BR(H -> t       tb     )
     1.04516121E-06    2          21        21   # BR(H -> g       g      )
     3.54565272E-11    2          22        22   # BR(H -> gam     gam    )
     1.83525652E-09    2          23        22   # BR(H -> Z       gam    )
     2.02693335E-06    2          24       -24   # BR(H -> W+      W-     )
     1.01276444E-06    2          23        23   # BR(H -> Z       Z      )
     7.59049152E-06    2          25        25   # BR(H -> h       h      )
     1.08246704E-24    2          36        36   # BR(H -> A       A      )
     4.79479333E-21    2          23        36   # BR(H -> Z       A      )
     1.85661254E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66017429E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66017429E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.88509089E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56421364E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60776501E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09053254E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.80529813E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.66463685E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62608994E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.79051520E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.19641188E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.33435305E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.33435167E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.33435167E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.32594055E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44915731E+01   # A decays
#          BR         NDA      ID1       ID2
     3.42159589E-01    2           5        -5   # BR(A -> b       bb     )
     6.08464164E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15138022E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54512693E-04    2           3        -3   # BR(A -> s       sb     )
     7.18351915E-08    2           4        -4   # BR(A -> c       cb     )
     7.16684945E-03    2           6        -6   # BR(A -> t       tb     )
     1.47260627E-05    2          21        21   # BR(A -> g       g      )
     4.69641854E-08    2          22        22   # BR(A -> gam     gam    )
     1.61833443E-08    2          23        22   # BR(A -> Z       gam    )
     2.02280053E-06    2          23        25   # BR(A -> Z       h      )
     1.85798660E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66032177E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66032177E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.49855073E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.93851098E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30058476E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.66606790E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.56608002E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.52115657E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.83113427E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.32897416E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.79629203E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.43037004E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.43037004E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44239774E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.46106147E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09381513E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15462374E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.49507633E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22063350E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51123564E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.48129368E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.02763476E-06    2          24        25   # BR(H+ -> W+      h      )
     3.25370671E-14    2          24        36   # BR(H+ -> W+      A      )
     5.56851059E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46424516E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.33497247E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66453224E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.20352287E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61542005E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00183321E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.97996581E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.67225905E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
