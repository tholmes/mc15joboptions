#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13002437E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.10485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     8.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178498E+01   # W+
        25     1.25727200E+02   # h
        35     4.00000100E+03   # H
        36     3.99999652E+03   # A
        37     4.00106008E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02591691E+03   # ~d_L
   2000001     4.02260158E+03   # ~d_R
   1000002     4.02526668E+03   # ~u_L
   2000002     4.02318684E+03   # ~u_R
   1000003     4.02591691E+03   # ~s_L
   2000003     4.02260158E+03   # ~s_R
   1000004     4.02526668E+03   # ~c_L
   2000004     4.02318684E+03   # ~c_R
   1000005     2.13396272E+03   # ~b_1
   2000005     4.02554854E+03   # ~b_2
   1000006     8.36406041E+02   # ~t_1
   2000006     2.14652817E+03   # ~t_2
   1000011     4.00447174E+03   # ~e_L
   2000011     4.00367950E+03   # ~e_R
   1000012     4.00335905E+03   # ~nu_eL
   1000013     4.00447174E+03   # ~mu_L
   2000013     4.00367950E+03   # ~mu_R
   1000014     4.00335905E+03   # ~nu_muL
   1000015     4.00560123E+03   # ~tau_1
   2000015     4.00689766E+03   # ~tau_2
   1000016     4.00481010E+03   # ~nu_tauL
   1000021     1.99193735E+03   # ~g
   1000022     9.13757900E+01   # ~chi_10
   1000023    -1.35434237E+02   # ~chi_20
   1000025     1.53005324E+02   # ~chi_30
   1000035     2.06523651E+03   # ~chi_40
   1000024     1.29994194E+02   # ~chi_1+
   1000037     2.06540755E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50985164E-01   # N_11
  1  2     1.41237090E-02   # N_12
  1  3     5.42919762E-01   # N_13
  1  4     3.75579467E-01   # N_14
  2  1    -1.36983214E-01   # N_21
  2  2     2.72318911E-02   # N_22
  2  3    -6.84897882E-01   # N_23
  2  4     7.15128600E-01   # N_24
  3  1     6.45953615E-01   # N_31
  3  2     2.35868206E-02   # N_32
  3  3     4.85955547E-01   # N_33
  3  4     5.88247223E-01   # N_34
  4  1    -8.99657957E-04   # N_41
  4  2     9.99251023E-01   # N_42
  4  3    -4.79481647E-04   # N_43
  4  4    -3.86827335E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.82359846E-04   # U_11
  1  2     9.99999767E-01   # U_12
  2  1    -9.99999767E-01   # U_21
  2  2     6.82359846E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47188488E-02   # V_11
  1  2    -9.98501801E-01   # V_12
  2  1    -9.98501801E-01   # V_21
  2  2    -5.47188488E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.02587739E-01   # cos(theta_t)
  1  2     9.94723960E-01   # sin(theta_t)
  2  1    -9.94723960E-01   # -sin(theta_t)
  2  2    -1.02587739E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999865E-01   # cos(theta_b)
  1  2    -5.19615225E-04   # sin(theta_b)
  2  1     5.19615225E-04   # -sin(theta_b)
  2  2     9.99999865E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03586413E-01   # cos(theta_tau)
  1  2     7.10609710E-01   # sin(theta_tau)
  2  1    -7.10609710E-01   # -sin(theta_tau)
  2  2    -7.03586413E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00323306E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30024373E+03  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43542246E+02   # higgs               
         4     1.60791192E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30024373E+03  # The gauge couplings
     1     3.62225796E-01   # gprime(Q) DRbar
     2     6.36740279E-01   # g(Q) DRbar
     3     1.02945775E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30024373E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.35700988E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30024373E+03  # The trilinear couplings
  1  1     4.97018707E-07   # A_d(Q) DRbar
  2  2     4.97065318E-07   # A_s(Q) DRbar
  3  3     8.90047120E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30024373E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.06629956E-07   # A_mu(Q) DRbar
  3  3     1.07729876E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30024373E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67530703E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30024373E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80136074E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30024373E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04249488E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30024373E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58733285E+07   # M^2_Hd              
        22     2.12648612E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.10485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     8.09989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40657137E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.89913420E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.44291327E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.38930844E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.25767554E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.02979561E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.77359801E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.22068908E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.86275443E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.49626707E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.58252732E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.63401865E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.53604293E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.03416889E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52199898E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     5.15642848E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.10663021E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.76440669E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.51852847E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.06408241E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     5.84047187E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.46753379E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     2.96675414E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64244946E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.83396275E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71697668E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42141977E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.74768823E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59564257E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.32604342E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14271225E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.14774386E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.36637254E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.09927134E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.29052289E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76382379E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45716866E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.39638444E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88642579E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83221300E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29779504E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65210061E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51283350E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58947792E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.18657817E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05910926E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.35379093E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49843089E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43537175E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76401337E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16967865E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.50433204E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.64808663E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83688984E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.23897707E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68383767E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51507722E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52166319E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.32024261E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.76536238E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.14580957E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.52213574E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85257405E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76382379E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45716866E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.39638444E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88642579E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83221300E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29779504E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65210061E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51283350E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58947792E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.18657817E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05910926E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.35379093E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49843089E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43537175E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76401337E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16967865E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.50433204E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.64808663E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83688984E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.23897707E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68383767E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51507722E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52166319E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.32024261E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.76536238E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.14580957E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.52213574E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85257405E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13369305E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.78531306E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.32141938E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.85810343E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76970476E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.78910871E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55273460E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08628976E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64434730E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87561536E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.16808679E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.36766703E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13369305E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.78531306E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.32141938E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.85810343E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76970476E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.78910871E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55273460E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08628976E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64434730E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87561536E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.16808679E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.36766703E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10629588E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23386402E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01597700E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66570900E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38353858E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41565865E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77372484E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11353082E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.07155611E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.38064392E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42714356E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41035981E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.25362455E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82751367E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13477950E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00216092E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.68243423E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.07277280E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77253466E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.07772583E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53042553E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13477950E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00216092E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.68243423E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.07277280E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77253466E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.07772583E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53042553E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46896562E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05945723E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.13688125E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.48974714E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50766520E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.83964165E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00208138E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.44460562E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33720680E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33720680E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241000E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241000E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10076640E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.69783684E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.61608610E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.45718715E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.18797964E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.42760693E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.73782993E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39696942E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.67917723E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.34315219E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18472960E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53672574E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18472960E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53672574E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37527037E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52344877E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52344877E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48032760E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04506665E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04506665E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04506638E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.74096622E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.74096622E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.74096622E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.74096622E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.24699185E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.24699185E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.24699185E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.24699185E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.48835708E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.48835708E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.37042795E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.84259196E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.07792743E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.58721058E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.63951484E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.58721058E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.63951484E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.37211025E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.05312275E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.05312275E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.04381705E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.13220631E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.13220631E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.13220311E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.99723362E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.07702390E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.99723362E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.07702390E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.40145178E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.08179872E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.08179872E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.96302388E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.16085808E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.16085808E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.16085819E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.22846691E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.22846691E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.22846691E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.22846691E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.07610250E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.07610250E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.07610250E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.07610250E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.98652366E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.98652366E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69643602E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.37492814E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.28813489E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.32303192E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.39663427E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.39663427E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.90336366E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.17272656E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.29079828E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.78328909E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78328909E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.08771204E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93582052E-01    2           5        -5   # BR(h -> b       bb     )
     6.38493664E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26025497E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79195932E-04    2           3        -3   # BR(h -> s       sb     )
     2.08074952E-02    2           4        -4   # BR(h -> c       cb     )
     6.80393131E-02    2          21        21   # BR(h -> g       g      )
     2.35862133E-03    2          22        22   # BR(h -> gam     gam    )
     1.63152087E-03    2          22        23   # BR(h -> Z       gam    )
     2.21003066E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80233433E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.40253980E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37675085E-01    2           5        -5   # BR(H -> b       bb     )
     6.13710127E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16993036E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56665397E-04    2           3        -3   # BR(H -> s       sb     )
     7.20057517E-08    2           4        -4   # BR(H -> c       cb     )
     7.21647537E-03    2           6        -6   # BR(H -> t       tb     )
     1.11828535E-06    2          21        21   # BR(H -> g       g      )
     3.37606792E-10    2          22        22   # BR(H -> gam     gam    )
     1.85132839E-09    2          23        22   # BR(H -> Z       gam    )
     2.11990673E-06    2          24       -24   # BR(H -> W+      W-     )
     1.05921906E-06    2          23        23   # BR(H -> Z       Z      )
     7.64347578E-06    2          25        25   # BR(H -> h       h      )
     3.34220288E-24    2          36        36   # BR(H -> A       A      )
     1.54525165E-20    2          23        36   # BR(H -> Z       A      )
     1.86760721E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67085859E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67085859E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.40579789E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.64738752E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70870528E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.42385214E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.49252972E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.11824356E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.12328828E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.41329094E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.22507986E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.59180898E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.64529170E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.64529170E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.40236486E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37712139E-01    2           5        -5   # BR(A -> b       bb     )
     6.13734338E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.17001426E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56717141E-04    2           3        -3   # BR(A -> s       sb     )
     7.24573878E-08    2           4        -4   # BR(A -> c       cb     )
     7.22892469E-03    2           6        -6   # BR(A -> t       tb     )
     1.48536124E-05    2          21        21   # BR(A -> g       g      )
     4.07084814E-08    2          22        22   # BR(A -> gam     gam    )
     1.63299674E-08    2          23        22   # BR(A -> Z       gam    )
     2.11551184E-06    2          23        25   # BR(A -> Z       h      )
     1.87093462E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67093293E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67093293E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.97159162E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.27851073E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34861721E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.90062008E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.39286655E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.78084922E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.39885884E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.29601452E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.67471284E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.36257587E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.36257587E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.38498153E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.36949379E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.15879254E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17759816E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43647304E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23364884E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53801239E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42485791E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.12477771E-06    2          24        25   # BR(H+ -> W+      h      )
     3.32545016E-14    2          24        36   # BR(H+ -> W+      A      )
     4.73009532E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.52016825E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.25572008E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67853365E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.94184154E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57772496E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.93403469E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.08173530E-06    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
