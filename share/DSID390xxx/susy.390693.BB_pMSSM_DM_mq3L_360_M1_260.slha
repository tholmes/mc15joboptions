#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90813437E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.06900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04173789E+01   # W+
        25     1.24629081E+02   # h
        35     4.00000709E+03   # H
        36     3.99999607E+03   # A
        37     4.00098019E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01279522E+03   # ~d_L
   2000001     4.01199887E+03   # ~d_R
   1000002     4.01213040E+03   # ~u_L
   2000002     4.01387708E+03   # ~u_R
   1000003     4.01279522E+03   # ~s_L
   2000003     4.01199887E+03   # ~s_R
   1000004     4.01213040E+03   # ~c_L
   2000004     4.01387708E+03   # ~c_R
   1000005     4.06666995E+02   # ~b_1
   2000005     4.01803894E+03   # ~b_2
   1000006     3.97376497E+02   # ~t_1
   2000006     2.32357685E+03   # ~t_2
   1000011     4.00235124E+03   # ~e_L
   2000011     4.00228600E+03   # ~e_R
   1000012     4.00122772E+03   # ~nu_eL
   1000013     4.00235124E+03   # ~mu_L
   2000013     4.00228600E+03   # ~mu_R
   1000014     4.00122772E+03   # ~nu_muL
   1000015     4.00431926E+03   # ~tau_1
   2000015     4.00803611E+03   # ~tau_2
   1000016     4.00380653E+03   # ~nu_tauL
   1000021     1.96127944E+03   # ~g
   1000022     2.52803149E+02   # ~chi_10
   1000023    -3.16554518E+02   # ~chi_20
   1000025     3.26249723E+02   # ~chi_30
   1000035     2.05813600E+03   # ~chi_40
   1000024     3.13945078E+02   # ~chi_1+
   1000037     2.05830000E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.91052115E-01   # N_11
  1  2    -1.16417783E-02   # N_12
  1  3    -3.56663564E-01   # N_13
  1  4    -2.80502583E-01   # N_14
  2  1    -5.75260313E-02   # N_21
  2  2     2.50302455E-02   # N_22
  2  3    -7.02257194E-01   # N_23
  2  4     7.09153775E-01   # N_24
  3  1     4.50239826E-01   # N_31
  3  2     2.84377264E-02   # N_32
  3  3     6.16124285E-01   # N_33
  3  4     6.45651810E-01   # N_34
  4  1    -9.91253912E-04   # N_41
  4  2     9.99214317E-01   # N_42
  4  3    -4.09892243E-03   # N_43
  4  4    -3.94076734E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.80226425E-03   # U_11
  1  2     9.99983167E-01   # U_12
  2  1    -9.99983167E-01   # U_21
  2  2     5.80226425E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.57430226E-02   # V_11
  1  2    -9.98445149E-01   # V_12
  2  1    -9.98445149E-01   # V_21
  2  2    -5.57430226E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97837974E-01   # cos(theta_t)
  1  2    -6.57219723E-02   # sin(theta_t)
  2  1     6.57219723E-02   # -sin(theta_t)
  2  2     9.97837974E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999547E-01   # cos(theta_b)
  1  2    -9.51840215E-04   # sin(theta_b)
  2  1     9.51840215E-04   # -sin(theta_b)
  2  2     9.99999547E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05393489E-01   # cos(theta_tau)
  1  2     7.08815932E-01   # sin(theta_tau)
  2  1    -7.08815932E-01   # -sin(theta_tau)
  2  2    -7.05393489E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00252723E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.08134369E+02  # DRbar Higgs Parameters
         1    -3.06900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44656693E+02   # higgs               
         4     1.63542592E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.08134369E+02  # The gauge couplings
     1     3.60741094E-01   # gprime(Q) DRbar
     2     6.35927218E-01   # g(Q) DRbar
     3     1.03842630E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.08134369E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.86550944E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.08134369E+02  # The trilinear couplings
  1  1     2.15798618E-07   # A_d(Q) DRbar
  2  2     2.15819472E-07   # A_s(Q) DRbar
  3  3     3.85683097E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.08134369E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.75393249E-08   # A_mu(Q) DRbar
  3  3     4.80127192E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.08134369E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72479070E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.08134369E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84740585E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.08134369E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03062734E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.08134369E+02  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57731157E+07   # M^2_Hd              
        22    -1.74716061E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40300211E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.47553326E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46857548E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46857548E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53142452E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53142452E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.98883533E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.36923967E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.85085633E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19288110E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03593954E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.22138658E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.36626979E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.21488350E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.02080658E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.12968130E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.83660218E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.88566047E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.36052932E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.93340325E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.72725288E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.33934387E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66510559E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.50596608E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.75755038E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61693940E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.92034937E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.54465680E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.16813197E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15266526E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.05095693E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.39744530E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.35578651E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.68933593E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78011264E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.13157244E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.72806878E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.11201822E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.75867524E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36264933E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.50519568E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53564155E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60911814E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.35576814E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.80768376E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.10651762E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.96806498E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45196344E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78031617E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81156064E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11246445E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.77151807E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76361687E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.81056974E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.53724486E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53785613E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54136367E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13625917E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.71556093E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.88648254E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.73754634E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85703762E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78011264E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.13157244E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.72806878E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.11201822E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.75867524E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36264933E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.50519568E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53564155E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60911814E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.35576814E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.80768376E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.10651762E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.96806498E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45196344E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78031617E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81156064E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11246445E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.77151807E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76361687E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.81056974E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.53724486E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53785613E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54136367E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13625917E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.71556093E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.88648254E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.73754634E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85703762E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12841622E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.24502141E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.93737484E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.09143236E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77700703E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.42270788E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56819231E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05357874E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.94844069E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.29781434E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01857580E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.36476567E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12841622E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.24502141E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.93737484E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.09143236E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77700703E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.42270788E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56819231E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05357874E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.94844069E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.29781434E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01857580E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.36476567E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08273069E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.85671540E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35758001E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.56147249E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39871549E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48084110E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80457975E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08178917E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.97772461E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25231090E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60492424E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42649174E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05377148E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86025117E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12949477E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.36433386E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68878190E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.61440369E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78040168E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15704813E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54536579E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12949477E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.36433386E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68878190E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.61440369E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78040168E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15704813E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54536579E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45835155E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23540697E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52920432E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.36736401E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51996654E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.66653435E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02594461E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.50453967E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33476844E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33476844E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11160178E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11160178E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10725956E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.79609487E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87053901E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.77999398E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.98274343E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.92341910E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.85018610E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.02649877E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.84289993E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.20424429E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.85679574E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17517502E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52368646E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17517502E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52368646E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45891529E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48999783E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48999783E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46632155E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.97620599E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.97620599E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.97620560E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.13162839E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.13162839E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.13162839E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.13162839E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.10543581E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.10543581E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.10543581E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.10543581E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.69198072E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.69198072E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.79165438E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.53875642E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.47567499E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.11254259E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.43938140E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.11254259E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.43938140E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.40951979E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.27206268E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.27206268E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.26246854E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.60133004E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.60133004E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.60131772E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.26738637E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.53316316E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.26738637E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.53316316E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.15487966E-07    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.26751327E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.26751327E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.02946648E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.53318151E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.53318151E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.53318155E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.74851664E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.74851664E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.74851664E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.74851664E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.58282567E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.58282567E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.58282567E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.58282567E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.43000036E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.43000036E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.79502170E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.61133164E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.62065274E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.00015078E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.83992302E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.83992302E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.40081520E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.31715442E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.07105062E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90436892E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90436892E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.92112762E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.92112762E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.91183943E-03   # h decays
#          BR         NDA      ID1       ID2
     6.06101770E-01    2           5        -5   # BR(h -> b       bb     )
     6.61171847E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.34058481E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.97076785E-04    2           3        -3   # BR(h -> s       sb     )
     2.15900208E-02    2           4        -4   # BR(h -> c       cb     )
     7.10642525E-02    2          21        21   # BR(h -> g       g      )
     2.36702069E-03    2          22        22   # BR(h -> gam     gam    )
     1.54595878E-03    2          22        23   # BR(h -> Z       gam    )
     2.04944741E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55379164E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50656029E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55330498E-01    2           5        -5   # BR(H -> b       bb     )
     6.02118309E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12894450E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51817428E-04    2           3        -3   # BR(H -> s       sb     )
     7.06257217E-08    2           4        -4   # BR(H -> c       cb     )
     7.07816775E-03    2           6        -6   # BR(H -> t       tb     )
     8.01389873E-07    2          21        21   # BR(H -> g       g      )
     5.78702442E-10    2          22        22   # BR(H -> gam     gam    )
     1.82110349E-09    2          23        22   # BR(H -> Z       gam    )
     1.70171056E-06    2          24       -24   # BR(H -> W+      W-     )
     8.50265807E-07    2          23        23   # BR(H -> Z       Z      )
     6.63958117E-06    2          25        25   # BR(H -> h       h      )
    -2.99375248E-24    2          36        36   # BR(H -> A       A      )
    -2.58014203E-23    2          23        36   # BR(H -> Z       A      )
     1.86021602E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60560953E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60560953E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.96480971E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.19107594E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.11900256E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82488906E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.68375105E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.01719750E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.58434260E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.03104390E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.05474363E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.02423446E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.18110462E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.18110462E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.45775009E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50630919E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55372280E-01    2           5        -5   # BR(A -> b       bb     )
     6.02148613E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12904998E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51870986E-04    2           3        -3   # BR(A -> s       sb     )
     7.10895799E-08    2           4        -4   # BR(A -> c       cb     )
     7.09246130E-03    2           6        -6   # BR(A -> t       tb     )
     1.45732167E-05    2          21        21   # BR(A -> g       g      )
     5.67518816E-08    2          22        22   # BR(A -> gam     gam    )
     1.60203880E-08    2          23        22   # BR(A -> Z       gam    )
     1.69828447E-06    2          23        25   # BR(A -> Z       h      )
     1.86804251E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60567231E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60567231E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.70587139E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.02855920E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.31971260E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.46532113E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.36936062E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.08050312E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.05803600E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.85051310E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.17170126E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.23768133E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.23768133E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51641630E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.70477945E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.01193240E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12567201E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65105574E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20423476E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47749815E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63255896E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.69701779E-06    2          24        25   # BR(H+ -> W+      h      )
     2.20203326E-14    2          24        36   # BR(H+ -> W+      A      )
     6.58966489E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.84209314E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.08793425E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60480037E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.09624050E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58601975E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.22247069E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.06600826E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     6.39967325E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
