#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13425172E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04033886E+01   # W+
        25     1.24328228E+02   # h
        35     3.00019077E+03   # H
        36     2.99999958E+03   # A
        37     3.00094462E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02897209E+03   # ~d_L
   2000001     3.02394530E+03   # ~d_R
   1000002     3.02805804E+03   # ~u_L
   2000002     3.02562072E+03   # ~u_R
   1000003     3.02897209E+03   # ~s_L
   2000003     3.02394530E+03   # ~s_R
   1000004     3.02805804E+03   # ~c_L
   2000004     3.02562072E+03   # ~c_R
   1000005     6.86249638E+02   # ~b_1
   2000005     3.02330115E+03   # ~b_2
   1000006     6.83990823E+02   # ~t_1
   2000006     3.00926361E+03   # ~t_2
   1000011     3.00638253E+03   # ~e_L
   2000011     3.00143964E+03   # ~e_R
   1000012     3.00499182E+03   # ~nu_eL
   1000013     3.00638253E+03   # ~mu_L
   2000013     3.00143964E+03   # ~mu_R
   1000014     3.00499182E+03   # ~nu_muL
   1000015     2.98565057E+03   # ~tau_1
   2000015     3.02208589E+03   # ~tau_2
   1000016     3.00499015E+03   # ~nu_tauL
   1000021     2.33581586E+03   # ~g
   1000022     3.02332973E+02   # ~chi_10
   1000023     6.29395646E+02   # ~chi_20
   1000025    -3.00024931E+03   # ~chi_30
   1000035     3.00107454E+03   # ~chi_40
   1000024     6.29555578E+02   # ~chi_1+
   1000037     3.00162720E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885204E-01   # N_11
  1  2    -7.72788841E-04   # N_12
  1  3     1.49665293E-02   # N_13
  1  4    -2.23266875E-03   # N_14
  2  1     1.19556029E-03   # N_21
  2  2     9.99605117E-01   # N_22
  2  3    -2.72536390E-02   # N_23
  2  4     6.73945967E-03   # N_24
  3  1    -8.99040404E-03   # N_31
  3  2     1.45155412E-02   # N_32
  3  3     7.06870140E-01   # N_33
  3  4     7.07137240E-01   # N_34
  4  1    -1.21376390E-02   # N_41
  4  2     2.40481263E-02   # N_42
  4  3     7.06659640E-01   # N_43
  4  4    -7.07040677E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99256461E-01   # U_11
  1  2    -3.85554686E-02   # U_12
  2  1     3.85554686E-02   # U_21
  2  2     9.99256461E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99954536E-01   # V_11
  1  2    -9.53551467E-03   # V_12
  2  1     9.53551467E-03   # V_21
  2  2     9.99954536E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98869992E-01   # cos(theta_t)
  1  2    -4.75261936E-02   # sin(theta_t)
  2  1     4.75261936E-02   # -sin(theta_t)
  2  2     9.98869992E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99917232E-01   # cos(theta_b)
  1  2     1.28658132E-02   # sin(theta_b)
  2  1    -1.28658132E-02   # -sin(theta_b)
  2  2     9.99917232E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06942777E-01   # cos(theta_tau)
  1  2     7.07270747E-01   # sin(theta_tau)
  2  1    -7.07270747E-01   # -sin(theta_tau)
  2  2     7.06942777E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01898508E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.34251716E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44500108E+02   # higgs               
         4     6.92083819E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.34251716E+03  # The gauge couplings
     1     3.61802972E-01   # gprime(Q) DRbar
     2     6.37242255E-01   # g(Q) DRbar
     3     1.02883362E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.34251716E+03  # The trilinear couplings
  1  1     1.69066537E-06   # A_u(Q) DRbar
  2  2     1.69068989E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.34251716E+03  # The trilinear couplings
  1  1     4.51641830E-07   # A_d(Q) DRbar
  2  2     4.51722614E-07   # A_s(Q) DRbar
  3  3     9.92059596E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.34251716E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.30060855E-08   # A_mu(Q) DRbar
  3  3     9.40956370E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.34251716E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54370827E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.34251716E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14577949E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.34251716E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06928186E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.34251716E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.73150391E+04   # M^2_Hd              
        22    -9.09583072E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40942470E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.84036309E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48247382E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48247382E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51752618E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51752618E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.90186810E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.67013862E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.32986138E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03750592E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.22805763E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.45149683E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.93630902E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.52783650E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.25226596E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.61347118E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51048989E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.99048412E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.43720051E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.73722525E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.26277475E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.24356828E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.84764942E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.11288860E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.89937896E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.49264528E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.49395319E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27323890E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.97506775E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.28269181E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.16064723E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.79628909E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.97001955E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57291241E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.10215756E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.99485994E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.14645864E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.10572876E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.22092824E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18262141E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56350866E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.09222585E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.22946239E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.22844507E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43648913E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.80155316E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.06559883E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57084362E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.87789533E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.68281072E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.14077694E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.92929726E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.22772086E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67857849E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44173004E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.94329452E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.05310736E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.79147992E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55582637E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.79628909E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.97001955E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57291241E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.10215756E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.99485994E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.14645864E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.10572876E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.22092824E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18262141E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56350866E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.09222585E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.22946239E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.22844507E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43648913E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.80155316E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.06559883E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57084362E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.87789533E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.68281072E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.14077694E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.92929726E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.22772086E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67857849E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44173004E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.94329452E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.05310736E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.79147992E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55582637E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.71045990E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03074015E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99394430E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.81582019E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.19912311E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97531544E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     9.72239728E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53135829E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998666E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.33364512E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.18894287E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.90014807E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.71045990E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03074015E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99394430E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.81582019E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.19912311E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97531544E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     9.72239728E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53135829E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998666E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.33364512E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.18894287E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.90014807E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64768013E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.61209084E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13240398E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25550518E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59202343E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.69767714E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10392342E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.00017105E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.12931665E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19807352E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.12974632E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.71062636E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03582243E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98405465E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.25258171E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.12572190E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.98012288E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.97976945E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.71062636E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03582243E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98405465E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.25258171E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.12572190E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.98012288E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.97976945E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.71095683E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03572959E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98378695E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.25158964E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.12372305E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.98048040E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.01921724E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.32507684E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.73943677E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.78177441E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.24250372E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.39620876E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.83583747E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.13136384E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.66322421E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.16269895E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.17040438E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.25744500E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.47425550E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.75753231E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.41920122E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.99099114E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.63868199E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.63868199E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.02393231E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.84668679E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65426253E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65426253E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.28900017E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.28900017E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.63205305E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.63205305E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.67821117E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.85915729E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.83857841E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.70306026E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.70306026E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.48337566E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.34215895E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.62563816E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.62563816E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31546608E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31546608E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.77030270E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.77030270E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.78715219E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85033658E-01    2           5        -5   # BR(h -> b       bb     )
     5.42520833E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92056499E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.08033286E-04    2           3        -3   # BR(h -> s       sb     )
     1.76078004E-02    2           4        -4   # BR(h -> c       cb     )
     5.69013141E-02    2          21        21   # BR(h -> g       g      )
     1.91968561E-03    2          22        22   # BR(h -> gam     gam    )
     1.22868103E-03    2          22        23   # BR(h -> Z       gam    )
     1.62367004E-01    2          24       -24   # BR(h -> W+      W-     )
     2.00896837E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39208375E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43273505E-01    2           5        -5   # BR(H -> b       bb     )
     1.78639464E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31626457E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75635405E-04    2           3        -3   # BR(H -> s       sb     )
     2.18937141E-07    2           4        -4   # BR(H -> c       cb     )
     2.18403993E-02    2           6        -6   # BR(H -> t       tb     )
     3.20071421E-05    2          21        21   # BR(H -> g       g      )
     5.82314728E-08    2          22        22   # BR(H -> gam     gam    )
     8.38873276E-09    2          23        22   # BR(H -> Z       gam    )
     3.39572188E-05    2          24       -24   # BR(H -> W+      W-     )
     1.69576807E-05    2          23        23   # BR(H -> Z       Z      )
     8.66069471E-05    2          25        25   # BR(H -> h       h      )
     2.39591612E-23    2          36        36   # BR(H -> A       A      )
     2.01191565E-17    2          23        36   # BR(H -> Z       A      )
     1.97768618E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08553709E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.86369044E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.66613678E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.09241101E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.22200074E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32241058E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82495741E-01    2           5        -5   # BR(A -> b       bb     )
     1.88043994E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.64877698E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.16607889E-04    2           3        -3   # BR(A -> s       sb     )
     2.30435171E-07    2           4        -4   # BR(A -> c       cb     )
     2.29747357E-02    2           6        -6   # BR(A -> t       tb     )
     6.76584082E-05    2          21        21   # BR(A -> g       g      )
     4.48350642E-08    2          22        22   # BR(A -> gam     gam    )
     6.66757572E-08    2          23        22   # BR(A -> Z       gam    )
     3.56097950E-05    2          23        25   # BR(A -> Z       h      )
     2.65516807E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22721859E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32415581E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.98387252E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.99907056E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.09200586E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.48772823E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.79600025E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.98882651E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.16921712E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.06347418E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.11344180E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.71691444E-05    2          24        25   # BR(H+ -> W+      h      )
     9.91909129E-14    2          24        36   # BR(H+ -> W+      A      )
     1.98025524E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.74943478E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.47618082E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
