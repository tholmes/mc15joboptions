#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871815E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04039593E+01   # W+
        25     1.24788556E+02   # h
        35     3.00008640E+03   # H
        36     2.99999996E+03   # A
        37     3.00089736E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04197186E+03   # ~d_L
   2000001     3.03688202E+03   # ~d_R
   1000002     3.04106912E+03   # ~u_L
   2000002     3.03780467E+03   # ~u_R
   1000003     3.04197186E+03   # ~s_L
   2000003     3.03688202E+03   # ~s_R
   1000004     3.04106912E+03   # ~c_L
   2000004     3.03780467E+03   # ~c_R
   1000005     1.05414730E+03   # ~b_1
   2000005     3.03577939E+03   # ~b_2
   1000006     1.05041428E+03   # ~t_1
   2000006     3.01853401E+03   # ~t_2
   1000011     3.00622395E+03   # ~e_L
   2000011     3.00223562E+03   # ~e_R
   1000012     3.00484014E+03   # ~nu_eL
   1000013     3.00622395E+03   # ~mu_L
   2000013     3.00223562E+03   # ~mu_R
   1000014     3.00484014E+03   # ~nu_muL
   1000015     2.98549936E+03   # ~tau_1
   2000015     3.02197474E+03   # ~tau_2
   1000016     3.00454084E+03   # ~nu_tauL
   1000021     2.35219476E+03   # ~g
   1000022     3.51379988E+02   # ~chi_10
   1000023     7.33742014E+02   # ~chi_20
   1000025    -2.99460444E+03   # ~chi_30
   1000035     2.99555853E+03   # ~chi_40
   1000024     7.33905230E+02   # ~chi_1+
   1000037     2.99605095E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99883693E-01   # N_11
  1  2    -7.18411820E-04   # N_12
  1  3     1.50295924E-02   # N_13
  1  4    -2.48920365E-03   # N_14
  2  1     1.15306390E-03   # N_21
  2  2     9.99587083E-01   # N_22
  2  3    -2.76492688E-02   # N_23
  2  4     7.73635830E-03   # N_24
  3  1    -8.85479761E-03   # N_31
  3  2     1.40900356E-02   # N_32
  3  3     7.06878953E-01   # N_33
  3  4     7.07138748E-01   # N_34
  4  1    -1.23638142E-02   # N_41
  4  2     2.50323319E-02   # N_42
  4  3     7.06634116E-01   # N_43
  4  4    -7.07028107E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99234748E-01   # U_11
  1  2    -3.91141777E-02   # U_12
  2  1     3.91141777E-02   # U_21
  2  2     9.99234748E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99940096E-01   # V_11
  1  2    -1.09455502E-02   # V_12
  2  1     1.09455502E-02   # V_21
  2  2     9.99940096E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447793E-01   # cos(theta_t)
  1  2    -5.56956430E-02   # sin(theta_t)
  2  1     5.56956430E-02   # -sin(theta_t)
  2  2     9.98447793E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99904960E-01   # cos(theta_b)
  1  2     1.37866228E-02   # sin(theta_b)
  2  1    -1.37866228E-02   # -sin(theta_b)
  2  2     9.99904960E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06964746E-01   # cos(theta_tau)
  1  2     7.07248788E-01   # sin(theta_tau)
  2  1    -7.07248788E-01   # -sin(theta_tau)
  2  2     7.06964746E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01765356E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718151E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43941152E+02   # higgs               
         4     7.68218089E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718151E+03  # The gauge couplings
     1     3.62568665E-01   # gprime(Q) DRbar
     2     6.37190999E-01   # g(Q) DRbar
     3     1.02370741E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718151E+03  # The trilinear couplings
  1  1     2.71850542E-06   # A_u(Q) DRbar
  2  2     2.71854210E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718151E+03  # The trilinear couplings
  1  1     7.43768255E-07   # A_d(Q) DRbar
  2  2     7.43894143E-07   # A_s(Q) DRbar
  3  3     1.61052482E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718151E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.50394503E-07   # A_mu(Q) DRbar
  3  3     1.52161818E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718151E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50400458E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718151E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15997971E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718151E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07560270E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718151E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.55175129E+04   # M^2_Hd              
        22    -9.03075172E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40917293E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.12432184E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47653772E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47653772E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52346228E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52346228E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.30198247E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.44578067E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.75004869E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.90537324E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10304429E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.86099765E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.25027456E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.54519339E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.24780465E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.01193024E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68203846E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59298478E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11322651E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.15086725E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.97055691E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.69486053E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.90808377E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.18150053E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.91314027E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.16760271E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.58721667E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.49916865E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.58224476E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30797013E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.86729713E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16328543E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.97147219E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.67983298E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.09248393E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55845489E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.83705959E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.10693102E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11757898E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.15093250E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.26303968E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14474263E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58115789E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.92027761E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.16436164E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.17375288E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41883986E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68506198E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.18264695E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55646015E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.74975906E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.37618615E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.11191996E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.50307923E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.26978606E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64577137E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.49242574E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.45562556E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.17440958E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.92061499E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55075679E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.67983298E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.09248393E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55845489E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.83705959E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.10693102E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11757898E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.15093250E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.26303968E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14474263E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58115789E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.92027761E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.16436164E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.17375288E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41883986E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68506198E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.18264695E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55646015E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.74975906E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.37618615E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.11191996E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.50307923E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.26978606E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64577137E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.49242574E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.45562556E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.17440958E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.92061499E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55075679E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59980612E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05943915E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98430836E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.64490604E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.48052233E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95625195E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.71112436E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52723044E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998786E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20862672E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.07820024E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.10287512E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59980612E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05943915E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98430836E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.64490604E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.48052233E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95625195E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.71112436E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52723044E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998786E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20862672E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.07820024E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.10287512E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58908649E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68252551E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10885933E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20861516E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53532773E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76948845E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07984556E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.64159080E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.81139661E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15013399E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.86703344E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59990499E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06425721E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97468426E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.70563318E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.31892684E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96105832E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.75625024E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59990499E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06425721E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97468426E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.70563318E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.31892684E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96105832E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.75625024E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59980098E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06417607E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97439875E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.37806291E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.23551823E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96140243E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.25801040E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.95856867E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.85686290E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.79860223E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.00095291E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.12378347E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.52221188E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.36367824E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.32418868E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.98175865E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.74134617E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.61054656E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.43894534E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.87641540E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.61151965E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.68753204E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.29371370E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.29371370E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.58201593E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.83842726E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54453876E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54453876E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26303608E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26303608E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.23139649E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.23139649E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.79253038E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.35729124E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.82573080E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.37723730E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.37723730E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.69933724E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.18143429E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.50579023E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.50579023E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.29370593E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.29370593E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.87237487E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.87237487E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.75116199E-03   # h decays
#          BR         NDA      ID1       ID2
     6.72357466E-01    2           5        -5   # BR(h -> b       bb     )
     5.48368480E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94124863E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.12133525E-04    2           3        -3   # BR(h -> s       sb     )
     1.77940397E-02    2           4        -4   # BR(h -> c       cb     )
     5.76383392E-02    2          21        21   # BR(h -> g       g      )
     1.96724373E-03    2          22        22   # BR(h -> gam     gam    )
     1.29066765E-03    2          22        23   # BR(h -> Z       gam    )
     1.72056398E-01    2          24       -24   # BR(h -> W+      W-     )
     2.14527396E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40853425E+01   # H decays
#          BR         NDA      ID1       ID2
     7.40180538E-01    2           5        -5   # BR(H -> b       bb     )
     1.76547200E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24228709E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66554537E-04    2           3        -3   # BR(H -> s       sb     )
     2.16258910E-07    2           4        -4   # BR(H -> c       cb     )
     2.15732118E-02    2           6        -6   # BR(H -> t       tb     )
     2.60414075E-05    2          21        21   # BR(H -> g       g      )
     2.32143295E-08    2          22        22   # BR(H -> gam     gam    )
     8.33088959E-09    2          23        22   # BR(H -> Z       gam    )
     2.98072074E-05    2          24       -24   # BR(H -> W+      W-     )
     1.48852324E-05    2          23        23   # BR(H -> Z       Z      )
     7.88242883E-05    2          25        25   # BR(H -> h       h      )
     6.38663718E-24    2          36        36   # BR(H -> A       A      )
     4.63270710E-19    2          23        36   # BR(H -> Z       A      )
     1.77855548E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.05583979E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.87170916E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.30137847E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.67516092E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.40445486E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33025619E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83817519E-01    2           5        -5   # BR(A -> b       bb     )
     1.86934968E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60956452E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.11791772E-04    2           3        -3   # BR(A -> s       sb     )
     2.29076131E-07    2           4        -4   # BR(A -> c       cb     )
     2.28392374E-02    2           6        -6   # BR(A -> t       tb     )
     6.72593680E-05    2          21        21   # BR(A -> g       g      )
     5.36293615E-08    2          22        22   # BR(A -> gam     gam    )
     6.62890331E-08    2          23        22   # BR(A -> Z       gam    )
     3.14427625E-05    2          23        25   # BR(A -> Z       h      )
     2.61857208E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22279362E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30604455E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.89580502E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04100203E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10518053E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38948415E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.44863312E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07314437E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96508718E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02147809E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18388105E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.02406951E-05    2          24        25   # BR(H+ -> W+      h      )
     7.35670035E-14    2          24        36   # BR(H+ -> W+      A      )
     1.85839434E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.35735141E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.77365966E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
