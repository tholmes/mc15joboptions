#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11090967E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.55959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.49900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04188818E+01   # W+
        25     1.26008145E+02   # h
        35     4.00000851E+03   # H
        36     3.99999660E+03   # A
        37     4.00100321E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02016191E+03   # ~d_L
   2000001     4.01796387E+03   # ~d_R
   1000002     4.01950922E+03   # ~u_L
   2000002     4.01910545E+03   # ~u_R
   1000003     4.02016191E+03   # ~s_L
   2000003     4.01796387E+03   # ~s_R
   1000004     4.01950922E+03   # ~c_L
   2000004     4.01910545E+03   # ~c_R
   1000005     6.67882377E+02   # ~b_1
   2000005     4.02227562E+03   # ~b_2
   1000006     6.49836692E+02   # ~t_1
   2000006     2.07182687E+03   # ~t_2
   1000011     4.00351543E+03   # ~e_L
   2000011     4.00302881E+03   # ~e_R
   1000012     4.00240282E+03   # ~nu_eL
   1000013     4.00351543E+03   # ~mu_L
   2000013     4.00302881E+03   # ~mu_R
   1000014     4.00240282E+03   # ~nu_muL
   1000015     4.00446566E+03   # ~tau_1
   2000015     4.00788701E+03   # ~tau_2
   1000016     4.00434327E+03   # ~nu_tauL
   1000021     1.97020173E+03   # ~g
   1000022     3.00976543E+02   # ~chi_10
   1000023    -3.60383821E+02   # ~chi_20
   1000025     3.71765940E+02   # ~chi_30
   1000035     2.05465940E+03   # ~chi_40
   1000024     3.57996796E+02   # ~chi_1+
   1000037     2.05482379E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.73093500E-01   # N_11
  1  2    -1.32847437E-02   # N_12
  1  3    -3.75248085E-01   # N_13
  1  4    -3.10998603E-01   # N_14
  2  1    -4.94177068E-02   # N_21
  2  2     2.45024790E-02   # N_22
  2  3    -7.03270876E-01   # N_23
  2  4     7.08778946E-01   # N_24
  3  1     4.85040809E-01   # N_31
  3  2     2.85152675E-02   # N_32
  3  3     6.03799803E-01   # N_33
  3  4     6.31939942E-01   # N_34
  4  1    -1.02220166E-03   # N_41
  4  2     9.99204696E-01   # N_42
  4  3    -4.97466402E-03   # N_43
  4  4    -3.95497685E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.04091591E-03   # U_11
  1  2     9.99975212E-01   # U_12
  2  1    -9.99975212E-01   # U_21
  2  2     7.04091591E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.59431718E-02   # V_11
  1  2    -9.98433955E-01   # V_12
  2  1    -9.98433955E-01   # V_21
  2  2    -5.59431718E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95086608E-01   # cos(theta_t)
  1  2    -9.90082955E-02   # sin(theta_t)
  2  1     9.90082955E-02   # -sin(theta_t)
  2  2     9.95086608E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999392E-01   # cos(theta_b)
  1  2    -1.10272373E-03   # sin(theta_b)
  2  1     1.10272373E-03   # -sin(theta_b)
  2  2     9.99999392E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05751375E-01   # cos(theta_tau)
  1  2     7.08459594E-01   # sin(theta_tau)
  2  1    -7.08459594E-01   # -sin(theta_tau)
  2  2    -7.05751375E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00278728E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10909673E+03  # DRbar Higgs Parameters
         1    -3.49900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44003685E+02   # higgs               
         4     1.62288927E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10909673E+03  # The gauge couplings
     1     3.61393428E-01   # gprime(Q) DRbar
     2     6.35997632E-01   # g(Q) DRbar
     3     1.03385844E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10909673E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.58051263E-07   # A_c(Q) DRbar
  3  3     2.55959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10909673E+03  # The trilinear couplings
  1  1     3.51613448E-07   # A_d(Q) DRbar
  2  2     3.51646680E-07   # A_s(Q) DRbar
  3  3     6.30556599E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10909673E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.76363102E-08   # A_mu(Q) DRbar
  3  3     7.84216871E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10909673E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71631683E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10909673E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85777602E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10909673E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03256912E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10909673E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57582725E+07   # M^2_Hd              
        22    -1.33126345E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40324714E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.55896998E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44162700E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44162700E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55837300E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55837300E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.91504906E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.32415203E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.01497010E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.36170871E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.29916916E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.24940603E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.03220740E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13440676E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.41603124E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.24337875E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.38937302E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.63487861E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.14780335E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.39470853E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.45489855E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.42612432E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.95274306E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.23181491E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.63893177E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66864880E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52205772E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79679086E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63220200E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.71505657E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61566844E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.72462817E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13706871E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.35445461E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.81619644E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.05217600E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.05975928E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78509833E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.99705841E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.97036065E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.24419538E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78000816E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37994637E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54790081E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52921958E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61054341E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.18690387E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.33505911E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.28486343E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.18326365E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45148789E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78529655E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.75593097E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.85447208E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.44617261E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78505722E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.52251643E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58009002E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53140912E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54330223E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.09200876E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.48202970E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.35111010E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.29920138E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85693974E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78509833E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.99705841E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.97036065E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.24419538E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78000816E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37994637E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54790081E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52921958E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61054341E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.18690387E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.33505911E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.28486343E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.18326365E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45148789E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78529655E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.75593097E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.85447208E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.44617261E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78505722E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.52251643E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58009002E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53140912E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54330223E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.09200876E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.48202970E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.35111010E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.29920138E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85693974E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13733423E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.18348927E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.46770285E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.66755236E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77825961E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.00964536E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57093025E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05385357E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.63373755E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.43352861E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.34192142E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.74135788E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13733423E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.18348927E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.46770285E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.66755236E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77825961E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.00964536E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57093025E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05385357E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.63373755E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.43352861E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.34192142E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.74135788E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08670666E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.74385755E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41154269E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.05482064E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40096226E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50004218E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80920106E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08287656E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.84355610E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.14149699E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.74634782E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42924718E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00816353E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86588285E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13840799E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.31649862E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.39586934E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.07922208E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78189429E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.16060063E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54812018E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13840799E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.31649862E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.39586934E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.07922208E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78189429E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.16060063E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54812018E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46562417E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.19278903E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26470749E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.78988806E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52220378E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.63160855E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03021045E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.96143041E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33502276E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33502276E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11168848E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11168848E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10657753E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.16842352E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.78592482E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.65747951E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.50033314E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07595393E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.34718046E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.24585001E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.39663915E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.79777054E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.35364510E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17735809E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52693333E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17735809E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52693333E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43978896E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49977071E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49977071E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47383636E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99642596E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99642596E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99642570E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.67991375E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.67991375E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.67991375E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.67991375E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.59971794E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.59971794E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.59971794E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.59971794E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.25084994E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.25084994E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.62409600E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.68731161E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.36773143E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.98853537E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29137162E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.98853537E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29137162E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.24254826E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.92749097E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.92749097E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.92139340E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.93193897E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.93193897E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.93192674E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.70445914E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.21061758E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.70445914E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.21061758E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.80496915E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.06726031E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.06726031E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.38284563E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.01284302E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.01284302E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.01284303E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.49509833E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.49509833E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.49509833E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.49509833E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.98361622E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.98361622E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.98361622E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.98361622E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.59429879E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.59429879E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.16728421E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.59953039E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.06585695E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.97426007E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.33971361E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.33971361E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.32426342E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.40096115E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.34697342E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85771095E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85771095E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.86429614E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.86429614E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.08409675E-03   # h decays
#          BR         NDA      ID1       ID2
     5.85007914E-01    2           5        -5   # BR(h -> b       bb     )
     6.40376271E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26690729E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80398791E-04    2           3        -3   # BR(h -> s       sb     )
     2.08633801E-02    2           4        -4   # BR(h -> c       cb     )
     6.83087125E-02    2          21        21   # BR(h -> g       g      )
     2.39031913E-03    2          22        22   # BR(h -> gam     gam    )
     1.67371188E-03    2          22        23   # BR(h -> Z       gam    )
     2.27978038E-01    2          24       -24   # BR(h -> W+      W-     )
     2.90332072E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53261412E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59381263E-01    2           5        -5   # BR(H -> b       bb     )
     5.99282909E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11891921E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50631598E-04    2           3        -3   # BR(H -> s       sb     )
     7.03004596E-08    2           4        -4   # BR(H -> c       cb     )
     7.04556975E-03    2           6        -6   # BR(H -> t       tb     )
     7.17553195E-07    2          21        21   # BR(H -> g       g      )
     1.07878704E-09    2          22        22   # BR(H -> gam     gam    )
     1.81133115E-09    2          23        22   # BR(H -> Z       gam    )
     1.82797874E-06    2          24       -24   # BR(H -> W+      W-     )
     9.13356240E-07    2          23        23   # BR(H -> Z       Z      )
     7.13331370E-06    2          25        25   # BR(H -> h       h      )
     1.48616636E-24    2          36        36   # BR(H -> A       A      )
    -6.00073572E-21    2          23        36   # BR(H -> Z       A      )
     1.84762867E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58842199E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58842199E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.08158922E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.73627330E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.24871002E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.72752996E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.35288027E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.17955337E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.04617088E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.09340285E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.65989627E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.17754526E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.32540488E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.32540488E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40985687E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53231266E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59426512E-01    2           5        -5   # BR(A -> b       bb     )
     5.99318420E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11904311E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50687149E-04    2           3        -3   # BR(A -> s       sb     )
     7.07554464E-08    2           4        -4   # BR(A -> c       cb     )
     7.05912550E-03    2           6        -6   # BR(A -> t       tb     )
     1.45047176E-05    2          21        21   # BR(A -> g       g      )
     5.94394804E-08    2          22        22   # BR(A -> gam     gam    )
     1.59492601E-08    2          23        22   # BR(A -> Z       gam    )
     1.82419507E-06    2          23        25   # BR(A -> Z       h      )
     1.86148693E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58845726E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58845726E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.80423188E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.52035622E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.04883690E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.31998805E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.10782037E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.30433460E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.15881763E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.66517869E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.96071028E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.49964850E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.49964850E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.54494245E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.77348611E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.98103821E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11474858E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69502797E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19804556E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46476498E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67511172E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.82205169E-06    2          24        25   # BR(H+ -> W+      h      )
     2.45262610E-14    2          24        36   # BR(H+ -> W+      A      )
     6.21865720E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.00025262E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.34596438E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58694047E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.48099408E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57285781E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.16157642E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.16931340E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.80508773E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
