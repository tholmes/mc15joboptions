#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90087485E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04198813E+01   # W+
        25     1.24776651E+02   # h
        35     4.00000211E+03   # H
        36     3.99999514E+03   # A
        37     4.00100395E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01270413E+03   # ~d_L
   2000001     4.01220526E+03   # ~d_R
   1000002     4.01204948E+03   # ~u_L
   2000002     4.01272719E+03   # ~u_R
   1000003     4.01270413E+03   # ~s_L
   2000003     4.01220526E+03   # ~s_R
   1000004     4.01204948E+03   # ~c_L
   2000004     4.01272719E+03   # ~c_R
   1000005     2.02677307E+03   # ~b_1
   2000005     4.01862498E+03   # ~b_2
   1000006     4.21982543E+02   # ~t_1
   2000006     2.03766678E+03   # ~t_2
   1000011     4.00160800E+03   # ~e_L
   2000011     4.00369544E+03   # ~e_R
   1000012     4.00049434E+03   # ~nu_eL
   1000013     4.00160800E+03   # ~mu_L
   2000013     4.00369544E+03   # ~mu_R
   1000014     4.00049434E+03   # ~nu_muL
   1000015     4.00405220E+03   # ~tau_1
   2000015     4.00905836E+03   # ~tau_2
   1000016     4.00310285E+03   # ~nu_tauL
   1000021     1.96781654E+03   # ~g
   1000022     1.44862131E+02   # ~chi_10
   1000023    -1.95779304E+02   # ~chi_20
   1000025     2.11487985E+02   # ~chi_30
   1000035     2.06758964E+03   # ~chi_40
   1000024     1.91786115E+02   # ~chi_1+
   1000037     2.06776500E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15219823E-01   # N_11
  1  2    -1.34895691E-02   # N_12
  1  3    -4.64240707E-01   # N_13
  1  4    -3.45998898E-01   # N_14
  2  1    -9.38636205E-02   # N_21
  2  2     2.64465488E-02   # N_22
  2  3    -6.95881012E-01   # N_23
  2  4     7.11505319E-01   # N_24
  3  1     5.71494006E-01   # N_31
  3  2     2.52095508E-02   # N_32
  3  3     5.47930091E-01   # N_33
  3  4     6.10353746E-01   # N_34
  4  1    -9.28478689E-04   # N_41
  4  2     9.99241257E-01   # N_42
  4  3    -1.67309685E-03   # N_43
  4  4    -3.89004954E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.37083495E-03   # U_11
  1  2     9.99997190E-01   # U_12
  2  1    -9.99997190E-01   # U_21
  2  2     2.37083495E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50265578E-02   # V_11
  1  2    -9.98484891E-01   # V_12
  2  1    -9.98484891E-01   # V_21
  2  2    -5.50265578E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.41365403E-02   # cos(theta_t)
  1  2     9.96454235E-01   # sin(theta_t)
  2  1    -9.96454235E-01   # -sin(theta_t)
  2  2    -8.41365403E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999709E-01   # cos(theta_b)
  1  2    -7.62889189E-04   # sin(theta_b)
  2  1     7.62889189E-04   # -sin(theta_b)
  2  2     9.99999709E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04465706E-01   # cos(theta_tau)
  1  2     7.09738028E-01   # sin(theta_tau)
  2  1    -7.09738028E-01   # -sin(theta_tau)
  2  2    -7.04465706E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00285754E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.00874845E+02  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44561119E+02   # higgs               
         4     1.62080636E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.00874845E+02  # The gauge couplings
     1     3.60967303E-01   # gprime(Q) DRbar
     2     6.35605549E-01   # g(Q) DRbar
     3     1.03762407E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.00874845E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.68568606E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.00874845E+02  # The trilinear couplings
  1  1     2.08941119E-07   # A_d(Q) DRbar
  2  2     2.08961444E-07   # A_s(Q) DRbar
  3  3     3.73191741E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.00874845E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.62214621E-08   # A_mu(Q) DRbar
  3  3     4.66905174E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.00874845E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74029450E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.00874845E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84600274E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.00874845E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03560229E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.00874845E+02  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58299101E+07   # M^2_Hd              
        22    -6.16083434E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40147388E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.71307832E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.45757849E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.29325981E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.50503080E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.66986391E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.19577930E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.88435282E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.83562552E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.48002426E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.73523500E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.54048130E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.76358324E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.78354683E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.09323156E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.68620626E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.46065588E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.89295417E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.14650530E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.77774076E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.28261007E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63564724E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68286671E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76421543E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.52924832E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.65470243E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60829326E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.18637077E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14026198E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.79876903E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.57200173E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.04514870E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.23280555E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74881690E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75612995E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.65445570E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.57127989E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80764479E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.35232189E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60294756E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52024780E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57932309E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.73658056E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.94392607E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83139358E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63741682E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43825840E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74899895E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49356343E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72767879E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.08005992E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81244601E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.93637097E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63489700E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52251239E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51234282E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.75387591E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29054938E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.78062044E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.88337586E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85336442E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74881690E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75612995E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.65445570E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.57127989E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80764479E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.35232189E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60294756E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52024780E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57932309E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.73658056E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.94392607E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83139358E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63741682E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43825840E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74899895E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49356343E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72767879E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.08005992E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81244601E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.93637097E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63489700E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52251239E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51234282E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.75387591E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29054938E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.78062044E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.88337586E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85336442E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11064842E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04170780E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.71173795E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.28908949E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77068366E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.78487925E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55493000E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06818774E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65240303E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.79998863E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25959243E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.65249713E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11064842E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04170780E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.71173795E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.28908949E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77068366E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.78487925E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55493000E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06818774E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65240303E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.79998863E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25959243E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.65249713E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08387642E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52068052E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16884428E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.35032717E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38690740E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44598410E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78060207E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09132134E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44986664E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76783451E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.10730883E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41294611E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.20282304E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83281267E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11176226E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16976657E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.27032560E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.60367765E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77364650E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11428871E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53237302E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11176226E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16976657E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.27032560E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.60367765E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77364650E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11428871E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53237302E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44473771E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05738715E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95615395E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16141871E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50955544E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.81746950E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00560705E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.08934884E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33591103E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33591103E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11198086E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11198086E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10421622E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.69826003E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.79015305E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.72295142E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.45123420E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.15005465E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40567002E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.85554144E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37630405E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.93817115E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.34840215E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18201698E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53281653E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18201698E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53281653E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41088580E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51230238E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51230238E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47899106E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02166231E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02166231E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02166187E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.30381622E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.30381622E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.30381622E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.30381622E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.43460786E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.43460786E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.43460786E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.43460786E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.49746752E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.49746752E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.40531141E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.75820183E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.67090972E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.15506745E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.25471226E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.15506745E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.25471226E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.82914753E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.10180433E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.10180433E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.08830541E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.24850507E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.24850507E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.24849510E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.80309987E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.22896788E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.80309987E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.22896788E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.41005255E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.42757872E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.42757872E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.32665967E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.85316595E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.85316595E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.85316604E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.06307333E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.06307333E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.06307333E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.06307333E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.68766931E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.68766931E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.68766931E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.68766931E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.62131150E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.62131150E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69977001E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.63607590E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.34404704E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.39995757E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38863832E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38863832E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.55268574E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08448741E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.02867348E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.87366920E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.87366920E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.59548377E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.59548377E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.95596378E-03   # h decays
#          BR         NDA      ID1       ID2
     6.07877428E-01    2           5        -5   # BR(h -> b       bb     )
     6.54659620E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31752450E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.92065081E-04    2           3        -3   # BR(h -> s       sb     )
     2.13695407E-02    2           4        -4   # BR(h -> c       cb     )
     6.90348275E-02    2          21        21   # BR(h -> g       g      )
     2.35956100E-03    2          22        22   # BR(h -> gam     gam    )
     1.54932760E-03    2          22        23   # BR(h -> Z       gam    )
     2.05893044E-01    2          24       -24   # BR(h -> W+      W-     )
     2.57264914E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43965672E+01   # H decays
#          BR         NDA      ID1       ID2
     3.45616143E-01    2           5        -5   # BR(H -> b       bb     )
     6.09522939E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15512548E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54914225E-04    2           3        -3   # BR(H -> s       sb     )
     7.15037196E-08    2           4        -4   # BR(H -> c       cb     )
     7.16616132E-03    2           6        -6   # BR(H -> t       tb     )
     8.94883751E-07    2          21        21   # BR(H -> g       g      )
     1.17180001E-12    2          22        22   # BR(H -> gam     gam    )
     1.84232279E-09    2          23        22   # BR(H -> Z       gam    )
     1.89700102E-06    2          24       -24   # BR(H -> W+      W-     )
     9.47843533E-07    2          23        23   # BR(H -> Z       Z      )
     6.92371507E-06    2          25        25   # BR(H -> h       h      )
    -1.67741660E-24    2          36        36   # BR(H -> A       A      )
     5.59536557E-20    2          23        36   # BR(H -> Z       A      )
     1.86754148E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64537026E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64537026E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.89133776E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.57066327E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60938956E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09190019E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.79968164E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.63444837E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62939559E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.72680564E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.13791737E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.10295465E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.75029871E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.75029871E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43953233E+01   # A decays
#          BR         NDA      ID1       ID2
     3.45650026E-01    2           5        -5   # BR(A -> b       bb     )
     6.09540577E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15518615E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54962956E-04    2           3        -3   # BR(A -> s       sb     )
     7.19622761E-08    2           4        -4   # BR(A -> c       cb     )
     7.17952841E-03    2           6        -6   # BR(A -> t       tb     )
     1.47521220E-05    2          21        21   # BR(A -> g       g      )
     4.70936655E-08    2          22        22   # BR(A -> gam     gam    )
     1.62241245E-08    2          23        22   # BR(A -> Z       gam    )
     1.89313166E-06    2          23        25   # BR(A -> Z       h      )
     1.86876711E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64544407E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64544407E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.50371894E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94638785E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30208043E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.66813525E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.56094122E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.49390603E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.83451019E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.24720796E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.75755807E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.96523909E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.96523909E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42618775E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.50536117E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11193721E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16103126E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.52342811E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22426555E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51870794E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.50917098E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.89988797E-06    2          24        25   # BR(H+ -> W+      h      )
     2.53387046E-14    2          24        36   # BR(H+ -> W+      A      )
     5.58063154E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46595468E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.34637760E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65160145E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.15427118E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60269901E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.93242615E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.21364737E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
