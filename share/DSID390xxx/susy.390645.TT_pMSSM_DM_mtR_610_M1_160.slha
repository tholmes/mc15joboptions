#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11041761E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.02485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194985E+01   # W+
        25     1.25515845E+02   # h
        35     4.00000299E+03   # H
        36     3.99999597E+03   # A
        37     4.00102788E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02006976E+03   # ~d_L
   2000001     4.01800593E+03   # ~d_R
   1000002     4.01941862E+03   # ~u_L
   2000002     4.01859062E+03   # ~u_R
   1000003     4.02006976E+03   # ~s_L
   2000003     4.01800593E+03   # ~s_R
   1000004     4.01941862E+03   # ~c_L
   2000004     4.01859062E+03   # ~c_R
   1000005     2.05223279E+03   # ~b_1
   2000005     4.02248568E+03   # ~b_2
   1000006     6.32517498E+02   # ~t_1
   2000006     2.06450159E+03   # ~t_2
   1000011     4.00320545E+03   # ~e_L
   2000011     4.00365276E+03   # ~e_R
   1000012     4.00209374E+03   # ~nu_eL
   1000013     4.00320545E+03   # ~mu_L
   2000013     4.00365276E+03   # ~mu_R
   1000014     4.00209374E+03   # ~nu_muL
   1000015     4.00489882E+03   # ~tau_1
   2000015     4.00784170E+03   # ~tau_2
   1000016     4.00405901E+03   # ~nu_tauL
   1000021     1.98058465E+03   # ~g
   1000022     1.44855854E+02   # ~chi_10
   1000023    -1.96038047E+02   # ~chi_20
   1000025     2.11157190E+02   # ~chi_30
   1000035     2.06582265E+03   # ~chi_40
   1000024     1.92076262E+02   # ~chi_1+
   1000037     2.06599720E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15311263E-01   # N_11
  1  2    -1.34638694E-02   # N_12
  1  3    -4.64119529E-01   # N_13
  1  4    -3.45947008E-01   # N_14
  2  1    -9.38119376E-02   # N_21
  2  2     2.63994859E-02   # N_22
  2  3    -6.95900327E-01   # N_23
  2  4     7.11494991E-01   # N_24
  3  1     5.71372039E-01   # N_31
  3  2     2.51665365E-02   # N_32
  3  3     5.48008220E-01   # N_33
  3  4     6.10399565E-01   # N_34
  4  1    -9.26324304E-04   # N_41
  4  2     9.99243933E-01   # N_42
  4  3    -1.67016546E-03   # N_43
  4  4    -3.88318898E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36666140E-03   # U_11
  1  2     9.99997199E-01   # U_12
  2  1    -9.99997199E-01   # U_21
  2  2     2.36666140E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49293968E-02   # V_11
  1  2    -9.98490241E-01   # V_12
  2  1    -9.98490241E-01   # V_21
  2  2    -5.49293968E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -9.68469222E-02   # cos(theta_t)
  1  2     9.95299288E-01   # sin(theta_t)
  2  1    -9.95299288E-01   # -sin(theta_t)
  2  2    -9.68469222E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999709E-01   # cos(theta_b)
  1  2    -7.62889189E-04   # sin(theta_b)
  2  1     7.62889189E-04   # -sin(theta_b)
  2  2     9.99999709E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04662934E-01   # cos(theta_tau)
  1  2     7.09542211E-01   # sin(theta_tau)
  2  1    -7.09542211E-01   # -sin(theta_tau)
  2  2    -7.04662934E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00301782E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10417610E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43979572E+02   # higgs               
         4     1.61419530E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10417610E+03  # The gauge couplings
     1     3.61623825E-01   # gprime(Q) DRbar
     2     6.35992179E-01   # g(Q) DRbar
     3     1.03312043E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10417610E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.37162975E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10417610E+03  # The trilinear couplings
  1  1     3.43612886E-07   # A_d(Q) DRbar
  2  2     3.43645815E-07   # A_s(Q) DRbar
  3  3     6.15136096E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10417610E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.52040488E-08   # A_mu(Q) DRbar
  3  3     7.59753370E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10417610E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71138276E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10417610E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83153978E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10417610E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03833769E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10417610E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58436228E+07   # M^2_Hd              
        22    -3.58947424E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.02485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40319612E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.37628966E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.55196747E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.48342490E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.08283820E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.08879021E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.98002910E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.34534171E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.53095792E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.33902829E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.62513610E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.35798373E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.93351672E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.91342473E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.55384437E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.38055971E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.95894413E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.56855188E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.07559609E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.23100926E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.10497416E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64058753E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68737052E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77117301E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53445378E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.65117635E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62244075E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.18104282E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13721124E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.73502911E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.48375594E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.34385908E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.04809496E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75735330E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75910578E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.55433064E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.57038381E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81861516E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.32505415E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62492084E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51696091E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58521971E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.74344129E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.94641063E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83360512E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63813971E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43734868E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75753935E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49494676E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72220304E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.09276195E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82339317E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.88582592E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65677925E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51920844E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51782492E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.77268926E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29131735E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.78683594E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.88575775E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85311336E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75735330E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75910578E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.55433064E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.57038381E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81861516E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.32505415E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62492084E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51696091E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58521971E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.74344129E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.94641063E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83360512E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63813971E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43734868E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75753935E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49494676E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72220304E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.09276195E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82339317E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.88582592E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65677925E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51920844E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51782492E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.77268926E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29131735E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.78683594E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.88575775E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85311336E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12166769E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04266379E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.72728170E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.28722254E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77043299E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.75340890E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55439615E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07570829E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65385602E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.79013448E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25823800E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.63658802E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12166769E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04266379E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.72728170E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.28722254E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77043299E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.75340890E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55439615E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07570829E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65385602E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.79013448E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25823800E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.63658802E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09435671E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52134602E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16802065E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34971644E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38711885E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44006426E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78101020E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09884191E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.45376920E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76677820E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.10861224E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41116053E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.20564052E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82921616E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12276595E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17040802E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.26306424E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.60622797E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77341429E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09733474E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53195091E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12276595E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17040802E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.26306424E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.60622797E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77341429E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09733474E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53195091E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45559693E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05820053E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95023929E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16463914E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50930753E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.81376174E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00514946E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.37844184E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33587736E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33587736E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196960E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196960E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10430609E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.69257499E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.97773785E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44865178E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.14073743E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40166908E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.85300976E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38482185E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.21044090E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.63683102E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18112917E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53195458E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18112917E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53195458E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41121232E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51196684E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51196684E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47889750E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02158479E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02158479E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02158446E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.02585235E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.02585235E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.02585235E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.02585235E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.34195308E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.34195308E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.34195308E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.34195308E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.06065235E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.06065235E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.31582266E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.28944049E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.86048663E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.41726850E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.59550461E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.41726850E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.59550461E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.14752637E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.18004971E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.18004971E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.16575629E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.40744459E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.40744459E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.40743674E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.26199102E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.52826732E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.26199102E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.52826732E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.94073937E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.26758197E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.26758197E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.17092751E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.53360860E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.53360860E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.53360865E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.62920468E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.62920468E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.62920468E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.62920468E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.54304833E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.54304833E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.54304833E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.54304833E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.47849465E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.47849465E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69192308E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.63615111E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.34430344E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.40439606E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38774363E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38774363E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.56485578E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08924420E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.03372615E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.96544099E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.96544099E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.80522665E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.80522665E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05125965E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96440122E-01    2           5        -5   # BR(h -> b       bb     )
     6.43097804E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27656271E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82811906E-04    2           3        -3   # BR(h -> s       sb     )
     2.09663062E-02    2           4        -4   # BR(h -> c       cb     )
     6.81442976E-02    2          21        21   # BR(h -> g       g      )
     2.36619782E-03    2          22        22   # BR(h -> gam     gam    )
     1.61591950E-03    2          22        23   # BR(h -> Z       gam    )
     2.17899400E-01    2          24       -24   # BR(h -> W+      W-     )
     2.75475080E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.42959971E+01   # H decays
#          BR         NDA      ID1       ID2
     3.44193807E-01    2           5        -5   # BR(H -> b       bb     )
     6.10651967E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15911745E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55386399E-04    2           3        -3   # BR(H -> s       sb     )
     7.16407629E-08    2           4        -4   # BR(H -> c       cb     )
     7.17989593E-03    2           6        -6   # BR(H -> t       tb     )
     9.46764941E-07    2          21        21   # BR(H -> g       g      )
     7.40843059E-12    2          22        22   # BR(H -> gam     gam    )
     1.84439787E-09    2          23        22   # BR(H -> Z       gam    )
     1.98831731E-06    2          24       -24   # BR(H -> W+      W-     )
     9.93470028E-07    2          23        23   # BR(H -> Z       Z      )
     7.29893521E-06    2          25        25   # BR(H -> h       h      )
    -1.37020451E-24    2          36        36   # BR(H -> A       A      )
     1.37492585E-20    2          23        36   # BR(H -> Z       A      )
     1.86430335E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65045029E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65045029E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.89560473E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.57056400E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.61271467E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09694489E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.81614702E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.64418831E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.63181420E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.75096583E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.15674370E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.02501750E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.72581961E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.72581961E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.42933075E+01   # A decays
#          BR         NDA      ID1       ID2
     3.44236859E-01    2           5        -5   # BR(A -> b       bb     )
     6.10686016E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15923614E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55442071E-04    2           3        -3   # BR(A -> s       sb     )
     7.20975046E-08    2           4        -4   # BR(A -> c       cb     )
     7.19301988E-03    2           6        -6   # BR(A -> t       tb     )
     1.47798397E-05    2          21        21   # BR(A -> g       g      )
     4.71432189E-08    2          22        22   # BR(A -> gam     gam    )
     1.62535259E-08    2          23        22   # BR(A -> Z       gam    )
     1.98424548E-06    2          23        25   # BR(A -> Z       h      )
     1.86562887E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65056857E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65056857E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.50743257E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94635669E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30477476E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67442593E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.57370866E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.50271346E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.83737361E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.27336813E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.77301480E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.91406841E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.91406841E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.41630265E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.48283710E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.12312852E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16498823E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.50901272E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22650633E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52331794E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.49523323E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.99125447E-06    2          24        25   # BR(H+ -> W+      h      )
     2.84280072E-14    2          24        36   # BR(H+ -> W+      A      )
     5.59284515E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.47175443E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.35047615E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65667215E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.17266551E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60770440E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.96652371E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.00576655E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
