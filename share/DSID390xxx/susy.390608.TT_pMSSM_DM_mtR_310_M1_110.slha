#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.84924221E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.30485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     3.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04189210E+01   # W+
        25     1.24169173E+02   # h
        35     3.99999922E+03   # H
        36     3.99999471E+03   # A
        37     4.00100507E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01058631E+03   # ~d_L
   2000001     4.01058493E+03   # ~d_R
   1000002     4.00993016E+03   # ~u_L
   2000002     4.01084105E+03   # ~u_R
   1000003     4.01058631E+03   # ~s_L
   2000003     4.01058493E+03   # ~s_R
   1000004     4.00993016E+03   # ~c_L
   2000004     4.01084105E+03   # ~c_R
   1000005     2.32186300E+03   # ~b_1
   2000005     4.01779681E+03   # ~b_2
   1000006     3.72816114E+02   # ~t_1
   2000006     2.32998951E+03   # ~t_2
   1000011     4.00102233E+03   # ~e_L
   2000011     4.00397344E+03   # ~e_R
   1000012     3.99990701E+03   # ~nu_eL
   1000013     4.00102233E+03   # ~mu_L
   2000013     4.00397344E+03   # ~mu_R
   1000014     3.99990701E+03   # ~nu_muL
   1000015     4.00375025E+03   # ~tau_1
   2000015     4.00963012E+03   # ~tau_2
   1000016     4.00270740E+03   # ~nu_tauL
   1000021     1.96903324E+03   # ~g
   1000022     9.10347326E+01   # ~chi_10
   1000023    -1.35224451E+02   # ~chi_20
   1000025     1.53997192E+02   # ~chi_30
   1000035     2.07158142E+03   # ~chi_40
   1000024     1.29607927E+02   # ~chi_1+
   1000037     2.07174762E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50803485E-01   # N_11
  1  2     1.41736091E-02   # N_12
  1  3     5.43152804E-01   # N_13
  1  4     3.75603870E-01   # N_14
  2  1    -1.37134786E-01   # N_21
  2  2     2.73283552E-02   # N_22
  2  3    -6.84828312E-01   # N_23
  2  4     7.15162495E-01   # N_24
  3  1     6.46132618E-01   # N_31
  3  2     2.36676162E-02   # N_32
  3  3     4.85793149E-01   # N_33
  3  4     5.88181520E-01   # N_34
  4  1    -9.03837268E-04   # N_41
  4  2     9.99245772E-01   # N_42
  4  3    -4.81132886E-04   # N_43
  4  4    -3.88180092E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.84741696E-04   # U_11
  1  2     9.99999766E-01   # U_12
  2  1    -9.99999766E-01   # U_21
  2  2     6.84741696E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49104290E-02   # V_11
  1  2    -9.98491284E-01   # V_12
  2  1    -9.98491284E-01   # V_21
  2  2    -5.49104290E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -6.23393102E-02   # cos(theta_t)
  1  2     9.98055014E-01   # sin(theta_t)
  2  1    -9.98055014E-01   # -sin(theta_t)
  2  2    -6.23393102E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999836E-01   # cos(theta_b)
  1  2    -5.72712819E-04   # sin(theta_b)
  2  1     5.72712819E-04   # -sin(theta_b)
  2  2     9.99999836E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03014981E-01   # cos(theta_tau)
  1  2     7.11175039E-01   # sin(theta_tau)
  2  1    -7.11175039E-01   # -sin(theta_tau)
  2  2    -7.03014981E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00296211E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.49242205E+02  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44740957E+02   # higgs               
         4     1.62061367E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.49242205E+02  # The gauge couplings
     1     3.60868156E-01   # gprime(Q) DRbar
     2     6.35846725E-01   # g(Q) DRbar
     3     1.03880352E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.49242205E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     4.87078608E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.49242205E+02  # The trilinear couplings
  1  1     1.78923453E-07   # A_d(Q) DRbar
  2  2     1.78941099E-07   # A_s(Q) DRbar
  3  3     3.19607415E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.49242205E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     3.92196290E-08   # A_mu(Q) DRbar
  3  3     3.96138776E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.49242205E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73524389E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.49242205E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83217231E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.49242205E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03690653E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.49242205E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58425533E+07   # M^2_Hd              
        22    -1.07279824E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.30485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     3.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40257959E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.77305403E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.03950798E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.50954139E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.58926592E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.57332001E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.18645993E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.50866987E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.91058492E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.27899114E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.48911505E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.41534324E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.72118007E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.07947076E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.82035491E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.88864189E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.20762000E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.40012106E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.57813972E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.06846106E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.13602633E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.25856066E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.43664835E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.37158912E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.86948483E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.07930261E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63612634E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.81607810E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69503513E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40693158E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.33652244E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.54986356E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.51400023E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15273463E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.11307167E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.31421573E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.96355258E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.27656453E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74943878E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.44915172E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.69902451E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88378675E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78975029E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34039670E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56720469E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52563302E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58102970E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.16667849E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05533022E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34140670E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.48523673E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43863793E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74962090E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16306718E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.51293328E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.59805929E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79445484E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.30510094E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59898755E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52791327E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51424716E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.26530536E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.75450300E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.11127319E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.48609063E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85347965E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74943878E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.44915172E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.69902451E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88378675E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78975029E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34039670E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56720469E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52563302E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58102970E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.16667849E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05533022E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34140670E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.48523673E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43863793E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74962090E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16306718E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.51293328E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.59805929E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79445484E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.30510094E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59898755E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52791327E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51424716E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.26530536E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.75450300E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.11127319E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.48609063E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85347965E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10494301E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.78484911E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31865319E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.87399509E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76917214E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.84946667E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55175206E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07080810E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64172662E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87980411E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.17028858E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.38921339E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10494301E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.78484911E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31865319E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.87399509E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76917214E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.84946667E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55175206E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07080810E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64172662E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87980411E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.17028858E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.38921339E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08151911E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23670699E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.02192096E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66869938E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38072052E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43563084E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.76811793E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09553803E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.06593015E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.38799735E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42476266E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41287660E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.25026528E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83260433E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10606899E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00290578E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.71390822E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.07631578E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77195217E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11652812E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52920611E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10606899E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00290578E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.71390822E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.07631578E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77195217E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11652812E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52920611E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44059009E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.06030626E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.16198529E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.48939208E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50676177E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.86420740E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00022780E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.40320107E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33721647E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33721647E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241324E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241324E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10074058E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.70113197E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.84211225E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.48290636E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.25981436E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.45595327E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.75732266E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39362505E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.70781355E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.42353934E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18626673E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53815441E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18626673E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53815441E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37726716E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52352903E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52352903E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48056145E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04405873E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04405873E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04405821E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.32868584E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.32868584E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.32868584E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.32868584E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.44289889E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.44289889E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.44289889E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.44289889E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.94286340E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.94286340E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.68719755E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.29502687E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.96920224E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.29075918E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.25478509E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.29075918E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.25478509E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.02323629E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.65156184E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.65156184E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.57156945E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.95319915E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.95319915E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.95319360E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.79473924E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.01078573E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.79473924E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.01078573E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.43104324E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.31611322E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.31611322E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.19970849E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.62842041E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.62842041E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.62842065E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.90275519E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.90275519E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.90275519E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.90275519E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.30086124E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.30086124E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.30086124E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.30086124E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.21505002E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.21505002E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.70039386E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.40915259E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.30123145E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.40302754E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.42357653E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.42357653E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.89629390E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.17143052E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.27613635E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.40861966E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.40861966E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     3.89510397E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16844490E-01    2           5        -5   # BR(h -> b       bb     )
     6.61671186E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.34237363E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.97814198E-04    2           3        -3   # BR(h -> s       sb     )
     2.16183713E-02    2           4        -4   # BR(h -> c       cb     )
     7.09586316E-02    2          21        21   # BR(h -> g       g      )
     2.32908518E-03    2          22        22   # BR(h -> gam     gam    )
     1.48915470E-03    2          22        23   # BR(h -> Z       gam    )
     1.95664064E-01    2          24       -24   # BR(h -> W+      W-     )
     2.41970337E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.39773062E+01   # H decays
#          BR         NDA      ID1       ID2
     3.39285435E-01    2           5        -5   # BR(H -> b       bb     )
     6.14256813E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.17186331E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56894047E-04    2           3        -3   # BR(H -> s       sb     )
     7.20620790E-08    2           4        -4   # BR(H -> c       cb     )
     7.22212050E-03    2           6        -6   # BR(H -> t       tb     )
     1.00397838E-06    2          21        21   # BR(H -> g       g      )
     2.23588232E-10    2          22        22   # BR(H -> gam     gam    )
     1.85544727E-09    2          23        22   # BR(H -> Z       gam    )
     1.96912696E-06    2          24       -24   # BR(H -> W+      W-     )
     9.83881462E-07    2          23        23   # BR(H -> Z       Z      )
     6.87760222E-06    2          25        25   # BR(H -> h       h      )
    -2.59585341E-24    2          36        36   # BR(H -> A       A      )
     5.35223097E-20    2          23        36   # BR(H -> Z       A      )
     1.88250410E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66487871E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66487871E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.41066363E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.65844471E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70890489E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.42255333E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.45853024E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.10396094E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.12595279E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.38656014E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.20409373E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.30221449E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.46570840E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.46570840E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.39765051E+01   # A decays
#          BR         NDA      ID1       ID2
     3.39316578E-01    2           5        -5   # BR(A -> b       bb     )
     6.14270100E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.17190858E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56941258E-04    2           3        -3   # BR(A -> s       sb     )
     7.25206440E-08    2           4        -4   # BR(A -> c       cb     )
     7.23523563E-03    2           6        -6   # BR(A -> t       tb     )
     1.48665883E-05    2          21        21   # BR(A -> g       g      )
     4.08137286E-08    2          22        22   # BR(A -> gam     gam    )
     1.63472788E-08    2          23        22   # BR(A -> Z       gam    )
     1.96515409E-06    2          23        25   # BR(A -> Z       h      )
     1.88577721E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66492320E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66492320E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.97589862E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.29218139E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34878939E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.89939049E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.37168832E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.76824284E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.40155104E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.26313436E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.66085048E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.58713456E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.58713456E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.37929011E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.39397432E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.16522393E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17987214E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.45214057E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23493923E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.54066712E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.44022174E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.97404632E-06    2          24        25   # BR(H+ -> W+      h      )
     2.57567283E-14    2          24        36   # BR(H+ -> W+      A      )
     4.73114263E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.52127506E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.26372737E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67276066E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.91638838E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57197655E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.90028287E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.50986979E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
