#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11087003E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.06900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.20485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04161134E+01   # W+
        25     1.24091115E+02   # h
        35     4.00000747E+03   # H
        36     3.99999653E+03   # A
        37     4.00101519E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02012801E+03   # ~d_L
   2000001     4.01790217E+03   # ~d_R
   1000002     4.01946517E+03   # ~u_L
   2000002     4.01921007E+03   # ~u_R
   1000003     4.02012801E+03   # ~s_L
   2000003     4.01790217E+03   # ~s_R
   1000004     4.01946517E+03   # ~c_L
   2000004     4.01921007E+03   # ~c_R
   1000005     6.23201419E+02   # ~b_1
   2000005     4.02215339E+03   # ~b_2
   1000006     6.15505966E+02   # ~t_1
   2000006     2.22289426E+03   # ~t_2
   1000011     4.00360329E+03   # ~e_L
   2000011     4.00288617E+03   # ~e_R
   1000012     4.00247996E+03   # ~nu_eL
   1000013     4.00360329E+03   # ~mu_L
   2000013     4.00288617E+03   # ~mu_R
   1000014     4.00247996E+03   # ~nu_muL
   1000015     4.00466168E+03   # ~tau_1
   2000015     4.00764372E+03   # ~tau_2
   1000016     4.00442227E+03   # ~nu_tauL
   1000021     1.97170978E+03   # ~g
   1000022     2.51950866E+02   # ~chi_10
   1000023    -3.17274054E+02   # ~chi_20
   1000025     3.26802337E+02   # ~chi_30
   1000035     2.05491498E+03   # ~chi_40
   1000024     3.14547121E+02   # ~chi_1+
   1000037     2.05507935E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.91070952E-01   # N_11
  1  2    -1.16230467E-02   # N_12
  1  3    -3.56633581E-01   # N_13
  1  4    -2.80481642E-01   # N_14
  2  1    -5.75224034E-02   # N_21
  2  2     2.49917187E-02   # N_22
  2  3    -7.02262874E-01   # N_23
  2  4     7.09149802E-01   # N_24
  3  1     4.50203012E-01   # N_31
  3  2     2.83948469E-02   # N_32
  3  3     6.16135207E-01   # N_33
  3  4     6.45668945E-01   # N_34
  4  1    -9.89677777E-04   # N_41
  4  2     9.99216719E-01   # N_42
  4  3    -4.09268319E-03   # N_43
  4  4    -3.93474145E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.79340583E-03   # U_11
  1  2     9.99983218E-01   # U_12
  2  1    -9.99983218E-01   # U_21
  2  2     5.79340583E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.56576561E-02   # V_11
  1  2    -9.98449911E-01   # V_12
  2  1    -9.98449911E-01   # V_21
  2  2    -5.56576561E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97240114E-01   # cos(theta_t)
  1  2    -7.42438888E-02   # sin(theta_t)
  2  1     7.42438888E-02   # -sin(theta_t)
  2  2     9.97240114E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999543E-01   # cos(theta_b)
  1  2    -9.56033363E-04   # sin(theta_b)
  2  1     9.56033363E-04   # -sin(theta_b)
  2  2     9.99999543E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05537640E-01   # cos(theta_tau)
  1  2     7.08672448E-01   # sin(theta_tau)
  2  1    -7.08672448E-01   # -sin(theta_tau)
  2  2    -7.05537640E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00253713E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10870031E+03  # DRbar Higgs Parameters
         1    -3.06900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44187403E+02   # higgs               
         4     1.62146327E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10870031E+03  # The gauge couplings
     1     3.61410403E-01   # gprime(Q) DRbar
     2     6.36170520E-01   # g(Q) DRbar
     3     1.03386207E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10870031E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.49189764E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10870031E+03  # The trilinear couplings
  1  1     3.50549411E-07   # A_d(Q) DRbar
  2  2     3.50582822E-07   # A_s(Q) DRbar
  3  3     6.23909447E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10870031E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.67521709E-08   # A_mu(Q) DRbar
  3  3     7.75324740E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10870031E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68129405E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10870031E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83242144E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10870031E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03245167E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10870031E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57861135E+07   # M^2_Hd              
        22    -1.13734060E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.20485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40413122E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.73918302E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46294142E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46294142E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53705858E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53705858E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.15994793E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.10460307E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.14830573E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.48093628E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.26615493E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20113555E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.30108474E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.29227743E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.11828921E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.56214381E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.04148792E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.68054797E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.25220194E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.18901256E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.85802247E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.46063681E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.30525220E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.96350299E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.44476142E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.62864834E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66782869E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51229531E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76831158E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62613712E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.98343166E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.56650811E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.18077281E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14797556E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.02837457E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.35976794E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.29069791E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.59865243E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78632639E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.13775708E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.71385178E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.11280423E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77850175E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34288238E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54484851E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52964509E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61211215E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.37124734E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.81363290E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.11013230E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.98094295E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45004810E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78653427E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81728544E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11031760E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.78556264E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78343444E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.78918974E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57688277E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53184165E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54448660E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.14027701E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.73100202E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.89586480E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.77255073E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85654047E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78632639E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.13775708E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.71385178E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.11280423E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77850175E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34288238E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54484851E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52964509E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61211215E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.37124734E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.81363290E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.11013230E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.98094295E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45004810E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78653427E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81728544E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11031760E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.78556264E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78343444E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.78918974E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57688277E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53184165E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54448660E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.14027701E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.73100202E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.89586480E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.77255073E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85654047E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14037263E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.24558520E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99468584E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.08908044E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77690515E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.40280372E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56796186E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06159030E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.94893484E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.29710186E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01808878E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.36070709E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14037263E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.24558520E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99468584E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.08908044E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77690515E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.40280372E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56796186E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06159030E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.94893484E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.29710186E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01808878E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.36070709E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09291537E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.85749815E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35179285E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.55590414E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39918811E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.47030021E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80551402E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09075684E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.98121939E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.24439475E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60493850E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42571943E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.04994425E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85868878E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14143418E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.36457140E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68453020E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.61633107E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78032423E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13868073E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54523915E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14143418E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.36457140E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68453020E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.61633107E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78032423E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13868073E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54523915E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46981650E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23603786E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52586562E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.36990267E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52016559E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65181019E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02636661E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.89415364E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33469048E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33469048E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11157539E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11157539E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10746825E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.29409466E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.78911066E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.68829260E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.41630278E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.49607260E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.26932953E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.38724293E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.30348491E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.53387857E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.73314620E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17579627E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52484529E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17579627E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52484529E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45202594E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49461699E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49461699E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47164906E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98619008E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98619008E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98618979E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.31369998E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.31369998E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.31369998E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.31369998E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.71234180E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.71234180E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.71234180E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.71234180E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.06262945E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.06262945E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.06874972E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.63467093E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.18588485E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.12189195E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.45192124E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.12189195E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.45192124E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.40430621E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.30325165E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.30325165E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.29450642E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.66291131E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.66291131E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.66290227E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.39770052E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.40653375E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.39770052E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.40653375E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.00999721E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.00999721E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.13438558E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.01874372E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.01874372E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.01874374E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.03800191E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.03800191E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.03800191E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.03800191E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.34598942E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.34598942E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.34598942E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.34598942E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.21504861E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.21504861E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.29291053E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.87515338E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.88282245E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.14528606E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.25893933E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.25893933E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.74605518E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49618833E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31617114E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.86078436E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.86078436E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.87819227E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.87819227E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.84477300E-03   # h decays
#          BR         NDA      ID1       ID2
     6.15138108E-01    2           5        -5   # BR(h -> b       bb     )
     6.69796810E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.37114271E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.03991279E-04    2           3        -3   # BR(h -> s       sb     )
     2.18903040E-02    2           4        -4   # BR(h -> c       cb     )
     7.04961667E-02    2          21        21   # BR(h -> g       g      )
     2.37049609E-03    2          22        22   # BR(h -> gam     gam    )
     1.49776077E-03    2          22        23   # BR(h -> Z       gam    )
     1.96614803E-01    2          24       -24   # BR(h -> W+      W-     )
     2.42715754E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49039029E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52318828E-01    2           5        -5   # BR(H -> b       bb     )
     6.03891687E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13521474E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52559085E-04    2           3        -3   # BR(H -> s       sb     )
     7.08340108E-08    2           4        -4   # BR(H -> c       cb     )
     7.09904266E-03    2           6        -6   # BR(H -> t       tb     )
     8.32107223E-07    2          21        21   # BR(H -> g       g      )
     4.61602106E-10    2          22        22   # BR(H -> gam     gam    )
     1.82586218E-09    2          23        22   # BR(H -> Z       gam    )
     1.71177976E-06    2          24       -24   # BR(H -> W+      W-     )
     8.55296854E-07    2          23        23   # BR(H -> Z       Z      )
     6.76932357E-06    2          25        25   # BR(H -> h       h      )
    -3.99356984E-24    2          36        36   # BR(H -> A       A      )
     2.88525337E-20    2          23        36   # BR(H -> Z       A      )
     1.85965611E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61388715E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61388715E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.97073036E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.20237240E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.12257345E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.83373926E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.68889665E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.02882528E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.61218760E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.07199173E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.08688364E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.31415882E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.18958286E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.18958286E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.41928623E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48997387E+01   # A decays
#          BR         NDA      ID1       ID2
     3.52371090E-01    2           5        -5   # BR(A -> b       bb     )
     6.03940366E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13538517E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52620449E-04    2           3        -3   # BR(A -> s       sb     )
     7.13011129E-08    2           4        -4   # BR(A -> c       cb     )
     7.11356552E-03    2           6        -6   # BR(A -> t       tb     )
     1.46165784E-05    2          21        21   # BR(A -> g       g      )
     5.68932067E-08    2          22        22   # BR(A -> gam     gam    )
     1.60645161E-08    2          23        22   # BR(A -> Z       gam    )
     1.70842802E-06    2          23        25   # BR(A -> Z       h      )
     1.86771752E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61399945E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61399945E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.71087379E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.03012671E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.35058363E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.47628346E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.37353451E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.09024178E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.06108542E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.89147182E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.20324052E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.26152475E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.26152475E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49848616E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.65342870E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03158961E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13262232E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.61819128E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20817092E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48559610E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.60071560E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.70768399E-06    2          24        25   # BR(H+ -> W+      h      )
     2.62500772E-14    2          24        36   # BR(H+ -> W+      A      )
     6.61269344E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.84725891E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.09389141E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61360956E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.11889573E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59470840E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.22922445E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.21480497E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     6.43459633E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
