#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13088243E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.89900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04171670E+01   # W+
        25     1.25664509E+02   # h
        35     4.00001075E+03   # H
        36     3.99999721E+03   # A
        37     4.00103056E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02611088E+03   # ~d_L
   2000001     4.02268161E+03   # ~d_R
   1000002     4.02545759E+03   # ~u_L
   2000002     4.02362670E+03   # ~u_R
   1000003     4.02611088E+03   # ~s_L
   2000003     4.02268161E+03   # ~s_R
   1000004     4.02545759E+03   # ~c_L
   2000004     4.02362670E+03   # ~c_R
   1000005     8.80118952E+02   # ~b_1
   2000005     4.02551302E+03   # ~b_2
   1000006     8.66106773E+02   # ~t_1
   2000006     2.14852024E+03   # ~t_2
   1000011     4.00469203E+03   # ~e_L
   2000011     4.00323254E+03   # ~e_R
   1000012     4.00357752E+03   # ~nu_eL
   1000013     4.00469203E+03   # ~mu_L
   2000013     4.00323254E+03   # ~mu_R
   1000014     4.00357752E+03   # ~nu_muL
   1000015     4.00436028E+03   # ~tau_1
   2000015     4.00779951E+03   # ~tau_2
   1000016     4.00498961E+03   # ~nu_tauL
   1000021     1.98060211E+03   # ~g
   1000022     3.48341547E+02   # ~chi_10
   1000023    -4.01238965E+02   # ~chi_20
   1000025     4.14809420E+02   # ~chi_30
   1000035     2.05221292E+03   # ~chi_40
   1000024     3.98878176E+02   # ~chi_1+
   1000037     2.05237761E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40061625E-01   # N_11
  1  2    -1.56235607E-02   # N_12
  1  3    -4.09849406E-01   # N_13
  1  4    -3.55071591E-01   # N_14
  2  1    -4.35152646E-02   # N_21
  2  2     2.40583970E-02   # N_22
  2  3    -7.03935001E-01   # N_23
  2  4     7.08521792E-01   # N_24
  3  1     5.40741868E-01   # N_31
  3  2     2.81629007E-02   # N_32
  3  3     5.80056239E-01   # N_33
  3  4     6.08555538E-01   # N_34
  4  1    -1.05805355E-03   # N_41
  4  2     9.99191648E-01   # N_42
  4  3    -5.80852095E-03   # N_43
  4  4    -3.97642138E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.22030098E-03   # U_11
  1  2     9.99966213E-01   # U_12
  2  1    -9.99966213E-01   # U_21
  2  2     8.22030098E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62456348E-02   # V_11
  1  2    -9.98416961E-01   # V_12
  2  1    -9.98416961E-01   # V_21
  2  2    -5.62456348E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94943187E-01   # cos(theta_t)
  1  2    -1.00439308E-01   # sin(theta_t)
  2  1     1.00439308E-01   # -sin(theta_t)
  2  2     9.94943187E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999224E-01   # cos(theta_b)
  1  2    -1.24579268E-03   # sin(theta_b)
  2  1     1.24579268E-03   # -sin(theta_b)
  2  2     9.99999224E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05992527E-01   # cos(theta_tau)
  1  2     7.08219282E-01   # sin(theta_tau)
  2  1    -7.08219282E-01   # -sin(theta_tau)
  2  2    -7.05992527E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00263681E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30882430E+03  # DRbar Higgs Parameters
         1    -3.89900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43676151E+02   # higgs               
         4     1.61533224E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30882430E+03  # The gauge couplings
     1     3.61920755E-01   # gprime(Q) DRbar
     2     6.36073220E-01   # g(Q) DRbar
     3     1.03002594E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30882430E+03  # The trilinear couplings
  1  1     1.38615431E-06   # A_u(Q) DRbar
  2  2     1.38616746E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30882430E+03  # The trilinear couplings
  1  1     5.10592267E-07   # A_d(Q) DRbar
  2  2     5.10639751E-07   # A_s(Q) DRbar
  3  3     9.13168500E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30882430E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11973175E-07   # A_mu(Q) DRbar
  3  3     1.13113934E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30882430E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66644595E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30882430E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84769550E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30882430E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03229843E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30882430E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57396932E+07   # M^2_Hd              
        22    -1.39344312E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40365568E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.47496694E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43756463E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43756463E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56243537E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56243537E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.89133691E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.32172796E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.26080534E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.32819660E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.08927010E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09917457E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.17601029E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.26828692E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.00200514E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.50976860E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.39730541E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.72888886E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.86233730E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.98721233E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.28612874E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.96504972E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.21613874E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.52795454E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.82908570E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66829647E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54802338E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80535457E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60973306E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.18323021E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62772494E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.34561267E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13328915E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.64260232E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.21526553E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.72265476E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.14462533E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78775146E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.77732814E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.13967193E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46174114E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79981979E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42073817E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58753791E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52323883E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61051978E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.87850782E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03575652E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.59725259E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.43616124E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45138786E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78795592E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.63442228E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.94325982E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.73478976E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80500080E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.15792655E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61996541E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52541425E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54343313E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01152370E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.70127062E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.16565926E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.95872995E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85692082E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78775146E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.77732814E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.13967193E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46174114E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79981979E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42073817E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58753791E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52323883E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61051978E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.87850782E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03575652E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.59725259E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.43616124E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45138786E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78795592E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.63442228E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.94325982E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.73478976E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80500080E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.15792655E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61996541E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52541425E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54343313E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01152370E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.70127062E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.16565926E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.95872995E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85692082E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14375673E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.07946282E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.47211206E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65890197E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77975472E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.79172290E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57421063E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05107944E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07027158E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88768236E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.91084541E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.18875011E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14375673E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.07946282E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.47211206E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65890197E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77975472E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.79172290E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57421063E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05107944E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07027158E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88768236E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.91084541E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.18875011E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08639665E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55171696E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44507749E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23438500E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40353521E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51350660E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81450443E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08045470E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60780254E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05181908E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98455869E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43307540E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95687342E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87369412E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14481326E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.22952939E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19664087E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.91745000E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78364642E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17768254E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55133595E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14481326E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.22952939E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19664087E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.91745000E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78364642E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17768254E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55133595E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46982784E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11476589E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08495102E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.55181384E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52506397E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58470478E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03566876E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.25555420E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33553402E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33553402E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11185966E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11185966E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10521264E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.44398508E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.63554674E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.49310958E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.29420985E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.60628096E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.11481608E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.48934646E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.20878346E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.22531988E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.27498376E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17955107E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53010446E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17955107E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53010446E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42020156E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50892452E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50892452E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47858237E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01518916E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01518916E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01518901E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.33716356E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.33716356E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.33716356E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.33716356E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.79055203E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.79055203E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.79055203E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.79055203E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.82150920E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.82150920E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.43341507E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.69317989E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.80179223E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.13648810E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.91967383E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.13648810E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.91967383E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.57920366E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.78410368E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.78410368E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.78287725E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.64623244E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.64623244E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.64622340E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.74352507E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.74791487E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.74352507E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.74791487E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.15681621E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.00627480E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.00627480E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.81683515E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.01049441E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.01049441E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.01049445E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.10418195E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.10418195E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.10418195E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.10418195E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.70137641E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.70137641E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.70137641E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.70137641E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.60071881E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.60071881E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.44255474E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.43493861E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.69911186E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.95262169E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.11009145E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.11009145E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.91153768E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.56572340E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63211158E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.77749011E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.77749011E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.78728994E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.78728994E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03500431E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90002113E-01    2           5        -5   # BR(h -> b       bb     )
     6.46356806E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28809307E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85146458E-04    2           3        -3   # BR(h -> s       sb     )
     2.10708466E-02    2           4        -4   # BR(h -> c       cb     )
     6.89764536E-02    2          21        21   # BR(h -> g       g      )
     2.38723120E-03    2          22        22   # BR(h -> gam     gam    )
     1.64370249E-03    2          22        23   # BR(h -> Z       gam    )
     2.22399087E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81709298E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50850267E+01   # H decays
#          BR         NDA      ID1       ID2
     3.58641853E-01    2           5        -5   # BR(H -> b       bb     )
     6.01906479E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12819552E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51728805E-04    2           3        -3   # BR(H -> s       sb     )
     7.06039649E-08    2           4        -4   # BR(H -> c       cb     )
     7.07598734E-03    2           6        -6   # BR(H -> t       tb     )
     8.59550514E-07    2          21        21   # BR(H -> g       g      )
     1.19198419E-09    2          22        22   # BR(H -> gam     gam    )
     1.81960724E-09    2          23        22   # BR(H -> Z       gam    )
     1.75731886E-06    2          24       -24   # BR(H -> W+      W-     )
     8.78050685E-07    2          23        23   # BR(H -> Z       Z      )
     6.97607583E-06    2          25        25   # BR(H -> h       h      )
    -1.85884303E-24    2          36        36   # BR(H -> A       A      )
     8.30509779E-20    2          23        36   # BR(H -> Z       A      )
     1.85702170E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58407635E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58407635E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.31854407E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.78479111E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.46062062E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.54617741E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.61649845E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.55803871E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.24391868E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.20413911E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.13099403E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.14268034E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.30520926E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.30520926E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.36332915E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50788267E+01   # A decays
#          BR         NDA      ID1       ID2
     3.58707728E-01    2           5        -5   # BR(A -> b       bb     )
     6.01976765E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12844236E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51799094E-04    2           3        -3   # BR(A -> s       sb     )
     7.10692889E-08    2           4        -4   # BR(A -> c       cb     )
     7.09043691E-03    2           6        -6   # BR(A -> t       tb     )
     1.45690519E-05    2          21        21   # BR(A -> g       g      )
     6.23092435E-08    2          22        22   # BR(A -> gam     gam    )
     1.60152203E-08    2          23        22   # BR(A -> Z       gam    )
     1.75380947E-06    2          23        25   # BR(A -> Z       h      )
     1.87849935E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58414936E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58414936E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.01047151E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.37463378E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23712649E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.07850999E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.07321371E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.77357595E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.38251217E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.54258277E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.57046689E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.52576897E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.52576897E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52325304E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.76636049E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00456636E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12306756E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69046758E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20275738E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47445872E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67093398E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.75089717E-06    2          24        25   # BR(H+ -> W+      h      )
     2.80728448E-14    2          24        36   # BR(H+ -> W+      A      )
     5.68926056E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.57161367E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.83497943E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58190680E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.32653571E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57075057E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.06937984E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.65990778E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.08038960E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
