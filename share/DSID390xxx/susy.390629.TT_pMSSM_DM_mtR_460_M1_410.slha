#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90314291E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.05959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.19900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.80485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04218473E+01   # W+
        25     1.24575239E+02   # h
        35     4.00000581E+03   # H
        36     3.99999593E+03   # A
        37     4.00097420E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01277050E+03   # ~d_L
   2000001     4.01222001E+03   # ~d_R
   1000002     4.01211633E+03   # ~u_L
   2000002     4.01284406E+03   # ~u_R
   1000003     4.01277050E+03   # ~s_L
   2000003     4.01222001E+03   # ~s_R
   1000004     4.01211633E+03   # ~c_L
   2000004     4.01284406E+03   # ~c_R
   1000005     1.82910912E+03   # ~b_1
   2000005     4.01869333E+03   # ~b_2
   1000006     4.57303227E+02   # ~t_1
   2000006     1.84307221E+03   # ~t_2
   1000011     4.00166741E+03   # ~e_L
   2000011     4.00343750E+03   # ~e_R
   1000012     4.00055535E+03   # ~nu_eL
   1000013     4.00166741E+03   # ~mu_L
   2000013     4.00343750E+03   # ~mu_R
   1000014     4.00055535E+03   # ~nu_muL
   1000015     4.00353205E+03   # ~tau_1
   2000015     4.00927457E+03   # ~tau_2
   1000016     4.00313208E+03   # ~nu_tauL
   1000021     1.96367197E+03   # ~g
   1000022     3.90967777E+02   # ~chi_10
   1000023    -4.30179717E+02   # ~chi_20
   1000025     4.53706755E+02   # ~chi_30
   1000035     2.06403621E+03   # ~chi_40
   1000024     4.27847937E+02   # ~chi_1+
   1000037     2.06419961E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.46502489E-01   # N_11
  1  2     2.02963095E-02   # N_12
  1  3     4.92228942E-01   # N_13
  1  4     4.47250224E-01   # N_14
  2  1    -3.93709924E-02   # N_21
  2  2     2.38051426E-02   # N_22
  2  3    -7.04335644E-01   # N_23
  2  4     7.08374577E-01   # N_24
  3  1     6.64215891E-01   # N_31
  3  2     2.58759788E-02   # N_32
  3  3     5.11449115E-01   # N_33
  3  4     5.44580101E-01   # N_34
  4  1    -1.09966525E-03   # N_41
  4  2     9.99175565E-01   # N_42
  4  3    -6.46319551E-03   # N_43
  4  4    -4.00650610E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.14625578E-03   # U_11
  1  2     9.99958172E-01   # U_12
  2  1    -9.99958172E-01   # U_21
  2  2     9.14625578E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.66704063E-02   # V_11
  1  2    -9.98392941E-01   # V_12
  2  1    -9.98392941E-01   # V_21
  2  2    -5.66704063E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.01592719E-01   # cos(theta_t)
  1  2     9.94826075E-01   # sin(theta_t)
  2  1    -9.94826075E-01   # -sin(theta_t)
  2  2    -1.01592719E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998636E-01   # cos(theta_b)
  1  2    -1.65166526E-03   # sin(theta_b)
  2  1     1.65166526E-03   # -sin(theta_b)
  2  2     9.99998636E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05994641E-01   # cos(theta_tau)
  1  2     7.08217175E-01   # sin(theta_tau)
  2  1    -7.08217175E-01   # -sin(theta_tau)
  2  2    -7.05994641E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00218352E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.03142912E+02  # DRbar Higgs Parameters
         1    -4.19900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44656254E+02   # higgs               
         4     1.63200061E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.03142912E+02  # The gauge couplings
     1     3.60802327E-01   # gprime(Q) DRbar
     2     6.34785856E-01   # g(Q) DRbar
     3     1.03766084E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.03142912E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.71850910E-07   # A_c(Q) DRbar
  3  3     2.05959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.03142912E+02  # The trilinear couplings
  1  1     2.11921490E-07   # A_d(Q) DRbar
  2  2     2.11941762E-07   # A_s(Q) DRbar
  3  3     3.77016349E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.03142912E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.81532120E-08   # A_mu(Q) DRbar
  3  3     4.86416862E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.03142912E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73987958E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.03142912E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.89979818E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.03142912E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02706221E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.03142912E+02  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56904468E+07   # M^2_Hd              
        22    -1.69867031E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.80485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39777586E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70334280E+01   # gluino decays
#          BR         NDA      ID1       ID2
     9.74303577E-03    2     1000005        -5   # BR(~g -> ~b_1  bb)
     9.74303577E-03    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.90256964E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     4.90256964E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.09010302E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.60575315E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.70742457E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.14556850E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.14878662E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.23643529E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.95490432E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.25635457E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.98558199E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.95408241E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.06222853E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.73681132E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.84865386E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.47603392E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63050281E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64585179E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.84226848E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55846454E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.64983387E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.69836763E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.27607216E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11757117E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.61805871E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.30100814E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.89783443E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.88628571E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74035548E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.28217941E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.32546865E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.99836060E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82977008E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.59169659E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64734579E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51365877E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.56939115E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.10176088E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.59409084E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.43985155E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.72911113E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44497897E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74053767E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28561629E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.50796775E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.67638067E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83521459E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.45662382E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68045116E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51590442E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.50353834E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.09270558E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.24225414E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.36572617E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.72741513E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85519136E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74035548E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.28217941E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.32546865E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.99836060E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82977008E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.59169659E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64734579E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51365877E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.56939115E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.10176088E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.59409084E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.43985155E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.72911113E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44497897E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74053767E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28561629E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.50796775E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.67638067E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83521459E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.45662382E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68045116E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51590442E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.50353834E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.09270558E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.24225414E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.36572617E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.72741513E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85519136E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10073428E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.28269056E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.02957530E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.20309055E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77850905E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.45787835E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57205675E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02827258E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.58913358E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.54835711E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.39537618E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.66416117E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10073428E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.28269056E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.02957530E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.20309055E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77850905E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.45787835E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57205675E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02827258E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.58913358E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.54835711E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.39537618E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.66416117E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05173301E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.04753010E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49450016E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.73790514E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40048181E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56076199E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80855675E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04724220E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.02511842E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02247240E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.57164538E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43301962E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.94217691E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87375165E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10184891E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00242287E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.07725063E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.22932375E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78257339E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.24492767E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54884958E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10184891E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00242287E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.07725063E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.22932375E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78257339E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.24492767E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54884958E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.42607298E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.08166363E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.75963330E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.64363779E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52322334E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62796379E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03169050E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.56329682E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33754806E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33754806E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11252958E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11252958E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09984473E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.90138543E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.22160325E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.95027298E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.65625767E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.12193627E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.66006429E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.07021639E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.29720862E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.06181890E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.88545506E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.21427122E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.19038976E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54391754E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19038976E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54391754E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35102962E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53918702E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53918702E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49162938E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07499068E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07499068E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07499047E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.87768848E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.87768848E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.87768848E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.87768848E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.29256468E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.29256468E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.29256468E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.29256468E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.34339058E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.34339058E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.93559694E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.01796632E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.12867661E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.72706913E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.20493563E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.72706913E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.20493563E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.11976949E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.78748669E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.78748669E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.81778504E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.02414252E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.02414252E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.02412777E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.31480442E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.00234588E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.31480442E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.00234588E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.06247632E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.88283536E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.88283536E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.66007780E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.37575075E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.37575075E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.37575078E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.27890576E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.27890576E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.27890576E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.27890576E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.26295804E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.26295804E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.26295804E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.26295804E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.16457192E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.16457192E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.89991751E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.87964075E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.39734512E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.37229544E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.07028153E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.07028153E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.82316115E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     7.08903227E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.67369917E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.08687291E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.08687291E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31579030E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     2.31579030E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.98888153E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.98888153E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.90113184E-03   # h decays
#          BR         NDA      ID1       ID2
     6.08777736E-01    2           5        -5   # BR(h -> b       bb     )
     6.62608486E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.34567305E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.98200233E-04    2           3        -3   # BR(h -> s       sb     )
     2.16417610E-02    2           4        -4   # BR(h -> c       cb     )
     6.90616865E-02    2          21        21   # BR(h -> g       g      )
     2.38426493E-03    2          22        22   # BR(h -> gam     gam    )
     1.54292658E-03    2          22        23   # BR(h -> Z       gam    )
     2.04163283E-01    2          24       -24   # BR(h -> W+      W-     )
     2.54347260E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56438042E+01   # H decays
#          BR         NDA      ID1       ID2
     3.63660474E-01    2           5        -5   # BR(H -> b       bb     )
     5.95861642E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10682244E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49200781E-04    2           3        -3   # BR(H -> s       sb     )
     6.98822263E-08    2           4        -4   # BR(H -> c       cb     )
     7.00365400E-03    2           6        -6   # BR(H -> t       tb     )
     2.41249183E-07    2          21        21   # BR(H -> g       g      )
     5.06049975E-09    2          22        22   # BR(H -> gam     gam    )
     1.80667678E-09    2          23        22   # BR(H -> Z       gam    )
     1.51537410E-06    2          24       -24   # BR(H -> W+      W-     )
     7.57162329E-07    2          23        23   # BR(H -> Z       Z      )
     5.93671635E-06    2          25        25   # BR(H -> h       h      )
     2.05459422E-24    2          36        36   # BR(H -> A       A      )
     3.58649551E-20    2          23        36   # BR(H -> Z       A      )
     1.85179632E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54406471E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.54406471E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.67312128E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.10207945E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.76783110E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.97276246E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.07117913E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.55200778E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.77156404E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.10163949E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.83152595E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.21349880E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.29467140E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     9.77191675E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.77191675E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     6.20813265E-08    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56426150E+01   # A decays
#          BR         NDA      ID1       ID2
     3.63693980E-01    2           5        -5   # BR(A -> b       bb     )
     5.95877158E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10687564E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49247718E-04    2           3        -3   # BR(A -> s       sb     )
     7.03491730E-08    2           4        -4   # BR(A -> c       cb     )
     7.01859243E-03    2           6        -6   # BR(A -> t       tb     )
     1.44214355E-05    2          21        21   # BR(A -> g       g      )
     6.34700634E-08    2          22        22   # BR(A -> gam     gam    )
     1.58658710E-08    2          23        22   # BR(A -> Z       gam    )
     1.51229204E-06    2          23        25   # BR(A -> Z       h      )
     1.87906735E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.54394518E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.54394518E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.31700383E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.54363339E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.50580318E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.38451916E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.96046789E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.94264207E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.98560432E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.26533489E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.27763500E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.02098777E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.02098777E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57007015E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.82951027E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95401339E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10519327E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.73088342E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19263337E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45363037E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.70978850E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.51235226E-06    2          24        25   # BR(H+ -> W+      h      )
     2.11672045E-14    2          24        36   # BR(H+ -> W+      A      )
     4.32311757E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.25982835E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.05307161E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.54444427E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.50122431E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.53526001E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.11819204E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.03913276E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     6.46039872E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
