#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11039403E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.16900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.02485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04214105E+01   # W+
        25     1.25516638E+02   # h
        35     4.00000931E+03   # H
        36     3.99999675E+03   # A
        37     4.00099142E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02004872E+03   # ~d_L
   2000001     4.01797948E+03   # ~d_R
   1000002     4.01939893E+03   # ~u_L
   2000002     4.01856233E+03   # ~u_R
   1000003     4.02004872E+03   # ~s_L
   2000003     4.01797948E+03   # ~s_R
   1000004     4.01939893E+03   # ~c_L
   2000004     4.01856233E+03   # ~c_R
   1000005     2.05254034E+03   # ~b_1
   2000005     4.02261774E+03   # ~b_2
   1000006     6.40087856E+02   # ~t_1
   2000006     2.06488489E+03   # ~t_2
   1000011     4.00318890E+03   # ~e_L
   2000011     4.00352996E+03   # ~e_R
   1000012     4.00207970E+03   # ~nu_eL
   1000013     4.00318890E+03   # ~mu_L
   2000013     4.00352996E+03   # ~mu_R
   1000014     4.00207970E+03   # ~nu_muL
   1000015     4.00409923E+03   # ~tau_1
   2000015     4.00844472E+03   # ~tau_2
   1000016     4.00402766E+03   # ~nu_tauL
   1000021     1.98056811E+03   # ~g
   1000022     3.89472795E+02   # ~chi_10
   1000023    -4.28045385E+02   # ~chi_20
   1000025     4.51646260E+02   # ~chi_30
   1000035     2.06571223E+03   # ~chi_40
   1000024     4.25755355E+02   # ~chi_1+
   1000037     2.06588641E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.29568772E-01   # N_11
  1  2     2.08564202E-02   # N_12
  1  3     5.05000468E-01   # N_13
  1  4     4.60726539E-01   # N_14
  2  1    -3.94926918E-02   # N_21
  2  2     2.37904517E-02   # N_22
  2  3    -7.04318057E-01   # N_23
  2  4     7.08385783E-01   # N_24
  3  1     6.82765357E-01   # N_31
  3  2     2.52668552E-02   # N_32
  3  3     4.98868534E-01   # N_33
  3  4     5.33219691E-01   # N_34
  4  1    -1.09649070E-03   # N_41
  4  2     9.99179969E-01   # N_42
  4  3    -6.38653343E-03   # N_43
  4  4    -3.99674833E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.03776938E-03   # U_11
  1  2     9.99959159E-01   # U_12
  2  1    -9.99959159E-01   # U_21
  2  2     9.03776938E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.65322378E-02   # V_11
  1  2    -9.98400774E-01   # V_12
  2  1    -9.98400774E-01   # V_21
  2  2    -5.65322378E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -9.72924358E-02   # cos(theta_t)
  1  2     9.95255837E-01   # sin(theta_t)
  2  1    -9.95255837E-01   # -sin(theta_t)
  2  2    -9.72924358E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998481E-01   # cos(theta_b)
  1  2    -1.74298528E-03   # sin(theta_b)
  2  1     1.74298528E-03   # -sin(theta_b)
  2  2     9.99998481E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06083554E-01   # cos(theta_tau)
  1  2     7.08128530E-01   # sin(theta_tau)
  2  1    -7.08128530E-01   # -sin(theta_tau)
  2  2    -7.06083554E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00244301E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10394027E+03  # DRbar Higgs Parameters
         1    -4.16900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44078930E+02   # higgs               
         4     1.62514736E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10394027E+03  # The gauge couplings
     1     3.61461374E-01   # gprime(Q) DRbar
     2     6.35098340E-01   # g(Q) DRbar
     3     1.03311580E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10394027E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.36185214E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10394027E+03  # The trilinear couplings
  1  1     3.45597523E-07   # A_d(Q) DRbar
  2  2     3.45630205E-07   # A_s(Q) DRbar
  3  3     6.17445367E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10394027E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.75733926E-08   # A_mu(Q) DRbar
  3  3     7.83646524E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10394027E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.70783015E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10394027E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88539260E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10394027E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02985943E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10394027E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57060207E+07   # M^2_Hd              
        22    -1.75600487E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.02485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39917139E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.35847946E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.56140641E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.82735281E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.48501805E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.27476838E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.45747829E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.14913085E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.05496500E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23185056E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.16152797E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.34123185E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.04381037E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.97372292E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.35843320E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.70790695E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.12612631E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.26060256E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.96768855E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.38066244E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.84781627E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64351511E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65448168E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83675084E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54046378E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.67642595E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68756685E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.33054522E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12109309E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.44193597E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.96788267E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.85321652E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.44217376E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75647228E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.20019923E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.29828368E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.06803185E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80687320E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.53163430E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60160200E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52070871E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58349987E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.95233068E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.61803086E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.56933461E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.69733283E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44697130E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75666117E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.20322268E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.46771925E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.38260605E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81225280E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.41387652E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63435255E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52293647E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51732201E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.70156214E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.24812982E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.70245163E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.64307354E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85573495E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75647228E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.20019923E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.29828368E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.06803185E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80687320E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.53163430E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60160200E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52070871E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58349987E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.95233068E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.61803086E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.56933461E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.69733283E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44697130E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75666117E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.20322268E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.46771925E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.38260605E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81225280E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.41387652E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63435255E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52293647E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51732201E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.70156214E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.24812982E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.70245163E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.64307354E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85573495E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10469866E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.89442121E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.72233338E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.63661386E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77704079E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.26114950E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56902087E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03578154E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.33911125E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.55827131E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.64529942E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.61802193E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10469866E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.89442121E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.72233338E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.63661386E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77704079E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.26114950E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56902087E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03578154E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.33911125E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.55827131E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.64529942E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.61802193E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05835255E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.96350960E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49826093E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.82866209E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39812640E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56088895E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80378692E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05202558E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.93591250E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.03052524E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67002654E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42934878E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95315996E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86634365E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10580386E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.64651822E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.08156041E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.65194115E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78110612E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.23020359E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54593031E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10580386E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.64651822E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.08156041E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.65194115E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78110612E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.23020359E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54593031E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.42998251E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.73920893E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.79837402E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.02632782E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52126110E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.64604263E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02778259E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.46368313E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33769019E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33769019E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11257647E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11257647E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09946667E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.65128986E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.17450719E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44401334E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.40159460E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38301052E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.43656582E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37880015E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.78124786E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.57125652E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.19061054E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54452443E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19061054E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54452443E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34566153E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.54242657E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.54242657E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49356338E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.08207002E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.08207002E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.08206986E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.47022045E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.47022045E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.47022045E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.47022045E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.15674190E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.15674190E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.15674190E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.15674190E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.69848211E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.69848211E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.88725220E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.56568019E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.18789474E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.83621239E-04    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.24955660E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.83621239E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.24955660E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.20340919E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.66638973E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.66638973E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.69083502E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.83368029E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.83368029E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.83359578E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.30832710E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.99456628E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.30832710E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.99456628E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.06235615E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.86858798E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.86858798E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.64764488E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.37301937E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.37301937E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.37301939E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26321293E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26321293E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26321293E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26321293E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.21064592E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.21064592E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.21064592E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.21064592E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.11370940E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.11370940E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.65020587E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.53943297E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.60588405E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.85277339E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38263073E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38263073E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.92480427E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.20500134E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.60701469E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.06241680E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.06241680E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.73422959E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.73422959E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02691139E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94054557E-01    2           5        -5   # BR(h -> b       bb     )
     6.46841783E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28981634E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85623583E-04    2           3        -3   # BR(h -> s       sb     )
     2.10931960E-02    2           4        -4   # BR(h -> c       cb     )
     6.85533626E-02    2          21        21   # BR(h -> g       g      )
     2.38229029E-03    2          22        22   # BR(h -> gam     gam    )
     1.62591370E-03    2          22        23   # BR(h -> Z       gam    )
     2.19175108E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77167893E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50338117E+01   # H decays
#          BR         NDA      ID1       ID2
     3.61459843E-01    2           5        -5   # BR(H -> b       bb     )
     6.02466518E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13017569E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51963038E-04    2           3        -3   # BR(H -> s       sb     )
     7.06641766E-08    2           4        -4   # BR(H -> c       cb     )
     7.08202178E-03    2           6        -6   # BR(H -> t       tb     )
     6.41206386E-07    2          21        21   # BR(H -> g       g      )
     2.48279379E-09    2          22        22   # BR(H -> gam     gam    )
     1.82463772E-09    2          23        22   # BR(H -> Z       gam    )
     1.66008075E-06    2          24       -24   # BR(H -> W+      W-     )
     8.29465528E-07    2          23        23   # BR(H -> Z       Z      )
     6.48587567E-06    2          25        25   # BR(H -> h       h      )
     4.45989971E-24    2          36        36   # BR(H -> A       A      )
    -8.64921909E-22    2          23        36   # BR(H -> Z       A      )
     1.86421555E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56001353E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56001353E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.73309733E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.17370215E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.81318310E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.89836546E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.83860407E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.78233890E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.88784403E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.18030848E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.68845173E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.75612557E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.72857805E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.72857805E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.50306023E+01   # A decays
#          BR         NDA      ID1       ID2
     3.61506620E-01    2           5        -5   # BR(A -> b       bb     )
     6.02504220E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13030731E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52019725E-04    2           3        -3   # BR(A -> s       sb     )
     7.11315611E-08    2           4        -4   # BR(A -> c       cb     )
     7.09664969E-03    2           6        -6   # BR(A -> t       tb     )
     1.45818197E-05    2          21        21   # BR(A -> g       g      )
     6.38917390E-08    2          22        22   # BR(A -> gam     gam    )
     1.60411006E-08    2          23        22   # BR(A -> Z       gam    )
     1.65668890E-06    2          23        25   # BR(A -> Z       h      )
     1.89142766E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55995306E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55995306E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.36658739E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.63264078E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.54245887E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.29948497E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.75747749E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.19116092E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.12080878E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.34484171E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.10914144E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     8.04948784E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     8.04948784E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50723891E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.79123141E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02196772E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12922025E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.70638496E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20624448E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48163280E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.68666869E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.65724651E-06    2          24        25   # BR(H+ -> W+      h      )
     2.32644670E-14    2          24        36   # BR(H+ -> W+      A      )
     4.16076099E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.18071025E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.31341193E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.56091797E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.98278809E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55155407E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.80849303E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.61122677E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
