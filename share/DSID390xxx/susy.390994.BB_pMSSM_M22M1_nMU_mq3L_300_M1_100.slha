#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.93812069E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     2.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03980300E+01   # W+
        25     1.27212916E+02   # h
        35     3.00022042E+03   # H
        36     3.00000052E+03   # A
        37     3.00111866E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00811655E+03   # ~d_L
   2000001     3.00230054E+03   # ~d_R
   1000002     3.00719109E+03   # ~u_L
   2000002     3.00734618E+03   # ~u_R
   1000003     3.00811655E+03   # ~s_L
   2000003     3.00230054E+03   # ~s_R
   1000004     3.00719109E+03   # ~c_L
   2000004     3.00734618E+03   # ~c_R
   1000005     3.09448953E+02   # ~b_1
   2000005     3.00728848E+03   # ~b_2
   1000006     3.00812367E+02   # ~t_1
   2000006     3.01579803E+03   # ~t_2
   1000011     3.00825585E+03   # ~e_L
   2000011     2.99808305E+03   # ~e_R
   1000012     3.00686068E+03   # ~nu_eL
   1000013     3.00825585E+03   # ~mu_L
   2000013     2.99808305E+03   # ~mu_R
   1000014     3.00686068E+03   # ~nu_muL
   1000015     2.98648183E+03   # ~tau_1
   2000015     3.02310409E+03   # ~tau_2
   1000016     3.00795534E+03   # ~nu_tauL
   1000021     2.30905955E+03   # ~g
   1000022     1.01773541E+02   # ~chi_10
   1000023     2.16363814E+02   # ~chi_20
   1000025    -3.01564855E+03   # ~chi_30
   1000035     3.01575711E+03   # ~chi_40
   1000024     2.16520553E+02   # ~chi_1+
   1000037     3.01663428E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891926E-01   # N_11
  1  2     7.66226943E-04   # N_12
  1  3    -1.46795469E-02   # N_13
  1  4     2.45345627E-04   # N_14
  2  1    -3.83055807E-04   # N_21
  2  2     9.99659307E-01   # N_22
  2  3     2.60947071E-02   # N_23
  2  4     4.36086459E-04   # N_24
  3  1     1.02171638E-02   # N_31
  3  2    -1.87539962E-02   # N_32
  3  3     7.06777737E-01   # N_33
  3  4     7.07113236E-01   # N_34
  4  1    -1.05640535E-02   # N_41
  4  2     1.81375623E-02   # N_42
  4  3    -7.06801817E-01   # N_43
  4  4     7.07100150E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99319595E-01   # U_11
  1  2     3.68828894E-02   # U_12
  2  1    -3.68828894E-02   # U_21
  2  2     9.99319595E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999811E-01   # V_11
  1  2    -6.15117458E-04   # V_12
  2  1    -6.15117458E-04   # V_21
  2  2    -9.99999811E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98665398E-01   # cos(theta_t)
  1  2    -5.16470990E-02   # sin(theta_t)
  2  1     5.16470990E-02   # -sin(theta_t)
  2  2     9.98665398E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99707228E-01   # cos(theta_b)
  1  2    -2.41962453E-02   # sin(theta_b)
  2  1     2.41962453E-02   # -sin(theta_b)
  2  2     9.99707228E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06877156E-01   # cos(theta_tau)
  1  2     7.07336332E-01   # sin(theta_tau)
  2  1    -7.07336332E-01   # -sin(theta_tau)
  2  2    -7.06877156E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00297747E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.38120685E+02  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.45064474E+02   # higgs               
         4     1.27104813E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.38120685E+02  # The gauge couplings
     1     3.60582496E-01   # gprime(Q) DRbar
     2     6.39051063E-01   # g(Q) DRbar
     3     1.03680040E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.38120685E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.41934064E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.38120685E+02  # The trilinear couplings
  1  1     2.65735007E-07   # A_d(Q) DRbar
  2  2     2.65767482E-07   # A_s(Q) DRbar
  3  3     5.57894643E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.38120685E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.32077708E-07   # A_mu(Q) DRbar
  3  3     1.33949002E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.38120685E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.63143338E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.38120685E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.15463816E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.38120685E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03012586E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.38120685E+02  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -1.00834577E+05   # M^2_Hd              
        22    -9.24868496E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     2.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41767395E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.75157764E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48046409E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48046409E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51953591E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51953591E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.26528521E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.30138050E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.86986195E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.28058929E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.10056890E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.27770820E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.58346495E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.84031488E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.73052245E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65362801E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.25861660E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.96735616E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.75271867E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.02472813E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.95418566E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.74892868E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -8.04603051E-09    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -8.68609730E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.03284819E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.09231809E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.16941975E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.71360833E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.08299972E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.83336454E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62303548E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.24870601E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.06992486E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.24804148E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54368614E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.24946009E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.45631363E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08811743E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.73674761E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62285504E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.24299219E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.07678529E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.71533709E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.40489420E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.41865125E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55951052E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.08299972E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.83336454E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62303548E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.24870601E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.06992486E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.24804148E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54368614E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.24946009E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.45631363E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08811743E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.73674761E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62285504E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.24299219E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.07678529E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.71533709E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.40489420E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.41865125E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55951052E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01276810E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.69773641E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01060199E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01962437E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54709783E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999854E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.45574015E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01276810E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.69773641E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01060199E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01962437E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54709783E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999854E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.45574015E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.80836380E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.44896170E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18428880E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36674949E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75047621E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.52929495E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15744269E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.41691499E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.41148829E-07    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.31322953E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.02435961E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01309666E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.63991374E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01153370E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.02447492E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.01309666E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.63991374E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01153370E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.02447492E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.01492097E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.63905748E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01128399E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.02481026E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.07343951E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.04097321E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.12948470E-12    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.80188931E-08    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     6.12948470E-12    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.80188931E-08    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.54255751E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.57871580E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.86417236E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.86417236E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     3.83716003E-07    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.92761946E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.92761946E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.83484456E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.68140737E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.78736802E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.54508631E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.77317876E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.72970824E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.04199710E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.39763099E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.74357852E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.54334802E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.54334802E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.80457374E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.25879467E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.13119819E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     2.13119819E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     4.28931749E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     4.28931749E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.57430733E-09    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.57430733E-09    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.66472458E-10    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.66472458E-10    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     2.13119819E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     2.13119819E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     4.28931749E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     4.28931749E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.57430733E-09    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.57430733E-09    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.66472458E-10    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.66472458E-10    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.34715856E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34715856E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.62327182E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.62327182E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.81134689E-06    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     2.81134689E-06    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.37818681E-10    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     2.37818681E-10    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     1.05438827E-09    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.05438827E-09    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.37818681E-10    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     2.37818681E-10    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.05438827E-09    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.05438827E-09    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.15028575E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.15028575E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.19690647E-09    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.19690647E-09    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.19690647E-09    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.19690647E-09    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     9.17626621E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     9.17626621E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.04491171E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.76986695E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.92801486E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.52769906E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.52769906E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.08843250E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.67030737E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.00500749E-09    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.00500749E-09    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.68799565E-10    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.68799565E-10    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.49972899E-09    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.49972899E-09    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.88510735E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.88510735E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.00500749E-09    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.00500749E-09    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.68799565E-10    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.68799565E-10    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.49972899E-09    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.49972899E-09    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.88510735E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.88510735E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.25866708E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25866708E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.60325772E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.60325772E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.08807368E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.08807368E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.14576059E-10    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.14576059E-10    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.13791113E-09    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.13791113E-09    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.14576059E-10    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.14576059E-10    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.13791113E-09    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.13791113E-09    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.91415507E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.91415507E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.18153715E-09    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.18153715E-09    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.18153715E-09    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.18153715E-09    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.08991858E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.08991858E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.94365581E-03   # h decays
#          BR         NDA      ID1       ID2
     5.29931295E-01    2           5        -5   # BR(h -> b       bb     )
     6.69587914E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.37026222E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.01373261E-04    2           3        -3   # BR(h -> s       sb     )
     2.17725606E-02    2           4        -4   # BR(h -> c       cb     )
     7.19599519E-02    2          21        21   # BR(h -> g       g      )
     2.57394869E-03    2          22        22   # BR(h -> gam     gam    )
     1.92137190E-03    2          22        23   # BR(h -> Z       gam    )
     2.69347404E-01    2          24       -24   # BR(h -> W+      W-     )
     3.47962777E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.65108676E+01   # H decays
#          BR         NDA      ID1       ID2
     9.18640625E-01    2           5        -5   # BR(H -> b       bb     )
     5.34686966E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.89052534E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.32155691E-04    2           3        -3   # BR(H -> s       sb     )
     6.51120998E-08    2           4        -4   # BR(H -> c       cb     )
     6.49535553E-03    2           6        -6   # BR(H -> t       tb     )
     1.65064077E-06    2          21        21   # BR(H -> g       g      )
     2.40110155E-08    2          22        22   # BR(H -> gam     gam    )
     2.64816786E-09    2          23        22   # BR(H -> Z       gam    )
     9.66637037E-07    2          24       -24   # BR(H -> W+      W-     )
     4.82722489E-07    2          23        23   # BR(H -> Z       Z      )
     4.79116445E-06    2          25        25   # BR(H -> h       h      )
     9.51812977E-30    2          36        36   # BR(H -> A       A      )
     1.18557911E-17    2          23        36   # BR(H -> Z       A      )
     7.20792686E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.33022621E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.60719547E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.19677708E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.96264588E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.18031221E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.56000885E+01   # A decays
#          BR         NDA      ID1       ID2
     9.36970545E-01    2           5        -5   # BR(A -> b       bb     )
     5.45331057E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.92815761E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.36817787E-04    2           3        -3   # BR(A -> s       sb     )
     6.68266226E-08    2           4        -4   # BR(A -> c       cb     )
     6.66271554E-03    2           6        -6   # BR(A -> t       tb     )
     1.96210550E-05    2          21        21   # BR(A -> g       g      )
     4.64461551E-08    2          22        22   # BR(A -> gam     gam    )
     1.93182406E-08    2          23        22   # BR(A -> Z       gam    )
     9.81911218E-07    2          23        25   # BR(A -> Z       h      )
     7.48280290E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.42365468E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.74460739E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.26287072E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.91420794E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.49236030E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.06214086E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.78984954E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.55109434E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.05184755E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.16398865E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.37436582E-01    2           6        -5   # BR(H+ -> t       bb     )
     9.12729337E-07    2          24        25   # BR(H+ -> W+      h      )
     4.67941963E-14    2          24        36   # BR(H+ -> W+      A      )
     4.18738562E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.17134000E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.61454461E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
