#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15008780E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03959530E+01   # W+
        25     1.24155440E+02   # h
        35     3.00012980E+03   # H
        36     2.99999981E+03   # A
        37     3.00091893E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03550339E+03   # ~d_L
   2000001     3.03028878E+03   # ~d_R
   1000002     3.03458367E+03   # ~u_L
   2000002     3.03163458E+03   # ~u_R
   1000003     3.03550339E+03   # ~s_L
   2000003     3.03028878E+03   # ~s_R
   1000004     3.03458367E+03   # ~c_L
   2000004     3.03163458E+03   # ~c_R
   1000005     8.42649291E+02   # ~b_1
   2000005     3.02949019E+03   # ~b_2
   1000006     8.40942347E+02   # ~t_1
   2000006     3.01643952E+03   # ~t_2
   1000011     3.00648159E+03   # ~e_L
   2000011     3.00181124E+03   # ~e_R
   1000012     3.00508026E+03   # ~nu_eL
   1000013     3.00648159E+03   # ~mu_L
   2000013     3.00181124E+03   # ~mu_R
   1000014     3.00508026E+03   # ~nu_muL
   1000015     2.98584644E+03   # ~tau_1
   2000015     3.02198012E+03   # ~tau_2
   1000016     3.00495129E+03   # ~nu_tauL
   1000021     2.34368483E+03   # ~g
   1000022     1.00311873E+02   # ~chi_10
   1000023     2.15253440E+02   # ~chi_20
   1000025    -2.99758321E+03   # ~chi_30
   1000035     2.99806347E+03   # ~chi_40
   1000024     2.15413266E+02   # ~chi_1+
   1000037     2.99875254E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888456E-01   # N_11
  1  2    -1.53719495E-03   # N_12
  1  3     1.48053120E-02   # N_13
  1  4    -1.23080520E-03   # N_14
  2  1     1.92826852E-03   # N_21
  2  2     9.99650672E-01   # N_22
  2  3    -2.61833639E-02   # N_23
  2  4     3.04077493E-03   # N_24
  3  1    -9.57039754E-03   # N_31
  3  2     1.63811627E-02   # N_32
  3  3     7.06830650E-01   # N_33
  3  4     7.07128346E-01   # N_34
  4  1    -1.13032909E-02   # N_41
  4  2     2.06839943E-02   # N_42
  4  3     7.06742999E-01   # N_43
  4  4    -7.07077606E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99313157E-01   # U_11
  1  2    -3.70569053E-02   # U_12
  2  1     3.70569053E-02   # U_21
  2  2     9.99313157E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990734E-01   # V_11
  1  2    -4.30479967E-03   # V_12
  2  1     4.30479967E-03   # V_21
  2  2     9.99990734E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98824757E-01   # cos(theta_t)
  1  2    -4.84675645E-02   # sin(theta_t)
  2  1     4.84675645E-02   # -sin(theta_t)
  2  2     9.98824757E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99915065E-01   # cos(theta_b)
  1  2     1.30331418E-02   # sin(theta_b)
  2  1    -1.30331418E-02   # -sin(theta_b)
  2  2     9.99915065E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06909487E-01   # cos(theta_tau)
  1  2     7.07304020E-01   # sin(theta_tau)
  2  1    -7.07304020E-01   # -sin(theta_tau)
  2  2     7.06909487E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01821112E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.50087805E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44267376E+02   # higgs               
         4     7.49963146E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.50087805E+03  # The gauge couplings
     1     3.62170466E-01   # gprime(Q) DRbar
     2     6.39899095E-01   # g(Q) DRbar
     3     1.02634981E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.50087805E+03  # The trilinear couplings
  1  1     2.12273086E-06   # A_u(Q) DRbar
  2  2     2.12276330E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.50087805E+03  # The trilinear couplings
  1  1     5.31580187E-07   # A_d(Q) DRbar
  2  2     5.31685966E-07   # A_s(Q) DRbar
  3  3     1.22707451E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.50087805E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.17626398E-07   # A_mu(Q) DRbar
  3  3     1.19058131E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.50087805E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51808180E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.50087805E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12476720E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.50087805E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05198452E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.50087805E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.30273350E+04   # M^2_Hd              
        22    -9.05896547E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42153097E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.19062326E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48221710E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48221710E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51778290E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51778290E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.08601490E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.23991971E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.09938393E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.77662409E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02042541E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.46559652E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.96675432E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.95410002E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.15457879E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.30067047E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.57078260E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50222930E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.97022167E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.87069238E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.40818434E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.48438873E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.37479284E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.21378916E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.95539724E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.59089467E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.49466325E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.47091416E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.94247801E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29791860E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.87428896E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.17643996E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.01168840E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07398560E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.80762938E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64750609E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.40087604E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.79186160E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29481044E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.93511445E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.99960652E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18048032E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59671639E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.89692646E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.49583428E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.15283235E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40327751E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.07903009E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.99765624E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64445925E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.91654122E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.01366783E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28904615E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.06012499E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.00651399E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66895272E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54624380E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.67898881E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.23858466E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.03412751E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54537389E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07398560E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.80762938E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64750609E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.40087604E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.79186160E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29481044E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.93511445E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.99960652E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18048032E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59671639E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.89692646E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.49583428E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.15283235E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40327751E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.07903009E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.99765624E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64445925E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.91654122E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.01366783E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28904615E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.06012499E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.00651399E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66895272E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54624380E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.67898881E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.23858466E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.03412751E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54537389E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02374738E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.67224149E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01669485E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.27822517E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.94250446E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01608074E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.20392697E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56280142E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996309E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.68912175E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.27584066E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.97576414E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02374738E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.67224149E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01669485E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.27822517E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.94250446E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01608074E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.20392697E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56280142E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996309E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.68912175E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.27584066E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.97576414E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82224683E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45649756E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18540254E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35809990E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76221429E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53805631E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15834205E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.17098007E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.32375177E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30321866E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.33500019E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02408122E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.77251649E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00187186E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.58908037E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.85362305E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02087641E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.99515090E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02408122E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.77251649E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00187186E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.58908037E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.85362305E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02087641E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.99515090E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02424485E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.77169787E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00161832E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.46681471E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.67705269E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02120157E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.02442973E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.55593093E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.38205986E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.64978351E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.17209143E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.31989945E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.11772837E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.20244135E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.95512889E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.35396033E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.75139450E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.41118030E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.32581640E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.34919072E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.91608399E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.91608399E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.77990158E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.78130417E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61328312E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61328312E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.23389848E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.23389848E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.56451447E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.56451447E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.32008766E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.60859250E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.75955152E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.99368503E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.99368503E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.38164591E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.58326676E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.59031058E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.59031058E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.26049561E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.26049561E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.18550372E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.18550372E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.72972132E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85041336E-01    2           5        -5   # BR(h -> b       bb     )
     5.48174448E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94058582E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.12400275E-04    2           3        -3   # BR(h -> s       sb     )
     1.78016880E-02    2           4        -4   # BR(h -> c       cb     )
     5.72514232E-02    2          21        21   # BR(h -> g       g      )
     1.93404207E-03    2          22        22   # BR(h -> gam     gam    )
     1.22382196E-03    2          22        23   # BR(h -> Z       gam    )
     1.61431379E-01    2          24       -24   # BR(h -> W+      W-     )
     1.98924061E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40070895E+01   # H decays
#          BR         NDA      ID1       ID2
     7.45719258E-01    2           5        -5   # BR(H -> b       bb     )
     1.77535980E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.27724799E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.70846262E-04    2           3        -3   # BR(H -> s       sb     )
     2.17518101E-07    2           4        -4   # BR(H -> c       cb     )
     2.16988311E-02    2           6        -6   # BR(H -> t       tb     )
     2.83760982E-05    2          21        21   # BR(H -> g       g      )
     1.15638777E-08    2          22        22   # BR(H -> gam     gam    )
     8.34637183E-09    2          23        22   # BR(H -> Z       gam    )
     3.15269534E-05    2          24       -24   # BR(H -> W+      W-     )
     1.57440325E-05    2          23        23   # BR(H -> Z       Z      )
     8.35075129E-05    2          25        25   # BR(H -> h       h      )
     1.62989096E-23    2          36        36   # BR(H -> A       A      )
     2.99080466E-18    2          23        36   # BR(H -> Z       A      )
     2.38447170E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12735421E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.18721804E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.33321731E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.90643657E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.85606462E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33222752E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84125435E-01    2           5        -5   # BR(A -> b       bb     )
     1.86658346E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59978385E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.10590508E-04    2           3        -3   # BR(A -> s       sb     )
     2.28737151E-07    2           4        -4   # BR(A -> c       cb     )
     2.28054406E-02    2           6        -6   # BR(A -> t       tb     )
     6.71598437E-05    2          21        21   # BR(A -> g       g      )
     3.33011891E-08    2          22        22   # BR(A -> gam     gam    )
     6.60996830E-08    2          23        22   # BR(A -> Z       gam    )
     3.30234444E-05    2          23        25   # BR(A -> Z       h      )
     2.62003996E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21061009E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30447754E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.94118925E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01797756E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10252481E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.44354671E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.63978517E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.05614776E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.07741856E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04458827E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.17379075E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.32845859E-05    2          24        25   # BR(H+ -> W+      h      )
     8.47824776E-14    2          24        36   # BR(H+ -> W+      A      )
     2.04046284E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.58185188E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.31135770E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
