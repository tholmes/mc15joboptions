#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11038409E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.70900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.19485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04205235E+01   # W+
        25     1.25199954E+02   # h
        35     4.00000955E+03   # H
        36     3.99999676E+03   # A
        37     4.00098937E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02005754E+03   # ~d_L
   2000001     4.01800362E+03   # ~d_R
   1000002     4.01940710E+03   # ~u_L
   2000002     4.01851577E+03   # ~u_R
   1000003     4.02005754E+03   # ~s_L
   2000003     4.01800362E+03   # ~s_R
   1000004     4.01940710E+03   # ~c_L
   2000004     4.01851577E+03   # ~c_R
   1000005     2.22021673E+03   # ~b_1
   2000005     4.02273334E+03   # ~b_2
   1000006     6.09156812E+02   # ~t_1
   2000006     2.23040126E+03   # ~t_2
   1000011     4.00315549E+03   # ~e_L
   2000011     4.00363391E+03   # ~e_R
   1000012     4.00204539E+03   # ~nu_eL
   1000013     4.00315549E+03   # ~mu_L
   2000013     4.00363391E+03   # ~mu_R
   1000014     4.00204539E+03   # ~nu_muL
   1000015     4.00426862E+03   # ~tau_1
   2000015     4.00836473E+03   # ~tau_2
   1000016     4.00399963E+03   # ~nu_tauL
   1000021     1.98344316E+03   # ~g
   1000022     3.40603280E+02   # ~chi_10
   1000023    -3.81892736E+02   # ~chi_20
   1000025     4.03646887E+02   # ~chi_30
   1000035     2.06771452E+03   # ~chi_40
   1000024     3.79416552E+02   # ~chi_1+
   1000037     2.06788193E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.51233320E-01   # N_11
  1  2     1.93303224E-02   # N_12
  1  3     4.91518844E-01   # N_13
  1  4     4.40095515E-01   # N_14
  2  1    -4.46723233E-02   # N_21
  2  2     2.42626069E-02   # N_22
  2  3    -7.03734213E-01   # N_23
  2  4     7.08642270E-01   # N_24
  3  1     6.58522411E-01   # N_31
  3  2     2.53001908E-02   # N_32
  3  3     5.12969904E-01   # N_33
  3  4     5.50063643E-01   # N_34
  4  1    -1.05613991E-03   # N_41
  4  2     9.99198461E-01   # N_42
  4  3    -5.40936344E-03   # N_43
  4  4    -3.96491926E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.65567129E-03   # U_11
  1  2     9.99970695E-01   # U_12
  2  1    -9.99970695E-01   # U_21
  2  2     7.65567129E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.60828177E-02   # V_11
  1  2    -9.98426120E-01   # V_12
  2  1    -9.98426120E-01   # V_21
  2  2    -5.60828177E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.07366484E-02   # cos(theta_t)
  1  2     9.96735468E-01   # sin(theta_t)
  2  1    -9.96735468E-01   # -sin(theta_t)
  2  2    -8.07366484E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998652E-01   # cos(theta_b)
  1  2    -1.64194951E-03   # sin(theta_b)
  2  1     1.64194951E-03   # -sin(theta_b)
  2  2     9.99998652E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05955270E-01   # cos(theta_tau)
  1  2     7.08256420E-01   # sin(theta_tau)
  2  1    -7.08256420E-01   # -sin(theta_tau)
  2  2    -7.05955270E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00246707E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10384086E+03  # DRbar Higgs Parameters
         1    -3.70900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44109267E+02   # higgs               
         4     1.62623821E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10384086E+03  # The gauge couplings
     1     3.61494375E-01   # gprime(Q) DRbar
     2     6.35168736E-01   # g(Q) DRbar
     3     1.03304806E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10384086E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.34321222E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10384086E+03  # The trilinear couplings
  1  1     3.44566484E-07   # A_d(Q) DRbar
  2  2     3.44599114E-07   # A_s(Q) DRbar
  3  3     6.15746849E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10384086E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.68263608E-08   # A_mu(Q) DRbar
  3  3     7.76087835E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10384086E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69831713E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10384086E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87088810E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10384086E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03103690E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10384086E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57414109E+07   # M^2_Hd              
        22    -1.63901025E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.19485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39952025E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.43154147E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.60088971E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.73125651E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.60793985E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.57720839E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.04172611E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.69790720E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.79025290E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.28776613E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.29832824E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.18993420E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.25891722E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.75956595E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.61130291E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.27603824E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.24935194E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.54116275E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.43478857E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.39873778E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.55918296E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.78157720E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.12481405E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.53054801E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64496434E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64284388E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81915108E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54454959E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.27499085E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65720867E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.53031592E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12863095E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.05679334E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.40407386E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.84995902E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.44630589E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75867248E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.32225776E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.16410737E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.95544657E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79552805E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.46290575E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57896814E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52409402E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58635441E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.13852888E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.10585491E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.39809378E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.41981277E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44523154E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75886252E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28258336E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.30574757E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.60068991E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80074996E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.01802324E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61137979E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52632812E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51991002E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.18823986E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.88510869E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.25647858E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.92051077E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85526422E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75867248E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.32225776E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.16410737E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.95544657E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79552805E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.46290575E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57896814E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52409402E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58635441E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.13852888E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.10585491E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.39809378E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.41981277E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44523154E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75886252E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28258336E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.30574757E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.60068991E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80074996E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.01802324E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61137979E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52632812E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51991002E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.18823986E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.88510869E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.25647858E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.92051077E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85526422E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10418016E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.49985966E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.85901519E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.11641216E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77438451E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95789115E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56338566E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04641845E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.65800792E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.99324699E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.32205351E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.10089387E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10418016E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.49985966E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.85901519E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.11641216E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77438451E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95789115E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56338566E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04641845E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.65800792E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.99324699E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.32205351E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.10089387E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06538634E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.09253281E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46547667E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.71718557E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39425285E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53618843E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79586225E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06087297E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.06208723E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.10919549E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.54995540E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42284087E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.01049485E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85314748E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10528938E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01795167E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.25371495E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.18921262E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77818196E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19525617E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54045539E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10528938E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01795167E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.25371495E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.18921262E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77818196E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19525617E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54045539E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43136569E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.21680576E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.13515113E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.60391069E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51719232E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.69402014E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01998251E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.50373814E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33713562E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33713562E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11239201E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11239201E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10094474E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.65132997E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.44507122E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46376478E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.60237529E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40326622E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.53430498E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39391938E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.90492570E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.78386454E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18782432E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54090117E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18782432E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54090117E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36527355E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53406052E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53406052E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49014369E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06541876E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06541876E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06541859E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.36199923E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.36199923E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.36199923E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.36199923E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.45400193E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.45400193E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.45400193E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.45400193E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.34907822E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.34907822E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.44686752E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.98071510E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.81369634E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.49167896E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.47228454E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.49167896E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.47228454E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.28732939E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.81695221E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.81695221E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.84270905E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.07310083E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.07310083E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.07308259E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.10729977E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.73376974E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.10729977E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.73376974E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.75225760E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.27037583E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.27037583E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.03538882E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.25343202E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.25343202E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.25343204E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.24053860E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.24053860E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.24053860E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.24053860E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.13506876E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.13506876E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.13506876E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.13506876E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.02662117E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.02662117E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.64994562E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.45500793E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.56639868E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.53318448E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40242120E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40242120E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.20376257E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.77644839E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.91103153E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.70407716E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.70407716E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     3.99287469E-03   # h decays
#          BR         NDA      ID1       ID2
     5.99458371E-01    2           5        -5   # BR(h -> b       bb     )
     6.50712060E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30353109E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.88772436E-04    2           3        -3   # BR(h -> s       sb     )
     2.12297898E-02    2           4        -4   # BR(h -> c       cb     )
     6.90610019E-02    2          21        21   # BR(h -> g       g      )
     2.37286209E-03    2          22        22   # BR(h -> gam     gam    )
     1.59431131E-03    2          22        23   # BR(h -> Z       gam    )
     2.13617849E-01    2          24       -24   # BR(h -> W+      W-     )
     2.68754834E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46463909E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57027948E-01    2           5        -5   # BR(H -> b       bb     )
     6.06737785E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14527785E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53749361E-04    2           3        -3   # BR(H -> s       sb     )
     7.11658449E-08    2           4        -4   # BR(H -> c       cb     )
     7.13229939E-03    2           6        -6   # BR(H -> t       tb     )
     7.48734797E-07    2          21        21   # BR(H -> g       g      )
     1.19045432E-09    2          22        22   # BR(H -> gam     gam    )
     1.83701610E-09    2          23        22   # BR(H -> Z       gam    )
     1.68405561E-06    2          24       -24   # BR(H -> W+      W-     )
     8.41444607E-07    2          23        23   # BR(H -> Z       Z      )
     6.49552295E-06    2          25        25   # BR(H -> h       h      )
     3.55095102E-24    2          36        36   # BR(H -> A       A      )
     6.60905035E-20    2          23        36   # BR(H -> Z       A      )
     1.87050605E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58562830E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58562830E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.77050356E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.06716481E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.81566563E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.00550819E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.04808803E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.70568272E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.81394432E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.16469743E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.03689286E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.01681955E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.25312729E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.25312729E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.46399270E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57095850E-01    2           5        -5   # BR(A -> b       bb     )
     6.06812124E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14553901E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53821666E-04    2           3        -3   # BR(A -> s       sb     )
     7.16401517E-08    2           4        -4   # BR(A -> c       cb     )
     7.14739073E-03    2           6        -6   # BR(A -> t       tb     )
     1.46860796E-05    2          21        21   # BR(A -> g       g      )
     6.14407824E-08    2          22        22   # BR(A -> gam     gam    )
     1.61533002E-08    2          23        22   # BR(A -> Z       gam    )
     1.68074026E-06    2          23        25   # BR(A -> Z       h      )
     1.88835429E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58573264E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58573264E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.38517552E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.70493364E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.52707725E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.45425030E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.89812799E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.97710288E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.04946887E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.58764520E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.33735396E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.40736299E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.40736299E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46367111E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.71215408E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.06998432E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14619775E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65577550E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21586264E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50142044E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63783728E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.68267910E-06    2          24        25   # BR(H+ -> W+      h      )
     2.32079587E-14    2          24        36   # BR(H+ -> W+      A      )
     4.51039222E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.12343801E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.12164769E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58792753E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.69689386E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57582350E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.39139603E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.08633201E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
