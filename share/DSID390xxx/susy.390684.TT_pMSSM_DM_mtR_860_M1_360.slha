#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13067353E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.76900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     8.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04198649E+01   # W+
        25     1.25821414E+02   # h
        35     4.00000958E+03   # H
        36     3.99999704E+03   # A
        37     4.00103464E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02607475E+03   # ~d_L
   2000001     4.02272226E+03   # ~d_R
   1000002     4.02542671E+03   # ~u_L
   2000002     4.02332471E+03   # ~u_R
   1000003     4.02607475E+03   # ~s_L
   2000003     4.02272226E+03   # ~s_R
   1000004     4.02542671E+03   # ~c_L
   2000004     4.02332471E+03   # ~c_R
   1000005     2.03556857E+03   # ~b_1
   2000005     4.02567105E+03   # ~b_2
   1000006     8.80524764E+02   # ~t_1
   2000006     2.05013926E+03   # ~t_2
   1000011     4.00450028E+03   # ~e_L
   2000011     4.00357877E+03   # ~e_R
   1000012     4.00339133E+03   # ~nu_eL
   1000013     4.00450028E+03   # ~mu_L
   2000013     4.00357877E+03   # ~mu_R
   1000014     4.00339133E+03   # ~nu_muL
   1000015     4.00448001E+03   # ~tau_1
   2000015     4.00784438E+03   # ~tau_2
   1000016     4.00480829E+03   # ~nu_tauL
   1000021     1.99057643E+03   # ~g
   1000022     3.42940937E+02   # ~chi_10
   1000023    -3.88434081E+02   # ~chi_20
   1000025     4.06973665E+02   # ~chi_30
   1000035     2.06374112E+03   # ~chi_40
   1000024     3.86006849E+02   # ~chi_1+
   1000037     2.06392066E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.82816567E-01   # N_11
  1  2    -1.81173881E-02   # N_12
  1  3    -4.65368278E-01   # N_13
  1  4    -4.12677052E-01   # N_14
  2  1    -4.42857881E-02   # N_21
  2  2     2.41669023E-02   # N_22
  2  3    -7.03804682E-01   # N_23
  2  4     7.08599816E-01   # N_24
  3  1     6.20673730E-01   # N_31
  3  2     2.62731143E-02   # N_32
  3  3     5.36712936E-01   # N_33
  3  4     5.70975541E-01   # N_34
  4  1    -1.05503573E-03   # N_41
  4  2     9.99198401E-01   # N_42
  4  3    -5.52803024E-03   # N_43
  4  4    -3.96343694E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.82350335E-03   # U_11
  1  2     9.99969396E-01   # U_12
  2  1    -9.99969396E-01   # U_21
  2  2     7.82350335E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.60617994E-02   # V_11
  1  2    -9.98427301E-01   # V_12
  2  1    -9.98427301E-01   # V_21
  2  2    -5.60617994E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.18030354E-01   # cos(theta_t)
  1  2     9.93009988E-01   # sin(theta_t)
  2  1    -9.93009988E-01   # -sin(theta_t)
  2  2    -1.18030354E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998801E-01   # cos(theta_b)
  1  2    -1.54854724E-03   # sin(theta_b)
  2  1     1.54854724E-03   # -sin(theta_b)
  2  2     9.99998801E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06042812E-01   # cos(theta_tau)
  1  2     7.08169152E-01   # sin(theta_tau)
  2  1    -7.08169152E-01   # -sin(theta_tau)
  2  2    -7.06042812E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00265466E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30673528E+03  # DRbar Higgs Parameters
         1    -3.76900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43628732E+02   # higgs               
         4     1.61262894E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30673528E+03  # The gauge couplings
     1     3.62018244E-01   # gprime(Q) DRbar
     2     6.35541140E-01   # g(Q) DRbar
     3     1.02938068E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30673528E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37146372E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30673528E+03  # The trilinear couplings
  1  1     5.05177942E-07   # A_d(Q) DRbar
  2  2     5.05224756E-07   # A_s(Q) DRbar
  3  3     9.03694884E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30673528E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11733498E-07   # A_mu(Q) DRbar
  3  3     1.12882266E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30673528E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67431386E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30673528E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85990400E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30673528E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03310456E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30673528E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57487138E+07   # M^2_Hd              
        22    -1.12442436E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     8.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40119057E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.77277758E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.72667850E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00906305E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.11124084E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.04809626E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.83159985E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.37632903E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     8.61645126E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.17232071E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.29050768E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.41360928E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.02620312E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.10796244E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.81101148E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.93023797E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.94690810E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.04646089E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.53781305E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.15607846E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -7.45781594E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64372780E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.62700690E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83531233E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58261642E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.27661760E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68912757E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.53345068E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12095871E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.17340255E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.58866125E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.48457550E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.74995871E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76069095E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48673420E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.15857321E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80239296E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82726837E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.46533466E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64231990E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51446871E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58573078E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.42351519E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.09131168E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.13980764E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.44398399E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44257606E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76088227E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.42696212E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.22500392E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.27746445E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83250120E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.06441687E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67485200E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51668865E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51891807E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.93386326E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.84783828E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.58394361E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.98542135E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85453706E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76069095E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48673420E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.15857321E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80239296E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82726837E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.46533466E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64231990E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51446871E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58573078E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.42351519E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.09131168E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.13980764E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.44398399E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44257606E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76088227E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.42696212E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.22500392E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.27746445E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83250120E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.06441687E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67485200E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51668865E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51891807E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.93386326E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.84783828E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.58394361E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.98542135E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85453706E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11827102E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.30451905E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.68087265E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.29063927E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77509924E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.19934970E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56475931E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05238803E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.14233218E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.95759919E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.83808572E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.10485481E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11827102E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.30451905E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.68087265E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.29063927E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77509924E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.19934970E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56475931E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05238803E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.14233218E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.95759919E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.83808572E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.10485481E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07542291E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25850170E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46238097E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.54810474E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39559470E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53037807E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79852296E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06960743E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.24822308E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.09442537E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.36373142E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42376922E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.99857010E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85497674E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11936132E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09405461E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.23442885E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.40948798E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77892725E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18127368E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54191231E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11936132E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09405461E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.23442885E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.40948798E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77892725E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18127368E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54191231E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44503510E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.90989979E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.11814370E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.89991217E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51840327E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.67062357E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02237174E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.92564128E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33640873E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33640873E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11215028E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11215028E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10288198E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.65532983E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.28191936E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.66879333E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.43776799E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.41657416E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.37672922E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.62774589E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38423875E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.23565437E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.12278373E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18387350E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53597799E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18387350E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53597799E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38905434E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52395893E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52395893E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48612899E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04563647E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04563647E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04563634E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.52874018E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.52874018E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.52874018E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.52874018E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.50958140E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.50958140E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.50958140E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.50958140E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.03920997E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.03920997E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.63134421E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.06283111E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.81806178E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.28144492E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.64809149E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.28144492E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.64809149E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.57873084E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.66837766E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.66837766E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.67022988E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.61598759E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.61598759E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.61595448E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.73491481E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.25097605E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.73491481E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.25097605E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.19142110E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.16474891E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.16474891E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.90202415E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.03248041E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.03248041E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.03248042E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.09610916E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.09610916E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.09610916E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.09610916E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.65364981E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.65364981E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.65364981E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.65364981E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.52652498E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.52652498E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.65526994E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.97412452E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.55538967E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.84709895E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.37414294E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.37414294E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.51758951E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.65211381E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.06006541E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.10664798E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.10664798E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.91837766E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.91837766E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07261428E-03   # h decays
#          BR         NDA      ID1       ID2
     5.89444038E-01    2           5        -5   # BR(h -> b       bb     )
     6.41193881E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26980962E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81152941E-04    2           3        -3   # BR(h -> s       sb     )
     2.08972414E-02    2           4        -4   # BR(h -> c       cb     )
     6.83178540E-02    2          21        21   # BR(h -> g       g      )
     2.37919971E-03    2          22        22   # BR(h -> gam     gam    )
     1.65128755E-03    2          22        23   # BR(h -> Z       gam    )
     2.24027636E-01    2          24       -24   # BR(h -> W+      W-     )
     2.84552215E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46943672E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57201001E-01    2           5        -5   # BR(H -> b       bb     )
     6.06205462E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14339568E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53526732E-04    2           3        -3   # BR(H -> s       sb     )
     7.11087490E-08    2           4        -4   # BR(H -> c       cb     )
     7.12657719E-03    2           6        -6   # BR(H -> t       tb     )
     8.22206996E-07    2          21        21   # BR(H -> g       g      )
     1.59865938E-09    2          22        22   # BR(H -> gam     gam    )
     1.83373822E-09    2          23        22   # BR(H -> Z       gam    )
     1.77917662E-06    2          24       -24   # BR(H -> W+      W-     )
     8.88972137E-07    2          23        23   # BR(H -> Z       Z      )
     6.90486557E-06    2          25        25   # BR(H -> h       h      )
    -1.75884096E-24    2          36        36   # BR(H -> A       A      )
     4.80548649E-20    2          23        36   # BR(H -> Z       A      )
     1.86433043E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58656801E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58656801E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.66586752E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.97560631E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.73335769E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.19430230E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.83556660E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.31818020E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62216164E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.18694928E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.41432514E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.49855324E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.98059356E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.98059356E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.46892411E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57260201E-01    2           5        -5   # BR(A -> b       bb     )
     6.06264995E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14360449E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53592808E-04    2           3        -3   # BR(A -> s       sb     )
     7.15755571E-08    2           4        -4   # BR(A -> c       cb     )
     7.14094626E-03    2           6        -6   # BR(A -> t       tb     )
     1.46728365E-05    2          21        21   # BR(A -> g       g      )
     6.17523490E-08    2          22        22   # BR(A -> gam     gam    )
     1.61368843E-08    2          23        22   # BR(A -> Z       gam    )
     1.77557859E-06    2          23        25   # BR(A -> Z       h      )
     1.88339183E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58662730E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58662730E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.29993832E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.59819415E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.46131898E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67369100E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.24764818E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.57248751E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.82377591E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.57801293E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.76021264E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.28855507E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.28855507E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47078374E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.71868844E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.06216125E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14343170E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65995748E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21429390E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49819303E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.64182765E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.77697818E-06    2          24        25   # BR(H+ -> W+      h      )
     2.89291540E-14    2          24        36   # BR(H+ -> W+      A      )
     4.92657898E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.25001822E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.68908130E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58825852E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.90067121E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57644859E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.18984574E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.05870273E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
