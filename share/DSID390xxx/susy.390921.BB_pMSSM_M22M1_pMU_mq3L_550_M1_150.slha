#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12853887E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03989360E+01   # W+
        25     1.24612212E+02   # h
        35     3.00019591E+03   # H
        36     2.99999958E+03   # A
        37     3.00095383E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02668317E+03   # ~d_L
   2000001     3.02144531E+03   # ~d_R
   1000002     3.02576345E+03   # ~u_L
   2000002     3.02328912E+03   # ~u_R
   1000003     3.02668317E+03   # ~s_L
   2000003     3.02144531E+03   # ~s_R
   1000004     3.02576345E+03   # ~c_L
   2000004     3.02328912E+03   # ~c_R
   1000005     6.25265886E+02   # ~b_1
   2000005     3.02089114E+03   # ~b_2
   1000006     6.22732398E+02   # ~t_1
   2000006     3.00689342E+03   # ~t_2
   1000011     3.00665153E+03   # ~e_L
   2000011     3.00129609E+03   # ~e_R
   1000012     3.00525457E+03   # ~nu_eL
   1000013     3.00665153E+03   # ~mu_L
   2000013     3.00129609E+03   # ~mu_R
   1000014     3.00525457E+03   # ~nu_muL
   1000015     2.98589657E+03   # ~tau_1
   2000015     3.02214551E+03   # ~tau_2
   1000016     3.00531118E+03   # ~nu_tauL
   1000021     2.33272595E+03   # ~g
   1000022     1.51083513E+02   # ~chi_10
   1000023     3.19964591E+02   # ~chi_20
   1000025    -3.00106696E+03   # ~chi_30
   1000035     3.00161954E+03   # ~chi_40
   1000024     3.20126157E+02   # ~chi_1+
   1000037     3.00227282E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888322E-01   # N_11
  1  2    -1.15353754E-03   # N_12
  1  3     1.48266043E-02   # N_13
  1  4    -1.47799155E-03   # N_14
  2  1     1.55002450E-03   # N_21
  2  2     9.99643432E-01   # N_22
  2  3    -2.63658157E-02   # N_23
  2  4     3.93077268E-03   # N_24
  3  1    -9.41766264E-03   # N_31
  3  2     1.58771407E-02   # N_32
  3  3     7.06841515E-01   # N_33
  3  4     7.07131032E-01   # N_34
  4  1    -1.14999286E-02   # N_41
  4  2     2.14381795E-02   # N_42
  4  3     7.06724903E-01   # N_43
  4  4    -7.07070059E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99303848E-01   # U_11
  1  2    -3.73071064E-02   # U_12
  2  1     3.73071064E-02   # U_21
  2  2     9.99303848E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984525E-01   # V_11
  1  2    -5.56320729E-03   # V_12
  2  1     5.56320729E-03   # V_21
  2  2     9.99984525E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98881321E-01   # cos(theta_t)
  1  2    -4.72874884E-02   # sin(theta_t)
  2  1     4.72874884E-02   # -sin(theta_t)
  2  2     9.98881321E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99920054E-01   # cos(theta_b)
  1  2     1.26445881E-02   # sin(theta_b)
  2  1    -1.26445881E-02   # -sin(theta_b)
  2  2     9.99920054E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06914147E-01   # cos(theta_tau)
  1  2     7.07299363E-01   # sin(theta_tau)
  2  1    -7.07299363E-01   # -sin(theta_tau)
  2  2     7.06914147E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01903401E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28538866E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44588398E+02   # higgs               
         4     6.86551534E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.28538866E+03  # The gauge couplings
     1     3.61656789E-01   # gprime(Q) DRbar
     2     6.38738117E-01   # g(Q) DRbar
     3     1.02981373E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28538866E+03  # The trilinear couplings
  1  1     1.53288336E-06   # A_u(Q) DRbar
  2  2     1.53290659E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28538866E+03  # The trilinear couplings
  1  1     3.86576603E-07   # A_d(Q) DRbar
  2  2     3.86652769E-07   # A_s(Q) DRbar
  3  3     8.87184036E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28538866E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     8.38398991E-08   # A_mu(Q) DRbar
  3  3     8.48402445E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28538866E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55398553E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28538866E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12213461E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28538866E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05575422E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28538866E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.72265791E+04   # M^2_Hd              
        22    -9.11210048E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41624584E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.05821425E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48254594E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48254594E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51745406E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51745406E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.98287809E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.71288676E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.64303296E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.18567837E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.04951113E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.23266635E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.95383594E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.93106186E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.22064279E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.61903725E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51745228E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.01071614E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.67190581E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.32147186E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.94411807E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.82373474E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24510235E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.89367452E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.28831093E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.69411416E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.50289092E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.32060952E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28334091E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.92847329E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.23769276E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.10562103E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02777184E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.82062146E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62780120E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.03009441E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.84073107E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25584802E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.90950622E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.05814426E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19682732E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57740306E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.72970832E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.57394777E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.07408107E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42259313E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03287869E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.96224431E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62524555E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.73234879E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.96913667E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25011476E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.40856879E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.06501537E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68537985E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48793415E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06113277E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.60044687E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.21553944E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55120550E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02777184E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.82062146E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62780120E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.03009441E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.84073107E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25584802E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.90950622E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.05814426E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19682732E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57740306E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.72970832E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.57394777E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.07408107E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42259313E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03287869E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.96224431E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62524555E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.73234879E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.96913667E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25011476E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.40856879E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.06501537E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68537985E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48793415E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06113277E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.60044687E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.21553944E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55120550E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96354361E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.77750970E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01234606E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.71508135E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.67235067E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00990289E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.25958596E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55368294E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997639E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.36074204E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.07854365E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.96354361E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.77750970E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01234606E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.71508135E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.67235067E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00990289E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.25958596E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55368294E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997639E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.36074204E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.07854365E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.78733677E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48096410E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17665746E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34237844E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72797732E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56338723E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14929049E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.75728564E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.00453890E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28703477E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.94792955E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96385499E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.85212334E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00009161E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.07347882E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.40489976E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01469604E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.49201632E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96385499E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.85212334E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00009161E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.07347882E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.40489976E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01469604E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.49201632E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96426842E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.85128332E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99983783E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.10254202E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44879312E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01503166E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.15164172E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.59544944E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.83704898E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.67293299E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.28386862E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.60044734E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.77325554E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.09759419E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.61403887E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.00342622E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.87856563E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.91998911E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50800109E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.86441897E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.29497553E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.35224707E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.58185319E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.58185319E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.91807139E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.42419370E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.66922480E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.66922480E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.24422786E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.24422786E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.92749816E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.92749816E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.77574811E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.77286832E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.41352516E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.65050485E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.65050485E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.34703257E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.59359661E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.64638300E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.64638300E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.26986521E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.26986521E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.46639972E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.46639972E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     5.46639972E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.46639972E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     7.93627656E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.93627656E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.84671708E-03   # h decays
#          BR         NDA      ID1       ID2
     6.82348499E-01    2           5        -5   # BR(h -> b       bb     )
     5.37090824E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.90133177E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.03767396E-04    2           3        -3   # BR(h -> s       sb     )
     1.74233603E-02    2           4        -4   # BR(h -> c       cb     )
     5.66455886E-02    2          21        21   # BR(h -> g       g      )
     1.91424823E-03    2          22        22   # BR(h -> gam     gam    )
     1.24504079E-03    2          22        23   # BR(h -> Z       gam    )
     1.65552129E-01    2          24       -24   # BR(h -> W+      W-     )
     2.05681508E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39306529E+01   # H decays
#          BR         NDA      ID1       ID2
     7.42273596E-01    2           5        -5   # BR(H -> b       bb     )
     1.78513895E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31182472E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75090015E-04    2           3        -3   # BR(H -> s       sb     )
     2.18787469E-07    2           4        -4   # BR(H -> c       cb     )
     2.18254694E-02    2           6        -6   # BR(H -> t       tb     )
     3.30131358E-05    2          21        21   # BR(H -> g       g      )
     3.82866869E-08    2          22        22   # BR(H -> gam     gam    )
     8.37203954E-09    2          23        22   # BR(H -> Z       gam    )
     3.40771077E-05    2          24       -24   # BR(H -> W+      W-     )
     1.70175432E-05    2          23        23   # BR(H -> Z       Z      )
     8.73627842E-05    2          25        25   # BR(H -> h       h      )
     1.41824818E-23    2          36        36   # BR(H -> A       A      )
     2.28020620E-17    2          23        36   # BR(H -> Z       A      )
     2.33030328E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12334363E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.16124206E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.25345939E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.14735946E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.21096127E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32173640E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82392399E-01    2           5        -5   # BR(A -> b       bb     )
     1.88139910E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.65216832E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.17024416E-04    2           3        -3   # BR(A -> s       sb     )
     2.30552709E-07    2           4        -4   # BR(A -> c       cb     )
     2.29864545E-02    2           6        -6   # BR(A -> t       tb     )
     6.76929187E-05    2          21        21   # BR(A -> g       g      )
     3.14734352E-08    2          22        22   # BR(A -> gam     gam    )
     6.66586041E-08    2          23        22   # BR(A -> Z       gam    )
     3.57779070E-05    2          23        25   # BR(A -> Z       h      )
     2.65116168E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22018957E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32108741E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.00928324E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.93052825E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08771577E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.50490667E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85673902E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.96136991E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.20490990E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07081734E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.08872798E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.76950610E-05    2          24        25   # BR(H+ -> W+      h      )
     1.04840282E-13    2          24        36   # BR(H+ -> W+      A      )
     2.07759821E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.97335019E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.54080186E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
