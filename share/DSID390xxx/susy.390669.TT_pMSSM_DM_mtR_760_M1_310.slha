#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13079101E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.30900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.26485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04189039E+01   # W+
        25     1.25453865E+02   # h
        35     4.00000928E+03   # H
        36     3.99999711E+03   # A
        37     4.00102456E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02611607E+03   # ~d_L
   2000001     4.02276998E+03   # ~d_R
   1000002     4.02546732E+03   # ~u_L
   2000002     4.02330306E+03   # ~u_R
   1000003     4.02611607E+03   # ~s_L
   2000003     4.02276998E+03   # ~s_R
   1000004     4.02546732E+03   # ~c_L
   2000004     4.02330306E+03   # ~c_R
   1000005     2.29213557E+03   # ~b_1
   2000005     4.02583680E+03   # ~b_2
   1000006     8.04714606E+02   # ~t_1
   2000006     2.30266277E+03   # ~t_2
   1000011     4.00447466E+03   # ~e_L
   2000011     4.00367168E+03   # ~e_R
   1000012     4.00336471E+03   # ~nu_eL
   1000013     4.00447466E+03   # ~mu_L
   2000013     4.00367168E+03   # ~mu_R
   1000014     4.00336471E+03   # ~nu_muL
   1000015     4.00470497E+03   # ~tau_1
   2000015     4.00769477E+03   # ~tau_2
   1000016     4.00478462E+03   # ~nu_tauL
   1000021     1.99472935E+03   # ~g
   1000022     2.93727373E+02   # ~chi_10
   1000023    -3.42191653E+02   # ~chi_20
   1000025     3.59324769E+02   # ~chi_30
   1000035     2.06672653E+03   # ~chi_40
   1000024     3.39527558E+02   # ~chi_1+
   1000037     2.06689360E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.01022390E-01   # N_11
  1  2    -1.66601845E-02   # N_12
  1  3    -4.52944423E-01   # N_13
  1  4    -3.91058716E-01   # N_14
  2  1    -5.09055052E-02   # N_21
  2  2     2.46567235E-02   # N_22
  2  3    -7.03003925E-01   # N_23
  2  4     7.08933112E-01   # N_24
  3  1     5.96465191E-01   # N_31
  3  2     2.61839622E-02   # N_32
  3  3     5.48275413E-01   # N_33
  3  4     5.85608870E-01   # N_34
  4  1    -1.01827847E-03   # N_41
  4  2     9.99214133E-01   # N_42
  4  3    -4.57197986E-03   # N_43
  4  4    -3.93595700E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.47124152E-03   # U_11
  1  2     9.99979061E-01   # U_12
  2  1    -9.99979061E-01   # U_21
  2  2     6.47124152E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.56738229E-02   # V_11
  1  2    -9.98449010E-01   # V_12
  2  1    -9.98449010E-01   # V_21
  2  2    -5.56738229E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.57221944E-02   # cos(theta_t)
  1  2     9.96319078E-01   # sin(theta_t)
  2  1    -9.96319078E-01   # -sin(theta_t)
  2  2    -8.57221944E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998889E-01   # cos(theta_b)
  1  2    -1.49063703E-03   # sin(theta_b)
  2  1     1.49063703E-03   # -sin(theta_b)
  2  2     9.99998889E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05897891E-01   # cos(theta_tau)
  1  2     7.08313608E-01   # sin(theta_tau)
  2  1    -7.08313608E-01   # -sin(theta_tau)
  2  2    -7.05897891E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00267911E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30791011E+03  # DRbar Higgs Parameters
         1    -3.30900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43669516E+02   # higgs               
         4     1.61537928E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30791011E+03  # The gauge couplings
     1     3.62062747E-01   # gprime(Q) DRbar
     2     6.35598746E-01   # g(Q) DRbar
     3     1.02926116E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30791011E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37092103E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30791011E+03  # The trilinear couplings
  1  1     5.04610852E-07   # A_d(Q) DRbar
  2  2     5.04657847E-07   # A_s(Q) DRbar
  3  3     9.02777733E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30791011E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10840962E-07   # A_mu(Q) DRbar
  3  3     1.11978383E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30791011E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66238290E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30791011E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84527919E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30791011E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03422542E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30791011E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57804225E+07   # M^2_Hd              
        22    -1.11573982E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.26485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40148654E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.98920167E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.62999504E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.70424676E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.11010810E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.03807666E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.88139056E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.64897938E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.28946410E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.09630507E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.35554659E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.40790361E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.78705202E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.97816626E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.35120306E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.40360810E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.46182486E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.47765862E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.21386708E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.12234273E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.35822126E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     4.32986764E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.18319212E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     6.21011527E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.38767035E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.85429459E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64671087E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.61707045E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81230193E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.57904278E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.54104921E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64946974E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.89101569E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13045004E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.05699886E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.05157634E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.26327901E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.83310808E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76442848E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.60372216E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.95753333E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.69128852E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80833668E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.40115742E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60456503E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52016281E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59023769E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.58851970E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.44399914E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.97962892E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19324608E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44174082E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76462183E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49002538E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.02623901E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.59522819E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81341645E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.29769310E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63674071E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52238959E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52319396E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.36488320E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.76837007E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.16618835E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.33180866E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85431236E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76442848E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.60372216E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.95753333E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.69128852E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80833668E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.40115742E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60456503E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52016281E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59023769E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.58851970E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.44399914E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.97962892E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19324608E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44174082E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76462183E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49002538E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.02623901E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.59522819E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81341645E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.29769310E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63674071E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52238959E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52319396E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.36488320E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.76837007E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.16618835E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.33180866E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85431236E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11559519E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87571605E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.59194465E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.80618002E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77233180E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.26392197E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55895628E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06188313E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.42871051E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.58632670E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.54542057E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.65054764E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11559519E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87571605E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.59194465E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.80618002E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77233180E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.26392197E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55895628E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06188313E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.42871051E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.58632670E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.54542057E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.65054764E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08062102E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.37521862E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.42022881E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.45030998E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39150447E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50752517E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79019154E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07649586E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.36698249E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.19121811E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.24945422E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41719927E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05558479E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84168374E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11669094E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14221733E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.46433868E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.99444481E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77591162E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15398428E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53624334E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11669094E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14221733E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.46433868E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.99444481E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77591162E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15398428E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53624334E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44410198E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03400781E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.32561622E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.52130664E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51420742E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.72088688E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01430925E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.69100171E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33604171E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33604171E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11202791E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11202791E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10386075E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.65019505E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.16467963E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46922209E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.70123974E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40865352E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.72779692E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40773554E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.05361526E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.86794045E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18202395E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53358027E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18202395E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53358027E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40237833E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51846249E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51846249E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48389539E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03477411E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03477411E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03477396E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.75205336E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.75205336E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.75205336E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.75205336E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.91735302E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.91735302E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.91735302E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.91735302E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.74967789E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.74967789E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     7.96108508E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.92832349E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.62994967E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.92424210E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.77083027E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.92424210E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.77083027E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.61280657E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     8.46976175E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     8.46976175E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.45411049E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.73862914E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.73862914E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.73862388E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.31923044E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.71165396E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.31923044E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.71165396E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.95616121E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.92736796E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.92736796E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.69423581E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     7.85114875E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     7.85114875E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     7.85114884E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.25938507E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.25938507E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.25938507E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.25938507E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.08642331E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.08642331E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.08642331E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.08642331E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.96633487E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.96633487E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.64868407E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.86526728E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.52345453E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.60967581E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40695671E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40695671E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.90263930E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.27726475E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.08468965E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.56228840E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.56228840E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.03100002E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95850520E-01    2           5        -5   # BR(h -> b       bb     )
     6.45922663E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28656540E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84980743E-04    2           3        -3   # BR(h -> s       sb     )
     2.10633131E-02    2           4        -4   # BR(h -> c       cb     )
     6.87703398E-02    2          21        21   # BR(h -> g       g      )
     2.37081783E-03    2          22        22   # BR(h -> gam     gam    )
     1.61513169E-03    2          22        23   # BR(h -> Z       gam    )
     2.17549584E-01    2          24       -24   # BR(h -> W+      W-     )
     2.74743897E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43243177E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52671876E-01    2           5        -5   # BR(H -> b       bb     )
     6.10334787E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15799598E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55253698E-04    2           3        -3   # BR(H -> s       sb     )
     7.15938262E-08    2           4        -4   # BR(H -> c       cb     )
     7.17519203E-03    2           6        -6   # BR(H -> t       tb     )
     8.95118905E-07    2          21        21   # BR(H -> g       g      )
     5.15106374E-10    2          22        22   # BR(H -> gam     gam    )
     1.84563529E-09    2          23        22   # BR(H -> Z       gam    )
     1.80417188E-06    2          24       -24   # BR(H -> W+      W-     )
     9.01461058E-07    2          23        23   # BR(H -> Z       Z      )
     6.87686390E-06    2          25        25   # BR(H -> h       h      )
    -2.94489547E-24    2          36        36   # BR(H -> A       A      )
     2.70305329E-20    2          23        36   # BR(H -> Z       A      )
     1.87116007E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60952537E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60952537E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.66616982E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.14818129E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.69594274E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.28374805E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.21411678E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.24134699E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.55795993E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.15033822E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.75637111E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.08581111E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.05266444E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.05266444E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43160236E+01   # A decays
#          BR         NDA      ID1       ID2
     3.52751465E-01    2           5        -5   # BR(A -> b       bb     )
     6.10430788E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15833372E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55335303E-04    2           3        -3   # BR(A -> s       sb     )
     7.20673699E-08    2           4        -4   # BR(A -> c       cb     )
     7.19001340E-03    2           6        -6   # BR(A -> t       tb     )
     1.47736568E-05    2          21        21   # BR(A -> g       g      )
     5.90546618E-08    2          22        22   # BR(A -> gam     gam    )
     1.62450465E-08    2          23        22   # BR(A -> Z       gam    )
     1.80066113E-06    2          23        25   # BR(A -> Z       h      )
     1.88274450E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60973580E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60973580E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.29265212E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     9.01354694E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41516347E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.80961274E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.32820568E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.38576320E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.75723787E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.80467737E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.95051499E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.15593527E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.15593527E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42846848E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.63696965E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10940080E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16013445E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.60765748E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22375670E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51766106E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.59133752E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.80372022E-06    2          24        25   # BR(H+ -> W+      h      )
     2.77557799E-14    2          24        36   # BR(H+ -> W+      A      )
     5.26390210E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.04104734E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.49955081E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61278774E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.64193790E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59719316E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.73050698E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.35900548E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
