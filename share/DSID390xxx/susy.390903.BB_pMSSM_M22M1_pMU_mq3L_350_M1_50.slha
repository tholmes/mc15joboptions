#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10164563E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04027284E+01   # W+
        25     1.27078399E+02   # h
        35     3.00027488E+03   # H
        36     3.00000026E+03   # A
        37     3.00111420E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01327479E+03   # ~d_L
   2000001     3.00781303E+03   # ~d_R
   1000002     3.01235278E+03   # ~u_L
   2000002     3.01030992E+03   # ~u_R
   1000003     3.01327479E+03   # ~s_L
   2000003     3.00781303E+03   # ~s_R
   1000004     3.01235278E+03   # ~c_L
   2000004     3.01030992E+03   # ~c_R
   1000005     3.71318380E+02   # ~b_1
   2000005     3.00745767E+03   # ~b_2
   1000006     3.58785073E+02   # ~t_1
   2000006     2.99228027E+03   # ~t_2
   1000011     3.00706718E+03   # ~e_L
   2000011     3.00064503E+03   # ~e_R
   1000012     3.00567093E+03   # ~nu_eL
   1000013     3.00706718E+03   # ~mu_L
   2000013     3.00064503E+03   # ~mu_R
   1000014     3.00567093E+03   # ~nu_muL
   1000015     2.98616882E+03   # ~tau_1
   2000015     3.02233102E+03   # ~tau_2
   1000016     3.00595539E+03   # ~nu_tauL
   1000021     2.31528735E+03   # ~g
   1000022     5.03075937E+01   # ~chi_10
   1000023     1.08159119E+02   # ~chi_20
   1000025    -3.00593732E+03   # ~chi_30
   1000035     3.00631265E+03   # ~chi_40
   1000024     1.08309275E+02   # ~chi_1+
   1000037     3.00700595E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886289E-01   # N_11
  1  2    -2.70308289E-03   # N_12
  1  3     1.48031335E-02   # N_13
  1  4    -9.85188093E-04   # N_14
  2  1     3.09201218E-03   # N_21
  2  2     9.99650391E-01   # N_22
  2  3    -2.61688536E-02   # N_23
  2  4     2.17387196E-03   # N_24
  3  1    -9.72162069E-03   # N_31
  3  2     1.69953921E-02   # N_32
  3  3     7.06817019E-01   # N_33
  3  4     7.07125412E-01   # N_34
  4  1    -1.11058348E-02   # N_41
  4  2     2.00734937E-02   # N_42
  4  3     7.06757215E-01   # N_43
  4  4    -7.07084122E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99313027E-01   # U_11
  1  2    -3.70604129E-02   # U_12
  2  1     3.70604129E-02   # U_21
  2  2     9.99313027E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995257E-01   # V_11
  1  2    -3.07989822E-03   # V_12
  2  1     3.07989822E-03   # V_21
  2  2     9.99995257E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98658947E-01   # cos(theta_t)
  1  2    -5.17716870E-02   # sin(theta_t)
  2  1     5.17716870E-02   # -sin(theta_t)
  2  2     9.98658947E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99925728E-01   # cos(theta_b)
  1  2     1.21876365E-02   # sin(theta_b)
  2  1    -1.21876365E-02   # -sin(theta_b)
  2  2     9.99925728E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06852181E-01   # cos(theta_tau)
  1  2     7.07361290E-01   # sin(theta_tau)
  2  1    -7.07361290E-01   # -sin(theta_tau)
  2  2     7.06852181E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01702143E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.01645628E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.45034641E+02   # higgs               
         4     5.60525727E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.01645628E+03  # The gauge couplings
     1     3.60834084E-01   # gprime(Q) DRbar
     2     6.41149690E-01   # g(Q) DRbar
     3     1.03505426E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.01645628E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.02107275E-07   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.01645628E+03  # The trilinear couplings
  1  1     2.15272570E-07   # A_d(Q) DRbar
  2  2     2.15318473E-07   # A_s(Q) DRbar
  3  3     5.17083530E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.01645628E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.71839427E-08   # A_mu(Q) DRbar
  3  3     4.77356133E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.01645628E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.62194548E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.01645628E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.08394050E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.01645628E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04478182E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.01645628E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.39512277E+04   # M^2_Hd              
        22    -9.23004190E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42679163E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.69021288E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47834797E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47834797E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52165203E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52165203E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.29675922E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     8.95634587E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.03077109E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.87966545E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.23553602E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.25867140E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.68291547E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.37370907E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.75852871E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.79032851E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64942057E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.26779844E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.92052093E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.95326916E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.66456175E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.14011134E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.26820056E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.85014318E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.43202220E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -3.76112207E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30738047E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.84930175E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.17958952E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.04712025E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12963756E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.60911801E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63941549E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.59522055E-10    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.07428017E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27726994E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.96263430E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.02722337E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.22602134E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55759495E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.48722274E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.58626707E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.73708449E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44239017E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.13466822E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.93964890E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63497899E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.71731571E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.14851566E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27152757E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.79482595E-09    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.03409682E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.71210489E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42929370E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.22916601E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.63323761E-12    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.27878392E-12    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55706640E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12963756E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.60911801E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63941549E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.59522055E-10    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.07428017E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27726994E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.96263430E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.02722337E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.22602134E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55759495E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.48722274E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.58626707E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.73708449E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44239017E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.13466822E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.93964890E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63497899E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.71731571E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.14851566E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27152757E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.79482595E-09    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.03409682E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.71210489E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42929370E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.22916601E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.63323761E-12    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.27878392E-12    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55706640E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06469374E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.48213858E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02563730E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.26783325E-11    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.45535947E-11    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02614884E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.37809207E-12    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55328468E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990457E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.54315425E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06469374E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.48213858E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02563730E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.26783325E-11    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.45535947E-11    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02614884E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.37809207E-12    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55328468E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990457E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.54315425E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.83827699E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.41526873E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.20098070E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38375057E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.77800951E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.49493285E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17478084E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.02845341E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.04637680E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.33011798E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     5.75815627E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06508540E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.66066850E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00295100E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.03098215E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.06508540E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.66066850E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00295100E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.03098215E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06581031E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.65986122E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00270122E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.20566479E-14    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.03131266E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.68157437E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35862334E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.38440102E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35862334E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.38440102E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09607323E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.46147454E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09607323E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.46147454E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09060666E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.20805916E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.52447287E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     9.58406027E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.37370833E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.98233324E-13    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     9.98233324E-13    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.01112739E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.57561435E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.01176541E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.45014782E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.69123110E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.25743713E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.26366486E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.75835385E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.49415358E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.75835385E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.49415358E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.84198716E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.93001773E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.93001773E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.08270981E-04    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.60928992E-05    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.60928992E-05    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.61438058E-05    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     9.25333917E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.18240495E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.79851364E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.39730467E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.39730467E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.20298743E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.75244837E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.71227671E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.71227671E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.15299752E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.15299752E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     9.84297753E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     9.84297753E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     9.84297753E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     9.84297753E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     9.78416310E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     9.78416310E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.05303889E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.05303889E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.05303889E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.05303889E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000035     9.14450200E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.16720352E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.75617435E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.47461194E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.47461194E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.21696126E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.93148568E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.69345048E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.69345048E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.18009098E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.18009098E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.49037573E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.49037573E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.49037573E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.49037573E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.21420297E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.21420297E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     8.48690778E-12    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     8.48690778E-12    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     8.48690778E-12    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     8.48690778E-12    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.63068118E-12    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.63068118E-12    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.27998142E-03   # h decays
#          BR         NDA      ID1       ID2
     6.51171600E-01    2           5        -5   # BR(h -> b       bb     )
     5.02396316E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.77842808E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.76234516E-04    2           3        -3   # BR(h -> s       sb     )
     1.62480003E-02    2           4        -4   # BR(h -> c       cb     )
     5.46499548E-02    2          21        21   # BR(h -> g       g      )
     1.91548160E-03    2          22        22   # BR(h -> gam     gam    )
     1.41900231E-03    2          22        23   # BR(h -> Z       gam    )
     1.98189126E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55730250E-02    2          23        23   # BR(h -> Z       Z      )
     4.01017639E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.39742792E+01   # H decays
#          BR         NDA      ID1       ID2
     7.27357079E-01    2           5        -5   # BR(H -> b       bb     )
     1.77961636E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29229817E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72689488E-04    2           3        -3   # BR(H -> s       sb     )
     2.17934683E-07    2           4        -4   # BR(H -> c       cb     )
     2.17404110E-02    2           6        -6   # BR(H -> t       tb     )
     4.01114292E-05    2          21        21   # BR(H -> g       g      )
     5.23055537E-08    2          22        22   # BR(H -> gam     gam    )
     8.39544730E-09    2          23        22   # BR(H -> Z       gam    )
     2.83334458E-05    2          24       -24   # BR(H -> W+      W-     )
     1.41492602E-05    2          23        23   # BR(H -> Z       Z      )
     7.81569942E-05    2          25        25   # BR(H -> h       h      )
    -2.61372113E-23    2          36        36   # BR(H -> A       A      )
     1.21216963E-16    2          23        36   # BR(H -> Z       A      )
     2.45589947E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.14160246E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.21963461E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.47021575E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.68349259E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.28376097E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.30410233E+01   # A decays
#          BR         NDA      ID1       ID2
     7.79453348E-01    2           5        -5   # BR(A -> b       bb     )
     1.90683980E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.74212043E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.28072377E-04    2           3        -3   # BR(A -> s       sb     )
     2.33670288E-07    2           4        -4   # BR(A -> c       cb     )
     2.32972818E-02    2           6        -6   # BR(A -> t       tb     )
     6.86082564E-05    2          21        21   # BR(A -> g       g      )
     4.35110140E-08    2          22        22   # BR(A -> gam     gam    )
     6.76041260E-08    2          23        22   # BR(A -> Z       gam    )
     3.02355961E-05    2          23        25   # BR(A -> Z       h      )
     2.68965315E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.24103429E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33570674E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.14453610E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.66148318E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.05561679E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.57479885E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     9.10386072E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.75593662E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.35010082E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.10068778E-03    2           4        -3   # BR(H+ -> c       sb     )
     6.89678336E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.08829370E-05    2          24        25   # BR(H+ -> W+      h      )
     2.33579088E-13    2          24        36   # BR(H+ -> W+      A      )
     2.16513388E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.07114222E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.75087935E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
