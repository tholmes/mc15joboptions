#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90279422E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.25485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     3.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04185839E+01   # W+
        25     1.24069140E+02   # h
        35     3.99999947E+03   # H
        36     3.99999497E+03   # A
        37     4.00101321E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01281080E+03   # ~d_L
   2000001     4.01232655E+03   # ~d_R
   1000002     4.01215483E+03   # ~u_L
   2000002     4.01266958E+03   # ~u_R
   1000003     4.01281080E+03   # ~s_L
   2000003     4.01232655E+03   # ~s_R
   1000004     4.01215483E+03   # ~c_L
   2000004     4.01266958E+03   # ~c_R
   1000005     2.27417775E+03   # ~b_1
   2000005     4.01887057E+03   # ~b_2
   1000006     4.12553245E+02   # ~t_1
   2000006     2.28260286E+03   # ~t_2
   1000011     4.00153668E+03   # ~e_L
   2000011     4.00389157E+03   # ~e_R
   1000012     4.00042103E+03   # ~nu_eL
   1000013     4.00153668E+03   # ~mu_L
   2000013     4.00389157E+03   # ~mu_R
   1000014     4.00042103E+03   # ~nu_muL
   1000015     4.00406271E+03   # ~tau_1
   2000015     4.00916908E+03   # ~tau_2
   1000016     4.00302722E+03   # ~nu_tauL
   1000021     1.97212314E+03   # ~g
   1000022     9.10796842E+01   # ~chi_10
   1000023    -1.35251428E+02   # ~chi_20
   1000025     1.53848056E+02   # ~chi_30
   1000035     2.07049840E+03   # ~chi_40
   1000024     1.29665570E+02   # ~chi_1+
   1000037     2.07066508E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50819283E-01   # N_11
  1  2     1.41677171E-02   # N_12
  1  3     5.43133099E-01   # N_13
  1  4     3.75601008E-01   # N_14
  2  1    -1.37123546E-01   # N_21
  2  2     2.73170165E-02   # N_22
  2  3    -6.84834764E-01   # N_23
  2  4     7.15158905E-01   # N_24
  3  1     6.46116647E-01   # N_31
  3  2     2.36581285E-02   # N_32
  3  3     4.85806084E-01   # N_33
  3  4     5.88188762E-01   # N_34
  4  1    -9.03390144E-04   # N_41
  4  2     9.99246391E-01   # N_42
  4  3    -4.80938747E-04   # N_43
  4  4    -3.88021034E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.84461604E-04   # U_11
  1  2     9.99999766E-01   # U_12
  2  1    -9.99999766E-01   # U_21
  2  2     6.84461604E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48879000E-02   # V_11
  1  2    -9.98492523E-01   # V_12
  2  1    -9.98492523E-01   # V_21
  2  2    -5.48879000E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -6.54675061E-02   # cos(theta_t)
  1  2     9.97854702E-01   # sin(theta_t)
  2  1    -9.97854702E-01   # -sin(theta_t)
  2  2    -6.54675061E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999844E-01   # cos(theta_b)
  1  2    -5.58569580E-04   # sin(theta_b)
  2  1     5.58569580E-04   # -sin(theta_b)
  2  2     9.99999844E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03093624E-01   # cos(theta_tau)
  1  2     7.11097290E-01   # sin(theta_tau)
  2  1    -7.11097290E-01   # -sin(theta_tau)
  2  2    -7.03093624E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00296671E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.02794218E+02  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44588372E+02   # higgs               
         4     1.61836888E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.02794218E+02  # The gauge couplings
     1     3.61062125E-01   # gprime(Q) DRbar
     2     6.35981561E-01   # g(Q) DRbar
     3     1.03746291E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.02794218E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.70230981E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.02794218E+02  # The trilinear couplings
  1  1     2.09668413E-07   # A_d(Q) DRbar
  2  2     2.09688848E-07   # A_s(Q) DRbar
  3  3     3.74067392E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.02794218E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.58520434E-08   # A_mu(Q) DRbar
  3  3     4.63159682E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.02794218E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72431863E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.02794218E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82702968E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.02794218E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03754379E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.02794218E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58473045E+07   # M^2_Hd              
        22    -8.38272459E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.25485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     3.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40318924E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.73464461E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.84506044E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.29922997E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.78841252E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.77471345E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.70695104E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.99198805E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.29793013E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.39975195E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.56427472E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.67558025E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.99629078E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.17632094E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.48132240E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.04325181E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.17345802E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.76959485E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.69128901E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.21675328E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.21748413E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.99292269E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.75202678E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.77664575E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.57748347E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.07024142E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63740126E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.81819874E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69765836E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40859205E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.19083858E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55535472E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.22003945E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15154102E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.12678105E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.32750284E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.08614001E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.28934651E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75195029E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.44985593E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65537575E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88379699E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79538706E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33401090E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57847457E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52394164E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58268410E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.16859384E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05575191E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34262545E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.48767751E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43832030E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75213420E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16345152E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.51151539E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.60295067E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80008755E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.29518229E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61025290E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52621614E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51571966E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.27073297E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.75574626E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.11477052E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.49259813E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85338915E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75195029E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.44985593E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65537575E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88379699E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79538706E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33401090E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57847457E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52394164E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58268410E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.16859384E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05575191E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34262545E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.48767751E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43832030E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75213420E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16345152E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.51151539E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.60295067E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80008755E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.29518229E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61025290E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52621614E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51571966E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.27073297E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.75574626E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.11477052E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.49259813E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85338915E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10946255E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.78346548E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31904441E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.87100954E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76931921E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.84112544E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55203800E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07299612E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64194807E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87949076E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.17009847E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.38808459E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10946255E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.78346548E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31904441E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.87100954E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76931921E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.84112544E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55203800E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07299612E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64194807E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87949076E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.17009847E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.38808459E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08521912E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23603929E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.02016828E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66825376E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38128064E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43173080E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.76923640E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09813573E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.06660180E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.38599770E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42500047E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41266018E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.24972109E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83216567E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11058181E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00264872E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.70959593E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.07513234E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77210756E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11116809E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52952285E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11058181E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00264872E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.70959593E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.07513234E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77210756E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11116809E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52952285E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44500237E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05908501E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.15871593E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.48898903E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50702471E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.85822382E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00075834E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.41212646E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33721383E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33721383E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241236E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241236E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10074763E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.70022490E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.93296390E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.47969768E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.25077780E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.45239363E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.75488216E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39465236E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.70435644E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.54634178E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18605342E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53795655E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18605342E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53795655E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37699569E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52351238E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52351238E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48052378E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04419713E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04419713E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04419664E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.21992785E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.21992785E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.21992785E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.21992785E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.40664614E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.40664614E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.40664614E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.40664614E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.67236179E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.67236179E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.63563832E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.65176788E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.12610156E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.33518586E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.31241134E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.33518586E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.31241134E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.07577101E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.78314493E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.78314493E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.70138987E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.97996110E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.97996110E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.97995584E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.68677739E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.96836782E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.68677739E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.96836782E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.28426821E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.28443440E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.28443440E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.16753941E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.56522466E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.56522466E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.56522488E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.80693289E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.80693289E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.80693289E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.80693289E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.26892104E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.26892104E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.26892104E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.26892104E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.18251519E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.18251519E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69942594E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.40485410E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.29958518E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.39297588E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.42015770E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.42015770E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.89790208E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.17185629E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.28020466E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.45324723E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.45324723E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     3.88094700E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18643429E-01    2           5        -5   # BR(h -> b       bb     )
     6.63549742E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.34902853E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.99306994E-04    2           3        -3   # BR(h -> s       sb     )
     2.16831673E-02    2           4        -4   # BR(h -> c       cb     )
     7.05480678E-02    2          21        21   # BR(h -> g       g      )
     2.33449590E-03    2          22        22   # BR(h -> gam     gam    )
     1.48095098E-03    2          22        23   # BR(h -> Z       gam    )
     1.94242545E-01    2          24       -24   # BR(h -> W+      W-     )
     2.39781599E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.39910193E+01   # H decays
#          BR         NDA      ID1       ID2
     3.39067071E-01    2           5        -5   # BR(H -> b       bb     )
     6.14100834E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.17131181E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56828811E-04    2           3        -3   # BR(H -> s       sb     )
     7.20439123E-08    2           4        -4   # BR(H -> c       cb     )
     7.22029983E-03    2           6        -6   # BR(H -> t       tb     )
     1.00901571E-06    2          21        21   # BR(H -> g       g      )
     2.31042089E-10    2          22        22   # BR(H -> gam     gam    )
     1.85478937E-09    2          23        22   # BR(H -> Z       gam    )
     1.97117088E-06    2          24       -24   # BR(H -> W+      W-     )
     9.84902695E-07    2          23        23   # BR(H -> Z       Z      )
     6.96249306E-06    2          25        25   # BR(H -> h       h      )
     2.42456724E-24    2          36        36   # BR(H -> A       A      )
     8.08502146E-20    2          23        36   # BR(H -> Z       A      )
     1.88045571E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66572428E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66572428E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.40968397E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.65695091E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70865388E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.42244959E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.46032971E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.10625118E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.12540251E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.39020865E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.20694764E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.33407893E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.49869490E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.49869490E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.39899645E+01   # A decays
#          BR         NDA      ID1       ID2
     3.39099791E-01    2           5        -5   # BR(A -> b       bb     )
     6.14117005E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.17136728E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56877218E-04    2           3        -3   # BR(A -> s       sb     )
     7.25025690E-08    2           4        -4   # BR(A -> c       cb     )
     7.23343233E-03    2           6        -6   # BR(A -> t       tb     )
     1.48628818E-05    2          21        21   # BR(A -> g       g      )
     4.07955167E-08    2          22        22   # BR(A -> gam     gam    )
     1.63422444E-08    2          23        22   # BR(A -> Z       gam    )
     1.96721228E-06    2          23        25   # BR(A -> Z       h      )
     1.88374117E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66577663E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66577663E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.97505193E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.29034336E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34858659E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.89920714E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.37294232E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.77024896E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.40097265E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.26779721E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.66261942E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.62795411E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.62795411E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.38082711E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.39076330E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.16347541E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17925390E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.45008552E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23458867E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53994591E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.43819604E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.97605369E-06    2          24        25   # BR(H+ -> W+      h      )
     2.67690279E-14    2          24        36   # BR(H+ -> W+      A      )
     4.73027484E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.52131125E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.26223828E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67356366E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.92039983E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57276317E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.90468453E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.60845162E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
