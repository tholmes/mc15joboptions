#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90072108E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.76900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04207251E+01   # W+
        25     1.24780974E+02   # h
        35     4.00000539E+03   # H
        36     3.99999557E+03   # A
        37     4.00098538E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01269289E+03   # ~d_L
   2000001     4.01218966E+03   # ~d_R
   1000002     4.01203895E+03   # ~u_L
   2000002     4.01271983E+03   # ~u_R
   1000003     4.01269289E+03   # ~s_L
   2000003     4.01218966E+03   # ~s_R
   1000004     4.01203895E+03   # ~c_L
   2000004     4.01271983E+03   # ~c_R
   1000005     2.02689343E+03   # ~b_1
   2000005     4.01871715E+03   # ~b_2
   1000006     4.26696062E+02   # ~t_1
   2000006     2.03781492E+03   # ~t_2
   1000011     4.00160698E+03   # ~e_L
   2000011     4.00364005E+03   # ~e_R
   1000012     4.00049459E+03   # ~nu_eL
   1000013     4.00160698E+03   # ~mu_L
   2000013     4.00364005E+03   # ~mu_R
   1000014     4.00049459E+03   # ~nu_muL
   1000015     4.00387717E+03   # ~tau_1
   2000015     4.00915406E+03   # ~tau_2
   1000016     4.00309706E+03   # ~nu_tauL
   1000021     1.96780464E+03   # ~g
   1000022     2.42336031E+02   # ~chi_10
   1000023    -2.86896182E+02   # ~chi_20
   1000025     3.06665813E+02   # ~chi_30
   1000035     2.06753571E+03   # ~chi_40
   1000024     2.83969039E+02   # ~chi_1+
   1000037     2.06771087E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.79854418E-01   # N_11
  1  2    -1.67701231E-02   # N_12
  1  3    -4.77313133E-01   # N_13
  1  4    -4.04620838E-01   # N_14
  2  1    -6.07464585E-02   # N_21
  2  2     2.53339051E-02   # N_22
  2  3    -7.01604432E-01   # N_23
  2  4     7.09520460E-01   # N_24
  3  1     6.23005602E-01   # N_31
  3  2     2.50457011E-02   # N_32
  3  3     5.29066402E-01   # N_33
  3  4     5.75608787E-01   # N_34
  4  1    -9.87177952E-04   # N_41
  4  2     9.99224534E-01   # N_42
  4  3    -3.48376046E-03   # N_43
  4  4    -3.92073948E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.93203818E-03   # U_11
  1  2     9.99987837E-01   # U_12
  2  1    -9.99987837E-01   # U_21
  2  2     4.93203818E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54594262E-02   # V_11
  1  2    -9.98460942E-01   # V_12
  2  1    -9.98460942E-01   # V_21
  2  2    -5.54594262E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.43101537E-02   # cos(theta_t)
  1  2     9.96439561E-01   # sin(theta_t)
  2  1    -9.96439561E-01   # -sin(theta_t)
  2  2    -8.43101537E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999344E-01   # cos(theta_b)
  1  2    -1.14542550E-03   # sin(theta_b)
  2  1     1.14542550E-03   # -sin(theta_b)
  2  2     9.99999344E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05384398E-01   # cos(theta_tau)
  1  2     7.08824979E-01   # sin(theta_tau)
  2  1    -7.08824979E-01   # -sin(theta_tau)
  2  2    -7.05384398E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00258395E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.00721081E+02  # DRbar Higgs Parameters
         1    -2.76900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44606634E+02   # higgs               
         4     1.62730916E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.00721081E+02  # The gauge couplings
     1     3.60888226E-01   # gprime(Q) DRbar
     2     6.35162936E-01   # g(Q) DRbar
     3     1.03762404E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.00721081E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.68127919E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.00721081E+02  # The trilinear couplings
  1  1     2.09290252E-07   # A_d(Q) DRbar
  2  2     2.09310472E-07   # A_s(Q) DRbar
  3  3     3.73640214E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.00721081E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.68042406E-08   # A_mu(Q) DRbar
  3  3     4.72781567E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.00721081E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73876485E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.00721081E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86788379E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.00721081E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03207929E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.00721081E+02  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57882743E+07   # M^2_Hd              
        22    -1.03759016E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39948590E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70537158E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.02795170E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.65964633E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.83403537E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.82655271E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     8.97247387E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.43659651E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.51082231E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.55471473E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.79816836E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.80169395E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.03627563E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.80640903E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.57258213E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.77138864E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.12383095E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.81868911E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.20737839E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63700803E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65353165E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80237129E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54477280E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.52868883E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64094056E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.09077688E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13283940E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.30576043E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     8.45319896E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.53461580E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.48129524E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74839552E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.50952470E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.67931939E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.79417166E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80198929E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.40288176E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59172851E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52202044E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57879153E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.40435518E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.05989344E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.16362347E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.98123364E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44114194E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74857855E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.38212576E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.19179255E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.57635740E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80699549E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.27322332E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62391232E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52427708E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51216812E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.88464976E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.37588395E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.64659521E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.77894190E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85414988E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74839552E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.50952470E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.67931939E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.79417166E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80198929E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.40288176E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59172851E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52202044E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57879153E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.40435518E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.05989344E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.16362347E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.98123364E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44114194E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74857855E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.38212576E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.19179255E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.57635740E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80699549E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.27322332E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62391232E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52427708E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51216812E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.88464976E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.37588395E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.64659521E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.77894190E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85414988E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10412216E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.33393732E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.31602235E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.34494569E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77246749E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.49151663E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55896346E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05599556E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.09224493E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.68560392E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.87089374E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.28825443E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10412216E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.33393732E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.31602235E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.34494569E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77246749E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.49151663E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55896346E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05599556E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.09224493E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.68560392E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.87089374E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.28825443E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07277874E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27516743E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35780344E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.55990143E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39107753E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48873597E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78919967E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07484746E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.23057054E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.32802646E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.36647119E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41720141E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.11389980E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84156425E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10524029E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.08533773E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.83283493E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.52827718E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77580203E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14834325E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53622074E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10524029E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.08533773E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.83283493E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.52827718E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77580203E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14834325E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53622074E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43526908E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.81716165E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.65785320E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.00049926E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51314031E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.76162070E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01235299E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.54931820E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33663072E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33663072E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11222360E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11222360E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10229137E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.68085984E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.85541161E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.71576849E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44957240E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.16660830E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39498470E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.66075501E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37532822E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.10953067E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.05939388E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18581221E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53787992E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18581221E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53787992E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38456826E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52475162E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52475162E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48539552E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04626023E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04626023E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04625993E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.35500813E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.35500813E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.35500813E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.35500813E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.51669896E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.51669896E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.51669896E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.51669896E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.73925984E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.73925984E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.20988249E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.31403864E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.32634761E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.64810718E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.12417670E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.64810718E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.12417670E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.02664204E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.76524443E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.76524443E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.74921855E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     9.78392668E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     9.78392668E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     9.78387107E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.64635209E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.13524176E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.64635209E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.13524176E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.23168167E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.89442024E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.89442024E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.67402528E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     9.78259774E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     9.78259774E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     9.78259800E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.12455353E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.12455353E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.12455353E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.12455353E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.74845970E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.74845970E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.74845970E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.74845970E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.63667283E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.63667283E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.68224727E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.24925607E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.44642292E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.75042534E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38654935E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38654935E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.95668061E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.76982357E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.94755349E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90583199E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90583199E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.59690330E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.59690330E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.94675114E-03   # h decays
#          BR         NDA      ID1       ID2
     6.06882861E-01    2           5        -5   # BR(h -> b       bb     )
     6.56138828E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32276076E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93174238E-04    2           3        -3   # BR(h -> s       sb     )
     2.14200252E-02    2           4        -4   # BR(h -> c       cb     )
     6.91909774E-02    2          21        21   # BR(h -> g       g      )
     2.36653862E-03    2          22        22   # BR(h -> gam     gam    )
     1.55359507E-03    2          22        23   # BR(h -> Z       gam    )
     2.06446112E-01    2          24       -24   # BR(h -> W+      W-     )
     2.58005570E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.47375055E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52959329E-01    2           5        -5   # BR(H -> b       bb     )
     6.05727124E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14170440E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53326718E-04    2           3        -3   # BR(H -> s       sb     )
     7.10506369E-08    2           4        -4   # BR(H -> c       cb     )
     7.12075307E-03    2           6        -6   # BR(H -> t       tb     )
     7.20363315E-07    2          21        21   # BR(H -> g       g      )
     4.25815809E-10    2          22        22   # BR(H -> gam     gam    )
     1.83319572E-09    2          23        22   # BR(H -> Z       gam    )
     1.74106964E-06    2          24       -24   # BR(H -> W+      W-     )
     8.69931891E-07    2          23        23   # BR(H -> Z       Z      )
     6.52974410E-06    2          25        25   # BR(H -> h       h      )
     2.46244586E-24    2          36        36   # BR(H -> A       A      )
     6.96970157E-20    2          23        36   # BR(H -> Z       A      )
     1.86251313E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61296719E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61296719E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.81711301E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.89128397E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.76236554E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.07358433E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.36406403E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.67233155E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.72387221E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.97176436E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.53221110E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.56565255E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.68582189E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.68582189E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.47350987E+01   # A decays
#          BR         NDA      ID1       ID2
     3.53000654E-01    2           5        -5   # BR(A -> b       bb     )
     6.05756838E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14180777E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53380263E-04    2           3        -3   # BR(A -> s       sb     )
     7.15155674E-08    2           4        -4   # BR(A -> c       cb     )
     7.13496121E-03    2           6        -6   # BR(A -> t       tb     )
     1.46605456E-05    2          21        21   # BR(A -> g       g      )
     5.47179588E-08    2          22        22   # BR(A -> gam     gam    )
     1.61257794E-08    2          23        22   # BR(A -> Z       gam    )
     1.73755102E-06    2          23        25   # BR(A -> Z       h      )
     1.86744173E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61304821E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61304821E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.41282600E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.11171045E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.45142840E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.58981927E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.77734146E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.70600197E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.95962590E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.95119315E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.51810703E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.77422313E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.77422313E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46684546E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.63571489E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.06645371E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14494941E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.60685445E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21515558E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49996580E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.59005636E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.74156981E-06    2          24        25   # BR(H+ -> W+      h      )
     2.28692570E-14    2          24        36   # BR(H+ -> W+      A      )
     4.96408117E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.55814445E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.80976368E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61707142E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.39782339E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59512850E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.07570540E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.58597668E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
