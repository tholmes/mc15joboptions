#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13060167E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04162399E+01   # W+
        25     1.25852262E+02   # h
        35     4.00000373E+03   # H
        36     3.99999662E+03   # A
        37     4.00106161E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02606013E+03   # ~d_L
   2000001     4.02266200E+03   # ~d_R
   1000002     4.02540701E+03   # ~u_L
   2000002     4.02351843E+03   # ~u_R
   1000003     4.02606013E+03   # ~s_L
   2000003     4.02266200E+03   # ~s_R
   1000004     4.02540701E+03   # ~c_L
   2000004     4.02351843E+03   # ~c_R
   1000005     9.22394182E+02   # ~b_1
   2000005     4.02543787E+03   # ~b_2
   1000006     9.04736050E+02   # ~t_1
   2000006     2.01881753E+03   # ~t_2
   1000011     4.00464360E+03   # ~e_L
   2000011     4.00339428E+03   # ~e_R
   1000012     4.00352828E+03   # ~nu_eL
   1000013     4.00464360E+03   # ~mu_L
   2000013     4.00339428E+03   # ~mu_R
   1000014     4.00352828E+03   # ~nu_muL
   1000015     4.00533755E+03   # ~tau_1
   2000015     4.00699615E+03   # ~tau_2
   1000016     4.00496100E+03   # ~nu_tauL
   1000021     1.97938135E+03   # ~g
   1000022     1.45932471E+02   # ~chi_10
   1000023    -1.95999077E+02   # ~chi_20
   1000025     2.09647955E+02   # ~chi_30
   1000035     2.05221419E+03   # ~chi_40
   1000024     1.92207221E+02   # ~chi_1+
   1000037     2.05237930E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15377608E-01   # N_11
  1  2    -1.34578282E-02   # N_12
  1  3    -4.64029569E-01   # N_13
  1  4    -3.45911553E-01   # N_14
  2  1    -9.37661538E-02   # N_21
  2  2     2.63899460E-02   # N_22
  2  3    -6.95910537E-01   # N_23
  2  4     7.11491394E-01   # N_24
  3  1     5.71284875E-01   # N_31
  3  2     2.51582557E-02   # N_32
  3  3     5.48071434E-01   # N_33
  3  4     6.10424735E-01   # N_34
  4  1    -9.25534681E-04   # N_41
  4  2     9.99244474E-01   # N_42
  4  3    -1.66957141E-03   # N_43
  4  4    -3.88179873E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36581659E-03   # U_11
  1  2     9.99997201E-01   # U_12
  2  1    -9.99997201E-01   # U_21
  2  2     2.36581659E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49097297E-02   # V_11
  1  2    -9.98491323E-01   # V_12
  2  1    -9.98491323E-01   # V_21
  2  2    -5.49097297E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92967075E-01   # cos(theta_t)
  1  2    -1.18390827E-01   # sin(theta_t)
  2  1     1.18390827E-01   # -sin(theta_t)
  2  2     9.92967075E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999827E-01   # cos(theta_b)
  1  2    -5.88217621E-04   # sin(theta_b)
  2  1     5.88217621E-04   # -sin(theta_b)
  2  2     9.99999827E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04641056E-01   # cos(theta_tau)
  1  2     7.09563938E-01   # sin(theta_tau)
  2  1    -7.09563938E-01   # -sin(theta_tau)
  2  2    -7.04641056E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00312788E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30601669E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43559284E+02   # higgs               
         4     1.60844846E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30601669E+03  # The gauge couplings
     1     3.62068261E-01   # gprime(Q) DRbar
     2     6.36860854E-01   # g(Q) DRbar
     3     1.03004369E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30601669E+03  # The trilinear couplings
  1  1     1.38110000E-06   # A_u(Q) DRbar
  2  2     1.38111319E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30601669E+03  # The trilinear couplings
  1  1     5.05951881E-07   # A_d(Q) DRbar
  2  2     5.05999319E-07   # A_s(Q) DRbar
  3  3     9.06446829E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30601669E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08901456E-07   # A_mu(Q) DRbar
  3  3     1.10019177E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30601669E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67790804E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30601669E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80891636E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30601669E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04013196E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30601669E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58555781E+07   # M^2_Hd              
        22    -6.47062783E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40717469E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.22890509E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42180416E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42180416E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57819584E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57819584E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.27160862E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.03942280E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.39354463E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.53687947E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03015310E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22560475E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.51689577E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19176737E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.30085682E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.32116891E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.57993026E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.13985300E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.28550520E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.31247761E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.97775725E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.78712838E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.07065938E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.91644550E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66388865E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.67181757E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.75702007E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.52099239E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.31360482E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59350844E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.50199551E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14411174E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.60053927E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.42147033E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.44270062E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.12492445E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78624577E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.73832637E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.47975377E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.55114329E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81639243E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25874215E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62056735E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51808579E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60946092E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.70213159E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.88644929E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.81272518E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63303369E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44362761E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78644520E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.46512097E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.70038881E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.01831482E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82111592E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.76268629E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65229240E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52027949E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54155983E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.66089185E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.27514160E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.73038915E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.86900989E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85481198E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78624577E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.73832637E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.47975377E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.55114329E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81639243E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25874215E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62056735E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51808579E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60946092E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.70213159E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.88644929E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.81272518E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63303369E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44362761E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78644520E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.46512097E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.70038881E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.01831482E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82111592E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.76268629E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65229240E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52027949E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54155983E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.66089185E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.27514160E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.73038915E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.86900989E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85481198E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15745555E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03390655E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.68993360E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.23206995E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77518437E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70170594E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56395514E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08067861E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65467822E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.78157050E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25750140E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.67247126E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15745555E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03390655E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.68993360E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.23206995E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77518437E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70170594E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56395514E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08067861E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65467822E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.78157050E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25750140E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.67247126E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11483752E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.50711779E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.14849435E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34400986E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39524098E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41471443E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79731049E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11802154E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44440989E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73556465E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.09974312E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41907154E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.18125926E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84509306E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15851184E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16054387E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23265312E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.56595346E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77822861E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06954447E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54161020E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15851184E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16054387E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23265312E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.56595346E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77822861E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06954447E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54161020E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49108262E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05036480E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92575780E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13248192E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51574823E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.73333913E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01804728E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.59026000E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33598644E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33598644E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11200604E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11200604E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10401503E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.34026245E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.57694139E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.17159952E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.42375355E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.62034312E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.59973365E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.47542614E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.77022740E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.52720311E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.28434786E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.16099903E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18089928E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53162369E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18089928E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53162369E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40735345E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51101941E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51101941E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47695862E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01963194E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01963194E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01963170E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.61947027E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.61947027E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.61947027E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.61947027E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.20649215E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.20649215E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.20649215E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.20649215E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.13915247E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.13915247E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.93851485E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.71209243E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.05685688E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.67660717E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.92980264E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.67660717E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.92980264E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.39614220E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.25504317E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.25504317E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.23833575E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.56110834E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.56110834E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.56110219E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.42614234E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.44398565E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.42614234E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.44398565E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.11881304E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.01891181E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.01891181E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.23581340E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.03655457E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.03655457E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.03655460E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.95477226E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.95477226E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.95477226E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.95477226E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.31824041E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.31824041E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.31824041E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.31824041E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.25264173E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.25264173E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.33914525E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.20080017E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.18286956E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.61640574E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.43066303E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.43066303E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.10786352E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.43274794E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.26252334E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.74782838E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.74782838E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.75283467E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.75283467E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09091861E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90092928E-01    2           5        -5   # BR(h -> b       bb     )
     6.38602507E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26063491E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79184058E-04    2           3        -3   # BR(h -> s       sb     )
     2.08078353E-02    2           4        -4   # BR(h -> c       cb     )
     6.81937091E-02    2          21        21   # BR(h -> g       g      )
     2.36912636E-03    2          22        22   # BR(h -> gam     gam    )
     1.64813943E-03    2          22        23   # BR(h -> Z       gam    )
     2.23887239E-01    2          24       -24   # BR(h -> W+      W-     )
     2.84355243E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45324882E+01   # H decays
#          BR         NDA      ID1       ID2
     3.42506995E-01    2           5        -5   # BR(H -> b       bb     )
     6.08003798E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14975416E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54278876E-04    2           3        -3   # BR(H -> s       sb     )
     7.13332256E-08    2           4        -4   # BR(H -> c       cb     )
     7.14907430E-03    2           6        -6   # BR(H -> t       tb     )
     1.05473861E-06    2          21        21   # BR(H -> g       g      )
     4.28862959E-11    2          22        22   # BR(H -> gam     gam    )
     1.83410021E-09    2          23        22   # BR(H -> Z       gam    )
     2.04086857E-06    2          24       -24   # BR(H -> W+      W-     )
     1.01972726E-06    2          23        23   # BR(H -> Z       Z      )
     7.66467518E-06    2          25        25   # BR(H -> h       h      )
    -1.25841482E-24    2          36        36   # BR(H -> A       A      )
    -5.77725304E-21    2          23        36   # BR(H -> Z       A      )
     1.85470415E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65914890E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65914890E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.88277685E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56266477E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60653834E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.08918758E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.80346772E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.66212636E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62474433E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.78572719E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.19328708E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.43461691E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.41736143E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.41736143E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.30841887E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.45288196E+01   # A decays
#          BR         NDA      ID1       ID2
     3.42555914E-01    2           5        -5   # BR(A -> b       bb     )
     6.08048538E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14991066E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54338844E-04    2           3        -3   # BR(A -> s       sb     )
     7.17861228E-08    2           4        -4   # BR(A -> c       cb     )
     7.16195396E-03    2           6        -6   # BR(A -> t       tb     )
     1.47160040E-05    2          21        21   # BR(A -> g       g      )
     4.69265984E-08    2          22        22   # BR(A -> gam     gam    )
     1.61741474E-08    2          23        22   # BR(A -> Z       gam    )
     2.03669234E-06    2          23        25   # BR(A -> Z       h      )
     1.85607928E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65929790E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65929790E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.49652723E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.93659121E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.29961078E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.66433633E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.56484569E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.51854412E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.82962095E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.32395226E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.79370720E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.55488175E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.55488175E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44580281E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.46679219E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09001059E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15327854E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.49874398E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21987128E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50966751E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.48482872E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.04168366E-06    2          24        25   # BR(H+ -> W+      h      )
     3.31046894E-14    2          24        36   # BR(H+ -> W+      A      )
     5.56502603E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46297839E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.33236579E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66360546E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.19882055E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61453514E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00132093E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.54950360E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.86240735E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
