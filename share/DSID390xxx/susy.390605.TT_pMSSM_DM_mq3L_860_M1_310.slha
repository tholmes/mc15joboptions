#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13059124E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.46900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04175550E+01   # W+
        25     1.25841731E+02   # h
        35     4.00000894E+03   # H
        36     3.99999697E+03   # A
        37     4.00104254E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02604758E+03   # ~d_L
   2000001     4.02265272E+03   # ~d_R
   1000002     4.02539562E+03   # ~u_L
   2000002     4.02350510E+03   # ~u_R
   1000003     4.02604758E+03   # ~s_L
   2000003     4.02265272E+03   # ~s_R
   1000004     4.02539562E+03   # ~c_L
   2000004     4.02350510E+03   # ~c_R
   1000005     9.23910715E+02   # ~b_1
   2000005     4.02548309E+03   # ~b_2
   1000006     9.06171910E+02   # ~t_1
   2000006     2.01898700E+03   # ~t_2
   1000011     4.00463014E+03   # ~e_L
   2000011     4.00334810E+03   # ~e_R
   1000012     4.00351687E+03   # ~nu_eL
   1000013     4.00463014E+03   # ~mu_L
   2000013     4.00334810E+03   # ~mu_R
   1000014     4.00351687E+03   # ~nu_muL
   1000015     4.00458706E+03   # ~tau_1
   2000015     4.00765263E+03   # ~tau_2
   1000016     4.00493817E+03   # ~nu_tauL
   1000021     1.97937527E+03   # ~g
   1000022     2.99549733E+02   # ~chi_10
   1000023    -3.57988172E+02   # ~chi_20
   1000025     3.69781760E+02   # ~chi_30
   1000035     2.05215178E+03   # ~chi_40
   1000024     3.55484852E+02   # ~chi_1+
   1000037     2.05231655E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.63825207E-01   # N_11
  1  2    -1.37652602E-02   # N_12
  1  3    -3.86666291E-01   # N_13
  1  4    -3.22654164E-01   # N_14
  2  1    -4.96424823E-02   # N_21
  2  2     2.45031136E-02   # N_22
  2  3    -7.03234064E-01   # N_23
  2  4     7.08799741E-01   # N_24
  3  1     5.01338802E-01   # N_31
  3  2     2.81782933E-02   # N_32
  3  3     5.96596146E-01   # N_33
  3  4     6.26049861E-01   # N_34
  4  1    -1.02050617E-03   # N_41
  4  2     9.99207736E-01   # N_42
  4  3    -4.90608605E-03   # N_43
  4  4    -3.94815155E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.94387780E-03   # U_11
  1  2     9.99975891E-01   # U_12
  2  1    -9.99975891E-01   # U_21
  2  2     6.94387780E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.58465003E-02   # V_11
  1  2    -9.98439366E-01   # V_12
  2  1    -9.98439366E-01   # V_21
  2  2    -5.58465003E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92926898E-01   # cos(theta_t)
  1  2    -1.18727315E-01   # sin(theta_t)
  2  1     1.18727315E-01   # -sin(theta_t)
  2  2     9.92926898E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999382E-01   # cos(theta_b)
  1  2    -1.11175520E-03   # sin(theta_b)
  2  1     1.11175520E-03   # -sin(theta_b)
  2  2     9.99999382E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05848435E-01   # cos(theta_tau)
  1  2     7.08362892E-01   # sin(theta_tau)
  2  1    -7.08362892E-01   # -sin(theta_tau)
  2  2    -7.05848435E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00277064E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30591239E+03  # DRbar Higgs Parameters
         1    -3.46900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43629140E+02   # higgs               
         4     1.61206131E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30591239E+03  # The gauge couplings
     1     3.61943768E-01   # gprime(Q) DRbar
     2     6.36165028E-01   # g(Q) DRbar
     3     1.03004375E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30591239E+03  # The trilinear couplings
  1  1     1.38001276E-06   # A_u(Q) DRbar
  2  2     1.38002592E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30591239E+03  # The trilinear couplings
  1  1     5.07419591E-07   # A_d(Q) DRbar
  2  2     5.07466792E-07   # A_s(Q) DRbar
  3  3     9.08322041E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30591239E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11135726E-07   # A_mu(Q) DRbar
  3  3     1.12272319E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30591239E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67487679E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30591239E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84299547E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30591239E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03408402E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30591239E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57707546E+07   # M^2_Hd              
        22    -9.27416599E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40404393E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.22000318E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42144530E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42144530E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57855470E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57855470E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.79298367E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.01481340E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.28035643E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.63188795E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07294222E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20023358E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.30115951E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15031621E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.32686107E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.26603326E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62014406E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.16545482E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.33524960E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.02457747E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.63560220E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.05900773E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.63943741E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.86659527E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66596095E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54086729E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80622685E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63096520E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.67191990E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63522982E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.71617778E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13272389E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.28958026E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.72324161E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.89070906E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.78266686E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78559705E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.94811370E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96065715E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.30702757E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80726268E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37310721E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60238889E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52091426E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60859596E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.12184230E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.35506577E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.38060442E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.20075211E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44839994E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78579830E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.72866260E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.90808514E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.85071776E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81231831E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.30735030E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63462899E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52309404E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54131021E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.07520831E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.53476685E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.60138432E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.34694770E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85611177E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78559705E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.94811370E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96065715E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.30702757E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80726268E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37310721E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60238889E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52091426E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60859596E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.12184230E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.35506577E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.38060442E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.20075211E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44839994E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78579830E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.72866260E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.90808514E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.85071776E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81231831E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.30735030E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63462899E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52309404E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54131021E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.07520831E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.53476685E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.60138432E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.34694770E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85611177E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14678502E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.15606160E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.05495092E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.94688554E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77810380E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.86292702E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57058921E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06034615E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.47309629E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.45617354E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.50233624E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.73278029E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14678502E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.15606160E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.05495092E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.94688554E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77810380E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.86292702E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57058921E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06034615E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.47309629E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.45617354E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.50233624E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.73278029E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09466667E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.69539383E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40596993E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.10432781E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40111651E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.49069255E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80949560E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09031367E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.77817113E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.13939636E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.81449197E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42837396E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00905808E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86411750E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14784408E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.29234824E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.40181670E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.32570557E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78175541E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14350976E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54787253E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14784408E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.29234824E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.40181670E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.32570557E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78175541E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14350976E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54787253E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47477325E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17118074E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.27039008E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.01390806E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52219978E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62296367E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03022841E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.87226762E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33509787E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33509787E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11171351E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11171351E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10637724E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.29460586E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.59535940E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.10958498E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.43890776E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.53348699E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.35082737E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.35151700E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.97247793E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.44890812E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.75520829E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.27365698E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17756069E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52749635E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17756069E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52749635E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43551539E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50278274E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50278274E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47623853E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00301585E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00301585E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00301567E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.18398409E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.18398409E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.18398409E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.18398409E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.27995407E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.27995407E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.27995407E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.27995407E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.35507032E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.35507032E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.72954568E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.68067845E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.60684389E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.48076024E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22563294E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.48076024E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.22563294E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.17953655E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.77721644E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.77721644E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.77150019E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.63453617E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.63453617E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.63452688E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.19743279E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.85054481E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.19743279E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.85054481E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.18104544E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.53733559E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.53733559E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.71629200E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.30678909E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.30678909E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.30678910E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.94775632E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.94775632E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.94775632E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.94775632E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.49245977E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.49245977E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.49245977E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.49245977E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.02019587E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.02019587E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.29335413E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.77502859E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.70136191E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.25495962E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.34333871E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.34333871E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.98034802E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.80303336E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.82767496E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.75623750E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.75623750E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.76130026E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.76130026E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06578374E-03   # h decays
#          BR         NDA      ID1       ID2
     5.87937181E-01    2           5        -5   # BR(h -> b       bb     )
     6.42404810E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27409541E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82046007E-04    2           3        -3   # BR(h -> s       sb     )
     2.09350675E-02    2           4        -4   # BR(h -> c       cb     )
     6.85898534E-02    2          21        21   # BR(h -> g       g      )
     2.38441827E-03    2          22        22   # BR(h -> gam     gam    )
     1.65687501E-03    2          22        23   # BR(h -> Z       gam    )
     2.24972361E-01    2          24       -24   # BR(h -> W+      W-     )
     2.85743066E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50425258E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55901202E-01    2           5        -5   # BR(H -> b       bb     )
     6.02370886E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12983755E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51923045E-04    2           3        -3   # BR(H -> s       sb     )
     7.06622310E-08    2           4        -4   # BR(H -> c       cb     )
     7.08182679E-03    2           6        -6   # BR(H -> t       tb     )
     8.24041760E-07    2          21        21   # BR(H -> g       g      )
     1.13511093E-09    2          22        22   # BR(H -> gam     gam    )
     1.82022645E-09    2          23        22   # BR(H -> Z       gam    )
     1.82860771E-06    2          24       -24   # BR(H -> W+      W-     )
     9.13670434E-07    2          23        23   # BR(H -> Z       Z      )
     7.17280228E-06    2          25        25   # BR(H -> h       h      )
     4.94100578E-24    2          36        36   # BR(H -> A       A      )
     8.35158485E-20    2          23        36   # BR(H -> Z       A      )
     1.85168322E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60031554E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60031554E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.18251249E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.80721549E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.32464164E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.67591382E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.20300580E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.33582974E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.11752238E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.14369776E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.57489946E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.95374628E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.81645538E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.81645538E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.33361078E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50384818E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55952976E-01    2           5        -5   # BR(A -> b       bb     )
     6.02417996E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13000245E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51983658E-04    2           3        -3   # BR(A -> s       sb     )
     7.11213811E-08    2           4        -4   # BR(A -> c       cb     )
     7.09563405E-03    2           6        -6   # BR(A -> t       tb     )
     1.45797318E-05    2          21        21   # BR(A -> g       g      )
     5.95165601E-08    2          22        22   # BR(A -> gam     gam    )
     1.60280427E-08    2          23        22   # BR(A -> Z       gam    )
     1.82487152E-06    2          23        25   # BR(A -> Z       h      )
     1.86531033E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60038509E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60038509E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.88918103E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.60656241E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.11159545E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.26180878E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.82002258E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.46226012E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.24153345E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.73081129E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.85543297E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.03940841E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.03940841E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51385531E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.71275983E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.01481846E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12669245E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65616318E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20481050E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47868264E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63757650E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.82362633E-06    2          24        25   # BR(H+ -> W+      h      )
     2.98227223E-14    2          24        36   # BR(H+ -> W+      A      )
     6.11056862E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.42642353E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.50558468E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59962484E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.79732927E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58522601E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14310640E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.56917672E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.84792943E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
