include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa ZZgamma + 0j@NLO + 1,2,3j@LO."
evgenConfig.keywords = ["SM", "multilepton", "photon", "neutrino", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "ZZy_leptonic"

genSeq.Sherpa_i.RunCard = """
(run){
  % tags for process setup
  NJET:=3; LJET:=3; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  NUM_ACCURACY=1e-6
  DIPOLE_AMIN=1e-8
  
  % prompt decay settings fully leptonic
  HARD_DECAYS On;
  HDH_STATUS[23,11,-11]=2
  HDH_STATUS[23,13,-13]=2
  HDH_STATUS[23,15,-15]=2
  STABLE[23]=0; WIDTH[23]=0;
}(run)

(processes){
  Process 93 93 -> 22 23 23 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  Integration_Error 0.02 {4,5,6}
  End process
}(processes)

(selector){
  PTNLO  22  10.0  E_CMS
  IsolationCut  22  0.1  2  0.10
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=redlib1=5=redlib2=5=write_parameters=1", "WIDTH[23]=0" ]

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppvvv" ]
