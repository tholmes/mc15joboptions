#--------------------------------------------------------------                 
# JO to be used with this input TXT sample:                                     
# mc15_13TeV.346306.Powheg_NNPDF30ME_ttH125_semilep_LHE.evgen.TXT.e7020         
#--------------------------------------------------------------   
#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# Decay to diphoton
genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description     = 'POWHEG+Pythia8 ttH production with A14 NNPDF2.3 tune, H->yy'
evgenConfig.inputfilecheck  = 'TXT'
evgenConfig.keywords        = [ 'SM', 'top', 'Higgs' ]
evgenConfig.generators     += [ 'Powheg', 'Pythia8' ]
evgenConfig.contact         = [ 'james.robinson@cern.ch', 'kathrin.becker@cern.ch', 'chris.meyer@cern.ch', 'ana.cueto@cern.ch' ]
