from MadGraphControl.MadGraphUtils import *
import fnmatch
import os
import fileinput
# Basic variable init
xqcut = 0
mode = 0
#nevents=80000
iexcfile=0
name="seesaw"

fcard = """
import model typeIIIseesaw
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = v1 v2 v3
generate p p > tr0 tr+, tr0 > l+ W-, tr+ > vl W+
add process p p > tr0 tr+, tr0 > l- W+, tr+ > vl W+
add process p p > tr0 tr-, tr0 > l- W+, tr- > vl W-
add process p p > tr0 tr-, tr0 > l+ W-, tr- > vl W-
output -f
"""

# Grab the run card and move it into place
oldcard_nom = get_default_runcard()
extras = { 'lhe_version':'2.0', 'pdlabel':'nn23lo' }
build_run_card( oldcard_nom , 'run_card.dat' , xqcut=xqcut , 
                rand_seed = runArgs.randomSeed , beamEnergy = runArgs.ecmEnergy*0.5 ,
                extras = extras )

print_cards()
runName='madgraph.303003.MadGraph_seesaw'


# Set up the process
process_dir = new_process(fcard)


str_param_card='MadGraph_param_card_type3Seesaw700.dat'
for root, dirnames, filenames in os.walk('.'):
  for filename in fnmatch.filter(filenames, str_param_card):
    param_grid_location=(os.path.join(root, filename))

generate(run_card_loc='run_card.dat',param_card_loc=param_grid_location,mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.tar.gz')  
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


### Modify the neutrino mass eigenstates to be neutrino flavour eigenstates
print "start"
for line in fileinput.FileInput("tmp_madgraph.303003.MadGraph_seesaw._00001.events",inplace=1):
    line = line.replace("8000012","12")
    line = line.replace("8000014","14")
    line = line.replace("8000016","16")
    print line,
print "end"


evgenConfig.description = "Type III SeeSaw Model Mass: 700 GeV Vu=0.055 Ve=0.055 Vt=0.055"
evgenConfig.contact = ['dpluth@cern.ch']
evgenConfig.keywords = ['BSM','seeSaw']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


genSeq.Pythia8.Commands += [ "15:onMode = off",
			     "15:onIfAny = 11 13",
    		             "Next:numberShowLHA = 10",
                             "Next:numberShowEvent = 10"]

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = MultiLeptonFilter
MultiLeptonFilter.Ptcut       = 5000  #5 GeV
MultiLeptonFilter.Etacut      = 3
MultiLeptonFilter.NLeptons = 2

include("MC15JobOptions/JetFilterAkt6.py")
JetFilter = QCDTruthJetFilter
JetFilter.MinPt = 10000 #10 GeV


