evgenConfig.description = "Inelastic minimum bias, with the Monash NNPDF23LO tune in combination with QCD inspired colour reconnection"

evgenConfig.keywords = ["QCD", "minBias", "SM"]

include ("MC15JobOptions/nonStandard/Pythia8_Monash_CRModeQCD_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]
## include ("Charged_Tracks.py")

evgenConfig.minevents = 1000
