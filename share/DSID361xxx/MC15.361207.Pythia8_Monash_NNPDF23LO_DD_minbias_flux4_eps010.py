###############################################################
# Pythia8 minimum bias (DD) samples
#==============================================================

evgenConfig.description = "DD Minbias with the Donnachie-Landshoff model with high epsilon"
evgenConfig.keywords = ["minBias"]
evgenConfig.generators = ["Pythia8"]

# ... Main generator : Pythia8
include ("MC15JobOptions/nonStandard/Pythia8_Monash_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]

# this uses the Donnachie-Landshoff model with high epsilon
genSeq.Pythia8.Commands += ["Diffraction:PomFlux = 4"]
genSeq.Pythia8.Commands += ["Diffraction:PomFluxEpsilon = 0.10"]
genSeq.Pythia8.Commands += ["Diffraction:PomFluxAlphaPrime = 0.25"]

