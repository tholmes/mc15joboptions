# Pythia8 minimum bias SD with Monash

evgenConfig.description = "SD Minbias with Pythia8 Monash"
evgenConfig.keywords = ["minBias"]
evgenConfig.generators = ["Pythia8"]

include ("MC15JobOptions/nonStandard/Pythia8_Monash_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

