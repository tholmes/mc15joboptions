#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
#PowhegConfig.vdecaymode = 3   # tautau
if hasattr(PowhegConfig, "vdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.vdecaymode = 3  # tautau
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 400. # increase number of generated events by 20%
PowhegConfig.running_width = 1

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common_ForceLeptonicTaus.py')

include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production with forced tau decays, lepton filter, chargedNTracksFilter and AZNLO CT10 tune'
evgenConfig.contact = ["Kristin Lohwasser <kristin.lohwasser@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
#evgenConfig.minevents = 10

# Set up multi-tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep12lep12filter = TauFilter("lep12lep12filter")
  filtSeq += lep12lep12filter

filtSeq.lep12lep12filter.Ntaus = 2

from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
chtrkfilter = ChargedTracksFilter("ChargedTracksFilter")
chtrkfilter.NTracks=-1
chtrkfilter.NTracksMax=10
chtrkfilter.Ptcut=500.
chtrkfilter.Etacut=2.5

filtSeq += chtrkfilter

