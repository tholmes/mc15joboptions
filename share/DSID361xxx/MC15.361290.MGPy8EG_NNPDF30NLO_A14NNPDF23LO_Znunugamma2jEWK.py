evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.inputfilecheck = 'nunugamma2j'
evgenConfig.contact = ['Evgeny Soldatov <Evgeny.Soldatov@cern.ch>']
evgenConfig.keywords = ['SM','Z','photon','neutrino','jets']
evgenConfig.description = 'MadGraph Z->vvg plus two EWK jets'
evgenConfig.minevents = 10000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]