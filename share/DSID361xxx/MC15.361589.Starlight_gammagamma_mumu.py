evgenConfig.description = "Starlight MC for gamma-gamma into di-muon"
evgenConfig.keywords = ["2photon","2muon"]
evgenConfig.contact  = [ "pozdnyak@mail.cern.ch" ]

evgenConfig.generators += [ 'Starlight' ]

from Starlight_i.Starlight_iConf import Starlight_i
genSeq += Starlight_i()

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# Use fixed seeds for reproducibility
import os
seed1 = int(os.popen2("date +%s")[1].read())
seed1 &= 0xfffffffe
Starlight = genSeq.Starlight_i
Starlight.McEventKey = "GEN_EVENT"

Starlight.Initialize = [
   "beam1Z 1", "beam1A 1", #Z,A of projectile
   "beam2Z 1", "beam2A 1", #Z,A of target
   "beamLorentzGamma 6927.4",   #Gamma of the colliding ions
   "maxW 40", #Max value of w
   "minW 9.8", #Min value of w
   "nmbWBins 400", #Bins n w
   "maxRapidity 8.", #max y
   "nmbRapidityBins 300", #Bins n y
   "accCutPt 1", #Cut in pT? 0 = (no, 1 = yes)
   "minPt 5.0", #Minimum pT in GeV
   "maxPt 33.0", #Maximum pT in GeV
   "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
   "minEta -2.5", #Minimum pseudorapidity
   "maxEta 2.5", #Maximum pseudorapidity
   "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
   "nmbEventsTot 1000", #Number of events
   "prodParticleId 13", #Channel of interest
   "randomSeed "+str(seed1), #Random number seed
   "outputFormat 1", #Form of the output
   "beamBreakupMode 0", #Controls the nuclear breakup
   "interferenceEnabled 0", #Interference (0 = off, 1 = on)
   "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
   "coherentProduction 1",   
]
