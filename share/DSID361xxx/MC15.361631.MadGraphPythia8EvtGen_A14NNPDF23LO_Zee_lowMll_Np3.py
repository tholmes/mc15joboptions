import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_Mll10to40_ptl5.py')
evgenConfig.inputconfcheck="361631.MadGraph_NNPDF23LO_Zee_Np3"
evgenConfig.minevents=500
