include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Electroweak production of lllljj (incl. VBS) with 0j@LO."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "jets", "VBS" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Sherpa_NNPDF30NNLO_lllljj_EW6"


evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.0; RSF:=1.0; QSF:=1.0;
  #SCALES=STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  #CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  # simplified setup as long as only 2->6 taken into account:
  SCALES=VAR{FSF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSF*Abs2(p[2]+p[3]+p[4]+p[5])};

  %tags for process setup
  NJET:=0; LJET:=0;

  EXCLUSIVE_CLUSTER_MODE=1;
  SOFT_SPIN_CORRELATIONS=1
  ACTIVE[25]=1;
  MASS[25]=126.0;
  WIDTH[25]=0.00418;
  MASSIVE[11]=1;
  MASSIVE[13]=1;
  MASSIVE[15]=1;
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;      

  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)

(processes){
  Process 93 93 -> -11 11 -11 11 93 93;
  Order (*,6);
  End process;

  Process 93 93 -> -11 11 -13 13 93 93;
  Order (*,6);
  End process;

  Process 93 93 -> -11 11 -15 15 93 93;
  Order (*,6);
  End process;

  Process 93 93 -> -13 13 -13 13 93 93;
  Order (*,6);
  End process;

  Process 93 93 -> -13 13 -15 15 93 93;
  Order (*,6);
  End process;

  Process 93 93 -> -15 15 -15 15 93 93;
  Order (*,6);
  End process;

}(processes)

(selector){
  Mass  11 -11  0.1  E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -2.7,2.7:-2.7,2.7 [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "MODEL=SM", "OL_IGNORE_MODEL=0" ]

