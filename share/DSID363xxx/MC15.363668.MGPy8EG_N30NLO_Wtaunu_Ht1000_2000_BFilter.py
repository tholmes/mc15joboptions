import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=1000
ihtmax=2000
HTrange='midhighHT'
include('MC15JobOptions/MadGraphControl_Wjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=100

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Wtaunu_Ht1000_2000_13TeV"
