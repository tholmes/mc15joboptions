evgenConfig.description = "Inclusive pp->bb->J/psi(mu4mu8) production with Photos"
evgenConfig.keywords = ["charmonium","2muon","inclusive"]
evgenConfig.process  = "bb -> Jpsi -> mumu"
evgenConfig.contact  = [ "jan.kretzschmar@cern.ch" ]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8']
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->mumu
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.NHadronizationLoops = 2

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [4.0, 8.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2, 1]
