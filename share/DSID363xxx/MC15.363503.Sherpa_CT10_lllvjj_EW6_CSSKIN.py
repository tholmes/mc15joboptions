include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "lllvjj-EW, CSS_KIN_SCHEME=1"
evgenConfig.keywords = ["SM", "diboson", "3lepton", "VBS"]
evgenConfig.contact  = ["philipp.anger@cern.ch", "alexander.melzer@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_CT10_lllvjj_EW6"

evgenConfig.process="""
(run){
  EW_SCHEME=3;
  ACTIVE[25]=1;
  MASS[25]=126.0;
  WIDTH[25]=0.00418;
  MASSIVE[5]=1; 
  MASSIVE[11]=1;
  MASSIVE[13]=1;
  MASSIVE[15]=1;
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;

  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=1.0; QSCF:=1.0;
  EXCLUSIVE_CLUSTER_MODE=1;
  #SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  #CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  # simplified setup as long as only 2->6 taken into account:
  SCALES=VAR{FSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSCF*Abs2(p[2]+p[3]+p[4]+p[5])};

  CSS_KIN_SCHEME=1
  
  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)

(processes){

  Process 93 93 -> 11 -11 -11 12 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 11 -11 -13 14 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 11 -11 -15 16 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 13 -13 -11 12 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 13 -13 -13 14 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 13 -13 -15 16 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 15 -15 -11 12 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 15 -15 -13 14 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 15 -15 -15 16 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 11 -11 11 -12 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 11 -11 13 -14 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 11 -11 15 -16 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 13 -13 11 -12 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 13 -13 13 -14 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 13 -13 15 -16 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 15 -15 11 -12 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 15 -15 13 -14 93 93;
  Order_EW 6;
  End process;

  Process 93 93 -> 15 -15 15 -16 93 93;
  Order_EW 6;
  End process;

}(processes)

(selector){
  Mass  11 -11  0.1  E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -2.7,2.7:-2.7,2.7 [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

