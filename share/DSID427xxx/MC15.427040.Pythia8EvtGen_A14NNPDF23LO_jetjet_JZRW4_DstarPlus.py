# JO for Pythia 8 jet jet JZ4W slice

evgenConfig.description = "Dijet truth jet slice JZ4W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/JetFilter_JZ4W.py")
include("MC15JobOptions/DstarPlusFilter.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 150.",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionRef = "+str(minDict[4]) ,
                            "PhaseSpace:bias2SelectionPow = 5.0"
                           ]

filtSeq.QCDTruthJetFilter.DoShape = False
filtSeq.UseEventWeight = True

evgenConfig.minevents = 5000
