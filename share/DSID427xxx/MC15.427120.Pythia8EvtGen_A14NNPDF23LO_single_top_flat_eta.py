evgenConfig.description = "single top with modified xsection"
evgenConfig.process = "single top  production"
evgenConfig.contact = ["fdibello@cern.ch"] 
evgenConfig.keywords    = [  'SM']
evgenConfig.generators += [ 'Pythia8' ]

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')



genSeq.Pythia8.Commands += ["Top:qq2tq(t:W) = on"]  





genSeq.Pythia8.UserHook = "SingleTopWideEta"
#
