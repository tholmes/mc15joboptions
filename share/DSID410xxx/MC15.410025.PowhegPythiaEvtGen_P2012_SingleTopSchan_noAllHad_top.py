#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 single-top s-channel production (top), inclusive, with CT10 and Perugia2012 tune'
evgenConfig.keywords    = [ 'top', 'singleTop', 'sChannel' ]
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg single-top s-channel setup
#--------------------------------------------------------------
if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_t_sch_Common.py')

  PowhegConfig.topdecaymode = 11100 # leptonic W-from-top decays
  #PowhegConfig.nEvents *= 3.
  PowhegConfig.PDF     = 10800 # CT10
  #PowhegConfig.PDF     = 10981 # CT10nlo
  PowhegConfig.mu_F    = 1.0
  PowhegConfig.mu_R    = 1.0
  PowhegConfig.generate()

  #--------------------------------------------------------------
  # Pythia6 (Perugia2012) showering
  #--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')
