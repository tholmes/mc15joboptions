from MadGraphControl.MadGraphUtils import *

# Shower/merging settings
maxjetflavor=5
parton_shower='HERWIGPP'
keywords=['SM','singleTop']

#### HERWIG++ Shower

### Shower


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = "aMcAtNlo + Herwigpp tWZ DR"
evgenConfig.keywords +=keywords
evgenConfig.contact  = ["Olga Bylund <olga.bylund@cern.ch>"]
evgenConfig.inputfilecheck = "aMcAtNloHerwigpp.410217.tWZ_DR_13TeV"

include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
"""

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()
