#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.generators  += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description  = 'MG5_aMC@NLO+Herwigpp t-channel single top, UE-EE-5 tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'lepton']
evgenConfig.contact      = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.tune = "CTEQ6L1-UE-EE-5"
#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------

include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10f4ME_LHEF_EvtGen_Common.py")
