#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, at least three leptons (with pT>10GeV and |eta|<2.6) filter, ME NNPDF30 NLO, A14 NNPDF23 LO based on LHE file production (mc15_13TeV.410450.Powheg_ttbar_hdamp258p75_LHE.evgen.TXT.e6078)'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','steffen.henkelmann@cern.ch']

# Low filter efficiency coupled with many event weights
evgenConfig.minevents   = 200

#--------------------------------------------------------------
# Powheg settings
#--------------------------------------------------------------

# Initial Powheg settings
# set in MC15JobOptions ->
# https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MC15JobOptions/tags/MC15JobOptions-00-07-17/share/DSID410xxx/MC15.410450.Powheg_ttbar_hdamp258p75_LHE.py


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter('MultiLeptonFilter')
myFilter = filtSeq.MultiLeptonFilter
myFilter.NLeptons = 3
myFilter.Ptcut = 10000.
myFilter.Etacut = 2.6
