from MadGraphControl.MadGraphUtils import *


# Shower/merging settings
maxjetflavor=5
parton_shower='PYTHIA8'
keywords=['SM','singleTop']



#### Shower
evgenConfig.description = "aMcAtNlo + Pythia8 tWZ DR"
evgenConfig.generators=["aMcAtNlo","Pythia8"]
evgenConfig.keywords+=keywords
evgenConfig.inputfilecheck = "aMcAtNloPythia8.410215.tWZ_DR_13TeV"


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "Check:epTolErr = 1e-2" ]
