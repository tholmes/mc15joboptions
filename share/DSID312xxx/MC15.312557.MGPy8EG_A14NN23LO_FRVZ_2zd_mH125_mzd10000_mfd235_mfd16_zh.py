mH = 125
mfd2 = 35
mfd1 = 6
mZd = 10000
nGamma = 2
avgtau = 900
decayMode = 'normal'
include("MC15JobOptions/MadGraphControl_A14N23LO_FRVZdisplaced_zh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
