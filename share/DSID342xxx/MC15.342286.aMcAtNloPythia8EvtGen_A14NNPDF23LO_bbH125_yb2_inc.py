evgenConfig.description = 'MadGraph5_aMC@NLO+PYTHIA8+EVTGEN, bbH yb2, H->inclusive'
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125", "bbHiggs", "inclusive" ]
evgenConfig.contact     = [ 'lorenz.hauswald@cern.ch', 'junichi.tanaka@cern.ch' ]

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbH125inc.py")
