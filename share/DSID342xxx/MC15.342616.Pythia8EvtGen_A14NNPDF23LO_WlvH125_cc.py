evgenConfig.description = "PYTHIA8+EVTGEN, WH(125GeV), W->lv, H->cc"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "mH125", "ccbar" ]
evgenConfig.contact     = [ 'paolo.francavilla@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 4 -4', # Higgs decay
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16'
                             ]
