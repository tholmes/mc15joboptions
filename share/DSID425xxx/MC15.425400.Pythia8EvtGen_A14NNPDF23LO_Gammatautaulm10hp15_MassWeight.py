include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "WeakZ0:gmZmode = 1",               # photon only
                            "23:onMode = off",                  # turn off all decay modes
                            "23:onIfAny = 15",                  # turn on the tautau decay mode
                            "15:onMode = off",                  # turn off all decay modes of taus
                            "15:onNegIfAny = 111 130 211 221 223 310 311 321 323", # Neg means antiparticle; tau+: turn on hadronic decay modes
                            "15:onPosIfAny = 11 13" , # Pos means particle; tau-: turn on leptonic decay modes
                            "PhaseSpace:mHatMin = 40.",         # lower invariant mass
                            "PhaseSpace:mHatMax = 220."]       # upper invariant mass

evgenConfig.description = "Pythia 8 gamma*->tau+(lep)tau-(had) production with NNPDF23LO tune"
evgenConfig.contact = ["Martin Werres <werres@physik.uni-bonn.de>"]
evgenConfig.keywords = ["drellYan", "electroweak", "2tau"]

# mHatReweight hook
# -----------------
# Modifies *slope* (by mHat^(slope)) of falling gamma mass
# distribution up to mass *mHatConstMin*, after which a
# constant cross section is used. The SM gamma distribution
# is parameterised as (mHat/cme)^(p1) * (1 - mHat/cme)^(p2)

genSeq.Pythia8.UserHook = "mHatReweight"
genSeq.Pythia8.UserParams += ["mHatReweight:Slope = 2.0",           # Multiplier to falling gamma: mHat^(slope)
                              "mHatReweight:mHatConstMin = 60.",  # mHat to start constant, right away
                              "mHatReweight:p1 = -3.0",
                              "mHatReweight:p2 = 10.0"]


# Set tau filters
if not hasattr(filtSeq, "TauFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TauFilter
    lep10had15filter = TauFilter("lep10had15filter")
    filtSeq += lep10had15filter

filtSeq.lep10had15filter.UseNewOptions = True
filtSeq.lep10had15filter.Ntaus = 2
filtSeq.lep10had15filter.Nleptaus = 1
filtSeq.lep10had15filter.Nhadtaus = 1
filtSeq.lep10had15filter.EtaMaxlep = 2.6
filtSeq.lep10had15filter.EtaMaxhad = 2.6
filtSeq.lep10had15filter.Ptcutlep = 10000.0 #MeV
filtSeq.lep10had15filter.Ptcutlep_lead = 10000.0 #MeV
filtSeq.lep10had15filter.Ptcuthad = 15000.0 #MeV
filtSeq.lep10had15filter.Ptcuthad_lead = 15000.0 #MeV
#filtSeq.lep10had15filter.filterEventNumber = 2 # keep even EventNumber events
