model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 300.
mHD = 125.
widthZp = 1.193662e+00
widthhd = 2.855607e-01
filteff = 8.210181e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
