model="InelasticVectorEFT"
mDM1 = 200.
mDM2 = 800.
mZp = 400.
mHD = 125.
widthZp = 1.814048e+00
widthN2 = 2.335134e-01
filteff = 9.746589e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
