model="InelasticVectorEFT"
mDM1 = 70.
mDM2 = 280.
mZp = 140.
mHD = 125.
widthZp = 5.570415e-01
widthN2 = 1.009603e-02
filteff = 8.431703e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
