model="LightVector"
mDM1 = 5.
mDM2 = 110.
mZp = 80.
mHD = 125.
widthZp = 3.183053e-01
widthN2 = 1.579362e+00
filteff = 3.237294e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
