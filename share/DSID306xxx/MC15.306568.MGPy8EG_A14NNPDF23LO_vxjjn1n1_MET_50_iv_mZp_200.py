model="InelasticVectorEFT"
mDM1 = 100.
mDM2 = 400.
mZp = 200.
mHD = 125.
widthZp = 7.957744e-01
widthN2 = 2.931080e-02
filteff = 9.165903e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
