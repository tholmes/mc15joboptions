#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------


include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")
## To modify Higgs BR
cmds = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
"""

from Herwigpp_i import config as hw
genSeq.Herwigpp.Commands += cmds.splitlines()
del cmds

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = "SM diHiggs production, decay to WWWW, with MG5_aMC@NLOh, inclusive of box diagrami FF. Single lepton channel."
evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "hh", "WW", "1lepton"]

evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.inputfilecheck = 'aMCatNLO_2.2.3.342053.hh_NLO_EFT_FF_HERWIGPP_CT10'
evgenConfig.minevents = 2000

if not hasattr( filtSeq, "DecaysFinalStateFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
  filtSeq += DecaysFinalStateFilter()
  pass

DecaysFinalStateFilter = filtSeq.DecaysFinalStateFilter
DecaysFinalStateFilter.PDGAllowedParents = [ -24, 24 ]
DecaysFinalStateFilter.NChargedLeptons = 1

