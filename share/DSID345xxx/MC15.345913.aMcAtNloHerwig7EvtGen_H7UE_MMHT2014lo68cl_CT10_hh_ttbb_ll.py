#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
include("MC15JobOptions/Herwig7_LHEF.py")

# fix default settings
Herwig7Config.commands += """
## fix for initial-state (backward evolution) splitting
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
"""

Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.commands += """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->b,bbar;
do /Herwig/Particles/h0:PrintDecayModes
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
"""

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
if (runArgs.runNumber == 345913):
    evgenConfig.description = "SM diHiggs production, decay to bbtautau (leplep), with MG5_aMC@NLO, inclusive of box diagram FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "bbbar", "tau"]

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10'
evgenConfig.minevents = 1000

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hTauTauFilter", PDGParent = [25], PDGChild = [15])
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [11,13]

#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (no tau(had)) with pt cuts on e and mu
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("LepTauPtFilter")
filtSeq.LepTauPtFilter.UseNewOptions = True
filtSeq.LepTauPtFilter.Ntaus = 2
filtSeq.LepTauPtFilter.Nleptaus = 2
filtSeq.LepTauPtFilter.Nhadtaus = 0
filtSeq.LepTauPtFilter.EtaMaxlep = 5.0
filtSeq.LepTauPtFilter.EtaMaxhad = 5.0
filtSeq.LepTauPtFilter.Ptcutlep = 7000.0
filtSeq.LepTauPtFilter.Ptcutlep_lead = 13000.0
filtSeq.LepTauPtFilter.Ptcuthad = 15000.0
filtSeq.LepTauPtFilter.Ptcuthad_lead = 15000.0

filtSeq.Expression = "hbbFilter and hTauTauFilter and XtoVVDecayFilterExtended and LepTauPtFilter"

Herwig7Config.run()
