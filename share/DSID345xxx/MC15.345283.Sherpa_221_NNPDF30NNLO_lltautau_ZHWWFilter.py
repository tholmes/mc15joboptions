include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "4 charged leptons, at least 2 tau, with 0,1j@NLO + 2,3j@LO and mll>2*ml+250 MeV, pTl1>5 GeV, pTl2>5 GeV, m4l>130 GeV. Filtered for >=4 FS leptons with pTl>8 GeV, etal>3.2. Filtered with ZHWWLeptonPairMassFilter."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "jets", "NLO", "tau" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "aliblong@cern.ch" ]
evgenConfig.minevents = 2
evgenConfig.inputconfcheck = "Sherpa_221_NNPDF30NNLO_lltautau_ZHWWFilter"

evgenConfig.process="""
(run){
  %[Q:0] means the particles in the container don't interact strongly
  PARTICLE_CONTAINER 98[Q:0] EMU 11 -11 13 -13;
  PARTICLE_CONTAINER 915[Q:0] TAU 15 -15;

  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 915 90 90 90 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  Enhance_Factor  3 {5};
  Enhance_Factor 10 {6};
  Enhance_Factor 10 {7};
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 98 -3.2,3.2:-3.2,3.2 [PT_UP]
  Mass 11 -11 4.0 E_CMS
  Mass 13 -13 4.0 E_CMS
  Mass 15 -15 4.0 E_CMS
  "m" 90,90,90,90 130,E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.NLeptons = 4
filtSeq.MultiLeptonFilter.Ptcut = 8.0*GeV
filtSeq.MultiLeptonFilter.Etacut = 3.2
if not hasattr(filtSeq, "ZHWWLeptonPairMassFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import ZHWWLeptonPairMassFilter
  filtSeq += ZHWWLeptonPairMassFilter()
ZHWWLeptonPairMassFilter.H_cand_mass_cut_upper = 75000 
ZHWWLeptonPairMassFilter.H_cand_mass_cut_lower = 5000 
ZHWWLeptonPairMassFilter.Z_cand_mass_max_distance_from_peak = 15000 
filtSeq.Expression = "(MultiLeptonFilter) and (ZHWWLeptonPairMassFilter)"


MessageSvc.defaultLimit = 9999999
