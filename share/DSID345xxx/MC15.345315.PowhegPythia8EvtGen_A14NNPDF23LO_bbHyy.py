#--------------------------------------------------------------
# Powheg HJ setup starting from ATLAS defaults
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_Hj_Common.py')

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('MC15JobOptions/Pythia8_Powheg.py')

genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22']

evgenConfig.description    = "Powheg+Pythia8 for bbH, H->GamGam"
evgenConfig.keywords       = [ 'Higgs', 'bbHiggs','quark',"diphoton", "2photon" ]
evgenConfig.contact        = ["lserkin@cern.ch", "cyril.becot@cern.ch"]
evgenConfig.inputfilecheck = 'powheg_V2.345335.bbH125_4FS_incl_NNPDF30_13TeV'
