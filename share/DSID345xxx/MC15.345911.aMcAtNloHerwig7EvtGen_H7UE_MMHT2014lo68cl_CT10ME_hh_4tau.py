#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
include("MC15JobOptions/Herwig7_LHEF.py")

# fix default settings
Herwig7Config.commands += """
## fix for initial-state (backward evolution) splitting
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
"""

Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.commands += """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->tau-,tau+;
do /Herwig/Particles/h0:PrintDecayModes
"""

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
if (runArgs.runNumber == 345911):
    evgenConfig.description = "SM diHiggs production, decay to 4tau, with MG5_aMC@NLO, inclusive of box diagram FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "tau"]

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>', 'Bowen Zhang <Bowen.Zhang@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10'
evgenConfig.minevents   = 5000

Herwig7Config.run()
