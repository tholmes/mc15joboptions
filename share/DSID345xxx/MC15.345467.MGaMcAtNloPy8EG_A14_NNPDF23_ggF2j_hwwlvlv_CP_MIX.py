include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'Showering for ggf-> h->WW->emu events with h having a CP mixed top-yukawa coupling'
evgenConfig.contact = ['Remco Castelijn <Remco.Castelijn@cern.ch>','Dominik Duda <dominik.duda@cern.ch>']
evgenConfig.keywords+=['gluonFusionHiggs','WW']
evgenConfig.minevents=1000
evgenConfig.inputfilecheck = 'ggF2j_CPMIX'

genSeq.Pythia8.Commands += [ '25:onMode = off',      # decay of Higgs
                             '25:m0 = 125.0',        # Higgs mass 
                             '25:mWidth = 0.00407',  # Higgs width
                             '25:onIfMatch  = 24 -24',
                             '24:onMode = off',      #decay of W
                             '24:mMin = 2.0',        ### look up what this means !!!
                             '24:onIfMatch = 11 12',
                             '24:onIfMatch = 13 14',
                             '24:onIfMatch = 15 16' 
                           ]

from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiElectronFilter("MultiElectronFilter")
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq += MultiLeptonFilter("MultiLeptonFilter")

MultiElectronFilter = filtSeq.MultiElectronFilter
MultiElectronFilter.Ptcut = 7000.
MultiElectronFilter.Etacut = 3.0
MultiElectronFilter.NElectrons = 1

MultiMuonFilter = filtSeq.MultiMuonFilter
MultiMuonFilter.Ptcut = 7000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 1

MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 16000.
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 1

filtSeq.Expression = "(MultiElectronFilter) and (MultiMuonFilter) and (MultiLeptonFilter)"
