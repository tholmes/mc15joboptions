include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/nonStandard/Pythia8_Photospp.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#taking these settings as the example:
#include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")

#enable only leptonic channels
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             #'25:addChannel = 1 0.0632   100 15 -15',
                             '25:addChannel = 1 0.0264   100 23 23',
                             #'25:addChannel = 1 0.2150   100 24 -24',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 15 15',
                             "PartonLevel:MPI = on" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators    += ["MadGraph", "Pythia8"]
evgenConfig.description    = "MadGraph5 tHjb->ZZ->4l LO showered with Pythia8"
evgenConfig.keywords      += ['Higgs', 'SMHiggs', 'ttHiggs', 'tHiggs']
evgenConfig.contact        = ["Antonio Salvucci <antonio.salvucci@cern.ch>"]
evgenConfig.inputfilecheck = 'thjb'
