
evgenConfig.process     = "VBF H->tautau->hh"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->tautau, for systematics"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 2000
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = on', # decay of taus
                             '15:offIfAny = 11 13' ]

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  had30had20filter = TauFilter("had30had20filter")
  filtSeq += had30had20filter

filtSeq.had30had20filter.UseNewOptions = True
filtSeq.had30had20filter.Ntaus = 2
filtSeq.had30had20filter.Nleptaus = 0
filtSeq.had30had20filter.Nhadtaus = 2
filtSeq.had30had20filter.EtaMaxlep = 2.6
filtSeq.had30had20filter.EtaMaxhad = 2.6
filtSeq.had30had20filter.Ptcutlep = 7000.0 #MeV
filtSeq.had30had20filter.Ptcutlep_lead = 7000.0 #MeV
filtSeq.had30had20filter.Ptcuthad = 20000.0 #MeV
filtSeq.had30had20filter.Ptcuthad_lead = 30000.0 #MeV
