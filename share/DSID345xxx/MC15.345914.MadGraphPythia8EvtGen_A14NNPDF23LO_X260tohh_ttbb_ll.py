from MadGraphControl.MadGraphUtils import *

mode=0

#---------------------------------------------------------------------------------------------------
# Setting mHH and WHH for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters={'1560':'2.600000e+02', #MHH
            '1561':'1.000000e-02'} #WHH

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'pdlabel':"'nn23lo1'",
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           #'parton_shower':'PYTHIA8',
           'ptj':'0',
           'ptb':'0',
           'pta':'0',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'-1',
           'etab':'-1',
           'etaa':'-1',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'0',
           'mmaamax':'-1',
           'mmbb':'0',
           'mmbbmax':'-1',
           'drjj':'0',
           'drbb':'0',
           'draa':'0',
           'drbj':'0',
           'draj':'0',
           'drab':'0',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1' }

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
if (runArgs.runNumber == 345914):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    import model HeavyHiggsTHDM
    generate p p > h h
    output -f""")
    fcard.close()
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Filter efficiency is very low
# Thus, setting the number of generated events to 100 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=100
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

runName='run_01'
process_dir = new_process(card_loc='proc_card_mg5.dat')

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------

build_param_card(param_card_old='param_card.HeavyScalar.dat',param_card_new='param_card_new.dat',masses=higgsMass,extras=parameters)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir), run_card_new='run_card.dat', xqcut=0, nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, extras=extras)

print_cards()

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
if (runArgs.runNumber == 345914):
    evgenConfig.description = "Di-Higgs production through 260 GeV Heavy Higgs resonance which decays to bbtautau."
    evgenConfig.keywords = ["hh","BSM", "BSMHiggs", "resonance", "tau", "bottom"]

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = runName
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

#---------------------------------------------------------------------------------------------------
# Decaying hh to bbtautau with Pythia8
#---------------------------------------------------------------------------------------------------

genSeq.Pythia8.Commands += ["25:onMode=off",
                            "25:onIfAny=5 15" ]

#---------------------------------------------------------------------------------------------------
# Filter for bbtautau
#---------------------------------------------------------------------------------------------------
#include("MC15JobOptions/XtoVVDecayFilterExtended.py")
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
filtSeq += XtoVVDecayFilterExtended("BBTauTauFilter")
filtSeq.BBTauTauFilter.PDGGrandParent = 1560
filtSeq.BBTauTauFilter.PDGParent = 25
filtSeq.BBTauTauFilter.StatusParent = 22
filtSeq.BBTauTauFilter.PDGChild1 = [5]
filtSeq.BBTauTauFilter.PDGChild2 = [15]

#---------------------------------------------------------------------------------------------------
# Filter for leplep
#---------------------------------------------------------------------------------------------------
filtSeq += XtoVVDecayFilterExtended("TauTauLepLepFilter")
filtSeq.TauTauLepLepFilter.PDGGrandParent = 25
filtSeq.TauTauLepLepFilter.PDGParent = 15
filtSeq.TauTauLepLepFilter.StatusParent = 2
filtSeq.TauTauLepLepFilter.PDGChild1 = [11,13]
filtSeq.TauTauLepLepFilter.PDGChild2 = [11,13]


#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (no tau(had)) with pt cuts on e and mu
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("LepTauPtFilter")
filtSeq.LepTauPtFilter.UseNewOptions = True
filtSeq.LepTauPtFilter.Ntaus = 2
filtSeq.LepTauPtFilter.Nleptaus = 2
filtSeq.LepTauPtFilter.Nhadtaus = 0
filtSeq.LepTauPtFilter.EtaMaxlep = 5.0
filtSeq.LepTauPtFilter.EtaMaxhad = 5.0
filtSeq.LepTauPtFilter.Ptcutlep = 7000.0
filtSeq.LepTauPtFilter.Ptcutlep_lead = 13000.0
filtSeq.LepTauPtFilter.Ptcuthad = 15000.0
filtSeq.LepTauPtFilter.Ptcuthad_lead = 15000.0

filtSeq.Expression = "BBTauTauFilter and TauTauLepLepFilter and LepTauPtFilter"
