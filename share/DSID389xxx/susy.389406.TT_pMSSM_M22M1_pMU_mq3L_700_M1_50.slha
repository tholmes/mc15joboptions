#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14468474E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04007462E+01   # W+
        25     1.25707656E+02   # h
        35     3.00017005E+03   # H
        36     2.99999965E+03   # A
        37     3.00097296E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03349027E+03   # ~d_L
   2000001     3.02821848E+03   # ~d_R
   1000002     3.03257066E+03   # ~u_L
   2000002     3.02966252E+03   # ~u_R
   1000003     3.03349027E+03   # ~s_L
   2000003     3.02821848E+03   # ~s_R
   1000004     3.03257066E+03   # ~c_L
   2000004     3.02966252E+03   # ~c_R
   1000005     7.91409890E+02   # ~b_1
   2000005     3.02748437E+03   # ~b_2
   1000006     7.86424931E+02   # ~t_1
   2000006     3.01248543E+03   # ~t_2
   1000011     3.00656896E+03   # ~e_L
   2000011     3.00171260E+03   # ~e_R
   1000012     3.00516707E+03   # ~nu_eL
   1000013     3.00656896E+03   # ~mu_L
   2000013     3.00171260E+03   # ~mu_R
   1000014     3.00516707E+03   # ~nu_muL
   1000015     2.98593566E+03   # ~tau_1
   2000015     3.02199678E+03   # ~tau_2
   1000016     3.00507643E+03   # ~nu_tauL
   1000021     2.34107866E+03   # ~g
   1000022     4.99834207E+01   # ~chi_10
   1000023     1.08460558E+02   # ~chi_20
   1000025    -2.99836028E+03   # ~chi_30
   1000035     2.99875670E+03   # ~chi_40
   1000024     1.08612179E+02   # ~chi_1+
   1000037     2.99946375E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886278E-01   # N_11
  1  2    -2.69730074E-03   # N_12
  1  3     1.48048912E-02   # N_13
  1  4    -9.85304972E-04   # N_14
  2  1     3.08540298E-03   # N_21
  2  2     9.99651966E-01   # N_22
  2  3    -2.61098181E-02   # N_23
  2  4     2.16897848E-03   # N_24
  3  1    -9.72299669E-03   # N_31
  3  2     1.69570504E-02   # N_32
  3  3     7.06817985E-01   # N_33
  3  4     7.07125348E-01   # N_34
  4  1    -1.11074157E-02   # N_41
  4  2     2.00282249E-02   # N_42
  4  3     7.06758395E-01   # N_43
  4  4    -7.07084201E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99316121E-01   # U_11
  1  2    -3.69768794E-02   # U_12
  2  1     3.69768794E-02   # U_21
  2  2     9.99316121E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995278E-01   # V_11
  1  2    -3.07296563E-03   # V_12
  2  1     3.07296563E-03   # V_21
  2  2     9.99995278E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98576617E-01   # cos(theta_t)
  1  2    -5.33361039E-02   # sin(theta_t)
  2  1     5.33361039E-02   # -sin(theta_t)
  2  2     9.98576617E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99918336E-01   # cos(theta_b)
  1  2     1.27797234E-02   # sin(theta_b)
  2  1    -1.27797234E-02   # -sin(theta_b)
  2  2     9.99918336E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06876180E-01   # cos(theta_tau)
  1  2     7.07337307E-01   # sin(theta_tau)
  2  1    -7.07337307E-01   # -sin(theta_tau)
  2  2     7.06876180E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01778252E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.44684745E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44273105E+02   # higgs               
         4     7.21517151E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.44684745E+03  # The gauge couplings
     1     3.62009829E-01   # gprime(Q) DRbar
     2     6.41696852E-01   # g(Q) DRbar
     3     1.02719795E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.44684745E+03  # The trilinear couplings
  1  1     1.98290516E-06   # A_u(Q) DRbar
  2  2     1.98293421E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.44684745E+03  # The trilinear couplings
  1  1     4.83928656E-07   # A_d(Q) DRbar
  2  2     4.84027808E-07   # A_s(Q) DRbar
  3  3     1.14254073E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.44684745E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.06732918E-07   # A_mu(Q) DRbar
  3  3     1.08021565E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.44684745E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54355809E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.44684745E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.10129307E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.44684745E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04737857E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.44684745E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.33955699E+04   # M^2_Hd              
        22    -9.08923177E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42931618E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.43102230E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47786587E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47786587E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52213413E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52213413E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.41955399E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.13448048E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.09745719E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.78909476E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16950178E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.63274510E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.94620522E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.90345169E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.86680563E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.87770104E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71801613E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62606283E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.20580916E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.23108024E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.31523379E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.49081329E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.37766333E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.21310644E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.95450159E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.04306940E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.21855615E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.20403622E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66567892E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31247333E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.83382408E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.12754537E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.95691898E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12011270E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.69577576E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65655874E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.22081061E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.05276691E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.31152881E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.71941671E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.97495415E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18604914E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59395702E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.51544189E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.43446029E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.46482810E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40602766E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12510687E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.02984374E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65207649E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.60172198E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.88360943E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30575001E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.62314992E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.98187159E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67391906E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53814207E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.31459525E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.93274676E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.45760171E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54618143E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12011270E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.69577576E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65655874E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.22081061E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.05276691E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.31152881E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.71941671E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.97495415E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18604914E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59395702E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.51544189E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.43446029E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.46482810E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40602766E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12510687E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.02984374E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65207649E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.60172198E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.88360943E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30575001E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.62314992E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.98187159E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67391906E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53814207E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.31459525E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.93274676E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.45760171E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54618143E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07280039E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.52394080E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02423583E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.18327180E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.54257120E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02336988E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.84277819E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56399170E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990497E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.50220860E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.71480092E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.78448348E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07280039E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.52394080E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02423583E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.18327180E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.54257120E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02336988E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.84277819E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56399170E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990497E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.50220860E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.71480092E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.78448348E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84756519E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42596446E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19740227E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37663327E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78717751E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50601759E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17100811E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.08417085E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.23335086E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32261850E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.24045756E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07316845E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.70245218E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00156667E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.11638649E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.79476817E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02818805E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     8.20720410E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07316845E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.70245218E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00156667E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.11638649E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.79476817E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02818805E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     8.20720410E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07338588E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.70164134E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00131545E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.03396373E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.68823942E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02851218E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     8.16612535E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.78041353E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35842922E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.34699087E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35842922E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.34699087E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09615247E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.44902825E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09615247E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.44902825E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09083643E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.52598122E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.58245647E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.21040542E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.64027954E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.03648649E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.16091656E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.89457411E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.22146575E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.77435394E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.01102821E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.71954094E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     4.71601208E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.71954094E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     4.71601208E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     8.06985821E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     4.01371307E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.01371307E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     4.22268876E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.47647900E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.47647900E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.47425682E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.56866450E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.27119925E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.06419761E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.83717749E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.83717749E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.97935443E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.96886437E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.63313583E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.63313583E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.18032655E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.18032655E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.93537248E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.93537248E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.46160373E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.86316760E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.95121617E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.92508761E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.92508761E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.32030499E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.24716307E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.61130197E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.61130197E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.20933653E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.20933653E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.57200039E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.57200039E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.95630278E-03   # h decays
#          BR         NDA      ID1       ID2
     6.64110625E-01    2           5        -5   # BR(h -> b       bb     )
     5.29579872E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.87470299E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.97437190E-04    2           3        -3   # BR(h -> s       sb     )
     1.71585893E-02    2           4        -4   # BR(h -> c       cb     )
     5.64609562E-02    2          21        21   # BR(h -> g       g      )
     1.94787598E-03    2          22        22   # BR(h -> gam     gam    )
     1.34248036E-03    2          22        23   # BR(h -> Z       gam    )
     1.82338546E-01    2          24       -24   # BR(h -> W+      W-     )
     2.30566851E-02    2          23        23   # BR(h -> Z       Z      )
     4.13475384E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.40923516E+01   # H decays
#          BR         NDA      ID1       ID2
     7.34091848E-01    2           5        -5   # BR(H -> b       bb     )
     1.76464288E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.23935555E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66191710E-04    2           3        -3   # BR(H -> s       sb     )
     2.16167684E-07    2           4        -4   # BR(H -> c       cb     )
     2.15641247E-02    2           6        -6   # BR(H -> t       tb     )
     3.45267238E-05    2          21        21   # BR(H -> g       g      )
     2.28527925E-08    2          22        22   # BR(H -> gam     gam    )
     8.31333868E-09    2          23        22   # BR(H -> Z       gam    )
     3.01482163E-05    2          24       -24   # BR(H -> W+      W-     )
     1.50555225E-05    2          23        23   # BR(H -> Z       Z      )
     8.05704266E-05    2          25        25   # BR(H -> h       h      )
    -3.72647523E-23    2          36        36   # BR(H -> A       A      )
     1.07573371E-17    2          23        36   # BR(H -> Z       A      )
     2.42403955E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13245867E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20381996E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.39233270E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.18429047E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.81992920E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32218572E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82487311E-01    2           5        -5   # BR(A -> b       bb     )
     1.88075978E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.64990783E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.16746780E-04    2           3        -3   # BR(A -> s       sb     )
     2.30474364E-07    2           4        -4   # BR(A -> c       cb     )
     2.29786433E-02    2           6        -6   # BR(A -> t       tb     )
     6.76699137E-05    2          21        21   # BR(A -> g       g      )
     4.29478962E-08    2          22        22   # BR(A -> gam     gam    )
     6.66567413E-08    2          23        22   # BR(A -> Z       gam    )
     3.20073495E-05    2          23        25   # BR(A -> Z       h      )
     2.64075223E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22454327E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31142993E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.01676874E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01297654E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.08563222E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.45565460E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.68259571E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.94803527E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.10256550E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04976180E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.06981788E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.18443755E-05    2          24        25   # BR(H+ -> W+      h      )
     1.13459055E-13    2          24        36   # BR(H+ -> W+      A      )
     2.06081637E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.96289211E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.22884446E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
