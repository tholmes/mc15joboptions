#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12202996E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04018367E+01   # W+
        25     1.26363252E+02   # h
        35     3.00024510E+03   # H
        36     2.99999959E+03   # A
        37     3.00103889E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02383015E+03   # ~d_L
   2000001     3.01846237E+03   # ~d_R
   1000002     3.02290910E+03   # ~u_L
   2000002     3.02043921E+03   # ~u_R
   1000003     3.02383015E+03   # ~s_L
   2000003     3.01846237E+03   # ~s_R
   1000004     3.02290910E+03   # ~c_L
   2000004     3.02043921E+03   # ~c_R
   1000005     5.70071375E+02   # ~b_1
   2000005     3.01796274E+03   # ~b_2
   1000006     5.62724348E+02   # ~t_1
   2000006     2.99973953E+03   # ~t_2
   1000011     3.00682309E+03   # ~e_L
   2000011     3.00117077E+03   # ~e_R
   1000012     3.00542361E+03   # ~nu_eL
   1000013     3.00682309E+03   # ~mu_L
   2000013     3.00117077E+03   # ~mu_R
   1000014     3.00542361E+03   # ~nu_muL
   1000015     2.98606216E+03   # ~tau_1
   2000015     3.02217083E+03   # ~tau_2
   1000016     3.00552714E+03   # ~nu_tauL
   1000021     2.32895883E+03   # ~g
   1000022     5.01391067E+01   # ~chi_10
   1000023     1.08309360E+02   # ~chi_20
   1000025    -3.00216538E+03   # ~chi_30
   1000035     3.00254845E+03   # ~chi_40
   1000024     1.08460569E+02   # ~chi_1+
   1000037     3.00325084E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886291E-01   # N_11
  1  2    -2.69996901E-03   # N_12
  1  3     1.48035381E-02   # N_13
  1  4    -9.85214995E-04   # N_14
  2  1     3.08845280E-03   # N_21
  2  2     9.99651215E-01   # N_22
  2  3    -2.61380155E-02   # N_23
  2  4     2.17131578E-03   # N_24
  3  1    -9.72200224E-03   # N_31
  3  2     1.69753617E-02   # N_32
  3  3     7.06817529E-01   # N_33
  3  4     7.07125378E-01   # N_34
  4  1    -1.11062754E-02   # N_41
  4  2     2.00498446E-02   # N_42
  4  3     7.06757837E-01   # N_43
  4  4    -7.07084163E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99314644E-01   # U_11
  1  2    -3.70167735E-02   # U_12
  2  1     3.70167735E-02   # U_21
  2  2     9.99314644E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995268E-01   # V_11
  1  2    -3.07627652E-03   # V_12
  2  1     3.07627652E-03   # V_21
  2  2     9.99995268E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98636686E-01   # cos(theta_t)
  1  2    -5.21993235E-02   # sin(theta_t)
  2  1     5.21993235E-02   # -sin(theta_t)
  2  2     9.98636686E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99923355E-01   # cos(theta_b)
  1  2     1.23807966E-02   # sin(theta_b)
  2  1    -1.23807966E-02   # -sin(theta_b)
  2  2     9.99923355E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06864379E-01   # cos(theta_tau)
  1  2     7.07349100E-01   # sin(theta_tau)
  2  1    -7.07349100E-01   # -sin(theta_tau)
  2  2     7.06864379E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01774092E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.22029965E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44632342E+02   # higgs               
         4     6.48063047E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.22029965E+03  # The gauge couplings
     1     3.61441448E-01   # gprime(Q) DRbar
     2     6.41446802E-01   # g(Q) DRbar
     3     1.03100121E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.22029965E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37612477E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.22029965E+03  # The trilinear couplings
  1  1     3.31962150E-07   # A_d(Q) DRbar
  2  2     3.32031566E-07   # A_s(Q) DRbar
  3  3     7.90540234E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.22029965E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.28547783E-08   # A_mu(Q) DRbar
  3  3     7.37211135E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.22029965E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58302027E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.22029965E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.09000007E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.22029965E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04619267E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.22029965E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.75581763E+04   # M^2_Hd              
        22    -9.15561217E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42815396E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.24037117E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47836476E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47836476E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52163524E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52163524E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.38433194E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.07965042E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.87648768E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.01554728E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20215252E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.43786532E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.77434482E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.55806995E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.79583867E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.76817959E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63933705E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.24452574E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.11458157E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.43803909E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.72020879E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.13598730E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24093650E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.90277779E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.46294299E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.47494630E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.39959301E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -5.16338493E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30666293E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.85593993E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16044393E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.01348210E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12420097E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.65436432E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64850700E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.43370472E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.14356872E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29543865E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.26395613E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.99951049E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20476669E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57655848E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.50186595E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.59690645E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.25202294E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42342645E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12921663E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.98674393E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64404564E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.41229064E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.89081890E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28967499E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.90596315E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.00641061E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69156452E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48630242E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.27374554E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.91969355E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.37544485E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55136547E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12420097E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.65436432E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64850700E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.43370472E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.14356872E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29543865E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.26395613E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.99951049E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20476669E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57655848E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.50186595E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.59690645E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.25202294E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42342645E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12921663E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.98674393E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64404564E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.41229064E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.89081890E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28967499E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.90596315E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.00641061E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69156452E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48630242E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.27374554E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.91969355E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.37544485E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55136547E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06906507E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.50334628E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02492579E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.83121665E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.64403879E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02473953E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.67383751E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55879733E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990479E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.52109797E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06906507E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.50334628E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02492579E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.83121665E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.64403879E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02473953E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.67383751E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55879733E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990479E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.52109797E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.84316963E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42069771E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19916397E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38013831E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78283375E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50056174E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17286805E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.65555021E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.91137971E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32631693E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     8.76106195E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06944415E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.68185509E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00224966E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.15650990E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.65111079E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02956481E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.19518425E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.06944415E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.68185509E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00224966E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.15650990E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.65111079E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02956481E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.19518425E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06992332E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.68104906E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00200014E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.61710890E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.21050788E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02989404E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     8.93382692E-08    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.73123959E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35852289E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.44160348E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35852289E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.44160348E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09611459E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.48055471E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09611459E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.48055471E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09072483E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.95302628E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.55483142E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.31701365E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.10597405E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.73182379E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.06350759E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.59964193E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.87998767E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.62657789E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.16876869E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.35246272E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.71403066E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.35246272E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.71403066E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.27388471E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.44840722E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.44840722E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.52386495E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.30707194E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.30707194E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.30798759E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.99733675E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.21338296E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.88854096E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.54522396E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.54522396E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.48543508E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.82930257E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.68464546E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.68464546E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.16837103E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.16837103E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.59741581E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     3.59741581E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.59741581E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     3.59741581E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     6.53901427E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.53901427E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.88952408E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.40956182E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.82215634E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.62585429E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.62585429E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.25445436E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.04383227E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.66472417E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.66472417E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.19611908E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.19611908E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.11447796E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     9.11447796E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.11447796E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     9.11447796E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     8.53367576E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     8.53367576E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     5.11426356E-03   # h decays
#          BR         NDA      ID1       ID2
     6.58426343E-01    2           5        -5   # BR(h -> b       bb     )
     5.15897541E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.82624520E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.86772795E-04    2           3        -3   # BR(h -> s       sb     )
     1.66984097E-02    2           4        -4   # BR(h -> c       cb     )
     5.55528935E-02    2          21        21   # BR(h -> g       g      )
     1.92984363E-03    2          22        22   # BR(h -> gam     gam    )
     1.37766996E-03    2          22        23   # BR(h -> Z       gam    )
     1.89601934E-01    2          24       -24   # BR(h -> W+      W-     )
     2.42130524E-02    2          23        23   # BR(h -> Z       Z      )
     4.07029961E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.40302208E+01   # H decays
#          BR         NDA      ID1       ID2
     7.30362756E-01    2           5        -5   # BR(H -> b       bb     )
     1.77250176E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.26714265E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.69601413E-04    2           3        -3   # BR(H -> s       sb     )
     2.17126069E-07    2           4        -4   # BR(H -> c       cb     )
     2.16597417E-02    2           6        -6   # BR(H -> t       tb     )
     3.88172563E-05    2          21        21   # BR(H -> g       g      )
     3.68133288E-08    2          22        22   # BR(H -> gam     gam    )
     8.34789917E-09    2          23        22   # BR(H -> Z       gam    )
     3.01692813E-05    2          24       -24   # BR(H -> W+      W-     )
     1.50660447E-05    2          23        23   # BR(H -> Z       Z      )
     8.05592202E-05    2          25        25   # BR(H -> h       h      )
    -2.63862678E-23    2          36        36   # BR(H -> A       A      )
     6.85589957E-17    2          23        36   # BR(H -> Z       A      )
     2.44021289E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13718677E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.21184886E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.43216714E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.46510389E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.10096355E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31238470E+01   # A decays
#          BR         NDA      ID1       ID2
     7.80853579E-01    2           5        -5   # BR(A -> b       bb     )
     1.89480545E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.69956991E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.22846315E-04    2           3        -3   # BR(A -> s       sb     )
     2.32195567E-07    2           4        -4   # BR(A -> c       cb     )
     2.31502498E-02    2           6        -6   # BR(A -> t       tb     )
     6.81752802E-05    2          21        21   # BR(A -> g       g      )
     4.32542898E-08    2          22        22   # BR(A -> gam     gam    )
     6.71671606E-08    2          23        22   # BR(A -> Z       gam    )
     3.21224531E-05    2          23        25   # BR(A -> Z       h      )
     2.66630923E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.23335465E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32411860E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.08419609E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.87843737E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.06971487E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.51818690E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.90369466E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.84616426E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.23248547E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07649052E-03    2           4        -3   # BR(H+ -> c       sb     )
     6.97797872E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.27469084E-05    2          24        25   # BR(H+ -> W+      h      )
     1.61505125E-13    2          24        36   # BR(H+ -> W+      A      )
     2.11523284E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.01873124E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.51296924E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
