#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954528E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03985234E+01   # W+
        25     1.25155383E+02   # h
        35     3.00011781E+03   # H
        36     2.99999987E+03   # A
        37     3.00092386E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03889350E+03   # ~d_L
   2000001     3.03374596E+03   # ~d_R
   1000002     3.03798405E+03   # ~u_L
   2000002     3.03486461E+03   # ~u_R
   1000003     3.03889350E+03   # ~s_L
   2000003     3.03374596E+03   # ~s_R
   1000004     3.03798405E+03   # ~c_L
   2000004     3.03486461E+03   # ~c_R
   1000005     9.48289367E+02   # ~b_1
   2000005     3.03280531E+03   # ~b_2
   1000006     9.44183321E+02   # ~t_1
   2000006     3.01668247E+03   # ~t_2
   1000011     3.00634382E+03   # ~e_L
   2000011     3.00204145E+03   # ~e_R
   1000012     3.00495258E+03   # ~nu_eL
   1000013     3.00634382E+03   # ~mu_L
   2000013     3.00204145E+03   # ~mu_R
   1000014     3.00495258E+03   # ~nu_muL
   1000015     2.98572848E+03   # ~tau_1
   2000015     3.02192319E+03   # ~tau_2
   1000016     3.00473560E+03   # ~nu_tauL
   1000021     2.34804462E+03   # ~g
   1000022     1.50509752E+02   # ~chi_10
   1000023     3.20701413E+02   # ~chi_20
   1000025    -2.99603183E+03   # ~chi_30
   1000035     2.99660858E+03   # ~chi_40
   1000024     3.20863370E+02   # ~chi_1+
   1000037     2.99723338E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888382E-01   # N_11
  1  2    -1.15117334E-03   # N_12
  1  3     1.48227740E-02   # N_13
  1  4    -1.47760996E-03   # N_14
  2  1     1.54684964E-03   # N_21
  2  2     9.99644708E-01   # N_22
  2  3    -2.63186146E-02   # N_23
  2  4     3.92374836E-03   # N_24
  3  1    -9.41530555E-03   # N_31
  3  2     1.58486999E-02   # N_32
  3  3     7.06842256E-01   # N_33
  3  4     7.07130961E-01   # N_34
  4  1    -1.14970621E-02   # N_41
  4  2     2.13998020E-02   # N_42
  4  3     7.06726001E-01   # N_43
  4  4    -7.07070170E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99306338E-01   # U_11
  1  2    -3.72403458E-02   # U_12
  2  1     3.72403458E-02   # U_21
  2  2     9.99306338E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984581E-01   # V_11
  1  2    -5.55326554E-03   # V_12
  2  1     5.55326554E-03   # V_21
  2  2     9.99984581E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98507456E-01   # cos(theta_t)
  1  2    -5.46155684E-02   # sin(theta_t)
  2  1     5.46155684E-02   # -sin(theta_t)
  2  2     9.98507456E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99911673E-01   # cos(theta_b)
  1  2     1.32908314E-02   # sin(theta_b)
  2  1    -1.32908314E-02   # -sin(theta_b)
  2  2     9.99911673E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06929560E-01   # cos(theta_tau)
  1  2     7.07283958E-01   # sin(theta_tau)
  2  1    -7.07283958E-01   # -sin(theta_tau)
  2  2     7.06929560E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01766286E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59545281E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44038638E+02   # higgs               
         4     7.55755333E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59545281E+03  # The gauge couplings
     1     3.62380494E-01   # gprime(Q) DRbar
     2     6.39029790E-01   # g(Q) DRbar
     3     1.02498894E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59545281E+03  # The trilinear couplings
  1  1     2.42123836E-06   # A_u(Q) DRbar
  2  2     2.42127322E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59545281E+03  # The trilinear couplings
  1  1     6.14008063E-07   # A_d(Q) DRbar
  2  2     6.14126294E-07   # A_s(Q) DRbar
  3  3     1.40862134E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59545281E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.32848007E-07   # A_mu(Q) DRbar
  3  3     1.34450055E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59545281E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51948703E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59545281E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12770439E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59545281E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05808618E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59545281E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.25754102E+04   # M^2_Hd              
        22    -9.05238752E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41755778E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.68824398E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47717605E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47717605E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52282395E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52282395E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.10563786E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.38204250E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.11621431E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.74558144E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13238824E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.81797854E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.48594534E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.00131078E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.74217893E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.95149522E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69559164E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60712151E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.15283230E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.96301127E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.52093942E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.45707135E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.39083471E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19431126E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.97384238E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.56093327E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.99811050E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.96072958E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.00755955E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31231978E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.83936673E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.12588933E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.93369927E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.01923707E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.87769605E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63862112E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.86548976E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.20963603E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27748268E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.43922054E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.02511835E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16967146E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60087984E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.76967899E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.06406261E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.54117064E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39911613E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02423566E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.02023703E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63606487E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.68542166E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.16080383E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27175807E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.95739663E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.03196945E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65998236E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.55780218E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.07323715E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.86435189E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.14505482E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54421864E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.01923707E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.87769605E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63862112E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.86548976E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.20963603E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27748268E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.43922054E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.02511835E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16967146E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60087984E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.76967899E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.06406261E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.54117064E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39911613E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02423566E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.02023703E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63606487E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.68542166E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.16080383E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27175807E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.95739663E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.03196945E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65998236E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.55780218E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.07323715E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.86435189E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.14505482E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54421864E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96759572E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80616174E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01138290E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.59438223E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.84925513E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00800057E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.12647754E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56035883E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997646E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35076212E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.42561292E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.73761941E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96759572E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80616174E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01138290E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.59438223E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.84925513E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00800057E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.12647754E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56035883E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997646E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35076212E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.42561292E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.73761941E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79257223E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48819440E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17423841E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33756719E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73317227E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57085756E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14674033E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.34381706E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.50489056E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28196394E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.53300827E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96790334E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.88073412E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99914632E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.85392365E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.37500284E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01278014E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.99526764E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96790334E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.88073412E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99914632E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.85392365E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.37500284E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01278014E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.99526764E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96794496E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.87990985E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99888832E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.62128907E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.99736262E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01310505E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.55285029E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.64924518E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.14029115E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.67272961E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.10399931E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.55839891E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.30002400E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.26676045E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.12313643E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.59732082E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.88906525E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.95374858E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50462514E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.17374863E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.39899208E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.68597548E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.08130785E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.08130785E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.69609568E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.62423330E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.58626596E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.58626596E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.21659966E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.21659966E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.77247902E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.77247902E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.07530939E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.49736604E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.60405027E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.17013218E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.17013218E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.46688414E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.99133445E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.55773239E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.55773239E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.24649807E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.24649807E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.31059077E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.31059077E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.83343655E-03   # h decays
#          BR         NDA      ID1       ID2
     6.69132974E-01    2           5        -5   # BR(h -> b       bb     )
     5.40624568E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.91382125E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.06078362E-04    2           3        -3   # BR(h -> s       sb     )
     1.75325173E-02    2           4        -4   # BR(h -> c       cb     )
     5.71498840E-02    2          21        21   # BR(h -> g       g      )
     1.95842700E-03    2          22        22   # BR(h -> gam     gam    )
     1.31080876E-03    2          22        23   # BR(h -> Z       gam    )
     1.76176806E-01    2          24       -24   # BR(h -> W+      W-     )
     2.20786664E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.41075160E+01   # H decays
#          BR         NDA      ID1       ID2
     7.37162171E-01    2           5        -5   # BR(H -> b       bb     )
     1.76271555E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.23254096E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.65356649E-04    2           3        -3   # BR(H -> s       sb     )
     2.15921767E-07    2           4        -4   # BR(H -> c       cb     )
     2.15395846E-02    2           6        -6   # BR(H -> t       tb     )
     2.98861424E-05    2          21        21   # BR(H -> g       g      )
     1.42286252E-08    2          22        22   # BR(H -> gam     gam    )
     8.30611398E-09    2          23        22   # BR(H -> Z       gam    )
     2.97866945E-05    2          24       -24   # BR(H -> W+      W-     )
     1.48749803E-05    2          23        23   # BR(H -> Z       Z      )
     7.95867114E-05    2          25        25   # BR(H -> h       h      )
    -2.96343717E-23    2          36        36   # BR(H -> A       A      )
     2.00843508E-18    2          23        36   # BR(H -> Z       A      )
     2.29202484E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.10882145E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.14217242E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.14767346E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.92182674E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.59118848E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32764532E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83381293E-01    2           5        -5   # BR(A -> b       bb     )
     1.87302578E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62256230E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.13388175E-04    2           3        -3   # BR(A -> s       sb     )
     2.29526613E-07    2           4        -4   # BR(A -> c       cb     )
     2.28841511E-02    2           6        -6   # BR(A -> t       tb     )
     6.73916370E-05    2          21        21   # BR(A -> g       g      )
     3.13972397E-08    2          22        22   # BR(A -> gam     gam    )
     6.63572097E-08    2          23        22   # BR(A -> Z       gam    )
     3.15304937E-05    2          23        25   # BR(A -> Z       h      )
     2.62960885E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21421147E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31035329E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.95700570E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03019179E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09703245E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.41457933E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.53736358E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.02099668E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.01722649E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03220482E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13603939E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.06974844E-05    2          24        25   # BR(H+ -> W+      h      )
     8.60218103E-14    2          24        36   # BR(H+ -> W+      A      )
     1.99856041E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.76383539E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98586977E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
