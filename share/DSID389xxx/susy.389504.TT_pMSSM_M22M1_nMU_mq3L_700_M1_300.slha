#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14468816E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04022199E+01   # W+
        25     1.25391348E+02   # h
        35     3.00011373E+03   # H
        36     2.99999991E+03   # A
        37     3.00109281E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03314195E+03   # ~d_L
   2000001     3.02797612E+03   # ~d_R
   1000002     3.03223258E+03   # ~u_L
   2000002     3.03012864E+03   # ~u_R
   1000003     3.03314195E+03   # ~s_L
   2000003     3.02797612E+03   # ~s_R
   1000004     3.03223258E+03   # ~c_L
   2000004     3.03012864E+03   # ~c_R
   1000005     7.95653915E+02   # ~b_1
   2000005     3.02713774E+03   # ~b_2
   1000006     7.92869485E+02   # ~t_1
   2000006     3.02249730E+03   # ~t_2
   1000011     3.00669281E+03   # ~e_L
   2000011     3.00096960E+03   # ~e_R
   1000012     3.00530581E+03   # ~nu_eL
   1000013     3.00669281E+03   # ~mu_L
   2000013     3.00096960E+03   # ~mu_R
   1000014     3.00530581E+03   # ~nu_muL
   1000015     2.98618493E+03   # ~tau_1
   2000015     3.02181750E+03   # ~tau_2
   1000016     3.00544219E+03   # ~nu_tauL
   1000021     2.34107762E+03   # ~g
   1000022     3.02593041E+02   # ~chi_10
   1000023     6.32403779E+02   # ~chi_20
   1000025    -3.00043148E+03   # ~chi_30
   1000035     3.00069023E+03   # ~chi_40
   1000024     6.32566327E+02   # ~chi_1+
   1000037     3.00150773E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890414E-01   # N_11
  1  2    -8.33228842E-07   # N_12
  1  3    -1.47854159E-02   # N_13
  1  4    -7.42488449E-04   # N_14
  2  1     3.98469794E-04   # N_21
  2  2     9.99635566E-01   # N_22
  2  3     2.66879837E-02   # N_23
  2  4     4.04082420E-03   # N_24
  3  1    -9.92709261E-03   # N_31
  3  2     1.60168170E-02   # N_32
  3  3    -7.06848049E-01   # N_33
  3  4     7.07114383E-01   # N_34
  4  1     1.09751476E-02   # N_41
  4  2    -2.17300962E-02   # N_42
  4  3     7.06707138E-01   # N_43
  4  4     7.07087244E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99287681E-01   # U_11
  1  2     3.77376554E-02   # U_12
  2  1    -3.77376554E-02   # U_21
  2  2     9.99287681E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99983683E-01   # V_11
  1  2    -5.71256754E-03   # V_12
  2  1    -5.71256754E-03   # V_21
  2  2    -9.99983683E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98579832E-01   # cos(theta_t)
  1  2    -5.32758775E-02   # sin(theta_t)
  2  1     5.32758775E-02   # -sin(theta_t)
  2  2     9.98579832E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99725429E-01   # cos(theta_b)
  1  2    -2.34321704E-02   # sin(theta_b)
  2  1     2.34321704E-02   # -sin(theta_b)
  2  2     9.99725429E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06945583E-01   # cos(theta_tau)
  1  2     7.07267943E-01   # sin(theta_tau)
  2  1    -7.07267943E-01   # -sin(theta_tau)
  2  2    -7.06945583E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00121495E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.44688161E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44142543E+02   # higgs               
         4     1.08270952E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.44688161E+03  # The gauge couplings
     1     3.62030194E-01   # gprime(Q) DRbar
     2     6.37198716E-01   # g(Q) DRbar
     3     1.02714592E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.44688161E+03  # The trilinear couplings
  1  1     1.99228323E-06   # A_u(Q) DRbar
  2  2     1.99231256E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.44688161E+03  # The trilinear couplings
  1  1     7.11252399E-07   # A_d(Q) DRbar
  2  2     7.11334548E-07   # A_s(Q) DRbar
  3  3     1.44445303E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.44688161E+03  # The trilinear couplings
  1  1     3.09533969E-07   # A_e(Q) DRbar
  2  2     3.09549112E-07   # A_mu(Q) DRbar
  3  3     3.13798354E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.44688161E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53834785E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.44688161E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.85711284E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.44688161E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01593512E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.44688161E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.62074611E+04   # M^2_Hd              
        22    -9.07989765E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40928342E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.40544288E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47994691E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47994691E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52005309E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52005309E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.62119035E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.66256191E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.23374381E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17357603E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.53597566E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.09902832E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.23298549E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.66471832E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.93313005E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70381929E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61980742E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.17954718E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.47519504E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.62054140E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.37945860E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.45537746E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.99781854E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.35904664E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.71733522E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.61461913E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -8.97023314E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.41594630E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.32525222E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.03902987E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.51218402E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.78983134E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.04164753E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57524227E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.73988912E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.30915577E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.15205913E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.31424670E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.21228157E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17797844E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56982197E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.33203883E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.00902867E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.42253230E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43017764E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.79503388E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.03905956E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57412995E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.94254666E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.12782843E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.14640032E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.45189785E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.21907587E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66864091E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46603547E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.63383352E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.72499376E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.07008937E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55339635E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.78983134E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.04164753E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57524227E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.73988912E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.30915577E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.15205913E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.31424670E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.21228157E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17797844E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56982197E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.33203883E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.00902867E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.42253230E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43017764E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.79503388E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.03905956E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57412995E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.94254666E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.12782843E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.14640032E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.45189785E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.21907587E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66864091E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46603547E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.63383352E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.72499376E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.07008937E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55339635E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.70824539E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03555129E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99053383E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.10372612E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.25087567E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97391474E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.10762788E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53299245E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999852E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.48015334E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.29369721E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.26233863E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.70824539E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03555129E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99053383E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.10372612E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.25087567E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97391474E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.10762788E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53299245E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999852E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.48015334E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.29369721E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.26233863E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64548906E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.62074512E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12829125E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25096364E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59367878E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70297672E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10082360E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.08060127E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.09309736E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19589496E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.05726764E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.70832728E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03503725E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98612643E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.61273993E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.41146346E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97883629E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.36306031E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.70832728E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03503725E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98612643E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.61273993E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.41146346E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97883629E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.36306031E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.70885387E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03493918E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98586305E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.70385234E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.55548070E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97919379E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.93176050E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.12953899E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.25815363E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.37580488E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.63195203E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.26621692E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.95305034E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.93637430E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.79566770E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.23728591E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.08159709E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.76749334E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.23250666E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.27908661E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.09199684E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.84521672E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.77973494E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.77973494E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.23735032E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.12489292E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34638940E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34638940E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.76338922E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.76338922E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.10297559E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.10297559E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.18465817E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.03127981E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.11461882E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.84120668E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.84120668E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14579568E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.14039865E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31736499E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31736499E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.83312403E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.83312403E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.88002320E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.88002320E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.65799505E-03   # h decays
#          BR         NDA      ID1       ID2
     5.56016601E-01    2           5        -5   # BR(h -> b       bb     )
     7.11015672E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.51699758E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.33911298E-04    2           3        -3   # BR(h -> s       sb     )
     2.32018579E-02    2           4        -4   # BR(h -> c       cb     )
     7.57852896E-02    2          21        21   # BR(h -> g       g      )
     2.60639529E-03    2          22        22   # BR(h -> gam     gam    )
     1.76894053E-03    2          22        23   # BR(h -> Z       gam    )
     2.38691484E-01    2          24       -24   # BR(h -> W+      W-     )
     3.00422528E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.77744453E+01   # H decays
#          BR         NDA      ID1       ID2
     9.02050413E-01    2           5        -5   # BR(H -> b       bb     )
     6.58326390E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.32768480E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.85840036E-04    2           3        -3   # BR(H -> s       sb     )
     8.01122339E-08    2           4        -4   # BR(H -> c       cb     )
     7.99171022E-03    2           6        -6   # BR(H -> t       tb     )
     7.60951907E-06    2          21        21   # BR(H -> g       g      )
     5.68574296E-08    2          22        22   # BR(H -> gam     gam    )
     3.27754961E-09    2          23        22   # BR(H -> Z       gam    )
     6.74917132E-07    2          24       -24   # BR(H -> W+      W-     )
     3.37042533E-07    2          23        23   # BR(H -> Z       Z      )
     5.34744180E-06    2          25        25   # BR(H -> h       h      )
    -7.23387456E-25    2          36        36   # BR(H -> A       A      )
     6.56365522E-19    2          23        36   # BR(H -> Z       A      )
     7.24553743E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.97186455E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.62028720E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.44378849E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.22165893E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.25123955E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.69458559E+01   # A decays
#          BR         NDA      ID1       ID2
     9.22293569E-01    2           5        -5   # BR(A -> b       bb     )
     6.73069788E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.37981060E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.92290159E-04    2           3        -3   # BR(A -> s       sb     )
     8.24801400E-08    2           4        -4   # BR(A -> c       cb     )
     8.22339493E-03    2           6        -6   # BR(A -> t       tb     )
     2.42171114E-05    2          21        21   # BR(A -> g       g      )
     6.45860934E-08    2          22        22   # BR(A -> gam     gam    )
     2.38605852E-08    2          23        22   # BR(A -> Z       gam    )
     6.87410499E-07    2          23        25   # BR(A -> Z       h      )
     8.74128688E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.19072734E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.36704242E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.67970536E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.04947284E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47101738E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.14307084E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17204001E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.41449969E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.27645228E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.62607280E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.25413042E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.28246146E-07    2          24        25   # BR(H+ -> W+      h      )
     5.06605848E-14    2          24        36   # BR(H+ -> W+      A      )
     4.72499460E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.91821640E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07101146E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
