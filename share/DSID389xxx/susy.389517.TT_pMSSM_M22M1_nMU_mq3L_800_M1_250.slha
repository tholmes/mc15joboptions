#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15475403E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04003299E+01   # W+
        25     1.25166671E+02   # h
        35     3.00007274E+03   # H
        36     2.99999993E+03   # A
        37     3.00109114E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03702894E+03   # ~d_L
   2000001     3.03184183E+03   # ~d_R
   1000002     3.03611963E+03   # ~u_L
   2000002     3.03360770E+03   # ~u_R
   1000003     3.03702894E+03   # ~s_L
   2000003     3.03184183E+03   # ~s_R
   1000004     3.03611963E+03   # ~c_L
   2000004     3.03360770E+03   # ~c_R
   1000005     8.96968818E+02   # ~b_1
   2000005     3.03022143E+03   # ~b_2
   1000006     8.94608689E+02   # ~t_1
   2000006     3.02335157E+03   # ~t_2
   1000011     3.00659522E+03   # ~e_L
   2000011     3.00137612E+03   # ~e_R
   1000012     3.00520635E+03   # ~nu_eL
   1000013     3.00659522E+03   # ~mu_L
   2000013     3.00137612E+03   # ~mu_R
   1000014     3.00520635E+03   # ~nu_muL
   1000015     2.98612620E+03   # ~tau_1
   2000015     3.02177864E+03   # ~tau_2
   1000016     3.00520841E+03   # ~nu_tauL
   1000021     2.34584204E+03   # ~g
   1000022     2.51899917E+02   # ~chi_10
   1000023     5.30293075E+02   # ~chi_20
   1000025    -2.99803373E+03   # ~chi_30
   1000035     2.99821301E+03   # ~chi_40
   1000024     5.30456230E+02   # ~chi_1+
   1000037     2.99906349E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891070E-01   # N_11
  1  2     7.59147501E-05   # N_12
  1  3    -1.47512037E-02   # N_13
  1  4    -4.93322877E-04   # N_14
  2  1     3.15171451E-04   # N_21
  2  2     9.99646479E-01   # N_22
  2  3     2.64041935E-02   # N_23
  2  4     3.10424495E-03   # N_24
  3  1    -1.00802302E-02   # N_31
  3  2     1.64774500E-02   # N_32
  3  3    -7.06838282E-01   # N_33
  3  4     7.07111395E-01   # N_34
  4  1     1.07766770E-02   # N_41
  4  2    -2.08663602E-02   # N_42
  4  3     7.06728281E-01   # N_43
  4  4     7.07095181E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99302813E-01   # U_11
  1  2     3.73348026E-02   # U_12
  2  1    -3.73348026E-02   # U_21
  2  2     9.99302813E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990372E-01   # V_11
  1  2    -4.38804824E-03   # V_12
  2  1    -4.38804824E-03   # V_21
  2  2    -9.99990372E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98535970E-01   # cos(theta_t)
  1  2    -5.40917426E-02   # sin(theta_t)
  2  1     5.40917426E-02   # -sin(theta_t)
  2  2     9.98535970E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99718440E-01   # cos(theta_b)
  1  2    -2.37284792E-02   # sin(theta_b)
  2  1     2.37284792E-02   # -sin(theta_b)
  2  2     9.99718440E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06944344E-01   # cos(theta_tau)
  1  2     7.07269181E-01   # sin(theta_tau)
  2  1    -7.07269181E-01   # -sin(theta_tau)
  2  2    -7.06944344E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00138504E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.54754032E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44005086E+02   # higgs               
         4     1.05094741E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.54754032E+03  # The gauge couplings
     1     3.62256163E-01   # gprime(Q) DRbar
     2     6.37688740E-01   # g(Q) DRbar
     3     1.02563530E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.54754032E+03  # The trilinear couplings
  1  1     2.28260728E-06   # A_u(Q) DRbar
  2  2     2.28264170E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.54754032E+03  # The trilinear couplings
  1  1     8.06558771E-07   # A_d(Q) DRbar
  2  2     8.06654081E-07   # A_s(Q) DRbar
  3  3     1.65150271E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.54754032E+03  # The trilinear couplings
  1  1     3.54380683E-07   # A_e(Q) DRbar
  2  2     3.54398147E-07   # A_mu(Q) DRbar
  3  3     3.59318621E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.54754032E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52227364E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.54754032E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.83927470E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.54754032E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02028259E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.54754032E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.11380823E+04   # M^2_Hd              
        22    -9.05643745E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41153490E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.93933518E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47966022E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47966022E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52033978E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52033978E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.54979587E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.28266276E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.87688020E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.89485352E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15156683E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.68328336E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.10365217E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.24190977E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.04395391E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.96212014E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68806937E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61145088E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.15964175E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.35361695E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.64779516E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.64552630E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.08969418E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.40027366E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.05324124E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.27506161E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.93779934E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.83316529E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.06357715E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.45453186E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.20496737E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.02657587E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.49229223E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.87718058E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.02054949E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60138172E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.59020764E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.28565340E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.20446408E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.45599542E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.13394797E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17185601E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58529453E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.50384465E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02798050E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.16345640E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41470511E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88229979E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.00832763E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60035969E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.31454228E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.63743039E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.19878058E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.37779974E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.14077198E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66153883E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.51282986E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.28072853E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.65375259E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.00193354E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54871691E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.87718058E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.02054949E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60138172E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.59020764E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.28565340E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.20446408E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.45599542E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.13394797E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17185601E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58529453E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.50384465E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02798050E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.16345640E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41470511E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88229979E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.00832763E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60035969E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.31454228E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.63743039E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.19878058E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.37779974E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.14077198E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66153883E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.51282986E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.28072853E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.65375259E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.00193354E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54871691E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80835440E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01618369E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99677262E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.19380308E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.15240408E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98704343E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.22907098E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54481905E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999904E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.45760733E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.10769588E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.22870527E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.80835440E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01618369E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99677262E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.19380308E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.15240408E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98704343E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.22907098E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54481905E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999904E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.45760733E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.10769588E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.22870527E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70247212E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.57164194E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14451557E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28384250E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64856860E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65348077E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11714581E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.30203729E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.11971921E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22900161E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.29638681E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80850029E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01511926E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99295075E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.57702712E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.02411715E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99192990E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.05010063E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80850029E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01511926E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99295075E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.57702712E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.02411715E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99192990E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.05010063E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80883628E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01503043E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99268905E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.57876146E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.02662712E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99227030E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.01409629E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.69608695E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.98911346E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.35622629E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.57419557E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.90228567E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.19109732E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.00054657E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.02743093E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.47666759E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.68172922E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.05183645E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.94816355E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.01116265E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.09309388E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.80114017E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.01057522E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.01057522E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.90488391E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.41542859E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.31729259E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31729259E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.66637848E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.66637848E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.48923764E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.48923764E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.91484041E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.67240807E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.39970984E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.07722259E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.07722259E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14847385E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.08954436E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.28816067E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.28816067E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.73865219E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.73865219E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.41377920E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.41377920E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.63669567E-03   # h decays
#          BR         NDA      ID1       ID2
     5.60536041E-01    2           5        -5   # BR(h -> b       bb     )
     7.13943906E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.52737445E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.36299200E-04    2           3        -3   # BR(h -> s       sb     )
     2.33040794E-02    2           4        -4   # BR(h -> c       cb     )
     7.58672633E-02    2          21        21   # BR(h -> g       g      )
     2.60181212E-03    2          22        22   # BR(h -> gam     gam    )
     1.74400938E-03    2          22        23   # BR(h -> Z       gam    )
     2.34377854E-01    2          24       -24   # BR(h -> W+      W-     )
     2.93855135E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.63938293E+01   # H decays
#          BR         NDA      ID1       ID2
     8.98857172E-01    2           5        -5   # BR(H -> b       bb     )
     6.83290841E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.41595313E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.96679936E-04    2           3        -3   # BR(H -> s       sb     )
     8.31559919E-08    2           4        -4   # BR(H -> c       cb     )
     8.29534214E-03    2           6        -6   # BR(H -> t       tb     )
     9.71203686E-06    2          21        21   # BR(H -> g       g      )
     5.57832508E-08    2          22        22   # BR(H -> gam     gam    )
     3.39813526E-09    2          23        22   # BR(H -> Z       gam    )
     7.45523640E-07    2          24       -24   # BR(H -> W+      W-     )
     3.72302176E-07    2          23        23   # BR(H -> Z       Z      )
     5.63313310E-06    2          25        25   # BR(H -> h       h      )
    -1.46288889E-24    2          36        36   # BR(H -> A       A      )
     1.51535824E-19    2          23        36   # BR(H -> Z       A      )
     8.05313670E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.17632054E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.02439025E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.62423971E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24463248E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.25685400E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.55854015E+01   # A decays
#          BR         NDA      ID1       ID2
     9.19301665E-01    2           5        -5   # BR(A -> b       bb     )
     6.98801715E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.47079242E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.03464616E-04    2           3        -3   # BR(A -> s       sb     )
     8.56334131E-08    2           4        -4   # BR(A -> c       cb     )
     8.53778104E-03    2           6        -6   # BR(A -> t       tb     )
     2.51429483E-05    2          21        21   # BR(A -> g       g      )
     6.71386490E-08    2          22        22   # BR(A -> gam     gam    )
     2.47647255E-08    2          23        22   # BR(A -> Z       gam    )
     7.59585398E-07    2          23        25   # BR(A -> Z       h      )
     9.19437695E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.36574181E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.59419230E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.81244628E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.90040228E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46655319E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.37785103E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.25505256E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.38592884E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.32523672E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.72643809E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.22908558E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.94200308E-07    2          24        25   # BR(H+ -> W+      h      )
     5.21916522E-14    2          24        36   # BR(H+ -> W+      A      )
     5.01605961E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.66111895E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08232910E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
