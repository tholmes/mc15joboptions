#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419716E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04034881E+01   # W+
        25     1.24853922E+02   # h
        35     3.00004013E+03   # H
        36     2.99999996E+03   # A
        37     3.00108535E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04035680E+03   # ~d_L
   2000001     3.03520765E+03   # ~d_R
   1000002     3.03945218E+03   # ~u_L
   2000002     3.03666873E+03   # ~u_R
   1000003     3.04035680E+03   # ~s_L
   2000003     3.03520765E+03   # ~s_R
   1000004     3.03945218E+03   # ~c_L
   2000004     3.03666873E+03   # ~c_R
   1000005     1.00340975E+03   # ~b_1
   2000005     3.03304415E+03   # ~b_2
   1000006     1.00107949E+03   # ~t_1
   2000006     3.02427674E+03   # ~t_2
   1000011     3.00646017E+03   # ~e_L
   2000011     3.00168112E+03   # ~e_R
   1000012     3.00507571E+03   # ~nu_eL
   1000013     3.00646017E+03   # ~mu_L
   2000013     3.00168112E+03   # ~mu_R
   1000014     3.00507571E+03   # ~nu_muL
   1000015     2.98616701E+03   # ~tau_1
   2000015     3.02158331E+03   # ~tau_2
   1000016     3.00497044E+03   # ~nu_tauL
   1000021     2.35014361E+03   # ~g
   1000022     4.02480505E+02   # ~chi_10
   1000023     8.37217303E+02   # ~chi_20
   1000025    -2.99589663E+03   # ~chi_30
   1000035     2.99635728E+03   # ~chi_40
   1000024     8.37380422E+02   # ~chi_1+
   1000037     2.99709774E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888453E-01   # N_11
  1  2    -9.76786353E-05   # N_12
  1  3    -1.48833326E-02   # N_13
  1  4    -1.24798862E-03   # N_14
  2  1     5.13586186E-04   # N_21
  2  2     9.99605231E-01   # N_22
  2  3     2.74383896E-02   # N_23
  2  4     6.02100462E-03   # N_24
  3  1    -9.63764185E-03   # N_31
  3  2     1.51486284E-02   # N_32
  3  3    -7.06866120E-01   # N_33
  3  4     7.07119455E-01   # N_34
  4  1     1.13988023E-02   # N_41
  4  2    -2.36620161E-02   # N_42
  4  3     7.06658270E-01   # N_43
  4  4     7.07067370E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99246965E-01   # U_11
  1  2     3.88008083E-02   # U_12
  2  1    -3.88008083E-02   # U_21
  2  2     9.99246965E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99963764E-01   # V_11
  1  2    -8.51301583E-03   # V_12
  2  1    -8.51301583E-03   # V_21
  2  2    -9.99963764E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98481969E-01   # cos(theta_t)
  1  2    -5.50795568E-02   # sin(theta_t)
  2  1     5.50795568E-02   # -sin(theta_t)
  2  2     9.98481969E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99720402E-01   # cos(theta_b)
  1  2    -2.36456724E-02   # sin(theta_b)
  2  1     2.36456724E-02   # -sin(theta_b)
  2  2     9.99720402E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06964576E-01   # cos(theta_tau)
  1  2     7.07248958E-01   # sin(theta_tau)
  2  1    -7.07248958E-01   # -sin(theta_tau)
  2  2    -7.06964576E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00134718E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64197161E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43912902E+02   # higgs               
         4     1.03533808E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64197161E+03  # The gauge couplings
     1     3.62452781E-01   # gprime(Q) DRbar
     2     6.36713439E-01   # g(Q) DRbar
     3     1.02428267E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64197161E+03  # The trilinear couplings
  1  1     2.57906566E-06   # A_u(Q) DRbar
  2  2     2.57910253E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64197161E+03  # The trilinear couplings
  1  1     9.26696773E-07   # A_d(Q) DRbar
  2  2     9.26800736E-07   # A_s(Q) DRbar
  3  3     1.84788893E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64197161E+03  # The trilinear couplings
  1  1     3.79018593E-07   # A_e(Q) DRbar
  2  2     3.79036894E-07   # A_mu(Q) DRbar
  3  3     3.84134815E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64197161E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50608103E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64197161E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.74946047E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64197161E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00944957E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64197161E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.10935632E+04   # M^2_Hd              
        22    -9.03355102E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40708209E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.39490044E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47895959E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47895959E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52104041E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52104041E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.54822602E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.10340421E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.89659579E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12265927E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.71893029E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.60047931E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.24433088E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.33631233E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.01343262E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67615846E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59938255E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12695222E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.98238104E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.14440476E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.85559524E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.26484135E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.07455248E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.18949796E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.11510787E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.92379816E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -2.45257699E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.59343646E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.89415086E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.83075454E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.42561329E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56398326E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.18276967E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52456302E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.93095962E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.47976381E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.05052678E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.26796414E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.36308123E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14729394E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56572880E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.66427425E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.23183529E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.68557619E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43427054E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.56930948E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.19251278E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52334488E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.94995492E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.12243647E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.04490835E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.90331516E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.36981512E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64846029E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44715713E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04064076E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.26266028E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.46074130E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55528411E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56398326E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.18276967E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52456302E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.93095962E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.47976381E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.05052678E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.26796414E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.36308123E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14729394E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56572880E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.66427425E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.23183529E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.68557619E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43427054E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.56930948E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.19251278E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52334488E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.94995492E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.12243647E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.04490835E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.90331516E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.36981512E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64846029E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44715713E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04064076E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.26266028E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.46074130E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55528411E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47020347E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.09131423E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97226931E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.60507221E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.63841890E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93641599E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.06792555E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51276862E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999764E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.32668407E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.42830296E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.69271469E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47020347E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.09131423E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97226931E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.60507221E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.63841890E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93641599E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.06792555E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51276862E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999764E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.32668407E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.42830296E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.69271469E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51373350E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75821637E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08266819E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15911544E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46670204E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84172468E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05473765E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60256313E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.38349568E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10307768E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.61381685E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47012312E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09154927E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96703584E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.53253052E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.06576793E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94141472E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.42323233E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47012312E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09154927E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96703584E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.53253052E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.06576793E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94141472E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.42323233E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47029599E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09145386E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96674912E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.40646805E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.04020726E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94177854E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.83152020E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.36624068E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.54930650E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.30682594E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.59340228E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.86746178E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.23711961E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.06967553E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.06745356E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.62344036E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.10675080E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.53605617E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.46394383E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.56444437E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.24008091E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.41071209E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.05199623E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.05199623E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.21209401E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.84418593E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33317018E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33317018E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.40817571E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.40817571E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.21273481E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.21273481E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.47645426E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.96357627E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.83171155E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.11819221E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.11819221E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30921523E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.81633499E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.29794825E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.29794825E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.47549148E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.47549148E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.40910818E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.40910818E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.58647147E-03   # h decays
#          BR         NDA      ID1       ID2
     5.64436992E-01    2           5        -5   # BR(h -> b       bb     )
     7.22117635E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.55632507E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.42706883E-04    2           3        -3   # BR(h -> s       sb     )
     2.35828959E-02    2           4        -4   # BR(h -> c       cb     )
     7.64000719E-02    2          21        21   # BR(h -> g       g      )
     2.61082969E-03    2          22        22   # BR(h -> gam     gam    )
     1.71983412E-03    2          22        23   # BR(h -> Z       gam    )
     2.29585610E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86536629E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.48896028E+01   # H decays
#          BR         NDA      ID1       ID2
     8.95758413E-01    2           5        -5   # BR(H -> b       bb     )
     7.12742446E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.52008697E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.09468056E-04    2           3        -3   # BR(H -> s       sb     )
     8.67390395E-08    2           4        -4   # BR(H -> c       cb     )
     8.65277199E-03    2           6        -6   # BR(H -> t       tb     )
     1.23482443E-05    2          21        21   # BR(H -> g       g      )
     7.45078039E-08    2          22        22   # BR(H -> gam     gam    )
     3.54698190E-09    2          23        22   # BR(H -> Z       gam    )
     7.67058407E-07    2          24       -24   # BR(H -> W+      W-     )
     3.83056402E-07    2          23        23   # BR(H -> Z       Z      )
     5.99740770E-06    2          25        25   # BR(H -> h       h      )
    -1.98557470E-24    2          36        36   # BR(H -> A       A      )
     7.77973638E-20    2          23        36   # BR(H -> Z       A      )
     6.39584544E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.16076283E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.19526213E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.41104750E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24865531E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.05696188E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.41225425E+01   # A decays
#          BR         NDA      ID1       ID2
     9.15928057E-01    2           5        -5   # BR(A -> b       bb     )
     7.28759876E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.57671717E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.16474374E-04    2           3        -3   # BR(A -> s       sb     )
     8.93045825E-08    2           4        -4   # BR(A -> c       cb     )
     8.90380220E-03    2           6        -6   # BR(A -> t       tb     )
     2.62208452E-05    2          21        21   # BR(A -> g       g      )
     6.69464581E-08    2          22        22   # BR(A -> gam     gam    )
     2.58404655E-08    2          23        22   # BR(A -> Z       gam    )
     7.81390479E-07    2          23        25   # BR(A -> Z       h      )
     9.09233713E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.50801756E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.54141130E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.82368243E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.75661676E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46224259E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.62195221E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.34136078E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.35834101E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.37595820E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.83078848E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.20511107E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.10977249E-07    2          24        25   # BR(H+ -> W+      h      )
     5.27595086E-14    2          24        36   # BR(H+ -> W+      A      )
     4.80369534E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.11094108E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07857149E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
