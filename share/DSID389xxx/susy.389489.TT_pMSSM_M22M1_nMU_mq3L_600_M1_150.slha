#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13385194E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03979658E+01   # W+
        25     1.25806538E+02   # h
        35     3.00014308E+03   # H
        36     2.99999993E+03   # A
        37     3.00109943E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02884165E+03   # ~d_L
   2000001     3.02347378E+03   # ~d_R
   1000002     3.02792563E+03   # ~u_L
   2000002     3.02607464E+03   # ~u_R
   1000003     3.02884165E+03   # ~s_L
   2000003     3.02347378E+03   # ~s_R
   1000004     3.02792563E+03   # ~c_L
   2000004     3.02607464E+03   # ~c_R
   1000005     6.85119868E+02   # ~b_1
   2000005     3.02348301E+03   # ~b_2
   1000006     6.82084874E+02   # ~t_1
   2000006     3.02122159E+03   # ~t_2
   1000011     3.00704077E+03   # ~e_L
   2000011     3.00053492E+03   # ~e_R
   1000012     3.00564740E+03   # ~nu_eL
   1000013     3.00704077E+03   # ~mu_L
   2000013     3.00053492E+03   # ~mu_R
   1000014     3.00564740E+03   # ~nu_muL
   1000015     2.98621564E+03   # ~tau_1
   2000015     3.02216654E+03   # ~tau_2
   1000016     3.00593761E+03   # ~nu_tauL
   1000021     2.33559147E+03   # ~g
   1000022     1.51611380E+02   # ~chi_10
   1000023     3.22195350E+02   # ~chi_20
   1000025    -3.00326905E+03   # ~chi_30
   1000035     3.00327620E+03   # ~chi_40
   1000024     3.22356957E+02   # ~chi_1+
   1000037     3.00420659E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891879E-01   # N_11
  1  2     3.82022131E-04   # N_12
  1  3    -1.46998260E-02   # N_13
  1  4     3.31789273E-07   # N_14
  2  1     1.66998278E-06   # N_21
  2  2     9.99658662E-01   # N_22
  2  3     2.60929597E-02   # N_23
  2  4     1.31032380E-03   # N_24
  3  1     1.03975931E-02   # N_31
  3  2    -1.93747253E-02   # N_32
  3  3     7.06763601E-01   # N_33
  3  4     7.07107999E-01   # N_34
  4  1    -1.03981191E-02   # N_41
  4  2     1.75223736E-02   # N_42
  4  3    -7.06815595E-01   # N_43
  4  4     7.07104349E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99319391E-01   # U_11
  1  2     3.68884030E-02   # U_12
  2  1    -3.68884030E-02   # U_21
  2  2     9.99319391E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998287E-01   # V_11
  1  2    -1.85119208E-03   # V_12
  2  1    -1.85119208E-03   # V_21
  2  2    -9.99998287E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98614133E-01   # cos(theta_t)
  1  2    -5.26290165E-02   # sin(theta_t)
  2  1     5.26290165E-02   # -sin(theta_t)
  2  2     9.98614133E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99717664E-01   # cos(theta_b)
  1  2    -2.37611508E-02   # sin(theta_b)
  2  1     2.37611508E-02   # -sin(theta_b)
  2  2     9.99717664E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06916153E-01   # cos(theta_tau)
  1  2     7.07297358E-01   # sin(theta_tau)
  2  1    -7.07297358E-01   # -sin(theta_tau)
  2  2    -7.06916153E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00143356E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33851943E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44294530E+02   # higgs               
         4     1.10629503E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.33851943E+03  # The gauge couplings
     1     3.61769846E-01   # gprime(Q) DRbar
     2     6.38654595E-01   # g(Q) DRbar
     3     1.02890041E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33851943E+03  # The trilinear couplings
  1  1     1.68529951E-06   # A_u(Q) DRbar
  2  2     1.68532560E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33851943E+03  # The trilinear couplings
  1  1     5.95000571E-07   # A_d(Q) DRbar
  2  2     5.95072611E-07   # A_s(Q) DRbar
  3  3     1.23856685E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33851943E+03  # The trilinear couplings
  1  1     2.78626958E-07   # A_e(Q) DRbar
  2  2     2.78640859E-07   # A_mu(Q) DRbar
  3  3     2.82590115E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33851943E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55729083E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33851943E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.96863690E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33851943E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02805544E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33851943E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.10953555E+04   # M^2_Hd              
        22    -9.11049473E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41591828E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.85186574E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48041910E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48041910E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51958090E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51958090E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.96153724E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.62930502E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.82598721E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.01108229E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20157236E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.47123428E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.60422975E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.23852968E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.91919976E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.89243705E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71096252E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63112131E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.21002101E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.70146081E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.01070878E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.74597362E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.05295550E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.61442807E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.94526515E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -6.61709902E-09    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60566471E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.56329737E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.34202352E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.26108035E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.65171257E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.08942848E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.58947792E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02320369E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.92843305E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62837520E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.56514370E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.24911541E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25889059E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.28128987E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.05344959E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19730634E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57976698E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.33475601E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.57930892E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.57697701E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42023292E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02827131E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.87824885E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62772693E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.52106752E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.10123586E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25317570E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.40125291E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.06031302E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67892506E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.50189232E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.23524737E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.03259709E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.03198981E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54981075E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02320369E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.92843305E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62837520E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.56514370E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.24911541E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25889059E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.28128987E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.05344959E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19730634E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57976698E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.33475601E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.57930892E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.57697701E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42023292E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02827131E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.87824885E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62772693E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.52106752E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.10123586E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25317570E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.40125291E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.06031302E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67892506E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.50189232E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.23524737E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.03259709E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.03198981E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54981075E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96235893E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.84082028E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00671681E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.51909828E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.60865574E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00920112E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.97477573E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55420827E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.73946045E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.96235893E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.84082028E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00671681E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.51909828E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.60865574E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00920112E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.97477573E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55420827E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.73946045E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.78598984E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48809388E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17185244E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34005369E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72882434E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56909292E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14476930E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.28259354E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.81994469E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28590762E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.91394554E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96262419E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.80905561E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00504630E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.91587402E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.19622538E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01404813E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.93780238E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96262419E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.80905561E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00504630E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.91587402E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.19622538E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01404813E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.93780238E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96335574E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.80820167E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00479522E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.18641631E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.28423337E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01438460E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.40514821E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.66154032E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.43590266E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.58590780E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.07922653E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.94945156E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.89122227E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.79831208E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.14283875E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.29669801E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.24342373E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.75657627E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.62458426E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.71276618E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.68578971E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.82248928E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.82248928E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.17892035E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.23395567E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.37036873E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.37036873E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.11928358E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.11928358E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.90929082E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.90929082E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.90929082E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.90929082E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.28277978E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.28277978E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.74830668E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.59548805E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.24689638E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.74791018E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.74791018E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.20776644E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.14893119E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.24401141E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.24401141E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.03033753E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.03033753E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.88768808E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.88768808E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.88768808E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.88768808E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     6.67762275E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.67762275E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.73078631E-03   # h decays
#          BR         NDA      ID1       ID2
     5.51183331E-01    2           5        -5   # BR(h -> b       bb     )
     6.99518071E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47627644E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.24935068E-04    2           3        -3   # BR(h -> s       sb     )
     2.28097779E-02    2           4        -4   # BR(h -> c       cb     )
     7.49504211E-02    2          21        21   # BR(h -> g       g      )
     2.59113185E-03    2          22        22   # BR(h -> gam     gam    )
     1.79883148E-03    2          22        23   # BR(h -> Z       gam    )
     2.44936517E-01    2          24       -24   # BR(h -> W+      W-     )
     3.10056195E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.96703133E+01   # H decays
#          BR         NDA      ID1       ID2
     9.05991753E-01    2           5        -5   # BR(H -> b       bb     )
     6.26870573E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.21646454E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.72181820E-04    2           3        -3   # BR(H -> s       sb     )
     7.62909337E-08    2           4        -4   # BR(H -> c       cb     )
     7.61051260E-03    2           6        -6   # BR(H -> t       tb     )
     5.67162111E-06    2          21        21   # BR(H -> g       g      )
     4.23812240E-08    2          22        22   # BR(H -> gam     gam    )
     3.11657290E-09    2          23        22   # BR(H -> Z       gam    )
     6.96018752E-07    2          24       -24   # BR(H -> W+      W-     )
     3.47580186E-07    2          23        23   # BR(H -> Z       Z      )
     5.06237774E-06    2          25        25   # BR(H -> h       h      )
     2.05384347E-24    2          36        36   # BR(H -> A       A      )
     1.64535439E-18    2          23        36   # BR(H -> Z       A      )
     8.15425660E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.89422189E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.07746363E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.53087738E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.16843830E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.36457194E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.88143361E+01   # A decays
#          BR         NDA      ID1       ID2
     9.25976075E-01    2           5        -5   # BR(A -> b       bb     )
     6.40668939E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.26524910E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.78219628E-04    2           3        -3   # BR(A -> s       sb     )
     7.85096355E-08    2           4        -4   # BR(A -> c       cb     )
     7.82752963E-03    2           6        -6   # BR(A -> t       tb     )
     2.30513258E-05    2          21        21   # BR(A -> g       g      )
     5.85455825E-08    2          22        22   # BR(A -> gam     gam    )
     2.26953187E-08    2          23        22   # BR(A -> Z       gam    )
     7.08597499E-07    2          23        25   # BR(A -> Z       h      )
     8.64994932E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.02102491E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.32506161E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.63126067E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.22723472E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47596497E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.88475801E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08070690E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.44616427E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22277780E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51564713E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.28190989E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.51755710E-07    2          24        25   # BR(H+ -> W+      h      )
     5.00134153E-14    2          24        36   # BR(H+ -> W+      A      )
     4.79614402E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.38297505E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05238906E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
