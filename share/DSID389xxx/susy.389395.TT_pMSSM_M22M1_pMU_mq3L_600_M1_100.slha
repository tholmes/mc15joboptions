#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13424954E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03967805E+01   # W+
        25     1.24527275E+02   # h
        35     3.00017693E+03   # H
        36     2.99999963E+03   # A
        37     3.00094657E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02921088E+03   # ~d_L
   2000001     3.02393808E+03   # ~d_R
   1000002     3.02828893E+03   # ~u_L
   2000002     3.02565125E+03   # ~u_R
   1000003     3.02921088E+03   # ~s_L
   2000003     3.02393808E+03   # ~s_R
   1000004     3.02828893E+03   # ~c_L
   2000004     3.02565125E+03   # ~c_R
   1000005     6.80850663E+02   # ~b_1
   2000005     3.02333175E+03   # ~b_2
   1000006     6.78600908E+02   # ~t_1
   2000006     3.01018571E+03   # ~t_2
   1000011     3.00664988E+03   # ~e_L
   2000011     3.00143574E+03   # ~e_R
   1000012     3.00524881E+03   # ~nu_eL
   1000013     3.00664988E+03   # ~mu_L
   2000013     3.00143574E+03   # ~mu_R
   1000014     3.00524881E+03   # ~nu_muL
   1000015     2.98593544E+03   # ~tau_1
   2000015     3.02209896E+03   # ~tau_2
   1000016     3.00525692E+03   # ~nu_tauL
   1000021     2.33581786E+03   # ~g
   1000022     1.00510372E+02   # ~chi_10
   1000023     2.15021101E+02   # ~chi_20
   1000025    -3.00018236E+03   # ~chi_30
   1000035     3.00065276E+03   # ~chi_40
   1000024     2.15180725E+02   # ~chi_1+
   1000037     3.00134396E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888468E-01   # N_11
  1  2    -1.53822427E-03   # N_12
  1  3     1.48043974E-02   # N_13
  1  4    -1.23072922E-03   # N_14
  2  1     1.92955869E-03   # N_21
  2  2     9.99650162E-01   # N_22
  2  3    -2.62025012E-02   # N_23
  2  4     3.04299311E-03   # N_24
  3  1    -9.56976493E-03   # N_31
  3  2     1.63931369E-02   # N_32
  3  3     7.06830357E-01   # N_33
  3  4     7.07128370E-01   # N_34
  4  1    -1.13025401E-02   # N_41
  4  2     2.06991063E-02   # N_42
  4  3     7.06742602E-01   # N_43
  4  4    -7.07077573E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99312153E-01   # U_11
  1  2    -3.70839666E-02   # U_12
  2  1     3.70839666E-02   # U_21
  2  2     9.99312153E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990721E-01   # V_11
  1  2    -4.30793904E-03   # V_12
  2  1     4.30793904E-03   # V_21
  2  2     9.99990721E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98869835E-01   # cos(theta_t)
  1  2    -4.75294932E-02   # sin(theta_t)
  2  1     4.75294932E-02   # -sin(theta_t)
  2  2     9.98869835E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99919495E-01   # cos(theta_b)
  1  2     1.26887162E-02   # sin(theta_b)
  2  1    -1.26887162E-02   # -sin(theta_b)
  2  2     9.99919495E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06901563E-01   # cos(theta_tau)
  1  2     7.07311940E-01   # sin(theta_tau)
  2  1    -7.07311940E-01   # -sin(theta_tau)
  2  2     7.06901563E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01878188E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.34249542E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44503347E+02   # higgs               
         4     7.07868817E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.34249542E+03  # The gauge couplings
     1     3.61797139E-01   # gprime(Q) DRbar
     2     6.39749031E-01   # g(Q) DRbar
     3     1.02884752E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.34249542E+03  # The trilinear couplings
  1  1     1.68169476E-06   # A_u(Q) DRbar
  2  2     1.68172066E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.34249542E+03  # The trilinear couplings
  1  1     4.17895913E-07   # A_d(Q) DRbar
  2  2     4.17980331E-07   # A_s(Q) DRbar
  3  3     9.69940487E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.34249542E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.20930474E-08   # A_mu(Q) DRbar
  3  3     9.32028020E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.34249542E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54421772E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.34249542E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11619972E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.34249542E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05119124E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.34249542E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.56131034E+04   # M^2_Hd              
        22    -9.09710158E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42082769E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.86386190E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48249706E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48249706E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51750294E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51750294E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.72785228E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.26409084E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.96080565E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.91278527E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.04456279E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.28904290E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.82185935E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.66287564E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.35999814E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.23735539E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.60372241E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51552747E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.00586970E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.45167169E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.55322580E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.62676392E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.21791350E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.23538337E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.91838171E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.97772911E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.76133700E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.59457272E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.01389646E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29044193E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.90180295E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.20834958E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.06549408E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07780323E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.77908331E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64206090E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.41150576E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.21266651E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28392421E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.26742000E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.01622367E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19380728E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58487674E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.86104590E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.81497009E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.47718311E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41511729E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08287570E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.96844924E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63902107E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.33591226E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.56165882E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27816575E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.79206632E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02312630E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68142515E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.51101123E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.66820689E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.19811232E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.60744933E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54889718E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07780323E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.77908331E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64206090E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.41150576E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.21266651E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28392421E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.26742000E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.01622367E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19380728E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58487674E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.86104590E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.81497009E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.47718311E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41511729E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08287570E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.96844924E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63902107E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.33591226E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.56165882E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27816575E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.79206632E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02312630E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68142515E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.51101123E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.66820689E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.19811232E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.60744933E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54889718E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02153817E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.65799459E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01717206E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.77861305E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.89535603E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01702836E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.04102549E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55937146E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996306E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.69416760E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.40115644E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.48507322E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02153817E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.65799459E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01717206E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.77861305E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.89535603E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01702836E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.04102549E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55937146E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996306E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.69416760E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.40115644E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.48507322E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.81948484E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45287776E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18661262E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36050962E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75947908E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53431103E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15961949E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.36957171E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.07327267E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30576174E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.06714627E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02187755E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.75826459E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00234453E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.64158098E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.08557689E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02182898E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.61523763E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02187755E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.75826459E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00234453E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.64158098E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.08557689E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02182898E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.61523763E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02222593E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.75744584E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00209275E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.64668911E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.09275106E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02215881E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.81453512E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.42194104E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.73091520E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.63873875E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.26229607E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.89569765E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.85560616E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.11857263E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.70144904E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.06261382E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.72362254E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.76126727E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.27489350E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.18906613E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.66388839E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.66388839E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.37696070E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.67201189E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65694778E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65694778E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.22974794E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.22974794E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.20735105E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.20735105E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.66945669E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.23616176E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.65697537E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.73620052E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.73620052E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.32447501E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.39922262E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63560490E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.63560490E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.25553520E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.25553520E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.07203823E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.07203823E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.82211533E-03   # h decays
#          BR         NDA      ID1       ID2
     6.82679569E-01    2           5        -5   # BR(h -> b       bb     )
     5.39407980E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.90953780E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.05564449E-04    2           3        -3   # BR(h -> s       sb     )
     1.75026509E-02    2           4        -4   # BR(h -> c       cb     )
     5.67621674E-02    2          21        21   # BR(h -> g       g      )
     1.91968722E-03    2          22        22   # BR(h -> gam     gam    )
     1.24171810E-03    2          22        23   # BR(h -> Z       gam    )
     1.64904193E-01    2          24       -24   # BR(h -> W+      W-     )
     2.04526987E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39588072E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43151957E-01    2           5        -5   # BR(H -> b       bb     )
     1.78152758E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29905578E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.73522645E-04    2           3        -3   # BR(H -> s       sb     )
     2.18323067E-07    2           4        -4   # BR(H -> c       cb     )
     2.17791392E-02    2           6        -6   # BR(H -> t       tb     )
     3.20374925E-05    2          21        21   # BR(H -> g       g      )
     2.42985206E-08    2          22        22   # BR(H -> gam     gam    )
     8.35893801E-09    2          23        22   # BR(H -> Z       gam    )
     3.32724075E-05    2          24       -24   # BR(H -> W+      W-     )
     1.66156851E-05    2          23        23   # BR(H -> Z       Z      )
     8.63688925E-05    2          25        25   # BR(H -> h       h      )
    -1.69889030E-23    2          36        36   # BR(H -> A       A      )
     1.37615102E-17    2          23        36   # BR(H -> Z       A      )
     2.39646337E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13102333E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19318727E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.36335408E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.08989794E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.10519049E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32504208E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82946664E-01    2           5        -5   # BR(A -> b       bb     )
     1.87670547E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63557278E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.14986139E-04    2           3        -3   # BR(A -> s       sb     )
     2.29977536E-07    2           4        -4   # BR(A -> c       cb     )
     2.29291088E-02    2           6        -6   # BR(A -> t       tb     )
     6.75240399E-05    2          21        21   # BR(A -> g       g      )
     3.34649434E-08    2          22        22   # BR(A -> gam     gam    )
     6.64676085E-08    2          23        22   # BR(A -> Z       gam    )
     3.49172244E-05    2          23        25   # BR(A -> Z       h      )
     2.63821067E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21693127E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31352308E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.98939211E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.99481632E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.09149796E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.48878874E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.79974994E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.98557595E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.17142029E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.06392745E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.11039923E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.63636270E-05    2          24        25   # BR(H+ -> W+      h      )
     1.00236375E-13    2          24        36   # BR(H+ -> W+      A      )
     2.07955645E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.73769898E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.48611751E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
