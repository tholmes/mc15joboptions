#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14500048E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03962239E+01   # W+
        25     1.24267213E+02   # h
        35     3.00014587E+03   # H
        36     2.99999975E+03   # A
        37     3.00092824E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03356556E+03   # ~d_L
   2000001     3.02833274E+03   # ~d_R
   1000002     3.03264512E+03   # ~u_L
   2000002     3.02979326E+03   # ~u_R
   1000003     3.03356556E+03   # ~s_L
   2000003     3.02833274E+03   # ~s_R
   1000004     3.03264512E+03   # ~c_L
   2000004     3.02979326E+03   # ~c_R
   1000005     7.89653105E+02   # ~b_1
   2000005     3.02759907E+03   # ~b_2
   1000006     7.87807476E+02   # ~t_1
   2000006     3.01467181E+03   # ~t_2
   1000011     3.00653432E+03   # ~e_L
   2000011     3.00169384E+03   # ~e_R
   1000012     3.00513303E+03   # ~nu_eL
   1000013     3.00653432E+03   # ~mu_L
   2000013     3.00169384E+03   # ~mu_R
   1000014     3.00513303E+03   # ~nu_muL
   1000015     2.98587542E+03   # ~tau_1
   2000015     3.02201791E+03   # ~tau_2
   1000016     3.00504749E+03   # ~nu_tauL
   1000021     2.34125032E+03   # ~g
   1000022     1.00373270E+02   # ~chi_10
   1000023     2.15181620E+02   # ~chi_20
   1000025    -2.99839884E+03   # ~chi_30
   1000035     2.99887565E+03   # ~chi_40
   1000024     2.15341388E+02   # ~chi_1+
   1000037     2.99956562E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888461E-01   # N_11
  1  2    -1.53750657E-03   # N_12
  1  3     1.48049779E-02   # N_13
  1  4    -1.23077744E-03   # N_14
  2  1     1.92865910E-03   # N_21
  2  2     9.99650515E-01   # N_22
  2  3    -2.61892584E-02   # N_23
  2  4     3.04145815E-03   # N_24
  3  1    -9.57016882E-03   # N_31
  3  2     1.63848507E-02   # N_32
  3  3     7.06830560E-01   # N_33
  3  4     7.07128353E-01   # N_34
  4  1    -1.13030196E-02   # N_41
  4  2     2.06886488E-02   # N_42
  4  3     7.06742877E-01   # N_43
  4  4    -7.07077596E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99312848E-01   # U_11
  1  2    -3.70652402E-02   # U_12
  2  1     3.70652402E-02   # U_21
  2  2     9.99312848E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990730E-01   # V_11
  1  2    -4.30576660E-03   # V_12
  2  1     4.30576660E-03   # V_21
  2  2     9.99990730E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98841651E-01   # cos(theta_t)
  1  2    -4.81181486E-02   # sin(theta_t)
  2  1     4.81181486E-02   # -sin(theta_t)
  2  2     9.98841651E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99916693E-01   # cos(theta_b)
  1  2     1.29076357E-02   # sin(theta_b)
  2  1    -1.29076357E-02   # -sin(theta_b)
  2  2     9.99916693E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06907011E-01   # cos(theta_tau)
  1  2     7.07306495E-01   # sin(theta_tau)
  2  1    -7.07306495E-01   # -sin(theta_tau)
  2  2     7.06907011E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01839809E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.45000482E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44339518E+02   # higgs               
         4     7.37377767E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.45000482E+03  # The gauge couplings
     1     3.62054919E-01   # gprime(Q) DRbar
     2     6.39854253E-01   # g(Q) DRbar
     3     1.02712426E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.45000482E+03  # The trilinear couplings
  1  1     1.97746214E-06   # A_u(Q) DRbar
  2  2     1.97749261E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.45000482E+03  # The trilinear couplings
  1  1     4.93972939E-07   # A_d(Q) DRbar
  2  2     4.94071756E-07   # A_s(Q) DRbar
  3  3     1.14223504E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.45000482E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09140578E-07   # A_mu(Q) DRbar
  3  3     1.10464933E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.45000482E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52636264E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.45000482E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12175244E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.45000482E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05174615E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.45000482E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.36996576E+04   # M^2_Hd              
        22    -9.07068627E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42131975E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.42742365E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48233340E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48233340E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51766660E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51766660E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.33229490E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.24577236E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.06580951E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.80961325E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02886153E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.40443827E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.91254039E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.84514701E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.42334910E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.27910610E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.58152817E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50694635E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.98277553E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.09932151E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.44314586E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.51845812E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.33722730E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.22118524E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.94325547E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.30277718E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.24655926E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.22509118E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.64662300E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29509446E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.88515657E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.18759717E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.03054884E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07553604E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.79849928E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64574730E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.06781882E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.25971566E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29129415E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.37195241E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.00497300E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18495244E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59291511E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.88527452E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.24718901E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.81286736E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40707883E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08059004E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.98831131E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64270265E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.38546797E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.18906679E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28553168E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.63296420E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.01187906E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67315045E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53490489E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.67547976E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.88247818E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.54517310E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54650779E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07553604E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.79849928E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64574730E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.06781882E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.25971566E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29129415E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.37195241E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.00497300E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18495244E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59291511E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.88527452E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.24718901E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.81286736E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40707883E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08059004E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.98831131E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64270265E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.38546797E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.18906679E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28553168E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.63296420E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.01187906E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67315045E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53490489E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.67547976E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.88247818E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.54517310E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54650779E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02308259E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.66778829E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01684398E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06973113E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.60968437E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01637698E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.79290428E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56173877E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996308E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.69064958E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.42045959E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.51144753E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02308259E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.66778829E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01684398E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06973113E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.60968437E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01637698E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.79290428E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56173877E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996308E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.69064958E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.42045959E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.51144753E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82140230E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45536643E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18578064E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35885293E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76137714E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53688616E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15874145E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.09482471E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.24245768E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30401387E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.24782486E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02341810E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.76806109E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00201966E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.89717433E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.86171642E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02117416E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.54559537E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02341810E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.76806109E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00201966E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.89717433E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.86171642E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02117416E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.54559537E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02364016E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.76724271E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00176677E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.82404480E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.75686318E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02150097E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.92008082E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.51439557E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.50449244E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.64728121E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.20464510E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.78681213E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.02242227E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.17206652E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.86278370E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.24894744E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.74281198E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.53398520E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.30740124E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.29099884E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.82432458E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.82432458E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.63534656E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.74189279E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.62904911E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.62904911E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.23323009E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.23323009E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.04739475E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.04739475E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.44268948E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.47383149E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.72225322E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.89994992E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.89994992E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.36111320E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.51695785E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.60666083E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.60666083E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.25953062E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.25953062E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.74491783E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.74491783E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.75830381E-03   # h decays
#          BR         NDA      ID1       ID2
     6.84413595E-01    2           5        -5   # BR(h -> b       bb     )
     5.45413989E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93080930E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.10250246E-04    2           3        -3   # BR(h -> s       sb     )
     1.77075675E-02    2           4        -4   # BR(h -> c       cb     )
     5.70878415E-02    2          21        21   # BR(h -> g       g      )
     1.92934945E-03    2          22        22   # BR(h -> gam     gam    )
     1.22895967E-03    2          22        23   # BR(h -> Z       gam    )
     1.62432286E-01    2          24       -24   # BR(h -> W+      W-     )
     2.00556701E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39920104E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44837189E-01    2           5        -5   # BR(H -> b       bb     )
     1.77728227E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28404541E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.71680435E-04    2           3        -3   # BR(H -> s       sb     )
     2.17769744E-07    2           4        -4   # BR(H -> c       cb     )
     2.17239367E-02    2           6        -6   # BR(H -> t       tb     )
     2.97180483E-05    2          21        21   # BR(H -> g       g      )
     1.50935638E-08    2          22        22   # BR(H -> gam     gam    )
     8.35011708E-09    2          23        22   # BR(H -> Z       gam    )
     3.20911908E-05    2          24       -24   # BR(H -> W+      W-     )
     1.60258038E-05    2          23        23   # BR(H -> Z       Z      )
     8.44646133E-05    2          25        25   # BR(H -> h       h      )
    -1.28084347E-23    2          36        36   # BR(H -> A       A      )
     5.17530647E-18    2          23        36   # BR(H -> Z       A      )
     2.38819387E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12848933E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.18907090E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.34256001E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.97177047E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.94665341E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32986802E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83739903E-01    2           5        -5   # BR(A -> b       bb     )
     1.86989519E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61149331E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12028674E-04    2           3        -3   # BR(A -> s       sb     )
     2.29142982E-07    2           4        -4   # BR(A -> c       cb     )
     2.28459025E-02    2           6        -6   # BR(A -> t       tb     )
     6.72790019E-05    2          21        21   # BR(A -> g       g      )
     3.33551206E-08    2          22        22   # BR(A -> gam     gam    )
     6.62200518E-08    2          23        22   # BR(A -> Z       gam    )
     3.36369974E-05    2          23        25   # BR(A -> Z       h      )
     2.62590542E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21267290E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30739747E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.95682347E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01195759E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09891548E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.45809059E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.69120876E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.03304805E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.10763706E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05080518E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.15300954E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.42729272E-05    2          24        25   # BR(H+ -> W+      h      )
     8.97267642E-14    2          24        36   # BR(H+ -> W+      A      )
     2.05299909E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.63159728E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.37157563E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
