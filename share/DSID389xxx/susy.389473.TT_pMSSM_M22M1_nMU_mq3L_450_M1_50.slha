#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11565390E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04000344E+01   # W+
        25     1.26542534E+02   # h
        35     3.00018787E+03   # H
        36     3.00000010E+03   # A
        37     3.00111285E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02052053E+03   # ~d_L
   2000001     3.01489093E+03   # ~d_R
   1000002     3.01959255E+03   # ~u_L
   2000002     3.01841349E+03   # ~u_R
   1000003     3.02052053E+03   # ~s_L
   2000003     3.01489093E+03   # ~s_R
   1000004     3.01959255E+03   # ~c_L
   2000004     3.01841349E+03   # ~c_R
   1000005     5.14259363E+02   # ~b_1
   2000005     3.01677545E+03   # ~b_2
   1000006     5.10014980E+02   # ~t_1
   2000006     3.01891372E+03   # ~t_2
   1000011     3.00759501E+03   # ~e_L
   2000011     2.99961493E+03   # ~e_R
   1000012     3.00619058E+03   # ~nu_eL
   1000013     3.00759501E+03   # ~mu_L
   2000013     2.99961493E+03   # ~mu_R
   1000014     3.00619058E+03   # ~nu_muL
   1000015     2.98631473E+03   # ~tau_1
   2000015     3.02264849E+03   # ~tau_2
   1000016     3.00679294E+03   # ~nu_tauL
   1000021     2.32504786E+03   # ~g
   1000022     5.08068079E+01   # ~chi_10
   1000023     1.10291264E+02   # ~chi_20
   1000025    -3.00834995E+03   # ~chi_30
   1000035     3.00853525E+03   # ~chi_40
   1000024     1.10442157E+02   # ~chi_1+
   1000037     3.00938616E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890721E-01   # N_11
  1  2     1.91572458E-03   # N_12
  1  3    -1.46504986E-02   # N_13
  1  4     4.88982132E-04   # N_14
  2  1    -1.53250323E-03   # N_21
  2  2     9.99657814E-01   # N_22
  2  3     2.61097123E-02   # N_23
  2  4    -4.35571055E-04   # N_24
  3  1     1.00449260E-02   # N_31
  3  2    -1.81369404E-02   # N_32
  3  3     7.06791706E-01   # N_33
  3  4     7.07117837E-01   # N_34
  4  1    -1.07376491E-02   # N_41
  4  2     1.87519801E-02   # N_42
  4  3    -7.06787896E-01   # N_43
  4  4     7.07095422E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99319689E-01   # U_11
  1  2     3.68803250E-02   # U_12
  2  1    -3.68803250E-02   # U_21
  2  2     9.99319689E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999810E-01   # V_11
  1  2     6.16509027E-04   # V_12
  2  1     6.16509027E-04   # V_21
  2  2    -9.99999810E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98649380E-01   # cos(theta_t)
  1  2    -5.19559027E-02   # sin(theta_t)
  2  1     5.19559027E-02   # -sin(theta_t)
  2  2     9.98649380E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99707975E-01   # cos(theta_b)
  1  2    -2.41653620E-02   # sin(theta_b)
  2  1     2.41653620E-02   # -sin(theta_b)
  2  2     9.99707975E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06861755E-01   # cos(theta_tau)
  1  2     7.07351722E-01   # sin(theta_tau)
  2  1    -7.07351722E-01   # -sin(theta_tau)
  2  2    -7.06861755E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00194562E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.15653904E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44633667E+02   # higgs               
         4     1.16385937E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.15653904E+03  # The gauge couplings
     1     3.61241661E-01   # gprime(Q) DRbar
     2     6.41201499E-01   # g(Q) DRbar
     3     1.03216059E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.15653904E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.22058672E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.15653904E+03  # The trilinear couplings
  1  1     4.33305015E-07   # A_d(Q) DRbar
  2  2     4.33358218E-07   # A_s(Q) DRbar
  3  3     9.12717463E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.15653904E+03  # The trilinear couplings
  1  1     2.14563334E-07   # A_e(Q) DRbar
  2  2     2.14574127E-07   # A_mu(Q) DRbar
  3  3     2.17671947E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.15653904E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58897806E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.15653904E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.10413947E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.15653904E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03658176E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.15653904E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -6.78736343E+04   # M^2_Hd              
        22    -9.16676552E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42714198E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.38451794E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48068455E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48068455E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51931545E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51931545E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.61182296E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.11068204E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.76406466E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.12486714E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.24269104E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.28814003E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.20083869E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.43248126E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.40477188E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.84770536E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71795554E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64728948E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.25146182E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.25662941E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.45537370E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.85117766E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.00328497E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.83002835E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.83116362E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -9.70408105E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.59542409E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.29937170E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.93198021E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.07933385E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.00633735E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.15105551E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.68581450E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12085309E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.92900997E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64308161E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.94316277E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.02184862E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29028646E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.01520406E-12    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.00734177E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21782847E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56875004E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.67926501E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.63313362E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.51351607E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43124627E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12593689E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.68965502E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64432374E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.21087329E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.44061861E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28451187E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.97209026E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.01426743E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69303014E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47380631E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04925896E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.06407325E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14952065E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55261832E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12085309E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.92900997E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64308161E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.94316277E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.02184862E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29028646E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.01520406E-12    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.00734177E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21782847E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56875004E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.67926501E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.63313362E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.51351607E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43124627E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12593689E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.68965502E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64432374E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.21087329E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.44061861E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28451187E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.97209026E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.01426743E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69303014E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47380631E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04925896E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.06407325E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14952065E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55261832E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06655508E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.65816321E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00929872E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.02488496E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55624575E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997656E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.34407158E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06655508E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.65816321E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00929872E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.02488496E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55624575E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997656E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.34407158E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.84031358E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43131397E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18835622E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38032981E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78103014E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.51151084E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16137116E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.79105714E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.64167892E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32699091E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.27515902E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06691018E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.52238584E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01803176E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.02972965E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.06691018E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.52238584E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01803176E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.02972965E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06806649E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.52158887E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01778174E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03005937E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.20461438E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39943366E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.76713631E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39943366E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.76713631E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06859079E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.89049107E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06859079E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.89049107E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06395063E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.01445594E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.51112654E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54162298E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.07756716E-13    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07756716E-13    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.63533145E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.33686142E-10    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.33686142E-10    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.11465057E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.88907756E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.84705443E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.74420814E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.99205046E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.24482554E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.18223995E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     9.48789912E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.17828590E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     9.48789912E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.17828590E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.48638572E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.03386282E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.03386282E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.03139845E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.79387020E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.79387020E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.79773291E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     1.01951680E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.89706037E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.03266481E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.72656810E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.72656810E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.24210013E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.51061866E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.31543288E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31543288E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.44037393E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.44037393E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.36585852E-12    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     2.36585852E-12    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     2.59909657E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.59909657E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.36585852E-12    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     2.36585852E-12    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     2.59909657E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.59909657E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.29326772E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.29326772E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     7.03638294E-11    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     7.03638294E-11    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     7.03638294E-11    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     7.03638294E-11    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     3.65902291E-11    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     3.65902291E-11    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.01328977E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.71555327E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.85588618E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.76191007E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.76191007E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.08382499E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.57407342E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25581230E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25581230E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.48718468E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.48718468E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.82609577E-12    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     3.82609577E-12    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.11592989E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.11592989E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.82609577E-12    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     3.82609577E-12    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.11592989E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.11592989E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.12207719E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.12207719E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     9.06582148E-11    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     9.06582148E-11    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     9.06582148E-11    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     9.06582148E-11    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     5.00704565E-11    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     5.00704565E-11    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.85229147E-03   # h decays
#          BR         NDA      ID1       ID2
     5.41345004E-01    2           5        -5   # BR(h -> b       bb     )
     6.81566715E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.41269568E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.10875790E-04    2           3        -3   # BR(h -> s       sb     )
     2.21943161E-02    2           4        -4   # BR(h -> c       cb     )
     7.35902156E-02    2          21        21   # BR(h -> g       g      )
     2.57011509E-03    2          22        22   # BR(h -> gam     gam    )
     1.85740080E-03    2          22        23   # BR(h -> Z       gam    )
     2.56655967E-01    2          24       -24   # BR(h -> W+      W-     )
     3.28531681E-02    2          23        23   # BR(h -> Z       Z      )
     2.49963046E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.28339057E+01   # H decays
#          BR         NDA      ID1       ID2
     9.12252699E-01    2           5        -5   # BR(H -> b       bb     )
     5.80580037E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.05279227E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52082366E-04    2           3        -3   # BR(H -> s       sb     )
     7.06716714E-08    2           4        -4   # BR(H -> c       cb     )
     7.04995727E-03    2           6        -6   # BR(H -> t       tb     )
     3.40188625E-06    2          21        21   # BR(H -> g       g      )
     3.66531873E-08    2          22        22   # BR(H -> gam     gam    )
     2.88418700E-09    2          23        22   # BR(H -> Z       gam    )
     7.68060933E-07    2          24       -24   # BR(H -> W+      W-     )
     3.83556952E-07    2          23        23   # BR(H -> Z       Z      )
     4.78131416E-06    2          25        25   # BR(H -> h       h      )
    -6.63504243E-25    2          36        36   # BR(H -> A       A      )
     5.89620761E-18    2          23        36   # BR(H -> Z       A      )
     7.98467175E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.59832610E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.00587667E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.40371090E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.06917355E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.38843078E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.19497839E+01   # A decays
#          BR         NDA      ID1       ID2
     9.31470551E-01    2           5        -5   # BR(A -> b       bb     )
     5.92783529E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09593797E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.57424704E-04    2           3        -3   # BR(A -> s       sb     )
     7.26416026E-08    2           4        -4   # BR(A -> c       cb     )
     7.24247785E-03    2           6        -6   # BR(A -> t       tb     )
     2.13284028E-05    2          21        21   # BR(A -> g       g      )
     4.37774200E-08    2          22        22   # BR(A -> gam     gam    )
     2.10064926E-08    2          23        22   # BR(A -> Z       gam    )
     7.81110514E-07    2          23        25   # BR(A -> Z       h      )
     8.22437068E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.70282515E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.12605243E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.47282206E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.53254153E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48379711E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.48839199E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.94056154E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.49628993E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.14041732E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.34620515E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.32603587E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.24195162E-07    2          24        25   # BR(H+ -> W+      h      )
     4.95234192E-14    2          24        36   # BR(H+ -> W+      A      )
     4.58294936E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.07572115E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.01200988E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
