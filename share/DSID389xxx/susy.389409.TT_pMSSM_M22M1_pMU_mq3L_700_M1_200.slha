#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14500160E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03999292E+01   # W+
        25     1.24148747E+02   # h
        35     3.00015332E+03   # H
        36     2.99999976E+03   # A
        37     3.00092584E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03348260E+03   # ~d_L
   2000001     3.02833534E+03   # ~d_R
   1000002     3.03256728E+03   # ~u_L
   2000002     3.02978363E+03   # ~u_R
   1000003     3.03348260E+03   # ~s_L
   2000003     3.02833534E+03   # ~s_R
   1000004     3.03256728E+03   # ~c_L
   2000004     3.02978363E+03   # ~c_R
   1000005     7.91277120E+02   # ~b_1
   2000005     3.02758392E+03   # ~b_2
   1000006     7.89450630E+02   # ~t_1
   2000006     3.01442159E+03   # ~t_2
   1000011     3.00644150E+03   # ~e_L
   2000011     3.00169639E+03   # ~e_R
   1000012     3.00504691E+03   # ~nu_eL
   1000013     3.00644150E+03   # ~mu_L
   2000013     3.00169639E+03   # ~mu_R
   1000014     3.00504691E+03   # ~nu_muL
   1000015     2.98574827E+03   # ~tau_1
   2000015     3.02204006E+03   # ~tau_2
   1000016     3.00495720E+03   # ~nu_tauL
   1000021     2.34124962E+03   # ~g
   1000022     2.01154098E+02   # ~chi_10
   1000023     4.24491997E+02   # ~chi_20
   1000025    -2.99847332E+03   # ~chi_30
   1000035     2.99912228E+03   # ~chi_40
   1000024     4.24654651E+02   # ~chi_1+
   1000037     2.99973860E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887577E-01   # N_11
  1  2    -9.61401315E-04   # N_12
  1  3     1.48635470E-02   # N_13
  1  4    -1.72729264E-03   # N_14
  2  1     1.36450764E-03   # N_21
  2  2     9.99634281E-01   # N_22
  2  3    -2.65720168E-02   # N_23
  2  4     4.83421691E-03   # N_24
  3  1    -9.27115669E-03   # N_31
  3  2     1.53823257E-02   # N_32
  3  3     7.06852086E-01   # N_33
  3  4     7.07133338E-01   # N_34
  4  1    -1.17054105E-02   # N_41
  4  2     2.22207870E-02   # N_42
  4  3     7.06705831E-01   # N_43
  4  4    -7.07061588E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99293060E-01   # U_11
  1  2    -3.75949589E-02   # U_12
  2  1     3.75949589E-02   # U_21
  2  2     9.99293060E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976601E-01   # V_11
  1  2    -6.84085120E-03   # V_12
  2  1     6.84085120E-03   # V_21
  2  2     9.99976601E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98841751E-01   # cos(theta_t)
  1  2    -4.81160728E-02   # sin(theta_t)
  2  1     4.81160728E-02   # -sin(theta_t)
  2  2     9.98841751E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99915468E-01   # cos(theta_b)
  1  2     1.30021865E-02   # sin(theta_b)
  2  1    -1.30021865E-02   # -sin(theta_b)
  2  2     9.99915468E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06933335E-01   # cos(theta_tau)
  1  2     7.07280185E-01   # sin(theta_tau)
  2  1    -7.07280185E-01   # -sin(theta_tau)
  2  2     7.06933335E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01850403E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.45001601E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44328084E+02   # higgs               
         4     7.31634799E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.45001601E+03  # The gauge couplings
     1     3.62060933E-01   # gprime(Q) DRbar
     2     6.38250064E-01   # g(Q) DRbar
     3     1.02711888E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.45001601E+03  # The trilinear couplings
  1  1     1.98224592E-06   # A_u(Q) DRbar
  2  2     1.98227566E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.45001601E+03  # The trilinear couplings
  1  1     5.12995982E-07   # A_d(Q) DRbar
  2  2     5.13092755E-07   # A_s(Q) DRbar
  3  3     1.15469427E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.45001601E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09797685E-07   # A_mu(Q) DRbar
  3  3     1.11113410E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.45001601E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52637741E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.45001601E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13741035E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.45001601E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06130120E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.45001601E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.46638987E+04   # M^2_Hd              
        22    -9.07046026E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41403865E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.41939476E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48234464E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48234464E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51765536E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51765536E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.81241037E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.90240895E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.86110865E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.94865045E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02553325E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.38801395E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.24991507E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.52793884E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.18435338E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.28777267E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.58628557E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50384137E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.97310271E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.57012224E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.30432013E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.68953033E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.08003766E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.22575035E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.91618804E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.16199845E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.25610188E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.22003779E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -8.52780088E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28562158E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.92378299E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.22481284E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.07651345E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96109010E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.89604675E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61811623E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.81176827E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.76114238E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.23668267E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.48178692E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.08624000E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18094477E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58523943E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.86732021E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.77273846E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.03653262E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41475753E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96621110E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.01467284E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61579716E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.01205901E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.90579938E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.23096093E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.69957904E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.09309160E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67212825E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.50997827E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.15723966E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.75898642E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.68663923E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54900131E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96109010E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.89604675E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61811623E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.81176827E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.76114238E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.23668267E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.48178692E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.08624000E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18094477E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58523943E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.86732021E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.77273846E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.03653262E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41475753E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96621110E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.01467284E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61579716E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.01205901E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.90579938E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.23096093E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.69957904E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.09309160E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67212825E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.50997827E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.15723966E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.75898642E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.68663923E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54900131E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89516487E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.93853027E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00657782E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.98160122E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.79558401E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99956895E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.75391754E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55125758E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998194E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.80472658E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.99644329E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.06431889E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89516487E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.93853027E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00657782E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.98160122E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.79558401E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99956895E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.75391754E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55125758E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998194E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.80472658E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.99644329E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.06431889E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.75137361E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52064627E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16314102E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31621271E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69293769E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60419187E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13534865E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.12353552E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.26781560E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26009273E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.27614068E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89543122E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00007326E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99557158E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.54375870E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.04037369E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00435510E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.64527595E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89543122E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00007326E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99557158E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.54375870E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.04037369E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00435510E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.64527595E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89564253E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.99988618E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99531238E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.47481637E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.91896369E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00469146E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.48177415E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23175161E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.51019194E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.72182886E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.19586828E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.03149255E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.00713705E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.17769673E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.83469371E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.29991051E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.24146392E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.77098226E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.52290177E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.53280201E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.38022037E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.71593554E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.80527052E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.80527052E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.92916442E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.29940585E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.62855627E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.62855627E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26488515E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26488515E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.24852385E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.24852385E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.44860398E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.74578227E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.28531958E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.87586830E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.87586830E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.44229604E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.02178248E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.60209105E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.60209105E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.29117226E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.29117226E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.08194123E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.08194123E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.73879085E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85852947E-01    2           5        -5   # BR(h -> b       bb     )
     5.47159550E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93699325E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.11640509E-04    2           3        -3   # BR(h -> s       sb     )
     1.77668417E-02    2           4        -4   # BR(h -> c       cb     )
     5.71604547E-02    2          21        21   # BR(h -> g       g      )
     1.92874490E-03    2          22        22   # BR(h -> gam     gam    )
     1.22090884E-03    2          22        23   # BR(h -> Z       gam    )
     1.60911365E-01    2          24       -24   # BR(h -> W+      W-     )
     1.98374431E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39731479E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44814831E-01    2           5        -5   # BR(H -> b       bb     )
     1.77968568E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29254327E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72723719E-04    2           3        -3   # BR(H -> s       sb     )
     2.18073383E-07    2           4        -4   # BR(H -> c       cb     )
     2.17542279E-02    2           6        -6   # BR(H -> t       tb     )
     2.97107790E-05    2          21        21   # BR(H -> g       g      )
     2.62737861E-08    2          22        22   # BR(H -> gam     gam    )
     8.36549751E-09    2          23        22   # BR(H -> Z       gam    )
     3.24372713E-05    2          24       -24   # BR(H -> W+      W-     )
     1.61986372E-05    2          23        23   # BR(H -> Z       Z      )
     8.45799910E-05    2          25        25   # BR(H -> h       h      )
     2.62727761E-23    2          36        36   # BR(H -> A       A      )
     6.56143840E-18    2          23        36   # BR(H -> Z       A      )
     2.22468154E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.10941197E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.10908554E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.06648749E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.97398512E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.00781522E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32845086E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83493205E-01    2           5        -5   # BR(A -> b       bb     )
     1.87188995E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61854629E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12894927E-04    2           3        -3   # BR(A -> s       sb     )
     2.29387426E-07    2           4        -4   # BR(A -> c       cb     )
     2.28702740E-02    2           6        -6   # BR(A -> t       tb     )
     6.73507732E-05    2          21        21   # BR(A -> g       g      )
     3.32405654E-08    2          22        22   # BR(A -> gam     gam    )
     6.63330406E-08    2          23        22   # BR(A -> Z       gam    )
     3.39902296E-05    2          23        25   # BR(A -> Z       h      )
     2.63822025E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21625969E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31518612E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.96073706E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01207280E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09908536E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.45780882E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.69021249E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.03413523E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.10705210E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05068484E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.15403592E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.46849715E-05    2          24        25   # BR(H+ -> W+      h      )
     8.85585677E-14    2          24        36   # BR(H+ -> W+      A      )
     2.01578645E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.71891874E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.36781552E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
