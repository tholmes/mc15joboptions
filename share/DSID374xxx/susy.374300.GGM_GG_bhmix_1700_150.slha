#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376829E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416344E+03   # H
        36     2.00000000E+03   # A
        37     2.00209631E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.97172810E+03   # ~d_L
   2000001     4.97161780E+03   # ~d_R
   1000002     4.97148165E+03   # ~u_L
   2000002     4.97154026E+03   # ~u_R
   1000003     4.97172810E+03   # ~s_L
   2000003     4.97161780E+03   # ~s_R
   1000004     4.97148165E+03   # ~c_L
   2000004     4.97154026E+03   # ~c_R
   1000005     4.97159090E+03   # ~b_1
   2000005     4.97175648E+03   # ~b_2
   1000006     5.10330871E+03   # ~t_1
   2000006     5.40288184E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     1.79544948E+03   # ~g
   1000022     1.46529353E+02   # ~chi_10
   1000023    -1.62954840E+02   # ~chi_20
   1000025     2.94798777E+02   # ~chi_30
   1000035     3.12909429E+03   # ~chi_40
   1000024     1.60743089E+02   # ~chi_1+
   1000037     3.12909390E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08736716E-01   # N_11
  1  2    -2.47370715E-02   # N_12
  1  3     6.79332139E-01   # N_13
  1  4    -6.65265031E-01   # N_14
  2  1     2.00316100E-02   # N_21
  2  2    -4.81337008E-03   # N_22
  2  3    -7.04243495E-01   # N_23
  2  4    -7.09659542E-01   # N_24
  3  1     9.50936487E-01   # N_31
  3  2     8.56919290E-03   # N_32
  3  3    -2.05714410E-01   # N_33
  3  4     2.30928449E-01   # N_34
  4  1     4.15243545E-04   # N_41
  4  2    -9.99645676E-01   # N_42
  4  3    -1.51830893E-02   # N_43
  4  4     2.18591783E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14648640E-02   # U_11
  1  2     9.99769603E-01   # U_12
  2  1     9.99769603E-01   # U_21
  2  2     2.14648640E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09099243E-02   # V_11
  1  2     9.99522174E-01   # V_12
  2  1     9.99522174E-01   # V_21
  2  2     3.09099243E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99746792E-01   # cos(theta_t)
  1  2    -2.25022640E-02   # sin(theta_t)
  2  1     2.25022640E-02   # -sin(theta_t)
  2  2     9.99746792E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08575395E-01   # cos(theta_b)
  1  2     9.12724573E-01   # sin(theta_b)
  2  1    -9.12724573E-01   # -sin(theta_b)
  2  2     4.08575395E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76623624E-01   # cos(theta_tau)
  1  2     7.36329051E-01   # sin(theta_tau)
  2  1    -7.36329051E-01   # -sin(theta_tau)
  2  2     6.76623624E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90210078E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51747958E+02   # vev(Q)              
         4     3.87987982E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146824E-01   # gprime(Q) DRbar
     2     6.29570096E-01   # g(Q) DRbar
     3     1.08305236E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02714326E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72393400E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79973076E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04060490E+06   # M^2_Hd              
        22    -5.47544985E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111765E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.06288495E-03   # gluino decays
#          BR         NDA      ID1       ID2
     8.38973848E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     9.48143475E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.63383324E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.81622664E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.13984124E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.41531769E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.38306664E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.22008711E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.23613229E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     8.81622664E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.13984124E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.41531769E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.38306664E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.22008711E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.23613229E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.06887529E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.15972486E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.43894210E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.80345722E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.05130330E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.98733241E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.15248465E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.15248465E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.15248465E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.15248465E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36713881E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36713881E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.06512324E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.04691990E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.20496819E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.62562077E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.48814311E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.11130531E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.96190432E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.64814942E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.19207541E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.58363615E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.87455581E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.31424993E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.01526431E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.62443784E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.69488654E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.01757574E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.39691603E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.66880825E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.52832278E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     7.44873064E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     7.51370697E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     3.74869661E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.39299605E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.83206450E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.34056575E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.37335740E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -2.33054319E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -3.73351754E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -4.67714822E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.41762897E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.51757204E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.87189156E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.72310722E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.10991141E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.89300017E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.51299556E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.80334647E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.62302469E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.19108263E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.37019914E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     9.81503107E-08    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.97573526E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.16097588E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.64465826E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.32137372E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.01942503E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.08131845E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.83270816E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.03368605E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.56200646E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.21248274E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.49526887E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.19112749E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.85504361E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.30476953E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.45369131E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.16245780E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.93138489E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.32550333E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.01995574E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00258022E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.25570048E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.28420192E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.18536323E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.34744979E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86885382E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.19108263E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.37019914E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     9.81503107E-08    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.97573526E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.16097588E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.64465826E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.32137372E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.01942503E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.08131845E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.83270816E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.03368605E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.56200646E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.21248274E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.49526887E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.19112749E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.85504361E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.30476953E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.45369131E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.16245780E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.93138489E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.32550333E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.01995574E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00258022E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.25570048E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.28420192E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.18536323E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.34744979E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86885382E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762245E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54455607E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89056829E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04819143E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563254E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45676449E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497460E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513798E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57715268E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03009035E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03825400E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42298646E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762245E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54455607E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89056829E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04819143E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563254E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45676449E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497460E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513798E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57715268E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03009035E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03825400E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42298646E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957264E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08466669E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84743999E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56339855E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26705855E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289572E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53593157E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825940E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319152E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61416582E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12659794E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05219547E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49093807E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85079445E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98399881E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747360E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74569386E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80485861E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92071249E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774191E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33892579E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178209E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747360E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74569386E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80485861E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92071249E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774191E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33892579E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178209E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068798E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74255380E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80279452E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91851591E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477106E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48071689E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584769E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23803000E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87422691E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36073076E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36073076E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024413E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024413E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03766279E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36659164E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99044351E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247547E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94615062E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26233587E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99429550E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31586430E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33903905E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91641811E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29742658E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34242089E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02290856E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82556518E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03281205E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96183705E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.35090549E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.26411134E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.50747259E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65760275E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18353597E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01640798E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27775306E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65369321E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27775306E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65369321E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83719543E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77310454E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77310454E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52635859E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53255738E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53255738E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53255738E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.36020797E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.36020797E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.36020797E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.36020797E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86736150E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86736150E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86736150E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86736150E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36108385E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36108385E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63827507E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83667117E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19034660E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99740348E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99740348E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79606264E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.17049535E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.84000579E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47793984E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.38213484E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36662719E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03763572E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93141972E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02329403E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94061089E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94061089E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71037511E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76485131E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63751531E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36394044E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01123773E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77915347E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22356350E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48865635E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19367440E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33267652E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33267652E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44986676E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77843274E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80592053E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81043929E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61872429E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21680682E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01604564E-01    2           5        -5   # BR(h -> b       bb     )
     6.22631750E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20384495E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66290776E-04    2           3        -3   # BR(h -> s       sb     )
     2.01160246E-02    2           4        -4   # BR(h -> c       cb     )
     6.64322487E-02    2          21        21   # BR(h -> g       g      )
     2.32833063E-03    2          22        22   # BR(h -> gam     gam    )
     1.62704081E-03    2          22        23   # BR(h -> Z       gam    )
     2.16850656E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80912849E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01484533E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38931710E-03    2           5        -5   # BR(H -> b       bb     )
     2.32090394E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20525949E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05056162E-06    2           3        -3   # BR(H -> s       sb     )
     9.48135615E-06    2           4        -4   # BR(H -> c       cb     )
     9.38140318E-01    2           6        -6   # BR(H -> t       tb     )
     7.51327336E-04    2          21        21   # BR(H -> g       g      )
     2.62939647E-06    2          22        22   # BR(H -> gam     gam    )
     1.09155542E-06    2          23        22   # BR(H -> Z       gam    )
     3.17524228E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58329021E-04    2          23        23   # BR(H -> Z       Z      )
     8.52355196E-04    2          25        25   # BR(H -> h       h      )
     8.38786506E-24    2          36        36   # BR(H -> A       A      )
     3.36192072E-11    2          23        36   # BR(H -> Z       A      )
     2.47200073E-12    2          24       -37   # BR(H -> W+      H-     )
     2.47200073E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71005429E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52159284E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36041474E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64290412E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94697034E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53629615E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87732441E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05932867E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39227177E-03    2           5        -5   # BR(A -> b       bb     )
     2.29747752E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12241300E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082289E-06    2           3        -3   # BR(A -> s       sb     )
     9.38382317E-06    2           4        -4   # BR(A -> c       cb     )
     9.39154281E-01    2           6        -6   # BR(A -> t       tb     )
     8.88927369E-04    2          21        21   # BR(A -> g       g      )
     2.69762405E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304521E-06    2          23        22   # BR(A -> Z       gam    )
     3.09431641E-04    2          23        25   # BR(A -> Z       h      )
     5.85770350E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28929551E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65393009E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84447563E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65632167E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36528990E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93378545E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97902268E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23405067E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630272E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29502768E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42038958E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13691861E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02355039E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40803597E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17122301E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33766108E-12    2          24        36   # BR(H+ -> W+      A      )
     5.57975404E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23541211E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29279902E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
