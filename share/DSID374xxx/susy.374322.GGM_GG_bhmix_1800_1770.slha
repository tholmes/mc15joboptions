#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.75900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.77000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05418624E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400052E+03   # H
        36     2.00000000E+03   # A
        37     2.00151583E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.96252848E+03   # ~d_L
   2000001     4.96241863E+03   # ~d_R
   1000002     4.96228285E+03   # ~u_L
   2000002     4.96234085E+03   # ~u_R
   1000003     4.96252848E+03   # ~s_L
   2000003     4.96241863E+03   # ~s_R
   1000004     4.96228285E+03   # ~c_L
   2000004     4.96234085E+03   # ~c_R
   1000005     4.96174351E+03   # ~b_1
   2000005     4.96320497E+03   # ~b_2
   1000006     5.07514752E+03   # ~t_1
   2000006     5.36545365E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007614E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007614E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99960720E+03   # ~tau_1
   2000015     5.00055169E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.88523315E+03   # ~g
   1000022     1.82614295E+03   # ~chi_10
   1000023    -1.87794957E+03   # ~chi_20
   1000025     1.91347326E+03   # ~chi_30
   1000035     3.12584369E+03   # ~chi_40
   1000024     1.87311210E+03   # ~chi_1+
   1000037     3.12583798E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.32993770E-01   # N_11
  1  2    -4.06359387E-02   # N_12
  1  3     4.81479456E-01   # N_13
  1  4    -4.78796811E-01   # N_14
  2  1     2.33140263E-03   # N_21
  2  2    -3.10336299E-03   # N_22
  2  3    -7.07037001E-01   # N_23
  2  4    -7.07165902E-01   # N_24
  3  1     6.80227887E-01   # N_31
  3  2     4.69861793E-02   # N_32
  3  3    -5.16271754E-01   # N_33
  3  4     5.18214045E-01   # N_34
  4  1     2.17239048E-03   # N_41
  4  2    -9.98063820E-01   # N_42
  4  3    -4.17095716E-02   # N_43
  4  4     4.60890852E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.89141769E-02   # U_11
  1  2     9.98263051E-01   # U_12
  2  1     9.98263051E-01   # U_21
  2  2     5.89141769E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.51061782E-02   # V_11
  1  2     9.97878342E-01   # V_12
  2  1     9.97878342E-01   # V_21
  2  2     6.51061782E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99851524E-01   # cos(theta_t)
  1  2     1.72316556E-02   # sin(theta_t)
  2  1    -1.72316556E-02   # -sin(theta_t)
  2  2     9.99851524E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.80013594E-01   # cos(theta_b)
  1  2     7.33199504E-01   # sin(theta_b)
  2  1    -7.33199504E-01   # -sin(theta_b)
  2  2     6.80013594E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04852889E-01   # cos(theta_tau)
  1  2     7.09353512E-01   # sin(theta_tau)
  2  1    -7.09353512E-01   # -sin(theta_tau)
  2  2     7.04852889E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201004E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.77000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52287284E+02   # vev(Q)              
         4     3.16708960E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52725182E-01   # gprime(Q) DRbar
     2     6.26809844E-01   # g(Q) DRbar
     3     1.08213410E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02495952E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71509289E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79746756E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.75900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.63037767E+05   # M^2_Hd              
        22    -8.33509153E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36885579E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.74193407E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.01361381E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.35108767E-07    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.77938741E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.43685848E-16    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.07514178E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.62772220E-16    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.77938741E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.43685848E-16    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.07514178E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.62772220E-16    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.47120121E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.58010497E-12    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.58010497E-12    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.58010497E-12    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.58010497E-12    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.76546632E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.58270952E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.79169387E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.37091384E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.71300162E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.01983445E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.35072485E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.12678403E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.40139433E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.72778249E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.40563265E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.42793850E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.74412618E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.86442858E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.18927211E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.75074721E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.39533688E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.23882949E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.60808440E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.69903681E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.27748104E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.46805963E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.25079458E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.23296985E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.25768737E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.84876640E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.34353839E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01733520E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.72546912E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.51422091E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.33483805E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.43037879E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.90950698E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.46627500E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.62796101E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.25268583E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.30310358E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.20435022E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.12691577E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.17517695E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.59500817E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.94537145E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.20297323E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.48996686E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.40882839E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.00769638E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.99361011E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.12415931E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.11048867E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.77383731E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.12166952E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61019732E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.12699068E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.08870308E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.10485403E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.14757659E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.20866832E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.49557766E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.41469043E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.00812584E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.93537731E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.47032049E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.43511822E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.56814581E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.34916479E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89961456E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.12691577E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.17517695E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.59500817E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.94537145E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.20297323E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.48996686E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.40882839E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.00769638E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.99361011E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.12415931E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.11048867E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.77383731E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.12166952E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61019732E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.12699068E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.08870308E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.10485403E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.14757659E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.20866832E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.49557766E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.41469043E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.00812584E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.93537731E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.47032049E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.43511822E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.56814581E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.34916479E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89961456E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.64178884E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.68100460E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.75131293E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.95367008E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72765685E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.79469215E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.47091125E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.83317394E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.44823861E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.41455425E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.55168360E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.36465235E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.64178884E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.68100460E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.75131293E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.95367008E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72765685E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.79469215E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.47091125E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.83317394E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.44823861E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.41455425E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.55168360E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.36465235E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.24256131E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.75109394E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.50489616E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.41321715E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.59040688E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.92325479E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.18994090E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.60369401E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.23987414E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.61546043E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.13822711E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48869547E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.62483065E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     6.02456328E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.25885065E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.64201907E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14036582E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.06371235E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.07566782E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.74034454E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.63349772E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.46528151E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.64201907E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14036582E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.06371235E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.07566782E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.74034454E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.63349772E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.46528151E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.64438915E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13934375E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.06275899E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.07022242E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73788847E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.52306665E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.46040860E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.55741325E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.49337385E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.05260828E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.05260828E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.01753719E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.01753719E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.01037167E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.73854697E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53649977E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.06766075E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46148576E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.40544314E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52881906E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.14943537E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.06409011E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00735575E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88452111E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.08123149E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02905364E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.31570066E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.44520580E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.52889835E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     6.26325578E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.10716070E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.43368034E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10716070E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.43368034E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32173443E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.27463552E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.27463552E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.24496128E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.53985636E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.53985636E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.53985636E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.79960354E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.79960354E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.79960354E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.79960354E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.59986795E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.59986795E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.59986795E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.59986795E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.38405237E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.38405237E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.97339312E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.25742390E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.82696881E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.67597669E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.08787603E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.50854579E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.42699967E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.69687005E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.42699967E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.69687005E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.17307282E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.95762624E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.95762624E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.24569768E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.28909401E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.28909401E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.28909401E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.80034987E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.33130468E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.80034987E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.33130468E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.97229725E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.32488369E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.32488369E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.24000773E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.06344089E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.06344089E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.06344089E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.23484436E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.23484436E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.23484436E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.23484436E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.11614208E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.11614208E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.11614208E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.11614208E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.07672863E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.07672863E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     2.17505700E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     5.35330586E-09    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     2.17505700E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     5.35330586E-09    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     3.47338373E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.73849883E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.86617354E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52377953E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.44147648E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46777941E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46777941E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09115779E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.14240739E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.43696230E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.18472943E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.96350364E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.03811608E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.01086104E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.66991204E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22117117E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02168202E-01    2           5        -5   # BR(h -> b       bb     )
     6.21971150E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20150671E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65796288E-04    2           3        -3   # BR(h -> s       sb     )
     2.00954706E-02    2           4        -4   # BR(h -> c       cb     )
     6.63648802E-02    2          21        21   # BR(h -> g       g      )
     2.30495527E-03    2          22        22   # BR(h -> gam     gam    )
     1.62559706E-03    2          22        23   # BR(h -> Z       gam    )
     2.16495592E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80622419E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78096644E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46751917E-03    2           5        -5   # BR(H -> b       bb     )
     2.46429749E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71220910E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548185E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668657E-05    2           4        -4   # BR(H -> c       cb     )
     9.96069531E-01    2           6        -6   # BR(H -> t       tb     )
     7.97756542E-04    2          21        21   # BR(H -> g       g      )
     2.71710029E-06    2          22        22   # BR(H -> gam     gam    )
     1.16026824E-06    2          23        22   # BR(H -> Z       gam    )
     3.34316618E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66702405E-04    2          23        23   # BR(H -> Z       Z      )
     9.01813666E-04    2          25        25   # BR(H -> h       h      )
     7.27879383E-24    2          36        36   # BR(H -> A       A      )
     2.92433206E-11    2          23        36   # BR(H -> Z       A      )
     6.58526970E-12    2          24       -37   # BR(H -> W+      H-     )
     6.58526970E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380833E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47043843E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898637E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269788E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677823E-06    2           3        -3   # BR(A -> s       sb     )
     9.96180226E-06    2           4        -4   # BR(A -> c       cb     )
     9.96999738E-01    2           6        -6   # BR(A -> t       tb     )
     9.43679193E-04    2          21        21   # BR(A -> g       g      )
     3.13663364E-06    2          22        22   # BR(A -> gam     gam    )
     1.35286414E-06    2          23        22   # BR(A -> Z       gam    )
     3.25795410E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471897E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35638948E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238593E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81148462E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49810062E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45696264E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732050E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402675E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33905687E-04    2          24        25   # BR(H+ -> W+      h      )
     2.81052250E-13    2          24        36   # BR(H+ -> W+      A      )
