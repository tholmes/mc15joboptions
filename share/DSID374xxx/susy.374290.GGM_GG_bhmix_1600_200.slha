#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373704E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416405E+03   # H
        36     2.00000000E+03   # A
        37     2.00209104E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98044680E+03   # ~d_L
   2000001     4.98033673E+03   # ~d_R
   1000002     4.98020085E+03   # ~u_L
   2000002     4.98025928E+03   # ~u_R
   1000003     4.98044680E+03   # ~s_L
   2000003     4.98033673E+03   # ~s_R
   1000004     4.98020085E+03   # ~c_L
   2000004     4.98025928E+03   # ~c_R
   1000005     4.98029355E+03   # ~b_1
   2000005     4.98049146E+03   # ~b_2
   1000006     5.11158683E+03   # ~t_1
   2000006     5.41069023E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     1.70500404E+03   # ~g
   1000022     1.91306511E+02   # ~chi_10
   1000023    -2.16753603E+02   # ~chi_20
   1000025     2.96410816E+02   # ~chi_30
   1000035     3.12904233E+03   # ~chi_40
   1000024     2.14524075E+02   # ~chi_1+
   1000037     3.12904192E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69182375E-01   # N_11
  1  2    -2.32977632E-02   # N_12
  1  3     6.31900677E-01   # N_13
  1  4    -6.16463014E-01   # N_14
  2  1     1.80286760E-02   # N_21
  2  2    -4.70678684E-03   # N_22
  2  3    -7.05102466E-01   # N_23
  2  4    -7.08860582E-01   # N_24
  3  1     8.82917147E-01   # N_31
  3  2     1.29533321E-02   # N_32
  3  3    -3.21386939E-01   # N_33
  3  4     3.42052567E-01   # N_34
  4  1     4.21116337E-04   # N_41
  4  2    -9.99633569E-01   # N_42
  4  3    -1.55718431E-02   # N_43
  4  4     2.21374973E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20145000E-02   # U_11
  1  2     9.99757652E-01   # U_12
  2  1     9.99757652E-01   # U_21
  2  2     2.20145000E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13032971E-02   # V_11
  1  2     9.99509932E-01   # V_12
  2  1     9.99509932E-01   # V_21
  2  2     3.13032971E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99791463E-01   # cos(theta_t)
  1  2    -2.04213249E-02   # sin(theta_t)
  2  1     2.04213249E-02   # -sin(theta_t)
  2  2     9.99791463E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71086786E-01   # cos(theta_b)
  1  2     8.82086867E-01   # sin(theta_b)
  2  1    -8.82086867E-01   # -sin(theta_b)
  2  2     4.71086786E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84680213E-01   # cos(theta_tau)
  1  2     7.28843609E-01   # sin(theta_tau)
  2  1    -7.28843609E-01   # -sin(theta_tau)
  2  2     6.84680213E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90208664E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51767571E+02   # vev(Q)              
         4     3.86147266E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099787E-01   # gprime(Q) DRbar
     2     6.29225178E-01   # g(Q) DRbar
     3     1.08402974E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02695942E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72357173E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79967308E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02021732E+06   # M^2_Hd              
        22    -5.50144718E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961814E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.46994992E-03   # gluino decays
#          BR         NDA      ID1       ID2
     8.44992170E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.09738178E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.31068592E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.00005079E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.34975999E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.73718320E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.90621254E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.55757667E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.03072561E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.00005079E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.34975999E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.73718320E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.90621254E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.55757667E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.03072561E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.15824668E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.16821416E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.79009492E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.53034794E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.00122664E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.60830904E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.66402668E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.66402668E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.66402668E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.66402668E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35694419E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35694419E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.12155756E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.79294630E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.18034487E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.10681007E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.45402041E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.79357126E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.89319131E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.70992750E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.08901301E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.42347121E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.92640993E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.33990813E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.99013014E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.45164084E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.56376854E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.99019693E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.12974114E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.72819239E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.04413200E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.00272166E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.02848275E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.81340639E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.68622294E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.19081640E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.49308460E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.46300523E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.07257561E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.92816569E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.15345182E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.20610667E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.34150140E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.42284405E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.17746818E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.68135418E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.62579468E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.11438534E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.26801118E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.09012535E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.25022780E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.48404292E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.49169350E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.64903544E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.09371788E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.64184456E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.18688242E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.04032224E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.13960604E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.08660784E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.60315622E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.83283718E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.23327628E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.50789515E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.25027011E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.03411029E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.46807896E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.93451171E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.09518308E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.12050725E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.19092942E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.04083580E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.06068660E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.82060042E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.16144909E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.94922277E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.39322588E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87226015E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.25022780E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.48404292E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.49169350E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.64903544E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.09371788E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.64184456E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.18688242E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.04032224E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.13960604E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.08660784E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.60315622E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.83283718E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.23327628E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.50789515E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.25027011E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.03411029E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.46807896E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.93451171E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.09518308E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.12050725E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.19092942E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.04083580E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.06068660E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.82060042E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.16144909E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.94922277E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.39322588E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87226015E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543466E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03095943E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04690269E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80165110E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484144E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77854787E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342828E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528931E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20838454E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25804906E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78835675E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60406921E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543466E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03095943E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04690269E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80165110E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484144E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77854787E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342828E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528931E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20838454E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25804906E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78835675E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60406921E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238515E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35945600E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64045459E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72963096E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460252E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259369E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107735E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677927E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832618E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14117206E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06880639E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45993928E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197128E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84938379E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604438E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528869E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74819256E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53666804E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62272563E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698333E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37056824E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022943E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528869E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74819256E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53666804E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62272563E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698333E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37056824E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022943E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849761E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74162482E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53491228E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62087154E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401609E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51124718E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430250E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26837058E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76390413E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387918E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387918E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462721E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462721E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08271084E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38822704E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03048845E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48713049E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98333030E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91740817E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03604580E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91789734E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94115623E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96041217E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90599182E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86167513E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01628260E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94980000E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02627226E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93167263E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20551156E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65234649E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59117424E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49100053E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56219705E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66154426E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22028715E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57942963E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22028715E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57942963E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13479105E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60414772E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60414772E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093500E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19564226E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19564226E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19564226E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.06039696E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.06039696E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.06039696E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.06039696E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.02013251E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.02013251E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.02013251E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.02013251E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95865779E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95865779E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.60975390E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.41014584E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77948869E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77948869E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.08307589E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.92727899E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.52259667E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38826349E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03651535E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97359292E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80753740E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97928367E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97928367E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49011221E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49611639E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70829273E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00742303E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60666646E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91124750E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.87970305E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81908443E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37670251E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93492586E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93492586E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43544857E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72657343E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78951201E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93284476E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85726760E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21692015E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01611783E-01    2           5        -5   # BR(h -> b       bb     )
     6.22612389E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20377642E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66276313E-04    2           3        -3   # BR(h -> s       sb     )
     2.01155221E-02    2           4        -4   # BR(h -> c       cb     )
     6.64303857E-02    2          21        21   # BR(h -> g       g      )
     2.32227970E-03    2          22        22   # BR(h -> gam     gam    )
     1.62697896E-03    2          22        23   # BR(h -> Z       gam    )
     2.16854627E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80905301E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01431805E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38931573E-03    2           5        -5   # BR(H -> b       bb     )
     2.32121389E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20635529E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05070191E-06    2           3        -3   # BR(H -> s       sb     )
     9.48256397E-06    2           4        -4   # BR(H -> c       cb     )
     9.38259843E-01    2           6        -6   # BR(H -> t       tb     )
     7.51425433E-04    2          21        21   # BR(H -> g       g      )
     2.63478622E-06    2          22        22   # BR(H -> gam     gam    )
     1.09161378E-06    2          23        22   # BR(H -> Z       gam    )
     3.17159534E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58147160E-04    2          23        23   # BR(H -> Z       Z      )
     8.52450424E-04    2          25        25   # BR(H -> h       h      )
     8.55744228E-24    2          36        36   # BR(H -> A       A      )
     3.36480318E-11    2          23        36   # BR(H -> Z       A      )
     2.50768418E-12    2          24       -37   # BR(H -> W+      H-     )
     2.50768418E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49638978E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87472696E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75234283E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52420102E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45508736E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45608565E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11355945E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891322E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39222886E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771268E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12324437E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093250E-06    2           3        -3   # BR(A -> s       sb     )
     9.38478366E-06    2           4        -4   # BR(A -> c       cb     )
     9.39250409E-01    2           6        -6   # BR(A -> t       tb     )
     8.89018356E-04    2          21        21   # BR(A -> g       g      )
     2.67562363E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307636E-06    2          23        22   # BR(A -> Z       gam    )
     3.09066976E-04    2          23        25   # BR(A -> Z       h      )
     5.99355411E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30111145E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12616759E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74012729E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19876937E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48814079E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69813465E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853842E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23392524E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658213E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601550E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42030818E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13753218E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367265E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40915682E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16752187E-04    2          24        25   # BR(H+ -> W+      h      )
     1.32109536E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28616290E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26676695E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55312359E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
