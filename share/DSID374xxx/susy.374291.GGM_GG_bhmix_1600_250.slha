#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375320E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416786E+03   # H
        36     2.00000000E+03   # A
        37     2.00208348E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98044672E+03   # ~d_L
   2000001     4.98033673E+03   # ~d_R
   1000002     4.98020092E+03   # ~u_L
   2000002     4.98025928E+03   # ~u_R
   1000003     4.98044672E+03   # ~s_L
   2000003     4.98033673E+03   # ~s_R
   1000004     4.98020092E+03   # ~c_L
   2000004     4.98025928E+03   # ~c_R
   1000005     4.98027588E+03   # ~b_1
   2000005     4.98050905E+03   # ~b_2
   1000006     5.11134635E+03   # ~t_1
   2000006     5.41047753E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001288E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     1.70500342E+03   # ~g
   1000022     2.38932813E+02   # ~chi_10
   1000023    -2.70418132E+02   # ~chi_20
   1000025     3.33144024E+02   # ~chi_30
   1000035     3.12901950E+03   # ~chi_40
   1000024     2.68174855E+02   # ~chi_1+
   1000037     3.12901907E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56378084E-01   # N_11
  1  2    -2.22767079E-02   # N_12
  1  3     5.94516507E-01   # N_13
  1  4    -5.80083871E-01   # N_14
  2  1     1.53221599E-02   # N_21
  2  2    -4.61397304E-03   # N_22
  2  3    -7.05672905E-01   # N_23
  2  4    -7.08357038E-01   # N_24
  3  1     8.30787862E-01   # N_31
  3  2     1.55248696E-02   # N_32
  3  3    -3.85124314E-01   # N_33
  3  4     4.01534270E-01   # N_34
  4  1     4.33069415E-04   # N_41
  4  2    -9.99620647E-01   # N_42
  4  3    -1.59729790E-02   # N_43
  4  4     2.24329765E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25815594E-02   # U_11
  1  2     9.99745004E-01   # U_12
  2  1     9.99745004E-01   # U_21
  2  2     2.25815594E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17208280E-02   # V_11
  1  2     9.99496768E-01   # V_12
  2  1     9.99496768E-01   # V_21
  2  2     3.17208280E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99813740E-01   # cos(theta_t)
  1  2    -1.92998784E-02   # sin(theta_t)
  2  1     1.92998784E-02   # -sin(theta_t)
  2  2     9.99813740E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13936639E-01   # cos(theta_b)
  1  2     8.57828148E-01   # sin(theta_b)
  2  1    -8.57828148E-01   # -sin(theta_b)
  2  2     5.13936639E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89410192E-01   # cos(theta_tau)
  1  2     7.24371167E-01   # sin(theta_tau)
  2  1    -7.24371167E-01   # -sin(theta_tau)
  2  2     6.89410192E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90208174E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51794511E+02   # vev(Q)              
         4     3.84227876E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53061986E-01   # gprime(Q) DRbar
     2     6.28967110E-01   # g(Q) DRbar
     3     1.08402977E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02685033E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72320288E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79955526E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99429352E+06   # M^2_Hd              
        22    -5.52129062E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37847979E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.37846920E-03   # gluino decays
#          BR         NDA      ID1       ID2
     7.83890111E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.13738285E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.35080506E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.86783163E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.55866072E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.16602844E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     8.70727946E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.87592325E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.84842164E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.86783163E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.55866072E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.16602844E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     8.70727946E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.87592325E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.84842164E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.00534886E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.16199255E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.23901749E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.36027971E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.97424568E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.30890655E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.10505031E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.10505031E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.10505031E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.10505031E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34826179E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.34826179E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.11900604E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.76398682E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.17701974E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.13415573E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.45400039E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.64691816E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.89260455E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.71521688E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.82757536E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.33365959E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.92272628E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.73883739E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.96386850E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.05537091E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.56336456E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.98810574E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.12662494E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.73317224E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.67660642E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.35781278E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.49713303E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.36550360E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.85409107E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.24651277E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.18391358E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.02988487E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -4.13008568E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.00387541E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -8.30076562E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.09715464E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.17173248E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.66109747E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.48799104E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.95488923E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.43573014E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.83749430E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.88723031E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.41464487E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.25005705E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.60065792E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.54968876E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.43490345E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.09137988E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.68133740E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.18223191E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.04100524E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.13943796E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.52543652E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.15546772E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.38702100E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.41903057E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.50863867E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.25009973E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.33602297E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.67541922E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.63088323E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.09287111E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.52093663E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.18628058E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.04151692E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.06064404E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.95947629E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.99917246E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.79147108E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.87490950E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87246053E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.25005705E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.60065792E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.54968876E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.43490345E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.09137988E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.68133740E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.18223191E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.04100524E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.13943796E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.52543652E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.15546772E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.38702100E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.41903057E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.50863867E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.25009973E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.33602297E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.67541922E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.63088323E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.09287111E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.52093663E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.18628058E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.04151692E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.06064404E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.95947629E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.99917246E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.79147108E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.87490950E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87246053E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80275510E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87859408E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10937020E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61567658E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59513845E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11842159E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409620E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125669E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10481770E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35167571E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89282992E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99437017E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80275510E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87859408E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10937020E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61567658E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59513845E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11842159E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409620E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125669E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10481770E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35167571E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89282992E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99437017E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63122771E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88568260E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46973722E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15099661E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31198960E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95802882E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591381E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36736226E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64275004E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64615268E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92650658E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00364617E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599598E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67386787E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89411127E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80261576E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.82336401E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21879030E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41417298E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734535E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40464080E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088006E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80261576E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.82336401E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21879030E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41417298E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734535E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40464080E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088006E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80581753E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81443663E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21739952E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41255924E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438148E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54380322E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496019E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.52124131E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64639160E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33996758E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33996758E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332352E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332352E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09305318E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41064411E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06994057E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33246050E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02062810E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85430147E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07665933E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52674930E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.55015007E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25238800E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51937101E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29914550E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00955471E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08132348E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00715084E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92384743E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.90017260E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77513517E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.18982716E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15606076E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14146292E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69620849E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20449097E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55906415E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20449097E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55906415E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25919700E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55803054E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55803054E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48793249E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10383547E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10383547E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10383547E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18435553E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18435553E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18435553E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18435553E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94785241E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94785241E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94785241E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94785241E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.18702086E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.18702086E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.43642679E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99749187E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.76213798E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.33375399E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.26128954E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41068134E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39817424E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01542543E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20449151E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01743626E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01743626E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33815298E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26877861E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02300623E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.68857912E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20890507E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02337416E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83775596E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74621799E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22102386E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54399403E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54399403E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42030372E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67442861E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.77965912E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06213356E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44153536E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21702981E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01630667E-01    2           5        -5   # BR(h -> b       bb     )
     6.22595288E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20371589E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66263519E-04    2           3        -3   # BR(h -> s       sb     )
     2.01150122E-02    2           4        -4   # BR(h -> c       cb     )
     6.64287286E-02    2          21        21   # BR(h -> g       g      )
     2.31876067E-03    2          22        22   # BR(h -> gam     gam    )
     1.62694586E-03    2          22        23   # BR(h -> Z       gam    )
     2.16843922E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80897997E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01153131E+01   # H decays
#          BR         NDA      ID1       ID2
     1.39008200E-03    2           5        -5   # BR(H -> b       bb     )
     2.32283233E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21207709E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05143424E-06    2           3        -3   # BR(H -> s       sb     )
     9.48915299E-06    2           4        -4   # BR(H -> c       cb     )
     9.38911898E-01    2           6        -6   # BR(H -> t       tb     )
     7.51946569E-04    2          21        21   # BR(H -> g       g      )
     2.63841009E-06    2          22        22   # BR(H -> gam     gam    )
     1.09241726E-06    2          23        22   # BR(H -> Z       gam    )
     3.17240689E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58187634E-04    2          23        23   # BR(H -> Z       Z      )
     8.53017973E-04    2          25        25   # BR(H -> h       h      )
     8.50693299E-24    2          36        36   # BR(H -> A       A      )
     3.38258276E-11    2          23        36   # BR(H -> Z       A      )
     2.57899156E-12    2          24       -37   # BR(H -> W+      H-     )
     2.57899156E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23229949E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15133244E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93512273E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.01978239E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96757801E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.32481330E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54770049E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05622040E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39295277E-03    2           5        -5   # BR(A -> b       bb     )
     2.29923807E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12863718E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07164346E-06    2           3        -3   # BR(A -> s       sb     )
     9.39101397E-06    2           4        -4   # BR(A -> c       cb     )
     9.39873953E-01    2           6        -6   # BR(A -> t       tb     )
     8.89608552E-04    2          21        21   # BR(A -> g       g      )
     2.66245890E-06    2          22        22   # BR(A -> gam     gam    )
     1.27397283E-06    2          23        22   # BR(A -> Z       gam    )
     3.09134780E-04    2          23        25   # BR(A -> Z       h      )
     6.12190788E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74274763E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47857236E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11549129E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99447232E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.13343140E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45927843E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97574468E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23503634E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34822220E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30181372E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42101271E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14112551E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02438863E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41573441E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16830333E-04    2          24        25   # BR(H+ -> W+      h      )
     1.29830116E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79084909E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30728964E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98223114E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
