#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410183E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402380E+03   # H
        36     2.00000000E+03   # A
        37     2.00150810E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98868328E+03   # ~d_L
   2000001     4.98857377E+03   # ~d_R
   1000002     4.98843843E+03   # ~u_L
   2000002     4.98849630E+03   # ~u_R
   1000003     4.98868328E+03   # ~s_L
   2000003     4.98857377E+03   # ~s_R
   1000004     4.98843843E+03   # ~c_L
   2000004     4.98849630E+03   # ~c_R
   1000005     4.98811317E+03   # ~b_1
   2000005     4.98914530E+03   # ~b_2
   1000006     5.10928987E+03   # ~t_1
   2000006     5.40391718E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.61386157E+03   # ~g
   1000022     1.28282265E+03   # ~chi_10
   1000023    -1.33139647E+03   # ~chi_20
   1000025     1.37002040E+03   # ~chi_30
   1000035     3.12827792E+03   # ~chi_40
   1000024     1.32802192E+03   # ~chi_1+
   1000037     3.12827604E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19534917E-01   # N_11
  1  2    -2.91842889E-02   # N_12
  1  3     4.92437697E-01   # N_13
  1  4    -4.88797397E-01   # N_14
  2  1     3.28642701E-03   # N_21
  2  2    -3.48261727E-03   # N_22
  2  3    -7.06988891E-01   # N_23
  2  4    -7.07208440E-01   # N_24
  3  1     6.94447680E-01   # N_31
  3  2     3.17800183E-02   # N_32
  3  3    -5.06838650E-01   # N_33
  3  4     5.09751933E-01   # N_34
  4  1     1.05999333E-03   # N_41
  4  2    -9.99062650E-01   # N_42
  4  3    -2.80428999E-02   # N_43
  4  4     3.29589606E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.96337888E-02   # U_11
  1  2     9.99214273E-01   # U_12
  2  1     9.99214273E-01   # U_21
  2  2     3.96337888E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.65872328E-02   # V_11
  1  2     9.98914225E-01   # V_12
  2  1     9.98914225E-01   # V_21
  2  2     4.65872328E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99981896E-01   # cos(theta_t)
  1  2     6.01728113E-03   # sin(theta_t)
  2  1    -6.01728113E-03   # -sin(theta_t)
  2  2     9.99981896E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68541002E-01   # cos(theta_b)
  1  2     7.43675284E-01   # sin(theta_b)
  2  1    -7.43675284E-01   # -sin(theta_b)
  2  2     6.68541002E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03859994E-01   # cos(theta_tau)
  1  2     7.10338728E-01   # sin(theta_tau)
  2  1    -7.10338728E-01   # -sin(theta_tau)
  2  2     7.03859994E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203941E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52155170E+02   # vev(Q)              
         4     3.42831310E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784106E-01   # gprime(Q) DRbar
     2     6.27183235E-01   # g(Q) DRbar
     3     1.08507291E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02534682E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71780408E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79804550E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29739255E+06   # M^2_Hd              
        22    -6.91534293E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37052308E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.40654179E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.51405260E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.44755594E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.18792393E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.81557891E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.39579727E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.39655215E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.50551987E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.21028585E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.30389995E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.81557891E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.39579727E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.39655215E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.50551987E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.21028585E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.30389995E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.81862917E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.29036374E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.54164418E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.43807590E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.43807590E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.43807590E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.43807590E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     6.05701350E-03    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     6.05701350E-03    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.05246638E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.03121584E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.04306389E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.57583636E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.49968276E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.55979364E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.96209368E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.02806038E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.57489773E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     6.58879179E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.76877982E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.80998897E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.11930953E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.97777015E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.95571431E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.82683183E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.98861196E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.08157122E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -4.19504801E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.62090406E-06    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.61246476E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.97701098E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.44721711E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.38717821E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.69429207E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.28369135E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.18841106E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.03183627E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.39465461E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.54237904E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.59021868E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.26940456E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.73110708E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.75237757E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.56361244E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.40944854E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.15133166E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.06786611E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.30535155E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.97384973E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.79738717E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.83478249E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.01534874E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.06515312E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.03131445E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.06594106E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.18167385E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.19517086E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.52960290E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.00431634E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.01838081E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58004655E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.30540599E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.92092316E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.18545503E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.77437233E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.01820668E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.21853959E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.03585337E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.06638000E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.11300685E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.66638326E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.16922449E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.17373492E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.21022071E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89159760E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.30535155E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.97384973E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.79738717E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.83478249E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.01534874E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.06515312E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.03131445E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.06594106E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.18167385E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.19517086E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.52960290E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.00431634E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.01838081E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58004655E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.30540599E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.92092316E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.18545503E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.77437233E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.01820668E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.21853959E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.03585337E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.06638000E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.11300685E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.66638326E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.16922449E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.17373492E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.21022071E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89159760E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.71397183E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.87251759E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.66152466E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.10032332E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.66161926E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.95650191E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.33122403E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.14024947E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.22699560E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07860129E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.77289172E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.81408156E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.71397183E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.87251759E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.66152466E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.10032332E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.66161926E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.95650191E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.33122403E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.14024947E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.22699560E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07860129E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.77289172E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.81408156E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43242045E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.87484868E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.70117578E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.67967674E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.46751233E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.23432089E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.93943914E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.47872518E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43048805E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72197825E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.82644007E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.75563209E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.50339942E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.88090594E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.01127571E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.71405162E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18437522E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.76845196E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.94023858E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.66744372E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.70298058E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.32695056E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.71405162E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18437522E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.76845196E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.94023858E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.66744372E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.70298058E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.32695056E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.71682676E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18316542E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.76664556E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.93212792E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.66471902E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.72047165E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.32152138E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.20794165E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.94414453E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.27129598E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.27129598E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09043325E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09043325E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08212708E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.84109860E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53100908E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12157720E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46282692E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34564924E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53884807E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.94871723E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     9.59112874E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00358223E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88467666E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11741112E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.39978271E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.38366583E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.60616661E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.51091047E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.59723078E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.16597036E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.50972605E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16597036E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.50972605E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37737492E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.44782408E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.44782408E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.41362807E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.88541020E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.88541020E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.88541020E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.15321556E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.15321556E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.15321556E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.15321556E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.84405209E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.84405209E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.84405209E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.84405209E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.70466899E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.70466899E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.19570002E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.82716686E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.71122246E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     9.75477241E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.61539467E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.44700700E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.03693099E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.23140378E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.03693099E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.23140378E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.34151417E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.13785454E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.13785454E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.84066748E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.03301125E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.03301125E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.03301125E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.38805854E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.09211908E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.38805854E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.09211908E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.68265438E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.06161541E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.06161541E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.96307469E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.41021763E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.41021763E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.41021763E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.30307341E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.30307341E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30307341E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30307341E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.34357176E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.34357176E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.34357176E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.34357176E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.30517707E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.30517707E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.84112416E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.54473693E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51372769E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.75563964E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46374076E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46374076E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14827358E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.60441958E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37808314E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.14604778E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.80173582E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.18629692E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     9.40885908E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.39972253E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21921699E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01957261E-01    2           5        -5   # BR(h -> b       bb     )
     6.22264680E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20254568E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66016036E-04    2           3        -3   # BR(h -> s       sb     )
     2.01046989E-02    2           4        -4   # BR(h -> c       cb     )
     6.63946367E-02    2          21        21   # BR(h -> g       g      )
     2.30682726E-03    2          22        22   # BR(h -> gam     gam    )
     1.62630193E-03    2          22        23   # BR(h -> Z       gam    )
     2.16622297E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80752389E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78106296E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47063859E-03    2           5        -5   # BR(H -> b       bb     )
     2.46425352E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71205363E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546006E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667977E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063449E-01    2           6        -6   # BR(H -> t       tb     )
     7.97763234E-04    2          21        21   # BR(H -> g       g      )
     2.71240620E-06    2          22        22   # BR(H -> gam     gam    )
     1.15999590E-06    2          23        22   # BR(H -> Z       gam    )
     3.35213783E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67149741E-04    2          23        23   # BR(H -> Z       Z      )
     9.03434173E-04    2          25        25   # BR(H -> h       h      )
     7.51654784E-24    2          36        36   # BR(H -> A       A      )
     3.01026947E-11    2          23        36   # BR(H -> Z       A      )
     7.00634594E-12    2          24       -37   # BR(H -> W+      H-     )
     7.00634594E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382373E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47355433E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897655E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266316E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677365E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176214E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995723E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675393E-04    2          21        21   # BR(A -> g       g      )
     3.17091691E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257425E-06    2          23        22   # BR(A -> Z       gam    )
     3.26665206E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472573E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36330667E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237181E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143469E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50252766E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693457E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731491E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401782E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34793993E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73959484E-13    2          24        36   # BR(H+ -> W+      A      )
