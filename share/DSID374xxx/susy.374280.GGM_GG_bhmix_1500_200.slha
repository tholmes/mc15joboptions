#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373704E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416234E+03   # H
        36     2.00000000E+03   # A
        37     2.00209126E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98868368E+03   # ~d_L
   2000001     4.98857373E+03   # ~d_R
   1000002     4.98843799E+03   # ~u_L
   2000002     4.98849636E+03   # ~u_R
   1000003     4.98868368E+03   # ~s_L
   2000003     4.98857373E+03   # ~s_R
   1000004     4.98843799E+03   # ~c_L
   2000004     4.98849636E+03   # ~c_R
   1000005     4.98853059E+03   # ~b_1
   2000005     4.98872829E+03   # ~b_2
   1000006     5.11957474E+03   # ~t_1
   2000006     5.41817557E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     1.61387501E+03   # ~g
   1000022     1.91304078E+02   # ~chi_10
   1000023    -2.16750322E+02   # ~chi_20
   1000025     2.96409982E+02   # ~chi_30
   1000035     3.12904230E+03   # ~chi_40
   1000024     2.14520810E+02   # ~chi_1+
   1000037     3.12904189E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69170369E-01   # N_11
  1  2    -2.32978389E-02   # N_12
  1  3     6.31905094E-01   # N_13
  1  4    -6.16467622E-01   # N_14
  2  1     1.80287432E-02   # N_21
  2  2    -4.70677726E-03   # N_22
  2  3    -7.05102439E-01   # N_23
  2  4    -7.08860608E-01   # N_24
  3  1     8.82923526E-01   # N_31
  3  2     1.29529586E-02   # N_32
  3  3    -3.21378318E-01   # N_33
  3  4     3.42044216E-01   # N_34
  4  1     4.21113223E-04   # N_41
  4  2    -9.99633572E-01   # N_42
  4  3    -1.55717689E-02   # N_43
  4  4     2.21374087E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20143952E-02   # U_11
  1  2     9.99757654E-01   # U_12
  2  1     9.99757654E-01   # U_21
  2  2     2.20143952E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13031717E-02   # V_11
  1  2     9.99509936E-01   # V_12
  2  1     9.99509936E-01   # V_21
  2  2     3.13031717E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99810866E-01   # cos(theta_t)
  1  2    -1.94481934E-02   # sin(theta_t)
  2  1     1.94481934E-02   # -sin(theta_t)
  2  2     9.99810866E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71080459E-01   # cos(theta_b)
  1  2     8.82090245E-01   # sin(theta_b)
  2  1    -8.82090245E-01   # -sin(theta_b)
  2  2     4.71080459E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84680397E-01   # cos(theta_tau)
  1  2     7.28843436E-01   # sin(theta_tau)
  2  1    -7.28843436E-01   # -sin(theta_tau)
  2  2     6.84680397E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90207605E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51766780E+02   # vev(Q)              
         4     3.86239294E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099783E-01   # gprime(Q) DRbar
     2     6.29225141E-01   # g(Q) DRbar
     3     1.08507309E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02684739E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72347829E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79967891E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02024021E+06   # M^2_Hd              
        22    -5.51073220E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961798E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.07333457E-03   # gluino decays
#          BR         NDA      ID1       ID2
     9.65749927E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.25389472E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.63676516E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.04204478E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.34317135E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.77836628E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     6.03019263E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.54665957E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.04529839E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.04204478E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.34317135E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.77836628E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     6.03019263E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.54665957E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.04529839E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.20124175E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.19044376E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.83202460E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.51865534E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.97368779E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.53195617E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.78523016E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.78523016E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.78523016E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.78523016E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35860326E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35860326E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.17768339E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.65718007E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.16106307E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.04781424E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.42299462E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.66135053E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.83120814E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.76337212E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.97176119E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.38266141E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.97869095E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.23130347E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.86728205E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.40591001E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.39818081E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.96587985E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -2.79597835E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.78062226E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.82923146E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.33714714E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     7.14169176E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.50279049E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.76671285E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.99946200E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.40813948E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.37672633E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -9.46077406E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.72222054E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.89953016E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.18277059E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.37493749E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.43265567E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.20952475E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.04298714E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.57113372E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.02479544E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.15842743E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.19495185E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.30725534E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.40482757E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.45783075E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.58900081E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.03479787E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.60461430E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.06905293E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.05871401E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.19562445E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.06105625E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.56554684E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.74293237E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.16905886E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.51944455E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.30729492E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.01063283E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.34413945E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.89069452E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.03623666E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.93636724E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.07302583E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.05921340E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.11653849E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.75180170E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.06017546E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.70712804E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.21913782E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87537009E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.30725534E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.40482757E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.45783075E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.58900081E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.03479787E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.60461430E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.06905293E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.05871401E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.19562445E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.06105625E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.56554684E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.74293237E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.16905886E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.51944455E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.30729492E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.01063283E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.34413945E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.89069452E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.03623666E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.93636724E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.07302583E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.05921340E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.11653849E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.75180170E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.06017546E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.70712804E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.21913782E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87537009E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543442E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03073115E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04693900E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80167400E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484144E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77848387E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342826E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528926E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20827168E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25807372E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78846958E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60397169E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543442E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03073115E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04693900E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80167400E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484144E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77848387E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342826E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528926E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20827168E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25807372E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78846958E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60397169E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238512E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35938740E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64049105E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72969772E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460313E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259328E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107855E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677928E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832598E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14111122E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06881338E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.46000217E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197057E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84954103E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604295E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528845E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74792607E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53667410E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62275239E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698332E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37055737E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022944E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528845E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74792607E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53667410E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62275239E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698332E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37055737E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022944E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849739E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74135860E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53491832E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62089826E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401605E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51124394E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430247E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26749940E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76410892E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387991E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387991E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462746E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462746E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08270884E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38821229E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03048065E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48714800E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98332269E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91715596E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03605082E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91795434E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94119876E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96049829E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90603232E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86122364E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01628699E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94977959E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02620092E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93174442E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20546632E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65131865E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59152884E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49113209E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56259930E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66167166E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22028997E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57943330E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22028997E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57943330E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13476755E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60415615E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60415615E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093702E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19565913E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19565913E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19565913E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.06075896E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.06075896E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.06075896E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.06075896E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.02025318E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.02025318E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.02025318E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.02025318E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95871046E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95871046E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.61608583E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.40025684E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77998315E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77998315E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.06835113E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.92256565E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.51636399E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38824874E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03658763E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97358512E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80746495E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97927602E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97927602E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49013843E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49618311E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70837662E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00752876E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60671909E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91125149E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.87948039E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81886044E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37628499E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93496656E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93496656E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43545927E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72660650E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78952714E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93284239E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85717344E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21691727E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01611798E-01    2           5        -5   # BR(h -> b       bb     )
     6.22610846E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20377096E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66275186E-04    2           3        -3   # BR(h -> s       sb     )
     2.01155644E-02    2           4        -4   # BR(h -> c       cb     )
     6.64303113E-02    2          21        21   # BR(h -> g       g      )
     2.32228266E-03    2          22        22   # BR(h -> gam     gam    )
     1.62697993E-03    2          22        23   # BR(h -> Z       gam    )
     2.16854777E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80905495E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01430144E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38937670E-03    2           5        -5   # BR(H -> b       bb     )
     2.32122480E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20639388E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05070701E-06    2           3        -3   # BR(H -> s       sb     )
     9.48256625E-06    2           4        -4   # BR(H -> c       cb     )
     9.38260024E-01    2           6        -6   # BR(H -> t       tb     )
     7.51428298E-04    2          21        21   # BR(H -> g       g      )
     2.63479220E-06    2          22        22   # BR(H -> gam     gam    )
     1.09161846E-06    2          23        22   # BR(H -> Z       gam    )
     3.16855594E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57995603E-04    2          23        23   # BR(H -> Z       Z      )
     8.52456238E-04    2          25        25   # BR(H -> h       h      )
     8.41879419E-24    2          36        36   # BR(H -> A       A      )
     3.35791477E-11    2          23        36   # BR(H -> Z       A      )
     2.49604343E-12    2          24       -37   # BR(H -> W+      H-     )
     2.49604343E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49642113E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87453531E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75240951E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52405408E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45502402E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45675239E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11363981E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891228E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39228319E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771321E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12324625E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093274E-06    2           3        -3   # BR(A -> s       sb     )
     9.38478582E-06    2           4        -4   # BR(A -> c       cb     )
     9.39250626E-01    2           6        -6   # BR(A -> t       tb     )
     8.89018561E-04    2          21        21   # BR(A -> g       g      )
     2.67562609E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307666E-06    2          23        22   # BR(A -> Z       gam    )
     3.08770380E-04    2          23        25   # BR(A -> Z       h      )
     5.99350902E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30103973E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12618088E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74006700E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19856327E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48827555E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69815770E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853787E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23400655E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658271E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601756E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42036022E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13753338E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367289E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40915954E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16448293E-04    2          24        25   # BR(H+ -> W+      h      )
     1.32179061E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28609777E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26675706E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55319205E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
