def fixMcEventCollectionKey(alg, prop = ['McEventsR', 'McEventsRW', 'McEventKey', 'MCEventKey', 'TruthCollKey', 'inputKeyName'], newvalue = 'OLD_GEN_EVENT'):

    if alg == genSeq:
        evgenLog.info('This is key fixing for %s: update %s to %s' % (alg.name(), prop, newvalue))
    else:
        evgenLog.debug('This is key fixing for %s: update %s to %s' % (alg.name(), prop, newvalue))
    
    for c in alg.getChildren():

        evgenLog.debug('Child %s' % c.name())

        if c.name() == 'TestHepMC':
            evgenLog.info('Found TestHepMC algorithm - stop fixing keys')
            break

        for p in prop:
            evgenLog.debug('test property %s on %s' % (p, c.name()))
            if p in c.getProperties():
                evgenLog.info('Found %s in algorithm %s - setting to %s' % (p, c.name(), newvalue))
                setattr(c, p, newvalue)

        # hack as this is a private tool we are after
        if hasattr(c,'INav4MomTruthEventBuilder'):
            evgenLog.info('Found INav4MomTruthEventBuilder algorithm - setting INav4MomTruthEventBuilder.CnvTool.McEvents to  %s' % (newvalue))
            truthParticleBuilder = getattr(c,'INav4MomTruthEventBuilder')
            setattr(truthParticleBuilder.CnvTool, 'McEvents', newvalue)

        # recursive calling
        fixMcEventCollectionKey(c, prop=prop, newvalue=newvalue)


evgenLog.debug('genSeq setup before McEventCollection key fix')
evgenLog.debug(genSeq)

fixMcEventCollectionKey(genSeq, newvalue='OLD_GEN_EVENT')
fixMcEventCollectionKey(filtSeq, newvalue='OLD_GEN_EVENT')

evgenLog.debug('genSeq setup after McEventCollection key fix')
evgenLog.debug(genSeq)

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
# move it to a specific position
#genSeq.insert(genSeq.getChildren().index(CfgMgr.TestHepMC()), BoostEvent())
genSeq += BoostEvent()

boost=genSeq.BoostEvent
boost.BetaZ=0.4345
boost.McInputKey='OLD_GEN_EVENT'
boost.McOutputKey='GEN_EVENT'

evgenLog.debug('genSeq setup after boost algorithm insertion')
evgenLog.debug(genSeq)

if 'BoostEvent' not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ['BoostEvent']

evgenLog.info("TestHepMC.EnergyDifference=10^7GeV")

TestHepMC = testSeq.TestHepMC
TestHepMC.EnergyDifference = 10000000*GeV

StreamEVGEN.ItemList.remove("McEventCollection#*")
StreamEVGEN.ItemList += ["McEventCollection#GEN_EVENT"]
